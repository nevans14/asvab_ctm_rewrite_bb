$(document).ready(function() {

    $('.menu-icon').click(function(){
      $('#slide-menu').toggleClass('open');
    });

    $('#slide-menu #close').click(function(){
      $('#slide-menu').removeClass('open');
      return false;
    });

    $('.tab').hide();
    var tab = $('.tab-nav .active a').attr('href');
    $(tab).show();

    $('.tab-nav a').click(function(){
    	var tab = $(this).attr('href');
    	$('.tab-nav li').removeClass('active');
    	$(this).parent('li').addClass('active');
    	$('.tab').hide();
    	$(tab).show().find('.career-detail').removeAttr('style').find('.columns').height('auto');
    	equalheight('.tab .career-list .career-detail > .columns');
    	return false;
    });
    
    $('body').on('click', '.recruiter-icon', function(){
        $('html').addClass('no-top');
    });

		$('body').on('click', '.recruiter-icon', function(){
			$('html').addClass('no-top');
		});

    $('#found-careers > div').hide();
    $('#hot-jobs > div').hide();
    $('#all > div').hide();
    var activeview = $('#sort i.active').data('link');
    $(activeview).show();

    $('#sort i').click(function(){
        $('#sort i').removeClass('active');
        $(this).addClass('active');
        var activeview = $(this).data('link');
        $('#found-careers > div').hide();
        $('#hot-jobs > div').hide();
        $('#all > div').hide();
        $(activeview).show();
    });

	

    // added on 10.5.2017
    
    $('#boot-camp-table .map_btn').on('click', function(){
      //var mapViewText=jQuery(this).text();
      //alert(mapViewText);
      $(this).css({'display':'none'});
      $('.list_btn').css({'display':'block'});
      $('#boot-camp-table .list-view').css({'display':'none'}); 
      $('#boot-camp-table .map-view').css({'display':'block'}); 
    });

    $('#boot-camp-table .list_btn').on('click', function(){
      //var mapViewText=jQuery(this).text();
      //alert(mapViewText);
      $(this).css({'display':'none'});
      $('.map_btn').css({'display':'block'});
      
      $('#boot-camp-table .list-view').css({'display':'block'}); 
      $('#boot-camp-table .map-view').css({'display':'none'}); 
    });

    $('.service-data .map_btn').on('click', function(){
       //alert('hi')
       $(this).css({'display':'none'});
       $(this).siblings('.service-data .list_btn').css({'display':'block'});
       $(this).parent().parent().parent().siblings('.list-view').css({'display':'none'});
       $(this).parent().parent().parent().siblings('.map-view').css({'display':'block'});
    });

    $('.service-data .list_btn').on('click', function(){
       //alert('hi')
       $(this).css({'display':'none'});
       $(this).siblings('.service-data .map_btn').css({'display':'block'});
       $(this).parent().parent().parent().siblings('.map-view').css({'display':'none'});
       $(this).parent().parent().parent().siblings('.list-view').css({'display':'block'});
    });

	// Options Enlistment Requirements
	$(document).on('click', '.cep_tabs a', function (e) {
		var $_cep_tabs = $(this) .closest('.cep_tabs')
		$_cep_tabs.find('.nav-tabs li').removeClass('active');
		var tagid = $(this).data('toggle');
		$_cep_tabs.find('.nav-tabs a[data-toggle=' + tagid + ']').parent().addClass('active');
		$_cep_tabs.find('.tab-pane').removeClass('active');
		$_cep_tabs.find('#' + tagid).addClass('active');
	});

// ********************************* END
	
	// $(".career-slider").slick({
	//   dots: false,
	//   infinite: true,
	//   prevArrow: '<button type="button" class="slick-prev" role="button"><img src="assets/images/career-left-arrow.png"></button>',
	//   nextArrow: '<button type="button" class="slick-next" role="button"><img src="assets/images/career-right-arrow.png"></button>',
	//   autoplay: true,
	//   autoplaySpeed: 3500,
	//   speed: 1500,
	//   slidesToShow: 1,
	//   slidesToScroll: 1,
	//   responsive: [
	// 	{
	// 	  breakpoint: 640,
	// 	  settings: {
	// 		arrows: false
	// 	  }
	// 	}
	//   ]
	// });
	
	// $(".snap-shot-slides").slick({
	//   dots: false,
	//   infinite: true,
	//   prevArrow: '<button type="button" class="slick-prev" role="button"><img src="assets/images/arrow-left-red.png"></button>',
	//   nextArrow: '<button type="button" class="slick-next" role="button"><img src="assets/images/arrow-right-red.png"></button>',
	//   slidesToShow: 1,
	//   speed: 1500,
	//   slidesToScroll: 1
	// });

	redirectToHttps();
});

/**
 * Prevents CloudFlare's redirect to https
 * @returns
 */
function redirectToHttps() {
	if (location.href.indexOf('localhost') > -1) return;
	if (location.href.indexOf('careersinthemilitary.com') > -1 && location.protocol === 'http:') {
			if (location.href.indexOf('www') === -1) {
					location.href = 'https://www.' + window.location.href.substring(window.location.protocol.length + 2);
			} else {
					location.href = 'https:' + window.location.href.substring(window.location.protocol.length);
			}
	}
}



/*$(window).load(function() {
  equalheight('.tab .career-list .career-detail > .columns');
  $(".question-group").setAllToMaxHeight();
});

$(window).resize(function(){
  equalheight('.tab .career-list .career-detail > .columns');
  $(".question-group").setAllToMaxHeight();
});

$.fn.setAllToMaxHeight = function(){
	return this.height( Math.max.apply(this, $.map( this , function(e){ return $(e).height() }) ) );
}

equalheight = function(container){

	var currentTallest = 0,
     currentRowStart = 0,
     rowDivs = new Array(),
     $el,
     topPosition = 0;

	$(container).each(function() {

	$el = $(this);
	$($el).height('auto')
	topPostion = $el.position().top;

	  if (currentRowStart != topPostion) {
			for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
			 rowDivs[currentDiv].height(currentTallest);
			}
			rowDivs.length = 0; // empty the array
			currentRowStart = topPostion;
			currentTallest = $el.height();
			rowDivs.push($el);
	  } else {
			rowDivs.push($el);
			currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
	  }
	   for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
	    rowDivs[currentDiv].height(currentTallest);
	  }
 	});
}
*/
var __slice = [].slice;

// Contact Us radio show and hide
function question() {
	if (document.getElementById('ssQuestion').checked) {
		document.getElementById('form_ssq').style.display = 'block';
		document.getElementById('form_acq').style.display = 'none';
	}
	else {
		document.getElementById('form_ssq').style.display = 'none';
		document.getElementById('form_acq').style.display = 'block';
	}
}
	
// Add multiple select / deselect functionality
$(function(){
	// Army All
	$("#army_all").click(function () {
		  $('input[name="army_all"]').attr('checked', this.checked);
	});
	$('input[name="army_all"]').click(function(){

		if($('input[name="army_all"]').length == $('input[name="army_all"]:checked').length) {
			$("#army_all").attr("checked", "checked");
		} else {
			$("#army_all").removeAttr("checked");
		}
	});
	
	// Marine Corps All
	$("#marine_corps_all").click(function () {
		  $('input[name="marine_corps_all"]').attr('checked', this.checked);
	});
	$('input[name="marine_corps_all"]').click(function(){

		if($('input[name="marine_corps_all"]').length == $('input[name="marine_corps_all"]:checked').length) {
			$("#marine_corps_all").attr("checked", "checked");
		} else {
			$("#marine_corps_all").removeAttr("checked");
		}
	});
	
	// Air Force All
	$("#air_force_all").click(function () {
		  $('input[name="air_force_all"]').attr('checked', this.checked);
	});
	$('input[name="air_force_all"]').click(function(){

		if($('input[name="air_force_all"]').length == $('input[name="air_force_all"]:checked').length) {
			$("#air_force_all").attr("checked", "checked");
		} else {
			$("#air_force_all").removeAttr("checked");
		}
	});
	
	// Navy All
	$("#navy_all").click(function () {
		  $('input[name="navy_all"]').attr('checked', this.checked);
	});
	$('input[name="navy_all"]').click(function(){

		if($('input[name="navy_all"]').length == $('input[name="navy_all"]:checked').length) {
			$("#navy_all").attr("checked", "checked");
		} else {
			$("#navy_all").removeAttr("checked");
		}
	});
	
	// Coast Guard All
	$("#coast_guard_all").click(function () {
		  $('input[name="coast_guard_all"]').attr('checked', this.checked);
	});
	$('input[name="coast_guard_all"]').click(function(){

		if($('input[name="coast_guard_all"]').length == $('input[name="coast_guard_all"]:checked').length) {
			$("#coast_guard_all").attr("checked", "checked");
		} else {
			$("#coast_guard_all").removeAttr("checked");
		}
	});
});


// Modal
/*$(document).ready(function(){
	$(document).on('click', '[custom-modal-target]', function(e){
		$('body').addClass('custom-modal-open');
		var customModal = $($(e.target).attr('custom-modal-target'));
		customModal.show();
		customModal.addClass('fade');
		var modelAddClassInterval = window.setInterval( function() {
			customModal.addClass('in');
			clearInterval(modelAddClassInterval);
		}, 100);
		customModal.find('.custom-modal-backdrop').addClass('in');
	});
	$('.custom-modal-backdrop, button.custom-close, button.modal-close, button.close-button, .custom-close-all').click( function(){
		$('body').removeClass('custom-modal-open');
		$('.custom-modal').hide();
		$('.custom-modal').removeClass('in');
		$('.custom-modal-backdrop').removeClass('in');
	});
	
	$('.forgot-pass').css('display', 'none');
	$('.forgotpassbtn').click(function() {
	  $('.pop-login-main').css('display', 'none');
	  $('.forgot-pass').css('display', 'block');
	});
	$('.loginpopmain  button.close-button').click(function() {
	  $('.pop-login-main').css('display', 'block');
	  $('.forgot-pass').css('display', 'none');
	});	
});*/


// For career-detail page
$(document).on("click", ".scroll-eligible-section", function() {
	// e.preventDefault();
	if ($("#mat-expansion-panel-header-3")
		&& $("#mat-expansion-panel-header-3").offset()) {
		$('html, body').animate({
			scrollTop: $("#mat-expansion-panel-header-3").offset().top
		}, 1000);
	}
});