export const environment = {
  production: true,
  VERSION: require('../../package.json').version,
  VERSIONDATE: require('../../package.json').versionDate
};
