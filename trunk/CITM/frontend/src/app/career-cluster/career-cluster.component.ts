import { Component, OnInit } from '@angular/core';
import { CareerSearchService } from 'app/services/career-search.service';
import {GoogleAnalyticsService} from '../services/google-analytics.service';
import { Title, Meta } from '@angular/platform-browser';

@Component({
  selector: 'app-career-cluster',
  templateUrl: './career-cluster.component.html',
  styleUrls: ['./career-cluster.component.scss']
})
export class CareerClusterComponent implements OnInit {
  description = 'The National Career Clusters® Framework provides 16 groupings (clusters) of occupations ' + 
    'based on similar skill sets, interests, abilities, and activities. The clusters contain sub-groupings called Career ' + 
     'Pathways. These pathways are used by many school systems to develop curriculum to enhance the ' + 
     'knowledge and skills required for a particular career field. You can explore military occupations by Career Cluster.';

  constructor(
    private _careerSearchService: CareerSearchService,
    private _meta: Meta,
    private _titleTag: Title,
    private _googleAnalyticsService: GoogleAnalyticsService
  ) { }

  careerCluster: any;

  ngOnInit() {

    this._careerSearchService.getCareerCluster().subscribe(data => {
      this.careerCluster = data;
      this._careerSearchService.setCareerCluster(data);

      this._titleTag.setTitle('Career Clusters | Careers in the Military');
      this._meta.updateTag({name: 'description', content: this.description});

      this._meta.updateTag({ property: 'og:title', content: 'Career Clusters | Careers in the Military' });
      this._meta.updateTag({property: 'og:description', content: this.description});
      this._meta.updateTag({ property: 'og:image', content: 'http://www.asvabprogram.com/media-center-content/thumbnails/social-share/CITM_Webshare.jpg' });
      this._meta.updateTag({ name: 'twitter:title', content: 'Career Clusters | Careers in the Military' });
      this._meta.updateTag({name: 'twitter:description', content: this.description});
      this._meta.updateTag({ name: 'twitter:image', content: 'http://www.asvabprogram.com/media-center-content/thumbnails/social-share/CITM_Webshare.jpg' });

    });
  }
    ga(param) {
        this._googleAnalyticsService.trackClick(param);
    }

    gaSocialShare(socialPlug, value) {
        this._googleAnalyticsService.trackSocialShare(socialPlug, value);
    }

}
