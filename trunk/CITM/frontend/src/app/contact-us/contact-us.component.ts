import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpHelperService } from '../services/http-helper.service';
import { ConfigService } from '../services/config.service';
import { GoogleAnalyticsService } from '../services/google-analytics.service';
import { Router } from '@angular/router';
import { Title, Meta } from '@angular/platform-browser';
import { MessageDialogComponent } from 'app/core/dialogs/message-dialog/message-dialog.component';
import { MatDialog } from '@angular/material';

@Component({
    selector: 'app-contact-us',
    templateUrl: './contact-us.component.html',
    styleUrls: ['./contact-us.component.scss']
})
export class ContactUsComponent implements OnInit {
    description = 'Many of the questions you have about the Military have been ' +
    'asked before. Explore our <b>Frequently ' +
    ' Asked Questions</b> to find the answers you\'re looking for. If you ' +
    'need more complete answers to specific questions, don\'t hesitate ' +
    'to contact us.';


    constructor(
        private _formBuilder: FormBuilder, 
        private _httpHelper: HttpHelperService, 
        private _meta: Meta,
        private _titleTag: Title,
        private _googleAnalyticsService: GoogleAnalyticsService, 
        private _router: Router,
        public _dialog: MatDialog,
        private _configService: ConfigService
    ) { }

    aFormGroup: FormGroup;
    userType: String = '';
    userTypeOptions = [
        { id: 1, name: '---Select One---', value: '' },
        { id: 2, name: 'Student', value: 'Student' },
        { id: 3, name: 'Parent', value: 'Parent' },
        { id: 4, name: 'Educator', value: 'Educator' },
        { id: 5, name: 'Other', value: 'Other' }
    ];
    firstName;
    lastName;
    email;
    topics;
    message;
    city;
    state;
    zip;
    questionType = 1;
    formDisabled = false;
    asvabParticipant: boolean;
    contactType = 1;
    recaptchaResponse = '';
    recaptchaKey = '6LcGnigTAAAAAJUUd9llQh4I5r_-j6mlJoAryekN';
    isDev = (location.href.indexOf('careersinthemilitary') < 0);
    data = {
        armyContact: false,
        armyActiveDutyContact: false,
        armyReserveContact: false,
        armyNationalGuardContact: false,

        navyContact: false,
        navyActiveDutyContact: false,
        navyReserveContact: false,

        marineContact: false,
        marineActiveDutyContact: false,
        marineReserveContact: false,

        coastGuardContact: false,
        coastGuardActiveDutyContact: false,
        coastGuardReserveContact: false,

        airForceContact: false,
        airForceActiveDutyContact: false,
        airForceReserveContact: false,
        airForceNationalGuardContact: false
    };
    
    contact = {
        army: false,
        marines: false,
        airForce: false,
        navy: false,
        coastGuard: false
    };

    validation;

    isServiceContactEmpty = function(serviceType) {
        const path = this._router.url;
        // 1 = army, 2 = air force, 3 = marine corps, 4 =  navy, 5 = coast guard
        switch (serviceType) {
            case 1:
                this.ga(path + '#CITM_EMAIL_ARMY');
                // tslint:disable-next-line:max-line-length
                return (!this.data.armyContact && !this.data.armyActiveDutyContact && !this.data.armyReserveContact && !this.data.armyNationalGuardContact);
            case 2:
                this.ga(path + '#CITM_EMAIL_AIR_FORCE');
                // tslint:disable-next-line:max-line-length
                return (!this.data.airForceContact && !this.data.airForceActiveDutyContact && !this.data.airForceReserveContact && !this.data.airForceNationalGuardContact);
            case 3:
                this.ga(path + '#CITM_EMAIL_MARINES');
                return (!this.data.marineContact && !this.data.marineActiveDutyContact && !this.data.marineReserveContact);
            case 4:
                this.ga(path + '#CITM_EMAIL_NAVY');
                return (!this.data.navyContact && !this.data.navyActiveDutyContact && !this.data.navyReserveContact);
            case 5:
                this.ga(path + '#CITM_EMAIL_COAST_GUARD');
                return (!this.data.coastGuardContact && !this.data.coastGuardActiveDutyContact && !this.data.coastGuardReserveContact);
        }
    };

    ga(param) {
        this._googleAnalyticsService.trackClick(param);
    }

    gaSocialShare(socialPlug, value) {
        this._googleAnalyticsService.trackSocialShare(socialPlug, value);
    }

    clearForm = function() {
        this.userType = undefined;
        this.questionType = undefined;
        this.firstName = undefined;
        this.lastName = undefined;
        this.email = undefined;
        this.topics = undefined;
        this.message = undefined;
        this.city = undefined;
        this.state = undefined;
        this.zip = undefined;
        this.questionType = 1;
        //        this.recaptchaResponse = '';
        //        vcRecaptchaService.reload();
        this.data = {
            armyContact: false,
            armyActiveDutyContact: false,
            armyReserveContact: false,
            armyNationalGuardContact: false,
            navyContact: false,
            navyActiveDutyContact: false,
            navyReserveContact: false,
            airForceContact: false,
            airForceActiveDutyContact: false,
            airForceReserveContact: false,
            airForceNationalGuardContact: false,
            marineContact: false,
            marineActiveDutyContact: false,
            marineReserveContact: false,
            coastGuardContact: false,
            coastGuardActiveDutyContact: false,
            coastGuardReserveContact: false
        };
    };
    emailUs = function() {
        this.validation = undefined;
        this.formDisabled = true;
        // tslint:disable-next-line:prefer-const
        let svcList = [];
        if (this.data.armyActiveDutyContact || this.data.armyReserveContact) {
            svcList.push('A');
        }
        if (this.data.armyNationalGuardContact) {
            svcList.push('G');
        }
        if (this.data.navyActiveDutyContact || this.data.navyReserveContact) {
            svcList.push('N');
        }
        if (this.data.marineActiveDutyContact || this.data.marineReserveContact) {
            svcList.push('M');
        }
        if (this.data.airForceActiveDutyContact || this.data.airForceReserveContact || this.data.airForceNationalGuardContact) {
            svcList.push('F');
        }
        if (this.data.airForceNationalGuardContact) {
            svcList.push('FG');
        }
        if (this.data.coastGuardActiveDutyContact || this.data.coastGuardReserveContact) {
            svcList.push('C');
        }
        
        

    this.contact.army = (this.data.armyActiveDutyContact || this.data.armyReserveContact || this.data.armyNationalGuardContact);
    if (this.data.armyActiveDutyContact) {
            this.ga('#CITM_CONTACTRECRUITERSENT_ARMY_ACTIVE_DUTY' + (this.isDev ? '_DEV' : '_PROD'));
    }
    if (this.data.armyReserveContact) {
            this.ga('#CITM_CONTACTRECRUITERSENT_ARMY_RESERVE' + (this.isDev ? '_DEV' : '_PROD'));
    }
    if (this.data.armyNationalGuardContact) {
             this.ga('#CITM_CONTACTRECRUITERSENT_ARMY_NATIONAL_GUARD' + (this.isDev ? '_DEV' : '_PROD'));
    }
    // for air force
        this.contact.airForce = (this.data.airForceActiveDutyContact ||
            this.data.airForceReserveContact ||
            this.data.airForceNationalGuardContact);
    if (this.data.airForceActiveDutyContact) {
            this.ga('#CITM_CONTACTRECRUITERSENT_AIR_FORCE_ACTIVE_DUTY' + (this.isDev ? '_DEV' : '_PROD'));
    }
    if (this.data.airForceReserveContact) {
            this.ga('#CITM_CONTACTRECRUITERSENT_AIR_FORCE_RESERVE' + (this.isDev ? '_DEV' : '_PROD'));
    }
    if (this.data.airForceNationalGuardContact) {
            this.ga('#CITM_CONTACTRECRUITERSENT_AIR_FORCE_NATIONAL_GUARD' + (this.isDev ? '_DEV' : '_PROD'));
    }
    // for marine corps
    this.contact.marines = (this.data.marineActiveDutyContact || this.data.marineReserveContact);
    if (this.data.marineActiveDutyContact) {
        this.ga('#CITM_CONTACTRECRUITERSENT_MARINE_CORPS_ACTIVE_DUTY' + (this.isDev ? '_DEV' : '_PROD'));
    }
    if (this.data.marineReserveContact) {
        this.ga('#CITM_CONTACTRECRUITERSENT_MARINE_CORPS_RESERVE' + (this.isDev ? '_DEV' : '_PROD'));
    }
    // for navy
    this.contact.navy = (this.data.navyActiveDutyContact || this.data.navyReserveContact);
    if (this.data.navyActiveDutyContact) {
        this.ga('#CITM_CONTACTRECRUITERSENT_NAVY_ACTIVE_DUTY' + (this.isDev ? '_DEV' : '_PROD'));
    }
    if (this.data.navyReserveContact) {
        this.ga('#CITM_CONTACTRECRUITERSENT_NAVY_RESERVE' + (this.isDev ? '_DEV' : '_PROD'));
    }
    // for coast guard
    this.contact.coastGuard = (this.data.coastGuardActiveDutyContact || this.data.coastGuardReserveContact);
    if (this.data.coastGuardActiveDutyContact) {
        this.ga('#CITM_CONTACTRECRUITERSENT_COAST_GUARD_ACTIVE_DUTY' + (this.isDev ? '_DEV' : '_PROD'));
    }
    if (this.data.coastGuardReserveContact) {
        this.ga('#CITM_CONTACTRECRUITERSENT_COAST_GUARD_RESERVE' + (this.isDev ? '_DEV' : '_PROD'));
    }
    // track service
    if (this.contact.army) {
        this.ga('#CITM_CONTACTRECRUITERSENT_ARMY' + (this.isDev ? '_DEV' : '_PROD'));
    }
    if (this.contact.airForce) {
        this.ga('#CITM_CONTACTRECRUITERSENT_AIR_FORCE' + (this.isDev ? '_DEV' : '_PROD'));
}
    if (this.contact.marines) {
        this.ga('#CITM_CONTACTRECRUITERSENT_MARINE_CORPS' + (this.isDev ? '_DEV' : '_PROD'));
}
    if (this.contact.navy) {
        this.ga('#CITM_CONTACTRECRUITERSENT_NAVY' + (this.isDev ? '_DEV' : '_PROD'));
}
    if (this.contact.coastGuard) {
        this.ga('#CITM_CONTACTRECRUITERSENT_COAST_GUARD' + (this.isDev ? '_DEV' : '_PROD'));
    }

        const emailObject = {
            userType: this.userType,
            questionType: this.questionType,
            firstName: this.firstName,
            lastName: this.lastName,
            email: this.email,
            topics: this.topics,
            message: this.message,
            city: this.city,
            state: this.state,
            zip: this.zip,
            recaptchaResponse: this.aFormGroup.value.recaptcha,
            svcList: svcList,
            asvabParticipant: this.asvabParticipant ? 'Yes' : 'No',
            contactType: this.contactType
        };

        if ((this.questionType === 1 &&
            (this.isServiceContactEmpty(1)) &&
            (this.isServiceContactEmpty(2)) &&
            (this.isServiceContactEmpty(3)) &&
            (this.isServiceContactEmpty(4)) &&
            (this.isServiceContactEmpty(5))) ||
            this.userType === undefined ||
            this.userType === '' ||
            this.questionType === undefined ||
            this.firstName === undefined ||
            this.lastName === undefined ||
            this.email === undefined ||
            (this.questionType === 2 && this.topics === undefined) ||
            this.message === undefined ||
            this.asvabParticipant === undefined) {
            this.validation = 'Fill out all fields.';
            this.formDisabled = false;
            return false;
        }

        if (this.aFormGroup.value.recaptcha === '') {
            // is empty
            this.validation = 'Please resolve the captcha and submit!';
            this.formDisabled = false;
            return false;
        } else {
            if (this.questionType === 2) {
                const fullUrl = this._configService.getAwsFullUrl(`/api/contactus/email/ctm/`);
                this._httpHelper.httpHelper('POST', fullUrl, null, emailObject, true)
                    .then(
                    (val) => {
                        this.clearForm();
                        this.formDisabled = false;
                        alert('Request Sent');
                    },
                    (err) => { 
						let errorMessage = '';
						
						if(err === 'Server error') {
							errorMessage = err;
						} else {
							for (const property in err) {
								errorMessage+= `${err[property]}` + '<br>';
							}
						}
						
						const data = {
	                        'title': 'Error',
	                        'message': errorMessage
	                    };
	
	                    this._dialog.open(MessageDialogComponent, {
	                        data: data,
	                        hasBackdrop: true,
	                        disableClose: true,
	                        minWidth: '400px',
	                        autoFocus: true
	                    });
						this.formDisabled = false; 
					}
                    );

            } else if (this.questionType = 1) {
                const fullUrl = this._configService.getAwsFullUrl(`/api/contactus/service-contact-us/`);
                this._httpHelper.httpHelper('POST', fullUrl, null, emailObject, true)
                    .then(
                    (val) => {
                        this.clearForm();
                        this.formDisabled = false;
                        alert('Request Sent');
                    },
                    (err) => { 
						let errorMessage = '';
					
						if(err === 'Server error') {
							errorMessage = err;
						} else {
							for (const property in err) {
								errorMessage+= `${err[property]}` + '<br>';
							}
						}
						
						const data = {
	                        'title': 'Error',
	                        'message': errorMessage
	                    };
	
	                    this._dialog.open(MessageDialogComponent, {
	                        data: data,
	                        hasBackdrop: true,
	                        disableClose: true,
	                        minWidth: '400px',
	                        autoFocus: true
	                    });
						this.formDisabled = false; 
					}
                    );
            }
        }

    };

    armyCheckAll = function() {
        if (this.data.armyContact) {
            this.data.armyActiveDutyContact = true;
            this.data.armyReserveContact = true;
            this.data.armyNationalGuardContact = true;
        } else {
            this.data.armyActiveDutyContact = false;
            this.data.armyReserveContact = false;
            this.data.armyNationalGuardContact = false;
        }
    };

    armyCheckOne = function() {
        if (this.data.armyContact) {
            this.data.armyContact = false;
        } else if (this.data.armyActiveDutyContact && this.data.armyReserveContact && this.data.armyNationalGuardContact) {
            this.data.armyContact = true;
        }
    };

    navyCheckAll = function() {
        if (this.data.navyContact) {
            this.data.navyActiveDutyContact = true;
            this.data.navyReserveContact = true;
        } else {
            this.data.navyActiveDutyContact = false;
            this.data.navyReserveContact = false;
        }
    };

    navyCheckOne = function() {
        if (this.data.navyContact) {
            this.data.navyContact = false;
        } else if (this.data.navyActiveDutyContact && this.data.navyReserveContact) {
            this.data.navyContact = true;
        }
    };

    marineCheckAll = function() {
        if (this.data.marineContact) {
            this.data.marineActiveDutyContact = true;
            this.data.marineReserveContact = true;
        } else {
            this.data.marineActiveDutyContact = false;
            this.data.marineReserveContact = false;
        }
    };

    marineCheckOne = function() {
        if (this.data.marineContact) {
            this.data.marineContact = false;
        } else if (this.data.marineActiveDutyContact && this.data.marineReserveContact) {
            this.data.marineContact = true;
        }
    };

    airForceCheckAll = function() {
        if (this.data.airForceContact) {
            this.data.airForceActiveDutyContact = true;
            this.data.airForceReserveContact = true;
            this.data.airForceNationalGuardContact = true;
        } else {
            this.data.airForceActiveDutyContact = false;
            this.data.airForceReserveContact = false;
            this.data.airForceNationalGuardContact = false;
        }
    };

    airForceCheckOne = function() {
        if (this.data.airForceContact) {
            this.data.airForceContact = false;
        } else if (this.data.airForceActiveDutyContact && this.data.airForceReserveContact && this.data.airForceNationalGuardContact) {
            this.data.airForceContact = true;
        }
    };

    coastGuardCheckAll = function() {
        if (this.data.coastGuardContact) {
            this.data.coastGuardActiveDutyContact = true;
            this.data.coastGuardReserveContact = true;
        } else {
            this.data.coastGuardActiveDutyContact = false;
            this.data.coastGuardReserveContact = false;
        }
    };

    coastGuardCheckOne = function() {
        if (this.data.coastGuardContact) {
            this.data.coastGuardContact = false;
        } else if (this.data.coastGuardActiveDutyContact && this.data.coastGuardReserveContact) {
            this.data.coastGuardContact = true;
        }
    };

    ngOnInit() {
        this.aFormGroup = this._formBuilder.group({
            recaptcha: ['', Validators.required]
        });

      this._titleTag.setTitle('Contact Us | Careers in the Military');
      this._meta.updateTag({name: 'description', content: this.description});

      this._meta.updateTag({ property: 'og:title', content: 'Contact Us | Careers in the Military' });
      this._meta.updateTag({property: 'og:description', content: this.description});
      this._meta.updateTag({ property: 'og:image', content: 'http://www.asvabprogram.com/media-center-content/thumbnails/social-share/CITM_Webshare.jpg' });
      this._meta.updateTag({ name: 'twitter:title', content: 'Contact Us | Careers in the Military' });
      this._meta.updateTag({name: 'twitter:description', content: this.description});
      this._meta.updateTag({ name: 'twitter:image', content: 'http://www.asvabprogram.com/media-center-content/thumbnails/social-share/CITM_Webshare.jpg' });

    }

}

