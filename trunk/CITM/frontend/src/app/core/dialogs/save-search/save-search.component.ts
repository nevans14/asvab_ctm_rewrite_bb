import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { UserService } from 'app/services/user.service';

@Component({
  selector: 'app-save-search',
  templateUrl: './save-search.component.html',
  styleUrls: ['./save-search.component.scss']
})
export class SaveSearchComponent implements OnInit {
  cancel = false;
  searchObj: any;
  title: string;
  error: string;

  constructor(
    public _dialogRef: MatDialogRef<SaveSearchComponent>,
    @Inject(MAT_DIALOG_DATA) public data,
    private _userService: UserService
  ) { }

  ngOnInit() {
    this.searchObj = this.data['searchValues'];
  }

  onCancel() {
    this.cancel = true;
    this._dialogRef.close();
  }

  onSave() {
    if (this.title.length > 50) {
      this.error = 'Search list name is over 50 characters. ';
      return;
    }
    this.searchObj['saveTitle'] =  this.title;
    return this._userService.insertSavedSearch(this.searchObj)
    .then (( result) => {
      this.cancel = false;
      this._dialogRef.close();
    })
    .catch((error) => {
      this.error = error;
    });
  }

}
