import { Component, OnInit, Inject } from '@angular/core';
import {  MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { MatDialog } from '@angular/material';
import { MessageDialogComponent } from 'app/core/dialogs/message-dialog/message-dialog.component';
import { CareerDetailService } from 'app/services/career-detail.service';
import { ConfigService } from 'app/services/config.service';

export interface JobTitlesDialogData {
  type: string;
  title: string;
  list: any;
  rSvcId: any; 
  rMcId: any; 
  rMocId: any;
  serviceDetail: any;
  compositeScores: any,
  serviceContacts: any;
  militaryCareer: any;
  isEligible: boolean;
  armyScore: any;
}

@Component({
  selector: 'app-job-titles',
  templateUrl: './job-titles.component.html',
  styleUrls: ['./job-titles.component.scss']
})
export class JobTitlesDialogComponent implements OnInit {
  serviceOfferings: any;
  serviceDetail: any = [];
  detailOfferings: any = [];
  noDetailOfferings: any = [];
  compositeScores: any;
  serviceContacts: any;
  militaryCareer: any;
  isEligible: boolean = false;
  armyScore: any;

  constructor(
    private _dialog: MatDialog,
    public _dialogRef: MatDialogRef<JobTitlesDialogComponent>,
    private _careerDetailService: CareerDetailService,
    private _configService: ConfigService,
    @Inject(MAT_DIALOG_DATA) public data: JobTitlesDialogData
    ) {}

  ngOnInit() {
    this.serviceOfferings = this.data.list;
    this.serviceDetail = this.data.serviceDetail;
    this.compositeScores = this.data.compositeScores && this.data.compositeScores.aFQTRawSSComposites ? this.data.compositeScores.aFQTRawSSComposites : this.data.compositeScores;
    this.serviceContacts = this.data.serviceContacts;
    this.militaryCareer = this.data.militaryCareer;
    this.isEligible = this.data.isEligible;
    this.armyScore = this.data.armyScore;

    for(let i = 0; i < this.serviceOfferings.length; ++i) {
      if (this._careerDetailService.displayInfoIcon(this.serviceDetail.find(s => s.mocId === this.serviceOfferings[i].mocId))) {
        this.noDetailOfferings.push(this.serviceOfferings[i]);
      } else {
        this.detailOfferings.push(this.serviceOfferings[i]);
      }
    }
  }

  isServiceEligible(so) {
    if (this.compositeScores) {
      if (so.svc === 'A') {
        let eligible = false;
        const armyList = this.armyScore.filter(item => item.mocId === so.mocId && item.mocTitle === so.mocTitle && item.mcId === so.mcId);

        for (let i = 0; i < armyList.length && !eligible; ++i) {
          eligible = this._careerDetailService.checkEligible(armyList[i], this.compositeScores, null, null, 1, this.armyScore);
        }
        return eligible;
      }
      return this._careerDetailService.checkEligible(so, this.compositeScores) || this._careerDetailService.checkEligible(so, this.compositeScores, null, null, 2);
    }
    return false;
  }

  onClickLink(svc, mcId, mocId){
    const returnData = {
      rSvcId : svc,
      rMcId: mcId,
      rMocId: mocId
    };
    this._dialogRef.close(returnData);
  }

  onNoClick(): void {
    this._dialogRef.close();
  }

  openDescription(section) {

    let headerTitle, bodyText;
    if (section === 'Eligible-text') {
      headerTitle = ' ';
      bodyText = 'Your scores are within range of the requirements to enter one or more jobs related to this career. Contact a recruiter for details.'
    } else if (section === 'Recruiter-link') {
      headerTitle = ' ';
      bodyText = 'Job that do not have details will not link to the modal, but will have a contact a recruiter direct link.';
    } else {
      return 0;
    }

    const data = {
      'title': headerTitle,
      'message': bodyText
    };
    this._dialog.open(MessageDialogComponent, {
      data: data,
      hasBackdrop: true,
      disableClose: true,
      maxWidth: '400px'
    });

  }
}
