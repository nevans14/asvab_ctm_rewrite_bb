import { Component, OnInit, OnDestroy } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { ContentManagementService } from 'app/services/content-management.service';
import { UtilityService } from 'app/services/utility.service';

@Component({
    selector: 'app-fitness-air-force-dialog',
    templateUrl: './fitness-air-force-dialog.component.html',
    styleUrls: ['./fitness-air-force-dialog.component.scss']
})
export class FitnessAirForceDialogComponent implements OnInit, OnDestroy {
    pageHtml: any = [];
    page: any;

    constructor(
        private _contentManagementService: ContentManagementService,
        public dialogRef: MatDialogRef<FitnessAirForceDialogComponent>,
        private _utility: UtilityService,
    ) { }

    ngOnInit() {
        this.getPageByName('dialog-fitness-air-force');
    }

  /***
   * Get content from DB
   */
  getPageByName(pageName) {
    // Remove leading slash before lookup
    if (pageName.indexOf('/') === 0) {
      pageName = pageName.substring(1);
    }
    console.log('pageName:', pageName);
    this._contentManagementService.getPageByPageName(pageName).subscribe(
      data => {
        this.page = data;
        this._utility.parseCms(this.page.pageHtml, this.pageHtml);
      });
  }

  close() {
    this.dialogRef.close();
  }

  ngOnDestroy() {
  }
}
