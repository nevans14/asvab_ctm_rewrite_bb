import { Component, AfterContentInit, Inject, ChangeDetectionStrategy } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';

@Component({
    changeDetection: ChangeDetectionStrategy.OnPush,
    selector: 'app-popup-player',
    templateUrl: './popup-player.component.html',
    styleUrls: ['./popup-player.component.scss']
})

export class PopupPlayerComponent implements AfterContentInit {
    youtubeURL: string;

    constructor(
        public dialogRef: MatDialogRef<PopupPlayerComponent>,
        @Inject(MAT_DIALOG_DATA) public data,
        private sanitizer: DomSanitizer
    ) { }

    ngAfterContentInit() {
        this.youtubeURL = 'https://www.youtube.com/embed/' + this.data.videoId + '?autoplay=0&rel=0';
    }

    safeURL(url: string): SafeResourceUrl {
        return this.sanitizer.bypassSecurityTrustResourceUrl(url);
    }

    closeDialog(): void {
        this.dialogRef.close();
    }

    onNoClick(): void {
        this.dialogRef.close();
    }

}
