import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { GoogleAnalyticsService } from 'app/services/google-analytics.service';

@Component({
    selector: 'app-military-info-dialog',
    templateUrl: './military-info-dialog.component.html',
    styleUrls: ['./military-info-dialog.component.scss']
})
export class MilitaryInfoDialogComponent implements OnInit {

    service;
    constructor(public dialogRef: MatDialogRef<MilitaryInfoDialogComponent>,
		@Inject(MAT_DIALOG_DATA) public data,
		private _googleAnalyticsService: GoogleAnalyticsService) { }

    ngOnInit() {
		if(this.data ==='A'){
			this.ga('#CITM_OPEN_SERVICE_ARMY'); 
		} else if (this.data ==='M'){
			this.ga('#CITM_OPEN_SERVICE_MARINE_CORPS');
		} else if (this.data ==='N'){
			this.ga('#CITM_OPEN_SERVICE_NAVY'); 
		} else if (this.data ==='F'){
			this.ga('#CITM_OPEN_SERVICE_AIR_FORCE');
		} else if (this.data ==='C'){
			this.ga('#CITM_OPEN_SERVICE_COAST_GUARD'); 
		} else if (this.data ==='S'){
			this.ga('#CITM_OPEN_SERVICE_SPACE_FORCE'); 
		}
        this.service = this.data;
    }

    close() {
        this.dialogRef.close();
	}
	
	ga(param) {
		this._googleAnalyticsService.trackClick(param);
	}

	gaSocialShare(socialPlug, value) {
		this._googleAnalyticsService.trackSocialShare(socialPlug, value);
	}
}

