import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { UserService } from 'app/services/user.service';
import { ConfigService } from 'app/services/config.service';
import { NeedToLoginDialogComponent } from 'app/core/dialogs/need-to-login-dialog/need-to-login-dialog.component';

@Component({
  selector: 'app-notes-dialog',
  templateUrl: './notes-dialog.component.html',
  styleUrls: ['./notes-dialog.component.scss']
})
export class NotesDialogComponent implements OnInit {
  mcId: any;
  noteId: any;
  isPerformingAction = false;
  notes = '';
  initialNotes = {title: '', notes: '', noteId: undefined};
  error: string;
  noteList = [];
  beforeEdit = '';

  constructor(
    public _dialogRef: MatDialogRef<NotesDialogComponent>,
    private _dialog: MatDialog,
    @Inject(MAT_DIALOG_DATA) public data,
    private _userService: UserService,
    private _config: ConfigService
  ) { }

  ngOnInit() {
    if (this._config.isLoggedIn()) {
      this.mcId = this.data.mcId;
        // If the note is not passed in by profile page then get the note
      if (!this.data.note) {
        //  Create a blank one for the screen to populate
        this.createNote();
        this.getNote();
      } else {
        this.getNote();
        this.notes = this.data.note.notes;
        this.noteId = this.data.note.noteId;
        this.beforeEdit = this.notes;
      }
    } else {
      this._dialog.open(NeedToLoginDialogComponent, { hasBackdrop: true, disableClose: true, autoFocus: true });
      this.onClose();
    }
  }

  getNote () {
    if (this.data.typeOfNote === 'occupational') {
      this._userService.getNotes().subscribe(
        data => {
          this.noteList = data;
          this.setNote(data);
      });
    } else if (this.data.typeOfNote === 'careercluster') {
      this._userService.getCareerClusterNote(this.mcId).subscribe(
        data => {
          this.setNote(data);
      });
    }
  }

  setNote(notes) {
    if (notes) {
      for (let i = 0; i < notes.length; i++) {
        if (notes[i].mcId === this.mcId) {
          this.data.note = notes[i];
          this.noteId = this.data.note.noteId;
        }
      }
    }
    if (!this.data.note) {
      this.createNote();
    }
    this.notes = this.data.note.notes;
    this.initialNotes = this.data.note ? this.data.note : '';
    this.beforeEdit = this.notes;
  }

  createNote() {
    this.data.note = {
      'notes': '',
      'title': this.data.title,
      'mcId': this.data.mcId,
      'noteId': null,
    };
  }

  save() {
    if (this.notes) {
      this.isPerformingAction = true;
      this.data.note.notes = this.notes;
      if (this.noteId) {
        this.updateNote(); 
      } else {
        this.insertNote();
      }
    } else {
      this.error = 'Note is blank';
      return;
    }
    
    this._dialogRef.close();
  }

  updateNote() {
    if (this.data.typeOfNote === 'occupational') {
      this._userService.updateNotes(this.data.note)
      .then((obj) => {
        this.isPerformingAction = false;
      });
    } else if (this.data.typeOfNote === 'careercluster') {
      this._userService.updateCareerClusterNotes(this.data.note)
      .then((obj) => {
        this.data.
         this.isPerformingAction = false;
       });
      }
  }

  insertNote() {
    if (this.data.typeOfNote === 'occupational') {
      this._userService.insertNotes(this.data.note)
     .then((obj) => {
        this.isPerformingAction = false;
      });
  } else if (this.data.typeOfNote === 'careercluster') {
    this._userService.insertCareerClusterNotes(this.data.note)
    .then((obj) => {
       this.isPerformingAction = false;
     });
    }
 }

  deleteNote() {
    this.isPerformingAction = true;

    const isConfirmed = confirm('Do you want to delete this note?');
    if (!isConfirmed) {
      this.isPerformingAction = false;
      return;
    }

    this._userService.deleteNote(this.data.note.noteId)
      .then(response  => {
        this.isPerformingAction = false;
        if (this.mcId = this.data.mcId) {
          this.data.note = null;
          this.noteId = null;
        } else {
          this.data.note = this.initialNotes;
          this.noteId = this.initialNotes.noteId;
        }
        
        this.getNote();
        this.isPerformingAction = false;
      })
      .catch(err => {
        console.log("error " + err)
      });
  }

  resetNotes(noteId=null) {
    if (!noteId) {
      this.notes = this.initialNotes.notes;
      this.data.note = this.initialNotes;
      this.noteId = this.initialNotes.noteId;
      this.beforeEdit = this.notes;
    } else {
      for (var i = 0; i < this.noteList.length; i++) {
        if (this.noteList[i].noteId === noteId) {
          this.notes = this.noteList[i].notes;
          this.data.note = this.noteList[i];
          this.noteId = this.noteList[i].noteId;
          this.beforeEdit = this.notes;
        }
      }
    }
  }

  clearNotes() {
    this.notes = '';
  }

  onClose(): void {
    if(this.beforeEdit != this.notes) {
      const isConfirmed = confirm('Do you want to save before closing your notes?');
      if (isConfirmed) {
        this.save();
      }
    }

    this._dialogRef.close();
  }
}
