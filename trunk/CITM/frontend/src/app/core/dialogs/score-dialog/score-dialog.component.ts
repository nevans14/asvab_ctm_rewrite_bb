import { Component, OnInit, Inject } from '@angular/core';
import { ScoreService } from 'app/services/score.service';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { CareerDetailService } from 'app/services/career-detail.service';
import { PortfolioService } from 'app/services/portfolio.service';
import { LoginDialogComponent } from 'app/core/dialogs/login-dialog/login-dialog.component';
import { forkJoin } from 'rxjs';
import { MessageDialogComponent } from 'app/core/dialogs/message-dialog/message-dialog.component';
import { ConfigService } from 'app/services/config.service';

@Component({
    selector: 'app-score-dialog',
    templateUrl: './score-dialog.component.html',
    styleUrls: ['./score-dialog.component.scss']
})

export class ScoreDialogComponent implements OnInit {
    modalOptions: any;
    viewModel: any;
    name: string;
    isDisabled: boolean;
    value: any;
    valid: boolean;
    invalid: boolean;
    pending: boolean;
    disabled: boolean;
    enabled: boolean;
    pristine: boolean;
    dirty: boolean;
    touched: boolean;
    status: string;
    untouched: boolean;
    scoreSummary: any;
    imageTitle;
    serviceContacts: any;
    serviceOffering: any;
    userSystem: any;
    groupIdList: any[];
    dialogOptions: any;
    model: any;
    path: string[];
    formDirective: any;
    scoreObj: any;
    compositeScores: any;
    testScores: any = {};
    fa: any;
    fb: any;
    na: any;
    nb: any;
    aa: any;
    ab: any;
    compositeFavoritesFlags: Array<{}> = [];
    mocId: any;
    groupId: any;
    armyOffering: any;
    filterServiceOffering(serviceOffering): any[] {
        const serviceFiltered = serviceOffering.filter(i => i.groupId === this.groupId && i.mocId === this.mocId);
        const sortServiceFiltered = serviceFiltered.sort((a, b) => (a.value > b.value) ? 1 : -1);
        return sortServiceFiltered;
    }
    reset(value?: any): void {
        throw new Error('Method not implemented.');
    }

    constructor(
		private _scoreService: ScoreService,
        public dialogRef: MatDialogRef<ScoreDialogComponent>,
        private _dialog: MatDialog,
        private _portfolioService: PortfolioService,
        @Inject(MAT_DIALOG_DATA) public data,
        private _careerDetailService: CareerDetailService,
        private _configService: ConfigService,
        ) {
    }

    ngOnInit() {
        this.scoreObj = this.data.scoreObj;
        if(this.scoreObj.isLoggedIn) {
			if(this.scoreObj.serviceName === 'Scores'){
				this.redirect('composite-scores');
			} else {

                this._portfolioService.getUserSystem().subscribe(usersys => {
					this.userSystem = usersys;
                });

                forkJoin([
                    this._scoreService.getScore(),
                    this._scoreService.getServiceOffering(this.scoreObj.mcId),
                    this._careerDetailService.getArmyScores(this.scoreObj.mcId)
                ]).subscribe( result => {
                    let scoreData = result[0];
                    let svcOffering = result[1];
                    let armyScore = result[2];
                        
                    // Score data
                    this.compositeScores = !scoreData || !scoreData.hasOwnProperty('aFQTRawSSComposites')
                        ? null
                        : scoreData['aFQTRawSSComposites'];

                    if (this.compositeScores) {
                        this._scoreService.setScoreObject(this.compositeScores);
                        this.compositeScores.el_cg = this.compositeScores.el_navy;
                        this.compositeScores.mr_cg = this.compositeScores.mec_navy;
                        this.compositeScores.gt_cg = this.compositeScores.gt_navy;
                        this.compositeScores.cl_cg = this.compositeScores.adm_navy;
                    }

                    // Service offering
                    this.serviceOffering = svcOffering; 
                    this.serviceOffering.forEach((service, index) => {
                        if (service.additionalRequirementOne && this.compositeScores) {
                            const abbreviation = this.getTestAbbreviation(service.additionalRequirementOne).toLowerCase();
                            this.testScores[abbreviation] = this.compositeScores[abbreviation];
                        }
                    })

                    // Army Score
                    this.armyOffering = armyScore;
                    this.groupIdList = [];

                    if (this.serviceOffering) {
                        for (let i = 0; i < this.serviceOffering.length; i++) {

                            let duplicate = false;
                            for (let j = 0; j < this.groupIdList.length; j++) {
                                if (this.groupIdList[j].groupId === this.serviceOffering[i].groupId
                                    && this.groupIdList[j].mocId === this.serviceOffering[i].mocId) {
                                    duplicate = true;
                                    this.groupIdList[j].duplicate = duplicate;
                                }
                            }

                            if (!duplicate) {
                                this.groupIdList.push({groupId: this.serviceOffering[i].groupId,
                                    mocId: this.serviceOffering[i].mocId, duplicate: duplicate});
                            }
                        }
                    }
                })

				this._scoreService.getCompositeFavoritesFlags().subscribe(flagData => {
					this.compositeFavoritesFlags = flagData;
                });
			}
		}
    }

    isCompOneEligible(s) {
        if (!s.svc && s.score) {
            return this._configService.isLoggedIn() && this._careerDetailService.checkEligible(s, this.compositeScores, null, null, 1, this.armyOffering);
        }
        return this._configService.isLoggedIn() && this._careerDetailService.checkEligible(s, this.compositeScores);
    }

    isCompTwoEligible(s) {
        return this._configService.isLoggedIn() && this._careerDetailService.checkEligible(s, this.compositeScores, null, null, 2);
    }

    getTestAbbreviation(additionalRequirement) {
        if (additionalRequirement.includes('AR')) return 'AR';
        if (additionalRequirement.includes('AS')) return 'AS';
        if (additionalRequirement.includes('EI')) return 'EI';
        if (additionalRequirement.includes('GS')) return 'GS';
        if (additionalRequirement.includes('MC')) return 'MC';
        if (additionalRequirement.includes('MK')) return 'MK';
        if (additionalRequirement.includes('PC')) return 'PC';
        if (additionalRequirement.includes('WK')) return 'WK';
        if (additionalRequirement.includes('VE')) return 'VE';
    }

    getTestScore(abbreviation) {
        return this.testScores[abbreviation.toLowerCase()] + '';
    }

    addRemoveFavComposite(compScore, value) {

        if (Array.isArray(compScore)) {
            compScore.forEach(score => {
                value === true ? this.compositeFavoritesFlags[score] = 1 : this.compositeFavoritesFlags[score] = 0;
            })
        } else {
            value === true ? this.compositeFavoritesFlags[compScore] = 1 : this.compositeFavoritesFlags[compScore] = 0;
        }
        
        this._scoreService.setCompositeFavoritesFlags(this.compositeFavoritesFlags);
    }

    // Army composite lookup.
    armyCompositeScores(id) {
        switch (id) {
            case 'GT':
                return 'General Technical';
            case 'CL':
                return 'Clerical';
            case 'CO':
                return 'Combat';
            case 'EL':
                return 'Electronics Repair';
            case 'FA':
                return 'Field Artillery';
            case 'GM':
                return 'General Maintenance';
            case 'MM':
                return 'Mechanical Maintenance';
            case 'OF':
                return 'Operations / Food';
            case 'SC':
                return 'Serveillance / Communications';
            case 'ST':
                return 'Skilled Technical';
        }

    }

    // Army composite property name conversion.
    armyCompositePropertyName(id) {
        switch (id) {
            case 'GT':
                return 'gt_army';
            case 'CL':
                return 'cl_army';
            case 'CO':
                return 'co_army';
            case 'EL':
                return 'el_army';
            case 'FA':
                return 'fa_army';
            case 'GM':
                return 'gm_army';
            case 'MM':
                return 'mm_army';
            case 'OF':
                return 'of_army';
            case 'SC':
                return 'sc_army';
            case 'ST':
                return 'st_army';
        }
    }

    // Army user composite score.
    armyUserCompositeScores(id) {
        switch (id) {
            case 'GT':
                return this.compositeScores.gt_army;
            case 'CL':
                return this.compositeScores.cl_army;
            case 'CO':
                return this.compositeScores.co_army;
            case 'EL':
                return this.compositeScores.el_army;
            case 'FA':
                return this.compositeScores.fa_army;
            case 'GM':
                return this.compositeScores.gm_army;
            case 'MM':
                return this.compositeScores.mm_army;
            case 'OF':
                return this.compositeScores.of_army;
            case 'SC':
                return this.compositeScores.sc_army;
            case 'ST':
                return this.compositeScores.st_army;
        }
    }

    armyCompositeCheckBox = function (id) {
        switch (id) {
            case 'GT':
                return this.compositeFavoritesFlags.gt_army;
            case 'CL':
                return this.compositeFavoritesFlags.cl_army;
            case 'CO':
                return this.compositeFavoritesFlags.co_army;
            case 'EL':
                return this.compositeFavoritesFlags.el_army;
            case 'FA':
                return this.compositeFavoritesFlags.fa_army;
            case 'GM':
                return this.compositeFavoritesFlags.gm_army;
            case 'MM':
                return this.compositeFavoritesFlags.mm_army;
            case 'OF':
                return this.compositeFavoritesFlags.of_army;
            case 'SC':
                return this.compositeFavoritesFlags.sc_army;
            case 'ST':
                return this.compositeFavoritesFlags.st_army;
        }
    };

    // Navy composite lookup.
    navyCompositeScores(id) {
        switch (id) {
            case 'GT':
                return 'General Technical';
            case 'EL':
                return 'Electronics';
            case 'BEE':
                return 'Basic Electricity & Electronics';
            case 'ENG':
                return 'Engineering';
            case 'MEC':
                return 'Mechanical';
            case 'MEC2':
                return 'Mechanical 2';
            case 'NUC':
                return 'Nuclear Field';
            case 'OPS':
                return 'Operations';
            case 'HM':
                return 'Hospitalman';
            case 'ADM':
                return 'Administration';
            default:
                return undefined;
        }

    }

    // Navy composite property name conversion.
    navyCompositePropertyName(id) {
        switch (id) {
            case 'GT':
                return 'gt_navy';
            case 'EL':
                return 'el_navy';
            case 'BEE':
                return 'bee_navy';
            case 'ENG':
                return 'eng_navy';
            case 'MEC':
                return 'mec_navy';
            case 'MEC2':
                return 'mec2_navy';
            case 'NUC':
                return 'nuc_navy';
            case 'OPS':
                return 'ops_navy';
            case 'HM':
                return 'hm_navy';
            case 'ADM':
                return 'adm_navy';
        }
    }

    // Navy user composite score.
    navyUserCompositeScores(id) {
        switch (id) {
            case 'GT':
                return this.compositeScores.gt_navy;
            case 'EL':
                return this.compositeScores.el_navy;
            case 'BEE':
                return this.compositeScores.bee_navy;
            case 'ENG':
                return this.compositeScores.eng_navy;
            case 'MEC':
                return this.compositeScores.mec_navy;
            case 'MEC2':
                return this.compositeScores.mec2_navy;
            case 'NUC':
                return this.compositeScores.nuc_navy;
            case 'OPS':
                return this.compositeScores.ops_navy;
            case 'HM':
                return this.compositeScores.hm_navy;
            case 'ADM':
                return this.compositeScores.adm_navy;
        }
    }

    navyCompositeCheckBox = function (id) {
        switch (id) {
            case 'GT':
                return this.compositeFavoritesFlags.gt_navy;
            case 'EL':
                return this.compositeFavoritesFlags.el_navy;
            case 'BEE':
                return this.compositeFavoritesFlags.bee_navy;
            case 'ENG':
                return this.compositeFavoritesFlags.eng_navy;
            case 'MEC':
                return this.compositeFavoritesFlags.mec_navy;
            case 'MEC2':
                return this.compositeFavoritesFlags.mec2_navy;
            case 'NUC':
                return this.compositeFavoritesFlags.nuc_navy;
            case 'OPS':
                return this.compositeFavoritesFlags.ops_navy;
            case 'HM':
                return this.compositeFavoritesFlags.hm_navy;
            case 'ADM':
                return this.compositeFavoritesFlags.adm_navy;
        }
    };

    afCompositeScores = function (id) {
        switch (id) {
            case 'M':
                return 'Mechanical';
            case 'A':
                return 'Administrative';
            case 'G':
                return 'General';
            case 'E':
                return 'Electronic';
            default:
                return undefined;
        }
    };

    afCompositePropertyName(id) {
        switch (id) {
            case 'M':
                return 'm_af';
            case 'A':
                return 'a_af';
            case 'G':
                return 'g_af';
            case 'E':
                return 'e_af';
        }
    }

    afUserCompositeScores = function (id) {
        switch (id) {
            case 'M':
                return this.compositeScores.m_af;
            case 'A':
                return this.compositeScores.a_af;
            case 'G':
                return this.compositeScores.g_af;
            case 'E':
                return this.compositeScores.e_af;
        }
    };

    afCompositeCheckBox = function (id) {
        switch (id) {
            case 'M':
                return this.compositeFavoritesFlags.m_af;
            case 'A':
                return this.compositeFavoritesFlags.a_af;
            case 'G':
                return this.compositeFavoritesFlags.g_af;
            case 'E':
                return this.compositeFavoritesFlags.e_af;
        }
    };

    coastGuardCompositeScores = function(id) {
        switch (id) {
            case 'EL':
                return 'Electronics';
            case 'MR':
                return 'Mechanical';
            case 'GT':
                return 'General Technical';
            case 'CL':
                return 'Clerical';
            default:
                return undefined;
        }
    }

    coastGuardUserCompositeScores = function(id) {
        switch (id) {
            case 'EL':
                return this.compositeScores.el_cg;
            case 'MR':
                return this.compositeScores.mr_cg;
            case 'GT':
                return this.compositeScores.gt_cg;
            case 'CL':
                return this.compositeScores.cl_cg;
        }
    }

    coastGuardCompositePropertyName(id) {
        switch (id) {
            case 'EL':
                return 'el_cg';
            case 'MR':
                return 'mr_cg';
            case 'GT':
                return 'gt_cg';
            case 'CL':
                return 'cl_cg';
        }
    }

    coastGuardCompositeCheckBox = function(id) {
        switch (id) {
            case 'EL':
                return this.compositeFavoritesFlags.el_cg;
            case 'MR':
                return this.compositeFavoritesFlags.mr_cg;
            case 'GT':
                return this.compositeFavoritesFlags.gt_cg;
            case 'CL':
                return this.compositeFavoritesFlags.cl_cg;
        }
    }

    redirect(path) {
        const returnData = {
            path : path
          };
          this.dialogRef.close(returnData);
    }

    close() {
        this.dialogRef.close();
    }

    openLoginDialog() {
        this._dialog.open( LoginDialogComponent, { hasBackdrop: true, disableClose: true, autoFocus: true } );
        this.close();
    }

    serviceContactUsModal(service) {
        for (let i = 0; i < this.serviceContacts.length; i++) {
            if (service === this.serviceContacts[i].name) {
                break;
            }
        }
    }

    openDescription(section) {

        let headerTitle, bodyText;
        if (section === 'Eligible-text') {
          headerTitle = ' ';
          bodyText = 'Your scores are within range of the requirements to enter one or more jobs related to this career. Contact a recruiter for details.'
        } else {
          return 0;
        }
    
        const data = {
          'title': headerTitle,
          'message': bodyText
        };
        this._dialog.open(MessageDialogComponent, {
          data: data,
          hasBackdrop: true,
          disableClose: true,
          maxWidth: '400px'
        });
    
      }
}
