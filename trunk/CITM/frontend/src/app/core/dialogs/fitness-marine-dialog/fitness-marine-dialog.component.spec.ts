import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FitnessMarineDialogComponent } from './fitness-marine-dialog.component';

describe('FitnessMarineDialogComponent', () => {
  let component: FitnessMarineDialogComponent;
  let fixture: ComponentFixture<FitnessMarineDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FitnessMarineDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FitnessMarineDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
