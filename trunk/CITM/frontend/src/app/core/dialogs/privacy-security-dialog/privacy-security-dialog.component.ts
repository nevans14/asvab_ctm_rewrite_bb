import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from "@angular/material";
import { ContentManagementService } from 'app/services/content-management.service';
import { UtilityService } from 'app/services/utility.service';

@Component({
  selector: 'app-privacy-security-dialog',
  templateUrl: './privacy-security-dialog.component.html',
  styleUrls: ['./privacy-security-dialog.component.scss']
})
export class PrivacySecurityDialogComponent implements OnInit {
  pageHtml: any = [];
  page: any;
  showSpinner = true;

  constructor(
    private _contentManagementService: ContentManagementService,
    private dialogRef: MatDialogRef<PrivacySecurityDialogComponent>,
    private _utility: UtilityService,
  ) { }

  ngOnInit() {
    this.getPageByName('dialog-privacy-security');
  }

  /***
   * Get content from DB
   */
  getPageByName(pageName) {
    // Remove leading slash before lookup
    if (pageName.indexOf('/') === 0) {
      pageName = pageName.substring(1);
    }
    console.log('pageName:', pageName);
    this._contentManagementService.getPageByPageName(pageName).subscribe(
      data => {
        this.page = data;
        this._utility.parseCms(this.page.pageHtml, this.pageHtml);
        this.showSpinner = false;
      });
  }

  onNoClick() {
    this.dialogRef.close();
  }
}
