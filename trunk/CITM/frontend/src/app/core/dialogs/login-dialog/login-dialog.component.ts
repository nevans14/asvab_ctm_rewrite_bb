import { Component, ChangeDetectorRef, OnInit, AfterViewInit, HostListener, OnDestroy, ViewChild } from '@angular/core';
import { MatDialog, MatDialogRef} from '@angular/material';
import { Router } from '@angular/router';
import { LoginService } from 'app/services/login.service';
import { ReCaptcha2Component } from 'ngx-captcha';

@Component({
  selector: 'app-login-dialog',
  templateUrl: './login-dialog.component.html',
  styleUrls: ['./login-dialog.component.scss']
})

export class LoginDialogComponent implements OnInit, AfterViewInit, OnDestroy {
  emailAddr = '';
  password = '';
  regEmail = '';
  password1 = '';
  password2 = '';
  errorMessage = '';
  errorMessage2 = '';
  regErrorMessage = '';
  isRegistered = false;
  showSpinner = false;
  recaptchaKey = '6LcGnigTAAAAAJUUd9llQh4I5r_-j6mlJoAryekN';
  responseOne = '';
  @ViewChild('captchaElem') captchaElem: ReCaptcha2Component;

  constructor(
    public _dialogRef: MatDialogRef<LoginDialogComponent>,
    private _dialog: MatDialog,
    private _router: Router,
    private _loginService: LoginService,
    private _ref: ChangeDetectorRef
  ) {
  }

  ngOnInit() {
  }

  ngAfterViewInit() {
    // (document.getElementsByName('regEmail')[0] as HTMLInputElement).value = '';
  }
  ngAfterViewChecked(){
  this._ref.detectChanges();
  }

  onForgotPassword() {
    this._dialogRef.close();
    this._dialog.open( ResetPasswordDialog, {hasBackdrop: true, disableClose: true, autoFocus: true} );
  }

  checkLoginValid() {
    // Check to see if  button should be enabled
    if (this.emailAddr && this.password) {
      return true;
    }
    // Do not allow the button to be enabled
    return false;
  }

  checkRegisterValid() {
    // Check to see if  button should be enabled
    if (this.password1 === this.password2 && this.password1.length > 7) {
      this.errorMessage2 = '';
    } else if (this.password1 && this.password1.length < 8 && this.password2) {
      this.errorMessage2 = 'Password too short!';
    } else if (this.password2) {
      this.errorMessage2 = 'Passwords do not match!';
    }
    
    if (this.password1 === this.password2 && this.password1.length > 7 && this.responseOne != '') {
      this.errorMessage2 = '';
      return true;
    }
    // Do not allow the button to be enabled
    return false;
  }

  onClickLogin() {
    this.showSpinner = true;
    return this._loginService.emailLogin(this.emailAddr, this.password)
      .then((returnMessage) => {
        if (returnMessage) {
          this.password = '';
          this.errorMessage = returnMessage;
          this.showSpinner = false;
          return;
        } else {
          this.showSpinner = false;
          this._dialogRef.close();
        }
      }
    );
  }

  onClickRegister() {
    this.showSpinner = true;
    return this._loginService.emailRegistration(this.regEmail, this.password1, this.responseOne)
      .then((obj) => {
        if (obj['status'] && obj['status'].toLowerCase() !== 'success') {
          this.regErrorMessage = obj['status'];
          this.captchaElem.reloadCaptcha();
          this.showSpinner = false;
          return;
        }
        this.regErrorMessage = '';
        this.errorMessage2 = '';
        this.isRegistered = true;
        this._dialogRef.close();
        this.showSpinner = false;
        return;
      });
  }

  onNoClick(): void {
    this._dialogRef.close();
  }

  checkToProcessEnter() {
    const login = this.checkLoginValid(); 
    const register = this.checkRegisterValid();
    if (login && register) {
      return;
    } else if (login) {
      this.onClickLogin();
    } else if (register) {
      this.onClickRegister();
    }
  }
    /**
    *
    * Key bindings for user answers and answer submit.
    */
   @HostListener( 'document:keypress', ['$event'] )
   handleEvent( event: any ) {
    this.errorMessage = '';
    this.errorMessage2 = '';
    this.regErrorMessage = '';
    if (event instanceof KeyboardEvent) {
      if ( event.key === 'Enter' ) {
        this.checkToProcessEnter();
      }
    }
    // console.log('evt:', event);
  }

  ngOnDestroy() {
    const currentPath = this._router.url;
    if (currentPath.indexOf('home') > -1) {
      this._router.navigate( [currentPath] );
    }

  }

}

@Component({
  selector: 'reset-password-dialog',
  templateUrl: './reset-password-dialog.html',
  styleUrls:  ['./login-dialog.component.scss']
})
export class ResetPasswordDialog {
  email = '';
  errorMessage = '';

  constructor(
    public _dialogRef: MatDialogRef<ResetPasswordDialog>,
    private _loginService: LoginService
  ) {}

  checkValid() {
    if (this.email) {
      return false;
    }
    return true;
  }

  requestResetPassword() {
    return this._loginService.passwordResetRequest(this.email)
    .then ((error) => {
      if (error) {
        this.errorMessage = error;
        return;
      } else {
        this._dialogRef.close();
        return;
      }

    });
  }

  onNoClick(): void {
    this._dialogRef.close();
  }

}
