import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FitnessNavyDialogComponent } from './fitness-navy-dialog.component';

describe('FitnessNavyDialogComponent', () => {
  let component: FitnessNavyDialogComponent;
  let fixture: ComponentFixture<FitnessNavyDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FitnessNavyDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FitnessNavyDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
