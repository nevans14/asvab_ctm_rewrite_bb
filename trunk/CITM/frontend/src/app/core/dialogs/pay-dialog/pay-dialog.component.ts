import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Router } from '@angular/router';

@Component({
    selector: 'app-pay-dialog',
    templateUrl: './pay-dialog.component.html',
    styleUrls: ['./pay-dialog.component.scss']
})
export class PayDialogComponent implements OnInit {

    pay;
    rank;
    payChart;
    payYear;
    basicAllowanceEnlisted;
    basicAllowanceOfficer;
    serviceType;
    serviceList = [{
        id: 'A',
        title: 'Army'
    }, {
            id: 'F',
            title: 'Air Force'
        }, {
            id: 'M',
            title: 'Marine'
        }, {
            id: 'N',
            title: 'Navy'
        }, {
            id: 'C',
            title: 'Coast Guard'
        }];
    rankSelection;

    constructor(public dialogRef: MatDialogRef<PayDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data,
        private _router: Router) { }

    ngOnInit() {
        this.pay = this.data[0];
        this.rank = this.data[1];
        this.payChart = this.data[2];
        this.payYear = this.data.payYear;
        this.basicAllowanceEnlisted = this.data.basicAllowanceEnlisted;
        this.basicAllowanceOfficer = this.data.basicAllowanceOfficer;
    }

    filterPayFunction(pay): any[] {
        let payFiltered = pay.filter(i => i.rate == this.rankSelection.rate && i.rank == this.rankSelection.rank && i.service == this.serviceType);
        let sortFiltered = payFiltered.sort((a, b) => (a.value > b.value) ? 1 : -1);
        return sortFiltered;
    }

    filterPayChartFunction(payChart): any[] {
        if (this.rankSelection) {
            let payChartFiltered = payChart.filter(i => i.payGrade == this.rankSelection.rate);
            return payChartFiltered;
        }
    }

    navigateTo(route) {
        this.dialogRef.close();
        this._router.navigate([route]);
    }

    close() {
        this.dialogRef.close();
    }
}
