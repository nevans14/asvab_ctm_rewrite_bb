import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { UserService } from 'app/services/user.service';

@Component({
  selector: 'app-undo-dialog',
  templateUrl: './undo-dialog.component.html',
  styleUrls: ['./undo-dialog.component.scss']
})
export class UndoDialogComponent implements OnInit {
  hiddenSearchList: any;
  searchList: any;
  error: any;
  myStyle = {'cursor': 'pointer'};

  constructor(
    public _dialogRef: MatDialogRef<UndoDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data,
    private _userService: UserService
  ) { }

  ngOnInit() {
    this.searchList = this.data;
    this.hiddenSearchList = this.searchList.filter(job => job.show === false);
  }

  onUndo(item) {
    this.myStyle = {'cursor': 'wait'};

    // set list to update database
    for (let i = 0; i < this._userService.hiddenListObject.length; i++) {
      if (this._userService.hiddenListObject[i] === item.mcId) {
        this._userService.hiddenListObject.splice(i, 1);
      }
    }

    const obj = {  };
    obj['jsonString'] = JSON.stringify(this._userService.hiddenListObject);
    return this._userService.setHidden(obj)
      .then((retObj) => {
        item.show = true;
        this.hiddenSearchList = this.searchList.filter(job => job.show === false);
        this.myStyle =  {'cursor': 'pointer'};
        return;
      })
      .catch((error) => {
        this.myStyle =  {'cursor': 'pointer'};
        this.error = error;
        return;
      });
  }

  onClose() {
    // this._favoriteService.resetSearchList();
    this._dialogRef.close();
  }

}
