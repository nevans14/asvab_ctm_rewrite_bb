import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ServiceContactUsDialogComponent } from './service-contact-us-dialog.component';

describe('ServiceContactUsDialogComponent', () => {
  let component: ServiceContactUsDialogComponent;
  let fixture: ComponentFixture<ServiceContactUsDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ServiceContactUsDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ServiceContactUsDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
