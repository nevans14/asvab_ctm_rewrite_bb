import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NeedToLoginDialogComponent } from './need-to-login-dialog.component';

describe('NeedToLoginDialogComponent', () => {
  let component: NeedToLoginDialogComponent;
  let fixture: ComponentFixture<NeedToLoginDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NeedToLoginDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NeedToLoginDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
