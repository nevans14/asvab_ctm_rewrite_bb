import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'rank'
})
export class RankPipe implements PipeTransform {

  transform(values: any[], serviceType: string): any {
    if (!serviceType) {
      return values;
    }

    return values.filter(v => 
      v.service.toLowerCase() === serviceType.toLowerCase()
    )
  }
}
