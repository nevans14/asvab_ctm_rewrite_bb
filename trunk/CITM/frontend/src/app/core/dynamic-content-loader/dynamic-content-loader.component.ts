import { Compiler, Component, Input, NgModule, NgModuleFactory, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ConfigService } from 'app/services/config.service';
import { FavoritesService } from 'app/services/favorites.service';
import { RankService } from 'app/services/rank.service';
import { PayService } from 'app/services/pay.service';
import { UserService } from 'app/services/user.service';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MaterialModule } from 'app/material.module';
import { LoginDialogComponent } from 'app/core/dialogs/login-dialog/login-dialog.component';
import { MilitaryInfoDialogComponent } from 'app/core/dialogs/military-info-dialog/military-info-dialog.component'
import { PopupPlayerComponent } from 'app/core/dialogs/popup-player/popup-player.component';
import { NeedToLoginDialogComponent } from 'app/core/dialogs/need-to-login-dialog/need-to-login-dialog.component';
import { Router, ActivatedRoute } from '@angular/router';
import { FitnessAirForceDialogComponent } from 'app/core/dialogs/fitness-air-force-dialog/fitness-air-force-dialog.component';
import { FitnessArmyDialogComponent } from 'app/core/dialogs/fitness-army-dialog/fitness-army-dialog.component';
import { FitnessCoastGuardDialogComponent } from 'app/core/dialogs/fitness-coast-guard-dialog/fitness-coast-guard-dialog.component';
import { FitnessMarineDialogComponent } from 'app/core/dialogs/fitness-marine-dialog/fitness-marine-dialog.component';
import { FitnessNavyDialogComponent } from 'app/core/dialogs/fitness-navy-dialog/fitness-navy-dialog.component';
import { MessageDialogComponent } from 'app/core/dialogs/message-dialog/message-dialog.component';
import { RankDialogComponent } from 'app/core/dialogs/rank-dialog/rank-dialog.component';
import { PayDialogComponent } from 'app/core/dialogs/pay-dialog/pay-dialog.component';
import { COMPILER_OPTIONS, CompilerFactory } from '@angular/core';
import { JitCompilerFactory } from '@angular/platform-browser-dynamic';
import {GoogleAnalyticsService} from 'app/services/google-analytics.service';

export function createCompiler(compilerFactory: CompilerFactory) {

    return compilerFactory.createCompiler();

}



@Component({
  selector: 'app-dynamic-content-loader',
  template: '<ng-container *ngComponentOutlet="dynamicComponent;ngModuleFactory: dynamicModule;"></ng-container>',
  styleUrls: ['./dynamic-content-loader.component.scss']
})
export class DynamicContentLoaderComponent implements OnInit {

  dynamicComponent: any;
  dynamicModule: NgModuleFactory<any>;
  serviceObjects: any;

  @Input()
  text: string;

  constructor(
    private compiler: Compiler,
    ) {
  }

  ngOnInit() {
    if (this.text === undefined || this.text === null) {
      this.text = '<div></div>';
    }
    this.dynamicComponent = this.createNewComponent(this.text);
    this.dynamicModule = this.compiler.compileModuleSync(
      this.createComponentModule(this.dynamicComponent));
  }

  protected createComponentModule(componentType: any) {
    @NgModule({
      imports: [MaterialModule,
        CommonModule,
        NgbModule,
        FormsModule,
        ReactiveFormsModule],
      declarations: [
        componentType
      ],
      entryComponents: [
        componentType
      ],
      providers: [
        { provide: COMPILER_OPTIONS, useValue: { }, multi: true },
        { provide: CompilerFactory, useClass: JitCompilerFactory, deps: [COMPILER_OPTIONS] },
        { provide: Compiler, useFactory: createCompiler, deps: [CompilerFactory] }
        // ],
        // schemas: [ CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA
      ]
    })
    class RuntimeComponentModule {
    }
    // a module for just this Type
    return RuntimeComponentModule;
  }

  protected createNewComponent(text: string) {
    const template = text;
    // console.debug('template:', template);

    @Component({
      selector: 'app-dynamic-component',
      template: template,
      //styleUrls: ['./dynamic-content-loader.component.scss']
    })
    class DynamicComponent implements OnInit {
      text: any;
      resource = this._config.getImageUrl();
      resources = this.resource + 'static/';
      documents = this.resource + 'pdf/';
      citmimages = this.resource + 'citm-images/';
      selectedTabId = 'first';
      panelOpenState = false;
      myStyle = { 'cursor': 'pointer' };
      disabled = false;
      mapView: Boolean = true;
      aText: String = 'List View';
      step: String;
      payData: any;
      rankData: any;
      payInfo: any;
      serviceObjects: any;
      path: String;
      option: String = null;

      constructor(
        private _config: ConfigService,
        private _userService: UserService,
        private _favoriteService: FavoritesService,
        private _rankService: RankService,
        private _payService: PayService,
        private _router: Router,
        private _dialog: MatDialog,
        private _googleAnalyticsService: GoogleAnalyticsService,
        private _activatedRoute: ActivatedRoute,
      ) {
        this._activatedRoute.params.subscribe(routeParams => {
          this.option = routeParams.option;
          });
      }

      ngOnInit() {
        this.text = text;
        this.path = this._router.url;

        if (this.path.indexOf('benefits') > -1 || this.path.indexOf('becoming-an-officer') > -1) {
          this.openPanel();
        }

        if (this.path.indexOf('enlisted-vs-officer') > -1) {
          this._rankService.getRank().subscribe(
            data => {
              this.rankData = data;
            });
        }

        if (this.path.indexOf('pay') > -1) {
          var rank, pay, payChart, payInfo;
          this._payService.getPay().subscribe(
            data => {
              pay = data;
              this.payData = pay && rank && payChart ? [pay, rank, payChart] : null;
            });
          this._payService.getRank().subscribe(
            data => {
              rank = data;
              this.payData = pay && rank && payChart ? [pay, rank, payChart] : null;
            });
          this._payService.getPayChart().subscribe(
            data => {
              payChart = data;
              this.payData = pay && rank && payChart ? [pay, rank, payChart] : null;
            });
          this._payService.getPayInfo().subscribe(
            data => {
              this.payInfo = data;
            });
        }

        if (this.path.indexOf('military') > -1) {
          this.getFavoritedServices();
          this.serviceObjects = [
            { svcId: 'A', name: 'ARMY', favoriteId: null, imageName: 'army' },
            { svcId: 'M', name: 'MARINE CORPS', favoriteId: null, imageName: 'marines' },
            { svcId: 'N', name: 'NAVY', favoriteId: null, imageName: 'navy' },
            { svcId: 'F', name: 'AIR FORCE', favoriteId: null, imageName: 'airforce' },
            { svcId: 'S', name: 'SPACE FORCE', favoriteId: null, imageName: 'spaceforce' },
            { svcId: 'C', name: 'COAST GUARD', favoriteId: null, imageName: 'coastguard' },
          ];
        }
      }

      getFavoritedServices() {
        if (this.isLoggedIn()) {
          this._userService.getFavoriteServices().subscribe(data => {
            this._favoriteService.setFavoritedServices(this.serviceObjects, data);
          });
        }
      }

      openPanel() {
        if (window.sessionStorage.getItem("optionsShowPanelID") &&
          window.sessionStorage.getItem("optionsShowPanelID") !== "undefined") {
          this.step = (this.path.indexOf('benefits') > -1) ? 'panel3' : 'panel2';
          window.sessionStorage.setItem('optionsShowPanelID', "undefined");
        } else {
          if (this.option && this.option.toLowerCase() === 'rotc') {
            this.step = 'panel2';
          } else if (this.option && this.option.toLowerCase() === 'service-academies') {
            this.step = 'panel1';
          } else {
            this.step = null;
          }
          window.sessionStorage.setItem('optionsShowPanelID', "undefined");
        }
      }

      isLoggedIn() {
        return this._config.isLoggedIn();
      }

      openLoginDialog() {
        this._dialog.open(LoginDialogComponent, { hasBackdrop: true, disableClose: true, autoFocus: true });
      }

      redirect(path) {
        this._router.navigate([path]);
      }

      loginRedirect(path) {
        if (this.isLoggedIn()) {
          this.redirect(path);
        } else {
          this._dialog.open( NeedToLoginDialogComponent, { hasBackdrop: true, disableClose: true, autoFocus: true } );
        }
      }

      showFitnessArmyDialogComponent() {
        this._dialog.open(FitnessArmyDialogComponent, {
          panelClass: 'custom-dialog'
        });
      }
      showFitnessMarineDialogComponent() {
        this._dialog.open(FitnessMarineDialogComponent, {
          panelClass: 'custom-dialog'
        });
      }
      showFitnessNavyDialogComponent() {
        this._dialog.open(FitnessNavyDialogComponent, {
          panelClass: 'custom-dialog'
        });
      }
      showFitnessAirForceDialogComponent() {
        this._dialog.open(FitnessAirForceDialogComponent, {
          panelClass: 'custom-dialog'
        });
      }
      showFitnessCoastGuardDialogComponent() {
        this._dialog.open(FitnessCoastGuardDialogComponent, {
          panelClass: 'custom-dialog'
        });
      }

      /**
       * Open video player dialog.
       */
      openPlayer(videoId) {
        this._dialog.open(PopupPlayerComponent, {
          width: '700px',
          data: { videoId: videoId }
        });
      }

      onClickBootCamp() {
        this.mapView = !this.mapView;
        if (this.mapView) {
          this.aText = 'List View';
        } else {
          this.aText = 'Map View';
        }
      }

      getDisplayMapBootCamp() {
        return this.mapView;
      }

      // Dialog opens with special parameters
      openRankDialog(data) {
        this._dialog.open(RankDialogComponent, {
          width: '700px',
          data: data,
          panelClass: 'custom-dialog'
        });
      }

      // Dialog opens with special parameters
      openPayDialog(data) {
        this._dialog.open(PayDialogComponent, {
          width: '700px',
          data: data,
          panelClass: 'custom-dialog'
        });
      }

      getFavoriteImage(svcObject, favoritId) {
        return favoritId ? '/assets/images/' + svcObject.imageName + '_fav_yes.png' :
          '/assets/images/' + svcObject.imageName + '_fav.png';
      }

      onFavoriteClick(svcObject) {
		console.log("svcObject: "+svcObject);
        if (this.disabled) {
          return;
        }
        this.disabled = true;
        this.myStyle = { 'cursor': 'wait' };
        this._favoriteService.onFavoriteServiceClick(svcObject.svcId, svcObject.favoriteId)
          .then((obj) => {
            this._favoriteService.setFavoritedServices(this.serviceObjects, obj);
            this.myStyle = { 'cursor': 'pointer' };
            this.disabled = false;
          });
      }

      openServiceScoreInfo() {
        this._dialog.open(MessageDialogComponent, {
          data: {
            title: 'Service Composite Categories',
            message: '<ul><li>Each military job has unique composite score requirements</li>' +
              '<li>Requirements are subject to change without notice</li>' +
              '<li>For the most up-to-date requirements, <a href="/options-contact-recruiter">contact a recruiter</a></li></ul>'
          }
        });
      }

    trackClick(param) {
		console.log("param: "+param);
        this._googleAnalyticsService.trackClick(param);
    }

    gaSocialShare(socialPlug, value) {
        this._googleAnalyticsService.trackSocialShare(socialPlug, value);
    }

      openMilitaryInfoDialog(service) {
        this._dialog.open(MilitaryInfoDialogComponent, {
          data: service,
          panelClass: 'custom-dialog'
        });
      }
    }
    return DynamicComponent;
  }
}
