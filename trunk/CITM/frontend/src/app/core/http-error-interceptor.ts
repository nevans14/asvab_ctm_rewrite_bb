import { Injectable } from '@angular/core';
import { SessionService } from 'app/services/session.service';
import { HttpClient } from '@angular/common/http';
import {
  HttpEvent,
  HttpInterceptor,
  HttpHandler,
  HttpRequest,
  HttpErrorResponse
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class HttpErrorInterceptor implements HttpInterceptor {
  lastTimeStamp = 0;
  lastErrorStatus;

  constructor(
    private _sessionService: SessionService,
    private _http: HttpClient,
	private _router: Router
  ) { }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(request)
      .pipe(
        catchError((error: HttpErrorResponse) => {
          if (error.status !== 200) {
            console.debug(error.status + ' : ' + error.statusText + ' : ' + error.message);
            const currentTimeStamp = new Date().getTime();
            console.debug(currentTimeStamp);
            if (error.status === this.lastErrorStatus && ((currentTimeStamp - this.lastTimeStamp) < 2000)) {
              // Same message too close but save current time so to ignore more as they come in close together
              this.lastTimeStamp = currentTimeStamp;
            // return;
            }
            this.lastErrorStatus = error.status;
            this.lastTimeStamp = new Date().getTime();
            let errorMessage = '';
            if (error.error instanceof ErrorEvent) {
              // client-side error
              errorMessage = `Error: ${error.error.message}`;
            } else {
              // server-side error
              errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
            }
            if (error.status == 403) {
              this._sessionService.sessionTimeOut();
              return;
            }

            console.debug("Log Error: " + JSON.stringify(error) + "         Error Message: " + errorMessage);
            const url = window.location.href;
            const JavascriptError = {
              'errorUrl': url,
              'errorMessage': errorMessage,
              'errorStackTrace': ''
            };

			const currentPath = this._router.url;
			
			if(currentPath === '/options-contact-recruiter' || currentPath === '/contact-us') {
				console.log(currentPath);
				return throwError(error);
			}
			
            this._http.post('/javascript-error-logging/log-error', JavascriptError);
            return throwError(errorMessage);
          } else {
            return throwError('');
          }
        })
      )
  }

  createErrorMsg(error) {
    let msg = error['message'] && error['message'] !== 'null' ? error['message'] : error['error'];
    msg = msg ? msg : error['statusText'];
    msg = error['status'] == 404 ? 'This site can\'t be reached' : msg;
    msg = msg ? msg : 'Service is not available currently.';
    msg = msg.replace(new RegExp('^[0-9]{3} \w*'), '');
    return msg;
  }

}