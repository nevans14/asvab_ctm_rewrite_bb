import { Component, OnInit, OnDestroy, AfterViewInit } from '@angular/core';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { MatDialog } from '@angular/material';
import { ConfigService } from '../services/config.service';
import { UserService } from 'app/services/user.service';
import { FavoritesService } from 'app/services/favorites.service';
import { CareerDetailService } from 'app/services/career-detail.service';
import { GoogleAnalyticsService } from '../services/google-analytics.service';
import { ServiceContactUsDialogComponent } from 'app/core/dialogs/service-contact-us-dialog/service-contact-us-dialog.component';
import { DomSanitizer} from '@angular/platform-browser';
import { NeedToLoginDialogComponent } from 'app/core/dialogs/need-to-login-dialog/need-to-login-dialog.component';
import { CareerSearchService  } from 'app/services/career-search.service';
import { SearchListService  } from 'app/services/search-list.service';
import { Title, Meta } from '@angular/platform-browser';

@Component({
    selector: 'app-career-detail-service',
    templateUrl: './career-detail-service.component.html',
    styleUrls: ['./career-detail-service.component.scss']
})
export class CareerDetailServiceComponent implements OnInit, OnDestroy, AfterViewInit {
    navigationSubscription;
    myStyle =  {'cursor': 'pointer'};
    disabled = false;
    service: any;
    favorited = false;
    mcId: any;
    mocId: any;
    serviceName: any;
    videoId: any;
    videoUrl: any;
    serviceDetail: any;
    milCareer: any;
    favoriteService: any;
    airForceCommunityMap: any;
    airForceCommunityTitle: any;
    airForceContextMap: any;
    airForceActivityMap: any;
    airForceActivityTitle: any;
    armyFiltered: any;
    navyImageUrl: String;
    mediaCollection: any[] = [];
    serviceContacts: any;
    slideConfig = {
        'slidesToShow': 1,
        'slidesToScroll': 1,
        'dots': false,
        'infinite': false,
        'autoplay': false,
        prevArrow: '<button type="button" class="slick-prev" role="button">' +
        '<img src="assets/images/arrow-left-red.png"></button>',
        nextArrow: '<button type="button" class="slick-next" role="button">' +
        '<img src="assets/images/arrow-right-red.png"></button>'
    };
    requiredHSCourses: String = '';
    searchList: any;
    serviceOffering: any;

    constructor(
        private _careerDetailService: CareerDetailService,
        private _config: ConfigService,
        private _userService: UserService,
        private _favoritesService: FavoritesService,
        private _route: ActivatedRoute,
        private _dialog: MatDialog,
        private _router: Router,
        private _meta: Meta,
        private _titleTag: Title,
        private _googleAnalyticsService: GoogleAnalyticsService,
        private _sanitizer: DomSanitizer,
        private _careerSearchService: CareerSearchService,
        private _searchListService: SearchListService,
    ) {
        // Subscribe to router event Navigation End so can refresh data after log in
        this.navigationSubscription = this._router.events.subscribe(event => {
            if (event instanceof NavigationEnd && (event.urlAfterRedirects.indexOf('/career-detail-service') > -1
            || event.urlAfterRedirects.indexOf('/service-career-detail') > -1)) {
                this._route.params.subscribe(res => {
                    this.service = res.svc;
                    this.mcId = res.mcId;
                    this.mocId = res.mocId;
                });
                this.loadData();
            }
        });

    }

    ngOnInit() {
        this._careerSearchService.getServiceContacts().subscribe(
            data => {
                this.serviceContacts = data;
        });
    }

    removeService(val) {

    }

    isLoggedIn() {
        return this._config.isLoggedIn();
    }

    ngAfterViewInit() {
        window.scrollTo( 0, 0 );
     }
    alertNotLoggedIn () {
        this._dialog.open( NeedToLoginDialogComponent, { hasBackdrop: true, disableClose: true, autoFocus: true } );
    }

    translateSlug() {
        if (this.searchList) {
            const career: any = this.searchList.find(i => i.slug === this.mcId);
            this.mcId = career && career.mcId ? career.mcId : '';
        }
    }

    translateMcId() {
        if (this.searchList) {
            const career: any = this.searchList.find(i => i.mcId === this.mcId);
            this.mcId = career && career.slug ? career.slug : '';
        }
    }

    loadDetailData() {
        this._careerDetailService.getMilitaryCareer(this.mcId).subscribe(data => {
            this.milCareer = data;
        });

       this.getFavoritedService();

        switch (this.service) {
            case ('F'):
                this._careerDetailService.getAirForceDetail(this.mcId, this.mocId).subscribe(data => {
                    this.serviceDetail = data;
                    this.setData();
                });
                break;
            case ('S'):
                this._careerDetailService.getSpaceForceDetail(this.mcId, this.mocId).subscribe(data => {
                    this.serviceDetail = data;
                    this.setData();
                });
                break;
            case ('M'):
                this._careerDetailService.getMarineDetail(this.mcId, this.mocId).subscribe(data => {
                    this.serviceDetail = data;
                    this.setData();
                });
                break;
            case ('N'):
                this._careerDetailService.getNavyDetail(this.mcId, this.mocId).subscribe(data => {
                    this.serviceDetail = data;
                    this.setData();
                });
                break;
            case ('A'):
                this._careerDetailService.getArmyDetail(this.mcId, this.mocId).subscribe(data => {
                    this.serviceDetail = data;
                    this.setData();
                });
                break;
            case ('C'):
                this._careerDetailService.getCoastGuardDetail(this.mcId, this.mocId).subscribe(data => {
                    this.serviceDetail = data;
                    this.setData();
                });
                break;
        }
    }
    
    loadData() {
        this.setServiceName();

        this.searchList = this._searchListService.getSearchList();
        if (!this.searchList) {
            const searchListInterval = setInterval(() => {
                if (this._searchListService.searchListObject) {
                    clearInterval(searchListInterval);
                    this.searchList = this._searchListService.searchListObject;
                    
                    if (!isNaN(this.mcId)) {
                        this._careerDetailService.getServiceOffering(this.mcId).subscribe(data => {
                            this.serviceOffering = !data ? [] : data;
                            this.mocId = this.serviceOffering.find(i => i.mocId === this.mocId)
                                .mocTitle.trim().split(/\/|\s|[,()]/).join('-').toLowerCase();

                            this.translateMcId();
                            this._router.navigate(['service-career-detail/' + this.serviceName.trim().split(' ').join('-').toLowerCase() + '/' + this.mcId + '/' + this.mocId]);
                        });
                    }

                    if (isNaN(this.mcId)) {
                        this.translateSlug();

                        this._careerDetailService.getServiceOffering(this.mcId).subscribe(data => {
                            this.serviceOffering = !data ? [] : data;
                            this.mocId = this.serviceOffering.find(i => 
                                i.mocTitle.trim().split(/\/|\s|[,()]/).join('-').toLowerCase() === this.mocId.toLowerCase()
                            ).mocId;
                            this.loadDetailData();
                        });
                    }
                }
            }, 200);
        } else {
            if (!isNaN(this.mcId)) {
                this._careerDetailService.getServiceOffering(this.mcId).subscribe(data => {
                    this.serviceOffering = !data ? [] : data;
                    this.mocId = this.serviceOffering.find(i => i.mocId === this.mocId)
                        .mocTitle.trim().split(/\/|\s|[,()]/).join('-').toLowerCase();
    
                    this.translateMcId();
                    this._router.navigate(['service-career-detail/' + this.serviceName.trim().split(' ').join('-').toLowerCase() + '/' + this.mcId + '/' + this.mocId]);
                });
            }

            if (isNaN(this.mcId)) {
                this.translateSlug();

                this._careerDetailService.getServiceOffering(this.mcId).subscribe(data => {
                    this.serviceOffering = !data ? [] : data;
                    this.mocId = this.serviceOffering.find(i => 
                        i.mocTitle.trim().split(/\/|\s|[,()]/).join('-').toLowerCase() === this.mocId.toLowerCase()
                    ).mocId;
                    this.loadDetailData();
                });
            }
        }

        // Original load with all Ids as numbers
        // this.loadDetailData();
    }

    displayInfoIcon () {
        if (this.serviceDetail) {
            switch (this.service) {
                case ('F'):
                    if (!this.serviceDetail.specialty && !this.serviceDetail.task &&
                    (!this.serviceDetail.requirement ||
                    !this.serviceDetail.requirement.educationLevel ||
                    this.serviceDetail.requirement.educationLevel === 'NONE' ||
                    this.serviceDetail.requirement.educationLevel === 'NA') && !this.serviceDetail.basicTraining && 
                    !this.serviceDetail.technicalTraining && 
                    !this.serviceDetail.techSchoolLocation && !this.requiredHSCourses) {
                        return true;
                    }
                break;
                case ('S'):
                    if (!this.serviceDetail.specialty &&  !this.serviceDetail.task &&
                    !this.serviceDetail.minEducation && !this.serviceDetail.basicTraining && 
                    !this.serviceDetail.technicalTraining && 
                    !this.serviceDetail.techSchoolLocation && !this.serviceDetail.desiredHSCourse) {
                        return true;
                    }
                break;
                case ('A'):
                    if (!this.serviceDetail.overview) {
                        return true;
                    }
                break;
                case ('C'):
                    if (!this.serviceDetail.description && ! this.serviceDetail.duties &&
                    !this.serviceDetail.training && !this.serviceDetail.qualifications &&
                    ! this.serviceDetail.specialNotes) {
                        return true;
                    }
                break;
                case ('M'):
                    if (!this.serviceDetail.description) {
                        return true;
                    }
                break;
                case ('N'):
                    if (!this.serviceDetail.rating && !this.serviceDetail.description && 
                    !this.serviceDetail.physicalRequirement && !this.serviceDetail.minEducation &&
                    !this.serviceDetail.basicTraining && !this.serviceDetail.technicalTrainingDuration &&
                    !this.serviceDetail.techTrainingLocation && !this.serviceDetail.desiredHighSchoolCourses &&
                    !this.serviceDetail.citizenshipRequirement) {
                        return true;
                    }
                break;
            }
        } else {
            return true;
        }
        return false;
    }

    getFavoritedService() {
        if (this.isLoggedIn()) {
            this._userService.getFavoriteServices().subscribe(data => {
                this.favoriteService = data;
                this. serviceFavorited();
            });

        }

    }

    ga(param) {
        this._googleAnalyticsService.trackClick(param);
    }

    gaSocialShare(socialPlug, value) {
        this._googleAnalyticsService.trackSocialShare(socialPlug, value);
    }

    setData() {
        if (this.serviceDetail) {
            this._titleTag.setTitle('Career Details Service | Careers in the Military');
            this._meta.updateTag({name: 'description', content: this.serviceName + ': ' + this.serviceDetail.title});

            this._meta.updateTag({ property: 'og:title', content: 'Career Details Service | Careers in the Military' });
            this._meta.updateTag({property: 'og:description', content: this.serviceName + ': ' + this.serviceDetail.title});
            this._meta.updateTag({ name: 'twitter:title', content: 'Career Details Service | Careers in the Military' });
            this._meta.updateTag({name: 'twitter:description', content: this.serviceName + ': ' + this.serviceDetail.title});

            let videoAttribute = '';
            switch (this.service) {
                case 'N':
                case 'A':
                    videoAttribute = 'youTubeLink';
                    break;
                case 'C':
                    videoAttribute = 'youTubeUrl';
                    break;
                case 'F':
                    videoAttribute = 'video';
                    break;
            }

            // Navy, Airforce, Coast Guard, Army video collection
            Object.keys(this.serviceDetail).forEach((key, index) => {
                if (key.includes(videoAttribute)) {
                    var regExp = /^.*(youtu\.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/;
                    var match = this.serviceDetail[key] ? this.serviceDetail[key].match(regExp) : undefined;
                    if ( match && match[2].length == 11 ) {
                        this.videoId = match[2];
                        this.videoUrl = this._sanitizer.bypassSecurityTrustResourceUrl( 'https://www.youtube.com/embed/' + this.videoId );
                        this.mediaCollection.push({type: 'video', content: this.videoUrl});
                    }
                }
            })

            if (this.mcId === '0008.00' && this.mocId === '1C1X1' && this.service === 'F') {
                this.videoUrl = this._sanitizer.bypassSecurityTrustResourceUrl('https://www.youtube.com/embed/nWAKxfq6PLk');
                this.mediaCollection.push({type: 'video', content: this.videoUrl});
            }

            this.airForceCommunityMap = this.serviceDetail.airForceCommunityMap;
            this.airForceContextMap = this.serviceDetail.airForceContextMap;
            this.airForceActivityMap = this.serviceDetail.airForceActivityMap;
            if (this.serviceDetail.airForceCommunityMap && this.serviceDetail.airForceCommunityMap.length > 0) {
                this.airForceCommunityTitle = this.serviceDetail.airForceCommunityMap[0].communityTitle;
            }
            if (this.serviceDetail.airForceActivityMap && this.serviceDetail.airForceActivityMap.length > 0) {
                this.airForceActivityTitle = this.serviceDetail.airForceActivityMap[0].activityTitle;
            }
            const imageBaseUrl = this._config.getImageUrl();

            if (this.serviceDetail.rating) {
                this.navyImageUrl = imageBaseUrl + 'citm-images/service/' + this.serviceDetail.rating + '.jpg';
                if (this.service === 'N') {
                    this.mediaCollection.push({type: 'image', content: this._sanitizer.bypassSecurityTrustResourceUrl(this.navyImageUrl.toString())});
                }
            }

            // Air Force required courses
            if (this.service === 'F' && this.serviceDetail.requirement) {
                Object.keys(this.serviceDetail.requirement).forEach((key, index) => {
                    if (key.includes('hs') && this.serviceDetail.requirement[key] === 'Y') {
                        const courseName = key.split('hs').length === 2 ? key.split('hs')[1] : undefined;
                        if (courseName) {
                            if (!this.requiredHSCourses) {
                                this.requiredHSCourses = courseName;
                            } else {
                                this.requiredHSCourses = this.requiredHSCourses.concat(', ' + courseName)
                            }
                        }
                    }
                })
            }
        }
    }

    setServiceName() {
        switch (this.service.toLowerCase()) {
            case 'a':
            case 'army':
                this.serviceName = 'Army';
                this.service = 'A';
                break;
            case 'c':
            case 'coast-guard':
                this.serviceName = 'Coast Guard';
                this.service = 'C';
                break;
            case 'f':
            case 'air-force':
                this.serviceName = 'Air Force';
                this.service = 'F';
                break;
            case 'm':
            case 'marine-corps':
                this.serviceName = 'Marine Corps';
                this.service = 'M';
                break;
            case 'n':
            case 'navy':
                this.serviceName = 'Navy';
                this.service = 'N';
                break;
            case 's':
            case 'space-force':
                this.serviceName = 'Space Force';
                this.service = 'S';
                break;
            default:
                this.serviceName = 'N/A';
                break;
        }
    }

    serviceFavorited() {
        if (this.favoriteService) {
            for (let i = 0; i < this.favoriteService.length; i++) {
                if (this.service === this.favoriteService[i].svcId) {
                    this.favorited = true;
                }
            }
        }
        return false;
    }

    getIdOfFavoritedService() {
        if (this.favoriteService) {
            for (let i = 0; i < this.favoriteService.length; i++) {
                if (this.service === this.favoriteService[i].svcId) {
                    return this.favoriteService[i].id;
                }
            }
        }
        return null;
    }


    onFavoriteClick() {
        if (this.disabled) {
            return;
        }
        this.disabled = true;
        this.myStyle =  {'cursor': 'wait'};
        const favorited = this.favorited;
        const id = this.getIdOfFavoritedService();
        return this._favoritesService.onFavoriteServiceClick(this.service, id)
        .then ((obj) => {
            this.favorited = !favorited;
            this.favoriteService = obj;
            this.disabled = false;
            this.myStyle =  {'cursor': 'pointer'};
            return;
        });
    }

    openServiceContactUsDialog(name = null) {

        var item;
        var serviceName;
        serviceName = name ? name : this.serviceName;

        for (var i = 0; i < this.serviceContacts.length; i++) {
            if (serviceName == this.serviceContacts[i].name) {
                item = this.serviceContacts[i];
                break;
            }
        }

        this._dialog.open(ServiceContactUsDialogComponent, {
            data: item,
            panelClass: 'custom-dialog'
        });
    }

    openNewTab(url) {
        window.open(url, '_blank');
    }

    ngOnDestroy() {
        if (this.navigationSubscription) {
            this.navigationSubscription.unsubscribe();
        }
    }
}
