import { Component } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { UtilityService } from './services/utility.service';

declare var ga: Function;

@Component( {
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
} )
export class AppComponent {
    title = 'app';
    public loading = false;
    public showHeaderFooter = true;

    constructor(
        public router: Router,
        private _utility: UtilityService,
        ) {
        this.router.events.subscribe( event => {
            // console.log('inside subscribe');
            if ( event instanceof NavigationEnd ) {
                // console.log('inside NavigationEnd');
                ga( 'set', 'page', event.urlAfterRedirects );
                ga( 'send', 'pageview' );
            }
        } );

        this._utility.showHeaderFooter.subscribe(show =>
            this.showHeaderFooter = show
        )
    }
}
