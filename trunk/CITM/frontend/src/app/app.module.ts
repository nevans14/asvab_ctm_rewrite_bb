import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA, APP_INITIALIZER } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import {MaterialModule} from './material.module';

import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgMasonryGridModule } from 'ng-masonry-grid';
import { NgxSpinnerModule } from 'ngx-spinner';
import { AngularFullpageModule } from '@fullpage/angular-fullpage';
import { SlickCarouselModule } from 'ngx-slick-carousel';
import { RatingModule } from 'ngx-rating';
import { CarouselModule } from 'ngx-owl-carousel-o';
import { ConfigService } from './services/config.service';
import { ReactiveFormsModule } from '@angular/forms';
import { NgxCaptchaModule } from 'ngx-captcha';
import { CookieService } from 'ngx-cookie-service';
import { AppComponent } from './app.component';
import { ShareButtonsModule } from '@ngx-share/buttons';
import { ShareButtonsConfig } from '@ngx-share/core';
import { OptionsComponent } from './options/options.component';
import { OverviewComponent } from './options/overview/overview.component';
import { CoreModule } from './core/core.module';
import { HomeComponent } from './home/home.component';
import { HeaderComponent } from './page-partials/header.component';
import { FooterComponent } from './page-partials/footer.component';
import { CareerProfileComponent } from './career-profile/career-profile.component';
import { CareerDetailComponent } from './career-detail/career-detail.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { ParentsComponent } from './parents/parents.component';
import { FaqComponent } from './faq/faq.component';
import { SiteMapComponent } from './site-map/site-map.component';
import { GuidedExplorationComponent } from './guided-exploration/guided-exploration.component';
import { JobFamilyComponent } from './job-family/job-family.component';
import { CareerClusterComponent } from './career-cluster/career-cluster.component';
import { SpinnerComponent } from './core/spinner/spinner.component';
import { AdvancedSearchComponent } from './advanced-search/advanced-search.component';
import { ProfileComponent } from './profile/profile.component';
import { PortfolioComponent } from './profile/portfolio.component';
import { CareerDetailServiceComponent } from './career-detail-service/career-detail-service.component';
import { RelatedCareersComponent } from './page-partials/related-careers.component';
import { CareerClusterPathwayComponent } from './career-cluster-pathway/career-cluster-pathway.component';
import { CareerPathwayComponent } from './career-pathway/career-pathway.component';
import { FavoritesComponent } from './page-partials/favorites.component';
import { ContactServicesComponent } from './page-partials/contact-services.component';
import { JobCompareComponent } from './job-compare/job-compare.component';
import { CompositeScoresComponent } from './composite-scores/composite-scores.component';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { HttpInterceptorService } from './services/http-interceptor.service';
import { LoginDialogComponent, ResetPasswordDialog } from 'app/core/dialogs/login-dialog/login-dialog.component';
import { UpdateUserInfoComponent } from 'app/core/dialogs/update-user-info/update-user-info.component';
import { SideNavComponent } from 'app/options/side-nav/side-nav.component';
import { UtilityService } from './services/utility.service';
import { OrderModule } from 'ngx-order-pipe';
import { SsoLoginComponent } from './sso-login/sso-login.component';
import { PasswordResetComponent } from './password-reset/password-reset.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';

const customConfig: ShareButtonsConfig = {
  twitterAccount: 'CareersintheMil'
}

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    HomeComponent,
    OptionsComponent,
    OverviewComponent,
    SideNavComponent,
    CareerProfileComponent,
    CareerDetailComponent,
    ContactUsComponent,
    ParentsComponent,
    FaqComponent,
    SiteMapComponent,
    GuidedExplorationComponent,
    JobFamilyComponent,
    CareerClusterComponent,
    SpinnerComponent,
    AdvancedSearchComponent,
    ProfileComponent,
    PortfolioComponent,
    CareerDetailServiceComponent,
    RelatedCareersComponent,
    CareerClusterPathwayComponent,
    CareerPathwayComponent,
    FavoritesComponent,
    ContactServicesComponent,
    JobCompareComponent,
    CompositeScoresComponent,
    UpdateUserInfoComponent,
    LoginDialogComponent,
    ResetPasswordDialog,
    SsoLoginComponent,
    PasswordResetComponent,
  ],
  imports: [
    CoreModule,
    BrowserModule,
    AppRoutingModule,
    MaterialModule,
    BrowserAnimationsModule,
    HttpClientModule,
    NgMasonryGridModule,
    NgxSpinnerModule,
    AngularFullpageModule,
	  SlickCarouselModule,
    CarouselModule,
    ShareButtonsModule.withConfig(customConfig),
    FormsModule,
    ReactiveFormsModule,
    NgxCaptchaModule,
    RatingModule,
    OrderModule,
    ServiceWorkerModule.register('ngsw-worker.js', 
      { enabled: environment.production })
  ],
  providers: [
    ConfigService,
    CookieService,
    UtilityService,
    GuidedExplorationComponent,
    {
      provide: APP_INITIALIZER,
      useFactory: (configService: ConfigService) =>
      () => configService.loadConfigurationData(),
    deps: [ConfigService],
      multi: true
    },
    {
        provide: HTTP_INTERCEPTORS,
        useClass: HttpInterceptorService,
        multi: true
      }
  ],
  entryComponents: [
    UpdateUserInfoComponent,
    LoginDialogComponent,
    ResetPasswordDialog
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ],


  bootstrap: [AppComponent]
})
export class AppModule {}