import { Component, OnInit } from '@angular/core';
import { ScoreService } from '../services/score.service';
import { SearchListService } from '../services/search-list.service';
import { GoogleAnalyticsService } from '../services/google-analytics.service';
import { MatDialog } from '@angular/material';
import { PopupPlayerComponent } from '../core/dialogs/popup-player/popup-player.component'
import { MessageDialogComponent } from 'app/core/dialogs/message-dialog/message-dialog.component';
import { ServiceContactUsDialogComponent } from 'app/core/dialogs/service-contact-us-dialog/service-contact-us-dialog.component';
import { ConfigService } from 'app/services/config.service';
import { PortfolioService } from 'app/services/portfolio.service';
import { Title, Meta } from '@angular/platform-browser';

@Component({
    selector: 'app-composite-scores',
    templateUrl: './composite-scores.component.html',
    styleUrls: ['./composite-scores.component.scss']
})
export class CompositeScoresComponent implements OnInit {
    scoreSummaryList: any;
    constructor(private _scoreService: ScoreService,
        private _configService: ConfigService,
        private _googleAnalyticsService: GoogleAnalyticsService,
        private _meta: Meta,
        private _titleTag: Title,
        private _dialog: MatDialog,
        private _portfolioService: PortfolioService) {
    }
    flagData: any;
    moreInfo;
    stripLeadingZero;
    disabled;
    scoreSummary;
    scoreService;
    setScoreData;
    a_af: any;
    adm_navy: any;
    afqt: any;
    ao: any;
    ar: any;
    as: any;
    bee_navy: any;
    bookletNum: any;
    cl_army: any;
    cl_mc: any;
    co_army: any;
    e_af: any;
    el: any;
    el_army: any;
    el_mc: any;
    el_navy: any;
    eng_navy: any;
    fa_army: any;
    g_af: any;
    gm_army: any;
    gs: any;
    gt_army: any;
    gt_mc: any;
    gt_navy: any;
    hm_navy: any;
    m_af: any;
    mc: any;
    mec2_navy: any;
    mec_navy: any;
    mk: any;
    mm_army: any;
    mm_mc: any;
    nuc_navy: any;
    of_army: any;
    ops_navy: any;
    pc: any;
    sc_army: any;
    st_army: any;
    testCompletionCd: any;
    ve: any;
    wk: any;
    el_cg: any;
    mr_cg: any;
    gt_cg: any;
    cl_cg: any;
    userSystem: any;
    compositeFavoritesFlags: Array<{}> = [];
    haveTestScores: any;
    haveAllAsvabScores: any;
    cepBaseUrl = this._configService.getCepBaseUrl();
    addRemoveFavComposite(itemCode, value) {
        if (itemCode === 'el_cg') {
            itemCode = 'el_navy';
        } else if (itemCode === 'mr_cg') {
            itemCode = 'mec_navy';
        } else if (itemCode === 'gt_cg') {
            itemCode = 'gt_navy';
        } else if (itemCode === 'cl_cg') {
            itemCode = 'adm_navy';
        }

        if (value === true) {
            this.compositeFavoritesFlags[itemCode] = 1;
        } else {
            this.compositeFavoritesFlags[itemCode] = 0;
        }
        this._scoreService.setCompositeFavoritesFlags(this.compositeFavoritesFlags);
    }

    ngOnInit() {
        this._titleTag.setTitle('Composite Scores | Careers in the Military');
        this._meta.updateTag({name: 'description', content: 'Careers in the Military powered by ASVAB Career Exploration Program allows students to explore different jobs across Services in the context of their skills and interests.'});

        this._meta.updateTag({ property: 'og:title', content: 'Composite Scores | Careers in the Military' });
        this._meta.updateTag({ property: 'og:description', content: 'Careers in the Military powered by ASVAB Career Exploration Program allows students to explore different jobs across Services in the context of their skills and interests.' });
        this._meta.updateTag({ property: 'og:image', content: 'http://www.asvabprogram.com/media-center-content/thumbnails/social-share/CITM_Webshare.jpg' });
        this._meta.updateTag({ name: 'twitter:title', content: 'Composite Scores | Careers in the Military' });
        this._meta.updateTag({ name: 'twitter:description', content: 'Careers in the Military powered by ASVAB Career Exploration Program allows students to explore different jobs across Services in the context of their skills and interests.' });
        this._meta.updateTag({ name: 'twitter:image', content: 'http://www.asvabprogram.com/media-center-content/thumbnails/social-share/CITM_Webshare.jpg' });


        this._scoreService.getCompositeFavoritesFlags().subscribe(flagList => {
            this.afqt = flagList.afqt;
            this.a_af = flagList.a_af;
            this.adm_navy = flagList.adm_navy;
            this.ao = flagList.ao;
            this.ar = flagList.ar;
            this.as = flagList.as;
            this.bee_navy = flagList.bee_navy;
            this.cl_army = flagList.cl_army;
            this.cl_mc = flagList.cl_mc;
            this.co_army = flagList.co_army;
            this.e_af = flagList.e_af;
            this.el = flagList.el;
            this.el_army = flagList.el_army;
            this.el_mc = flagList.el_mc;
            this.el_navy = flagList.el_navy;
            this.eng_navy = flagList.eng_navy;
            this.fa_army = flagList.fa_army;
            this.g_af = flagList.g_af;
            this.gm_army = flagList.gm_army;
            this.gt_army = flagList.gt_army;
            this.gt_navy = flagList.gt_navy;
            this.hm_navy = flagList.hm_navy;
            this.m_af = flagList.m_af;
            this.mc = flagList.mc;
            this.mec2_navy = flagList.mec2_navy;
            this.mec_navy = flagList.mec_navy;
            this.mk = flagList.mk;
            this.mm_army = flagList.mm_army;
            this.mm_mc = flagList.mm_mc;
            this.gt_mc = flagList.gt_mc;
            this.nuc_navy = flagList.nuc_navy;
            this.of_army = flagList.of_army;
            this.pc = flagList.pc;
            this.sc_army = flagList.sc_army;
            this.st_army = flagList.st_army;
            this.ve = flagList.ve;
            this.wk = flagList.wk;
            this.el_cg = flagList.el_navy;
            this.mr_cg = flagList.mec_navy;
            this.gt_cg = flagList.gt_navy;
            this.cl_cg = flagList.adm_navy;
        });
        this._scoreService.getScore().subscribe(scoreData => {
            this.haveTestScores = false;
            this.haveAllAsvabScores = false;
            if (scoreData) {
                this.haveAllAsvabScores = scoreData.verbalAbility !== null;
                this.scoreSummaryList = scoreData['aFQTRawSSComposites'];
                if (this.scoreSummaryList.mec2_navy === null)
                    this.scoreSummaryList.mec2_navy = 0;
                if (this.scoreSummaryList.ops_navy === null)
                    this.scoreSummaryList.ops_navy = 0;
                if (this.scoreSummaryList) {
                    this._scoreService.setScoreObject(this.scoreSummaryList);
                    this.scoreSummaryList.el_cg = this.scoreSummaryList.el_navy;
                    this.scoreSummaryList.mr_cg = this.scoreSummaryList.mec_navy;
                    this.scoreSummaryList.gt_cg = this.scoreSummaryList.gt_navy;
                    this.scoreSummaryList.cl_cg = this.scoreSummaryList.adm_navy;
                    this.haveTestScores = true;
                }
            }
        });

        this._portfolioService.getUserSystem().subscribe(usersys => {
            this.userSystem = usersys;
        });

        this.stripLeadingZero = function (score) {
            const strippedScore = parseInt(score, 10);
            const strippedScoreString = strippedScore === 0 ? 'NA' : strippedScore.toString;
            return strippedScoreString;
        };

        this._scoreService.getCompositeFavoritesFlags().subscribe(flagData => {
            this.compositeFavoritesFlags = flagData;
        });
        window.scroll(0, 0);

    }

    ga(param) {
        this._googleAnalyticsService.trackClick(param);
    }

    gaSocialShare(socialPlug, value) {
        this._googleAnalyticsService.trackSocialShare(socialPlug, value);
    }
    haveAsvabTestScores() {
        return this.haveTestScores;
    }

    openPlayer(videoId) {
        this._dialog.open(PopupPlayerComponent, {
            data: { videoId: videoId }
        });
    }

    openNewTab(url) {
        window.open(url, '_blank');
    }

    setMessage(area) {
        let modalOptions;
        switch (area) {
            case 'serviceComps':
                modalOptions = {
                    headerText: 'Service Composite Categories',
                    bodyText: '<ul><li>Each military job has unique composite score requirements</li>' +
                        '<li>Requirements are subject to change without notice</li>' +
                        '<li>For the most up-to-date requirements, <a href="/options-contact-recruiter">' +
                        'contact a recruiter</a></li></ul>'
                };
                break;
            case 'yourComps':
                modalOptions = {
                    headerText: 'Your Composite Scores',
                    bodyText: '<ul><li>Each of your composite scores is calculated from using a combination of your ASVAB ' +
                        'subtest scores</li>' +
                        '<li>The table displays which ASVAB subtests are used to generate each of your composite scores</li>' +
                        '<li>The table does not show the weights (importance) of the subtest scores, except for AFQT</li></ul>'
                };
                break;
            case 'afqt':
                modalOptions = {
                    headerText: 'AFQT Calculation',
                    bodyText: '<p>Your scores on Arithmetic Reasoning, Mathematics Knowledge, ' +
                        'Word Knowledge, and Paragraph Comprehension determine your AFQT score.</p>'
                };
                break;
            case 'colors':
                modalOptions = {
                    headerText: 'What Do These Colors Mean',
                    bodyText: '<p>The subtest scores are color coded to show which scores are used to build each ASVAB Career Exploration Score.</p>' +
                        '<ul class="color-codes"><li><span id="greenBar"></span>Verbal Skills</li>' +
                        '<li><span id="redBar"></span>Math Skills</li>' +
                        '<li><span id="purpleBar"></span>Science and Technical Skills</li></ul>' +
                        '<p><a href="' + this.cepBaseUrl + 'my-asvab-summary-results" target="_blank">Learn more about Career Exploration Scores.</a></p>'

                };
                break;
        }
        const data = {
            'title': modalOptions.headerText,
            'message': modalOptions.bodyText
        };
        this._dialog.open(MessageDialogComponent, {
            data: data,
            hasBackdrop: true,
            disableClose: true,
            minWidth: '400px',
            autoFocus: true
        });
    }

    openServiceContactUsDialog() {
        this._dialog.open(ServiceContactUsDialogComponent, {
            panelClass: 'custom-dialog'
        });
    }

    openDescription(section) {

        let headerTitle, bodyText;
        if (section === 'enlisted-codes') {
          headerTitle = ' ';
          bodyText = 'Your scores are within range of the requirements to enter one or more jobs related to this career. Contact a recruiter for details.'
        } else if (section === 'occufind-explain') {
            headerTitle = ' ';
            bodyText = 'Your scores are within range of the requirements to enter one or more jobs related to this career. Contact a recruiter for details.'
        } else if (section === 'service-specific-composite-score') {
            headerTitle = '<h5>Service-Specific Composite Scores<h5>';
            bodyText = '<div>Service Composite Categories</div>' +
                    '<ul><li>Each military job has unique composite score requirements</li><li>Requirements are subject to change without notice</li><li>For the most up-to-date requirements, <a href="https://www.careersinthemilitary.com/options-contact-recruiter">contact a recruiter</a></li></ul>'+
                    '<div>Your Composite Scores</div>' +
                    '<ul><li>Each of your composite scores is calculated from using a combination of your ASVAB subtest scores</li><li>The table displays which ASVAB subtests are used to generate each of your composite scores</li><li>The table does not show the weights (importance) of the subtest scores, except for AFQT</li></ul>'
        } else {
          return 0;
        }
    
        const data = {
          'title': headerTitle,
          'message': bodyText
        };
        this._dialog.open(MessageDialogComponent, {
          data: data,
          hasBackdrop: true,
          disableClose: true,
          maxWidth: '400px'
        });
    
    }
}
