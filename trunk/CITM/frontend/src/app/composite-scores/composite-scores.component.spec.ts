import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompositeScoresComponent } from './composite-scores.component';

describe('CompositeScoresComponent', () => {
  let component: CompositeScoresComponent;
  let fixture: ComponentFixture<CompositeScoresComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompositeScoresComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompositeScoresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
