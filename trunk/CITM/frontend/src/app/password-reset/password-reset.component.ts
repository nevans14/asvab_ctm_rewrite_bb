import { Component, OnInit } from '@angular/core';
import { HttpHelperService } from 'app/services/http-helper.service';
import { UtilityService } from 'app/services/utility.service';

export interface RestCredentials {
  resetId: String,
  username: String,
  Password1: String,
  Password2: String,
}

@Component({
  selector: 'app-password-reset',
  templateUrl: './password-reset.component.html',
  styleUrls: ['./password-reset.component.scss']
})
export class PasswordResetComponent implements OnInit {

  readonly loginBaseUrl = 'applicationAccess/';
  restCredentials = {
    resetId: '',
    username: '',
    Password1: '',
    Password2: '',
  };
  registrationLabel = '';
  registrationLoginStatus;

  constructor(
    private _httpHelper: HttpHelperService,
    private _utility: UtilityService,
  ) {
    let absoluteUrl = window.location.href;
    absoluteUrl = absoluteUrl.replace("%20", "+");
    absoluteUrl = absoluteUrl.replace("%3D", "=");
    var questionMarkPosition = absoluteUrl.indexOf("?");
    var urlParametersString = absoluteUrl.substr(questionMarkPosition + 1);
    var urlParameters = urlParametersString.split("+");

    var resetParameters = {};
    for (let i = 0; i < urlParameters.length; i++) {
      var nameValue = urlParameters[i].split("=");
      resetParameters[nameValue[0]] = nameValue[1];
      this.restCredentials[nameValue[0]] = nameValue[1];
    }

    this._utility.setShowHeaderFooter(false);
  }

  ngOnInit() {
  }

  ResetPassword = function () {
    var data = {
      email: this.restCredentials.username,
      accessCode: "",
      password: "",
      resetKey: this.restCredentials.resetId,
    };
    var pass1 = this.restCredentials.Password1;
    var pass2 = this.restCredentials.Password2;
    if (pass1.length <= 7) {
      this.registrationLabel = "Password too short!";
      return;
    }
    if (pass1 !== pass2) {
      this.registrationLabel = "Passwords don't match!";
      this.restCredentials.Password1 = '';
      this.restCredentials.Password2 = '';
      return;
    }

    data.password = pass1;
    return this._httpHelper.httpHelper('POST', this.loginBaseUrl + 'passwordReset', null, data)
      .then((response) => {
        this.registrationLoginStatus = response;
        if (this.registrationLoginStatus.statusNumber === 0) {

          // Set the location to dashboard
          window.location.replace('/');
        } else { // Error
          this.registrationLabel = this.registrationLoginStatus.status
        }
      }).catch(( error ) => {
        console.log( error );
      });
  }

  clearRegistrationLabel() {
    this.registrationLabel = '';
  }
}
