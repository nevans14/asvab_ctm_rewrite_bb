import { Component, OnInit, Input } from '@angular/core';
import { UserService } from 'app/services/user.service';
import { ConfigService } from 'app/services/config.service';
import { SessionService } from 'app/services/session.service';
import { MatDialog } from '@angular/material';
import { NotesDialogComponent } from 'app/core/dialogs/notes-dialog/notes-dialog.component';
import { MessageDialogComponent } from 'app/core/dialogs/message-dialog/message-dialog.component';
import { Router } from '@angular/router';
import { throwError } from 'rxjs';

@Component({
  selector: 'app-favorites',
  templateUrl: './favorites.component.html',
  styleUrls: ['./favorites.component.scss']
})
export class FavoritesComponent implements OnInit {
  occupationView = true;
  serviceView = false;
  careerClusterView = false;
  cepOccupationsView = false;
  fullOptions = true;
  disabled = false;
  limit = 4;
  checked = 0;
  myStyle =  {'cursor': 'pointer'};

  @Input()  view: string;
  @Input() options: boolean;
  @Input() favoriteList: any;
  @Input() careerClusterFavoriteList: any;
  @Input() serviceFavoriteList: any;
  @Input() cepFavoriteOccupationsList: any;
  @Input() parent;

  constructor(
    private _userService: UserService,
    private _router: Router,
    private _dialog: MatDialog,
    private _sessionService: SessionService,
    private _config: ConfigService
  ) { }

  ngOnInit() {
    console.log(this.view);
    this.fullOptions = false;
    if (this.options) {
      this.fullOptions = true;
    }


    if (this.view) {
      switch (this.view) {
        case ('occupationView'):
          this.occupationToggle();
          break;
        case ('careerClusterView'):
          this.careerClusterToggle();
          break;
        case ('serviceView'):
          this.serviceToggle();
          break;
        case ('cepOccupationsView'):
          this.cepToggle();
          break;
      }
    }
  }

  // The favorite list needs to be updated when the favorite list is changed on the same page.
  updateFavoriteList(type) {
    if (this._config.isLoggedIn() ) {
      if (type === 'occ') {
        const self = this;
        this._userService.getFavoritesList().subscribe(
          data => {
            this.favoriteList = data ? data : [];
          });
                  // Refresh the search list in the background
        this._sessionService.resetSearchList('', false);

      }

      if (type === 'cc') {
        this._userService.getFavoriteCareerCluster().subscribe(
          data => {
            this.careerClusterFavoriteList = data ? data : [];
            this._userService.favoriteCareerClusterLIstObject = this.careerClusterFavoriteList;
          });
      }

      if (type === 'svc') {
        this._userService.getFavoriteServices().subscribe(
          data => {
            this.serviceFavoriteList = data;
          });
      }
    }
  }

  ifFullOptions() {
    return this.fullOptions;
  }

  removeFavorite(i) {
    // Disable link if process was already started.
    if (this.disabled) {
      return;
    } else {
      this.disabled = true;
    }

    this.myStyle = { 'cursor': 'wait' };

    const id = this.favoriteList[i].mcId;
    const self = this;
    return this._userService.deleteFavorite(id)
      .then((obj) => {
        self.updateFavoriteList('occ');
        self.parent.refreshFavorites();
        self.disabled = false;
        self.myStyle = { 'cursor': 'pointer' };
        return;
      })
      .catch(error => {
        self.myStyle = { 'cursor': 'pointer' };
        self.disabled = false;
        throwError(error);
      });

  }

  /**
 * Tracks how many selections are made to limit user selection to two
 */
  checkChanged(event, i) {
    const checkBox = (event.target || event.srcElement || event.currentTarget) as HTMLInputElement;
    this.favoriteList[i]. checked = checkBox.checked;

    if (checkBox.checked)  {
        this.checked++;
    } else {
        this.checked--;
    }
  }

  checkDisabled() {
    return this.disabled;
  }

  compareSelection() {
    const occupationSelected = [];

    for (let i = 0; i < this.favoriteList.length; i++) {

      if (this.favoriteList[i].checked) {
        occupationSelected.push(this.favoriteList[i].mcId);
      }
    }

    if (occupationSelected.length < 2) {
      const messageInfo = {
        title: 'Favorites',
        message: 'Select at least 2 occupations.'
      };

      this._dialog.open( MessageDialogComponent, {
        data: messageInfo,
        hasBackdrop: true,
        disableClose: true
      });
      return;
    }
      if (occupationSelected.length < 3) {
        occupationSelected.push('null');
      }
      if (occupationSelected.length < 4) {
        occupationSelected.push('null');
      }

      this._router.navigate(['job-compare/' + occupationSelected[0] + '/' + occupationSelected[1] + '/' + occupationSelected[2] + '/' + occupationSelected[3]]);
  }

  // remove career cluster favorites using user factory service
  removeCareerClusterFavorite(i) {
    // Disable link if process was already started.
    if (this.disabled) {
      return;
    } else {
      this.disabled = true;
    }
    const id = this.careerClusterFavoriteList[i].id;

    this.myStyle = { 'cursor': 'wait' };

    const self = this;
    return this._userService.deleteFavoriteCareerCluster(id)
      .then(function (response) {
        self.updateFavoriteList('cc');
        self.myStyle = { 'cursor': 'pointer' };
        self.disabled = false;
        return;
      })
      .catch(error => {
        self.myStyle = { 'cursor': 'pointer' };
        self.disabled = false;
        throwError(error);
      });
  }

  onNotesClick(index) {
    const favorite = this.favoriteList[index];
    // Currently only have occupational
    const noteInfo = {
      typeOfNote: 'occupational',
      title: favorite.title,
      mcId: favorite.mcId,
      note: null
    };

    const dialogRef = this._dialog.open( NotesDialogComponent, {
      data: noteInfo,
      panelClass: 'custom-dialog'
    });

    dialogRef.afterClosed().subscribe(result => {
      this.parent.getNotes();
    });

  }


  /**
	 * Favorites toggle between occupations, career clusters, and services.
 */

  occupationToggle() {
    this.occupationView = true;
    this.careerClusterView = false;
    this.serviceView = false;
    this.cepOccupationsView = false;
  }
  careerClusterToggle() {
    this.occupationView = false;
    this.careerClusterView = true;
    this.serviceView = false;
    this.cepOccupationsView = false;
  }
  serviceToggle() {
    this.occupationView = false;
    this.careerClusterView = false;
    this.serviceView = true;
    this.cepOccupationsView = false;
  }
  cepToggle() {
    this.occupationView = false;
    this.careerClusterView = false;
    this.serviceView = false;
    this.cepOccupationsView = true;
    for (let i = 0; i < this.cepFavoriteOccupationsList.length; i++) {
      this.cepFavoriteOccupationsList[i].link = this._config.getCepBaseUrl()  + 'occufind-occupation-details/' + this.cepFavoriteOccupationsList[i].onetSocCd;
    }
  }

  removeService(i) {
    // Disable link if process was already started.
    if (this.disabled) {
      return;
    } else {
      this.disabled = true;
    }
    const id = this.serviceFavoriteList[i].id;
    this.myStyle = { 'cursor': 'wait' };

    const self = this;
    return this._userService.deleteFavoriteService(id)
      .then((obj) => {
        self.updateFavoriteList('svc');
        self.myStyle = { 'cursor': 'pointer' };
        self.disabled = false;
        return;
      })
      .catch(error => {
        self.myStyle = { 'cursor': 'pointer' };
        self.disabled = false;
        throwError(error);
      });
  }

  goToLink(url){
    window.open(url, "_blank");
  }
}
