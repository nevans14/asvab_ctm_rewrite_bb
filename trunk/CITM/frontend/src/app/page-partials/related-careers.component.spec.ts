import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RelatedCareersComponent } from './related-careers.component';

describe('RelatedCareersComponent', () => {
  let component: RelatedCareersComponent;
  let fixture: ComponentFixture<RelatedCareersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RelatedCareersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RelatedCareersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
