import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ServiceContactUsDialogComponent } from 'app/core/dialogs/service-contact-us-dialog/service-contact-us-dialog.component';
import { CareerSearchService  } from 'app/services/career-search.service';

@Component({
  selector: 'app-contact-services',
  templateUrl: './contact-services.component.html',
  styleUrls: ['./contact-services.component.scss']
})
export class ContactServicesComponent implements OnInit {
  serviceContacts: any;

  constructor(
    private _dialog: MatDialog,
    private _careerSearchService: CareerSearchService
    ) { }

  ngOnInit() {
    this._careerSearchService.getServiceContacts().subscribe(
      data => {
        this.serviceContacts = data;
    });

  }

  openServiceContactUsDialog(service) {
    window.scrollTo(0, 0);
    this._dialog.open(ServiceContactUsDialogComponent, {
      data: service,
      panelClass: 'custom-dialog'
    });
  }

}
