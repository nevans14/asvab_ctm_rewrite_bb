import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd} from '@angular/router';
import { MatDialog } from '@angular/material';
import { LoginDialogComponent } from 'app/core/dialogs/login-dialog/login-dialog.component';
import { SessionService } from '../services/session.service';
import { ConfigService } from 'app/services/config.service';
import { ContentManagementService } from 'app/services/content-management.service';
import { UtilityService } from '../services/utility.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})

export class HeaderComponent implements OnInit {
  showSlideMenu = false;
  displayBanner: boolean;
  bannerText: String;
  disableLogin = false;
  page: any;

  constructor(
    private _dialog: MatDialog,
    private _config: ConfigService,
    private _router: Router,
    private _cmsService: ContentManagementService,
    private _sessionService: SessionService,
    private _utility: UtilityService,
  ) { }

  ngOnInit() {
    this.disableLogin = this._config.getDisabledLogin();

    this._cmsService.getDbinfoByName('show_maintenance_banner')
    .subscribe(data => {
        this.displayBanner = data.value.toLowerCase() === 'true';
        if (this.displayBanner) {
          this._cmsService.getDbinfoByName('citm_maintenance_banner_text')
          .subscribe(data => {
            this.bannerText = data.value;
          });
        }
    });

    

    this._router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        this._utility.removeCanonicalUrl();
        this._utility.createCanonicalUrl('https://www.careersinthemilitary.com' + this._router.url);
      }
    })
  }

  onOpenSlideOut() {
    this.showSlideMenu = true;
  }

  onLogOff() {
    this._sessionService.logOff();
    // location.href = this._config.getBaseUrl() + 'CITM/rest/auth/logout';
    this.onClose();
  }

  onRouteChange(path) {
    this._router.navigate([path]);
    this.onClose();
  }
  
  routeToAnchor(anchor) {
    // this._router.navigate([path], {fragment: anchor}); // Angular bug/long time before scroll
    const el = document.getElementById(anchor);
    el.scrollIntoView();
  }

  isLoggedIn() {
    return this._config.isLoggedIn();
  }

  onClose() {
    this.showSlideMenu = false;
  }

  notHomePage() {
    const currentPath = this._router.url;
      if (currentPath.indexOf('/home') < 0) {
        return true;
      }
      return false;
  }


  openLoginDialog() {
    const dialogRef = this._dialog.open( LoginDialogComponent, { hasBackdrop: true, disableClose: true, autoFocus: true } );
    this.onClose();

    const self = this;
    dialogRef.afterClosed().subscribe( results => {
      if (this._config.isLoggedIn() && !this._config.getSSO()) {
        self.onRouteChange('/profile');
      }
    });

  }
}
