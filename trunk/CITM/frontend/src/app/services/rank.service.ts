import { Injectable } from '@angular/core';
import { HttpHelperService } from './http-helper.service';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class RankService  {

    constructor(
        private _httpHelper: HttpHelperService,
        private _http: HttpClient
    ) { }

    getRank(): Observable<any> {
        return this._http.get(this._httpHelper.getFullUrl('/option/get-rank/', null));
    }
}
