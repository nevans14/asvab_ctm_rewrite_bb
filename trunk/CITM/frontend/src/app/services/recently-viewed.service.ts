import { Injectable } from '@angular/core';
import { HttpHelperService } from 'app/services/http-helper.service';
import { ConfigService } from 'app/services/config.service';

@Injectable({
  providedIn: 'root'
})
export class RecentlyViewedService {
  readonly keyName = 'recentlyViewed';
  recentlyViewed: any;
  refreshedAfterSignIn = false;

  constructor(
    private _httpHelper: HttpHelperService,
    private _config: ConfigService
  ) { }

  getRecentlyViewed() {
    return new Promise((resolve, reject) => {

      if (!this._config.isLoggedIn()  && sessionStorage.getItem(this.keyName)) {
          this.recentlyViewed = JSON.parse(sessionStorage.getItem(this.keyName));
        resolve(this.recentlyViewed);
      } else if (this._config.isLoggedIn()) {
        if (!this.refreshedAfterSignIn) {
          this.refreshedAfterSignIn = true;
          return this._httpHelper.httpHelper('GET', 'detail-page/get-recently-viewed/', null, null)
            .then((obj) => {
              if (obj) {
                this.recentlyViewed = obj;
                sessionStorage.setItem(this.keyName, JSON.stringify(obj));
                resolve(this.recentlyViewed);
              } else {
                sessionStorage.setItem(this.keyName, JSON.stringify([]));
                resolve([]);
              }
            });
          } else {
            this.recentlyViewed = JSON.parse(sessionStorage.getItem(this.keyName));
            resolve(this.recentlyViewed);
          }
      } else {
        sessionStorage.setItem(this.keyName, JSON.stringify([]));
        resolve([]);
      }
    });
  }

  setRecentlyViewed(mcId, title) {
    return new Promise((resolve, reject) => {

      const currentView = {
        mcId: mcId,
        title: title
      };
      // If this entry is already in the list remove it and add it to the top
    if (this.recentlyViewed) {
      for (let i = 0; i < this.recentlyViewed.length; i++) {
        if (this.recentlyViewed[i].mcId === mcId) {
          this.recentlyViewed.splice(i, 1);
        }
      }
    } else {
      this.recentlyViewed = [];
    }
      // Add the most recently viewed to the top
      if (this.recentlyViewed && this.recentlyViewed.length > 0) {
      this.recentlyViewed.unshift(currentView);
      } else {
        this.recentlyViewed.push(currentView);
      }
      // Make sure to keep it at 15 entries or less because that is all the backend can handle
      if (this.recentlyViewed.length === 15) {
        this.recentlyViewed.splice(this.recentlyViewed.length - 1, 1);
      }
      sessionStorage.setItem(this.keyName, JSON.stringify(this.recentlyViewed));

      if (this._config.isLoggedIn()) {
        const currentList = {
          jsonString: sessionStorage.getItem(this.keyName)
        };
      return this._httpHelper.httpHelper('POST', 'detail-page/set-recently-viewed', null, currentList)
      .then ((obj) => {
        resolve();
      });
      }
      resolve();
    });
  }
}
