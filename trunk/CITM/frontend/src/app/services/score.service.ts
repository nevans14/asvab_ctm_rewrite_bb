import { Injectable } from '@angular/core';
import { HttpHelperService } from './http-helper.service';
import { HttpClient } from '@angular/common/http';
import { ConfigService } from 'app/services/config.service';
import { Observable } from 'rxjs';
import { resolve } from 'q';

@Injectable({
  providedIn: 'root'
})

export class ScoreService {
   public scoreObject: any;
   public favorateObject: any;
    constructor(
    private _http: HttpClient,
    private _config: ConfigService,
    private _httpHelper: HttpHelperService
  ) { }

  /**
 * Get Favorites
*/
    getScore(): Observable<any> {
        return this._http.get(this._httpHelper.getFullUrl('testScore/get-test-score/', null));
    }

    insertScore(scoreObject) {
        return this._httpHelper.httpHelper('post', 'testScore/insertManualTestScore', null, scoreObject)
        .then((obj) => {
            resolve(this.scoreObject);
        });
    }

    updateScore(obj) {
        if (this._config.isLoggedIn()) {
            return this._httpHelper.httpHelper('put', 'testScore/updateManualTestScore', null, obj)
                .then((cfgObj) => {
                resolve(cfgObj);
            });
        }
    }

    deleteScore() {
        return this._httpHelper.httpHelper('Delete', 'testScore/deleteManualTestScore', null, null)
        .then((obj) => {
            resolve(obj);
        });
    }

    setScoreObject(score) {
        this.scoreObject = score;
    }

    getCompositeFavoritesFlags(): Observable<any> {
        return this._http.get(this._httpHelper.getFullUrl('testScore/getCompositeFavoritesFlags/', null));
    }

    setCompositeFavoritesFlags( favorateObject ) {
        return new Promise((resolve, reject) => {
            this._httpHelper.httpHelper('post', 'testScore/setCompositeFavoritesFlags', null, favorateObject)
                .then((obj) => {
                    resolve(obj);
                }).catch(error => {
                    reject(error);
                });
        });
    }

    deleteCompositeFavoritesFlags() {
        return this._httpHelper.httpHelper('Delete', 'testScore/deleteCompositeFavoritesFlags', null, null)
        .then((obj) => {
            resolve(obj);
        });
    }

    getServiceOfferingCompositeScore(): Observable<any> {
        return this._http.get(this._httpHelper.getFullUrl('detail-page/get-service-offering-composite/', null));
    }

    getServiceOffering(mcId): Observable<any> {
        return this._http.get(this._config.getAwsFullUrl( '/api/detail-page/get-service-offering/' + mcId + '/'));
    }

    createMcIdParam(mcId) {
        const params = [];
        params.push({
        key: null,
        value: mcId
        });
        return params;
    }

    navyUserCompositeScores(compositeScores, id) {
        switch (id) {
            case 'GT':
                return compositeScores.gt_navy;
            case 'EL':
                return compositeScores.el_navy;
            case 'BEE':
                return compositeScores.bee_navy;
            case 'ENG':
                return compositeScores.eng_navy;
            case 'MEC':
                return compositeScores.mec_navy;
            case 'MEC2':
                return compositeScores.mec2_navy;
            case 'NUC':
                return compositeScores.nuc_navy;
            case 'OPS':
                return compositeScores.ops_navy;
            case 'HM':
                return compositeScores.hm_navy;
            case 'ADM':
                return compositeScores.adm_navy;
        }
    }

    afUserCompositeScores = function (compositeScores, id) {
        switch (id) {
            case 'M':
                return compositeScores.m_af;
            case 'A':
                return compositeScores.a_af;
            case 'G':
                return compositeScores.g_af;
            case 'E':
                return compositeScores.e_af;
        }
    };

    coastGuardUserCompositeScores = function(compositeScores, id) {
        switch (id) {
            case 'EL':
                return compositeScores.el_cg;
            case 'MR':
                return compositeScores.mr_cg;
            case 'GT':
                return compositeScores.gt_cg;
            case 'CL':
                return compositeScores.cl_cg;
        }
    }

    armyUserCompositeScores(compositeScores, id) {
        switch (id) {
            case 'GT':
                return compositeScores.gt_army;
            case 'CL':
                return compositeScores.cl_army;
            case 'CO':
                return compositeScores.co_army;
            case 'EL':
                return compositeScores.el_army;
            case 'FA':
                return compositeScores.fa_army;
            case 'GM':
                return compositeScores.gm_army;
            case 'MM':
                return compositeScores.mm_army;
            case 'OF':
                return compositeScores.of_army;
            case 'SC':
                return compositeScores.sc_army;
            case 'ST':
                return compositeScores.st_army;
        }
    }

    getTestAbbreviation(additionalRequirement) {
        if (additionalRequirement.includes('AR')) return 'AR';
        if (additionalRequirement.includes('AS')) return 'AS';
        if (additionalRequirement.includes('EI')) return 'EI';
        if (additionalRequirement.includes('GS')) return 'GS';
        if (additionalRequirement.includes('MC')) return 'MC';
        if (additionalRequirement.includes('MK')) return 'MK';
        if (additionalRequirement.includes('PC')) return 'PC';
        if (additionalRequirement.includes('WK')) return 'WK';
        if (additionalRequirement.includes('VE')) return 'VE';
    }

    getTestScore(compositeScores, abbreviation) {
        return compositeScores[abbreviation.toLowerCase()] + '';
    }
}
