import { Injectable } from '@angular/core';
import { HttpHelperService } from 'app/services/http-helper.service';
import { SessionService } from 'app/services/session.service';
import { UtilityService } from 'app/services/utility.service';
import { GoogleAnalyticsService } from '../services/google-analytics.service';

@Injectable( {
    providedIn: 'root'
} )
export class LoginService {
    readonly loginBaseUrl = 'applicationAccess/';
    restApiUrl: any;
    ssoApiUrl: any;

    constructor(
        private _httpHelper: HttpHelperService,
        private _sessionService: SessionService,
        private _utilityService: UtilityService,
        private _googleAnalyticsService: GoogleAnalyticsService
    ) {
        this.restApiUrl = this._httpHelper.getApiUrl();
        this.restApiUrl = this.restApiUrl[this.restApiUrl.length - 1] === '/' ? this.restApiUrl : this.restApiUrl + '/';
        this.ssoApiUrl = this._httpHelper.getSsoUrl();

        let csrfToken = this._utilityService.getCsrfCookie()
        if (!csrfToken) {
            this._httpHelper.httpHelper( 'GET', 'get-token/', null, null )
            .then((csrfToken: any) => {
                //if ('token' in csrfToken) {
                //    this._utilityService.createCsrfCookie(csrfToken['token'])
                //} else {
                //    throw new Error('Returned CSRF token missing token property.');
                //}
            })
            .catch((err) => {
                console.log("Token initialization failed: " + err)
            })
        } 
    }

    emailLogin( email, password ) {
        let data = {
            email: email,
            accessCode: "",
            password: password,
            resetKey: ""
        }

        return this._httpHelper.httpHelper( 'POST', this.loginBaseUrl + 'loginEmail', null, data )
            .then(( obj ) => {
                let retStatus: number = obj['statusNumber'];
                switch ( retStatus ) {

                    case ( 1002 ):
                    case ( 1008 ):
                        return 'Username / Password combination is invalid';

                    case ( 1010 ):
                        return 'Username has been locked, try resetting your password.';

                    case ( 0 ):
                        if (!this.setCredentials()) {
                            return;
                        }
                        window.location.href =
                        this.restApiUrl  + 'sso/confirm_login?redirect=' + this.ssoApiUrl + 'login_callback/test';
                        this._googleAnalyticsService.trackStudent(obj);
                        return;

                    case ( 1 ):
                        console.log("window.location.href: " + window.location.href)
                        if (!this.setCredentials()) {
                            return;
                        }
                        this._googleAnalyticsService.trackStudent(obj);
                        return;

                    default:
                        const error = obj['status'];
                        return error.substring( error.indexOf( ":" ) + 1, error.length + 1 );
                }
            } );

    }

    emailRegistration( email, password, recaptchaResponse ) {
        var data = {
            email: email,
            accessCode: "",
            password: password,
            resetKey: "",
            recaptchaResponse: recaptchaResponse,

        };

        return new Promise(( resolve, reject ) => {
            return this._httpHelper.httpHelper( 'POST', this.loginBaseUrl + 'register', null, data )
                .then(( obj ) => {
                    let retStatus = obj['statusNumber'];
                    switch ( retStatus ) {

                        case ( 0 ):
                            if (!this.setCredentials()) {
                                break;
                            }
                            window.location.href =
                            this.restApiUrl + 'sso/confirm_login?redirect=' + this.ssoApiUrl + 'login_callback/test';
                            break;
                        case ( 1 ):
                            this.setCredentials();
                            break;
                        default:
                    }
                    resolve( obj );
                }).catch(( error ) => {
                    console.log( error );
                    reject( error );
                });
        } );
    }

    passwordResetRequest( email ) {
        var data = {
            email: email,
            accessCode: "",
            password: "",
            resetKey: ""
        };

        return this._httpHelper.httpHelper( 'POST', this.loginBaseUrl + 'passwordResetRequest', null, data )
            .then(( obj ) => {
                return obj[status];
            } );
    }

    setCredentials() {
        this._sessionService.resetSearchList();

        if (this._utilityService.getCookie() === null) {
            return false;
        }
        return true;
    }
}
