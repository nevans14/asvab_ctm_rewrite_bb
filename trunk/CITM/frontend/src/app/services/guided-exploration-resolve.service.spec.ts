import { TestBed } from '@angular/core/testing';

import { GuidedExplorationResolveService } from './guided-exploration-resolve.service';

describe('GuidedExplorationResolveService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GuidedExplorationResolveService = TestBed.get(GuidedExplorationResolveService);
    expect(service).toBeTruthy();
  });
});
