import { Injectable } from '@angular/core';
import { HttpHelperService } from './http-helper.service';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ConfigService } from './config.service';

@Injectable({
    providedIn: 'root'
})
export class PayService {

    constructor(
        private _httpHelper: HttpHelperService,
        private _http: HttpClient,
        private _configService: ConfigService,
    )  { }


    getPay(): Observable<any> {
        return this._http.get(this._configService.getAwsFullUrl(`/api/option/get-Pay/`));
    }

    getRank(): Observable<any> {
        return this._http.get(this._configService.getAwsFullUrl(`/api/option/get-rank/`));
    }

    getPayChart(): Observable<any> {
        return this._http.get(this._configService.getAwsFullUrl(`/api/option/get-pay-chart/`));
    }

    getPayInfo(): Observable<any> {
        return this._http.get(this._configService.getAwsFullUrl(`/api/option/get-pay-info/`));
    }
}
