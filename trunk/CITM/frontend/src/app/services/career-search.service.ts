import { Injectable } from '@angular/core';
import { HttpHelperService } from './http-helper.service';
import { HttpClient } from '@angular/common/http';
import { ConfigService } from './config.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CareerSearchService {
  careerClusterListObject: any;

  constructor(
    private _http: HttpClient,
    private _httpHelper: HttpHelperService,
    private _configService: ConfigService,
  ) { }

  getCareerCluster(): Observable<any> {
    return this._http.get(this._configService.getAwsFullUrl('/api/careerCluster/careerClusterList/'));
  }

getCareerClusterPathway (ccId): Observable<any> {
    const params = this.createIdParam(ccId);

    return this._http.get(this._configService.getAwsFullUrl('/api/careerCluster/CareerClusterPathway/' + ccId));
}

  getCareerPath(mcId): Observable<any> {
    return this._http.get(this._configService.getAwsFullUrl('/api/detail-page/get-career-path/' + mcId + '/'));
  }

  getServiceContacts(): Observable<any> {
    return this._http.get(this._configService.getAwsFullUrl('/api/search/get-service-contacts'));
  }

  createIdParam(id) {
    const params = [];
    params.push({
      key: null,
      value: id
    });
    return params;
  }

  setCareerCluster(careerCluster) {
    this.careerClusterListObject = careerCluster;
  }

}
