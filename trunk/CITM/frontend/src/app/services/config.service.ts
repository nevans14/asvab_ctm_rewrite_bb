import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { UtilityService} from 'app/services/utility.service';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ConfigService {
  keyValue = 'CitmInfo';
  configurations: any;
  restUrl: any;
  awsRestUrl: any;
  user: any;
  userId: String;
  sso: boolean = null;

  constructor(
    private _http: HttpClient,
    private _utilityService: UtilityService,
  ) {  }

  loadConfigurationData(): Promise<Object> {
    return new Promise((resolve, reject) => {
      this._http.get('assets/config.json')
      .toPromise()
      .then(obj => {
        this.configurations = obj;
        if (this.getBaseUrl()) {
          this.restUrl = this.getBaseUrl() + 'CITM/rest';
          this.awsRestUrl = this.getBaseUrl();
         } else {
           this.restUrl = '/CITM/rest';
         }
        resolve(this.configurations);
      });
    });
  }

  public getBaseUrl() {
    if (this.configurations['serviceUrl']) {
      const workUrl = this.configurations['serviceUrl'];
      const serviceUrl = workUrl[workUrl.length - 1] === '/' ?
            this.configurations['serviceUrl'] : this.configurations['serviceUrl'] + '/';
      return serviceUrl;
    }
    return null;
  }

  public getRestUrl() {
    return this.restUrl;
  }

  public getFullUrl(url, params) {
    const mainUrl = this.restUrl;

    let fullUrl = url[0] === '/' ? mainUrl + url : mainUrl + '/' + url;

    if (params) {
      for (const i in params) {
        if (params.hasOwnProperty(i)) {
          if (params[i].key) {
            fullUrl = fullUrl[fullUrl.length - 1] ===  '/' ? fullUrl + params[i].key : fullUrl + '/' + params[i].key;
          }
          fullUrl = fullUrl[fullUrl.length - 1] ===  '/'  ? fullUrl + params[i].value : fullUrl + '/' + params[i].value;
        }
      }
    }
    if (!(fullUrl[fullUrl.length - 1] ===  '/')) {
      fullUrl = fullUrl + '/';
    }
    return fullUrl;
  }

  public getAwsFullUrl(url) {
    const mainUrl = this.awsRestUrl;

    if (this.configurations.awsApiUrl) {
      if (mainUrl.indexOf('legacy-dev.careersinthemilitary.com') !== -1
        || mainUrl.indexOf('localhost') !== -1) {
        return this.configurations.awsApiUrl.dev + '/Dev' + url;
      } else if (mainUrl.indexOf('legacy-stg.careersinthemilitary.com') !== -1) {
        return this.configurations.awsApiUrl.stg + url;
      } else {
        return this.configurations.awsApiUrl.prod + '/Prod' + url;
      }
    }
  }

  public getImageUrl() {
    const workUrl = this.configurations['imageUrl'];
    if (this.configurations['imageUrl']) {
      const imageUrl = workUrl[workUrl.length - 1] === '/' ?
            this.configurations['imageUrl'] : this.configurations['imageUrl'] + '/';
      return imageUrl;
    }
  return null;
  }

  public getSsoUrl() {
    const workUrl = this.configurations['ssoUrl'];
    if (this.configurations['ssoUrl']) {
      let ssoUrl = workUrl[workUrl.length - 1] === '/' ? this.configurations['ssoUrl'] : this.configurations['ssoUrl'] + '/';
      return ssoUrl;
    }
  return null;
  }
  
  public getCepBaseUrl() {
      const workUrl = this.configurations['cepBaseUrl'];
      if (this.configurations['cepBaseUrl']) {
        const cepBaseUrl: string = workUrl[workUrl.length - 1] === '/' ? this.configurations['cepBaseUrl'] : this.configurations['cepBaseUrl'] + '/';
        return cepBaseUrl;
      }
    return null;
    }

  public getVersion() {
    if (environment.VERSION) {
      return environment.VERSION;
    }
  return null;
  }

  public getVersionDate() {
    if (environment.VERSIONDATE) {
      return environment.VERSIONDATE;
    }
  return null;
  }

  public getDisabledLogin() {
    return (this.configurations['disableLogin'] as String).toLowerCase() === 'true' ? true : false;
  }
  
  /**
   * Check for globals cookie and return it if it exists.
   *     NOTE:  This has to be here instead of Session Service because circular references
  */
  getUser(){
    if (!this.user) {
      const globalValues = this._utilityService.getCookie();
      if (globalValues) {
        this.user = globalValues.currentUser.authdata;
      }
    }
    return this.user;
  }

  isLoggedIn() {
    return this.getUser() ? true : false;
  }

  getUserId() {
    if (!this.userId && this.isLoggedIn()) {
      const userData = this._utilityService.decodeData(this.user);
      this.userId = userData.substr(0, userData.indexOf(':'));
    }
    return this.userId;
  }

  getSSO() {
    if (!this.sso && this.isLoggedIn()) {
      const userData = this._utilityService.decodeData(this.user);
      this.sso = userData.indexOf('SSO') > -1  ? true : false ;
    }
    return this.sso;
  }

  logOff(checkSSO = true) {
    this._http.post(this.getFullUrl('/auth/logout', {}), {}, {headers: new HttpHeaders().set('X-XSRF-TOKEN', String(this._utilityService.getCsrfCookie()))})
      .toPromise()
      .then(res => {
        this._utilityService.deleteCookie();
        this._utilityService.clearSessionStorage();
        // this._utilityService.deleteCsrfCookie();
        this.user = null;
        this.userId = null;
        // if (checkSSO && this.getSSO()) {
        //   window.location.href = this.getSsoUrl() + 'perform_logout';
        // }
        window.location.href = "/";
      });
  }

}
