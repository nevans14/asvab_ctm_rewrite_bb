import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { MatDialog } from '@angular/material';
import { ConfigService } from './config.service';
import { ServiceContactUsDialogComponent } from 'app/core/dialogs/service-contact-us-dialog/service-contact-us-dialog.component';
import { AdvancedSearchService } from 'app/services/advanced-search.service';
import { UtilityService } from '../services/utility.service';

@Injectable({
  providedIn: 'root'
})
export class CareerDetailService {
  mcId: any;
  allFilter: any;

  constructor(
    private _http: HttpClient,
    private _configService: ConfigService,
    private _dialog: MatDialog,
    private _advancedSearchService: AdvancedSearchService,
    private _utility: UtilityService,
  ) { }

  getWhatTheyDo(mcId): Observable<any> {
    return this._http.get(this._configService.getAwsFullUrl( `/api/detail-page/get-what-they-do/${mcId}/`));
  }

  getTrainingProvided(mcId): Observable<any> {
    return this._http.get(this._configService.getAwsFullUrl( `/api/detail-page/get-training-provided/${mcId}/`));
  }

  getRelatedCareer(mcId): Observable<any> {
    return this._http.get(this._configService.getAwsFullUrl( `/api/detail-page/get-related-career/${mcId}/`));
  }

  getRelatedCivilianCareer(mcId): Observable<any> {
    return this._http.get(this._configService.getAwsFullUrl( `/api/detail-page/get-related-civilian-career/${mcId}/`));
  }

  getRelatedCivilianCareerByMocService(mocId, service): Observable<any> {
    return this._http.get(this._configService.getAwsFullUrl( `/api/detail-page/get-related-civilian-career/${mocId}/${service}/`));
  }

  getMilitaryCareer(mcId): Observable<any> {
    return this._http.get(this._configService.getAwsFullUrl( `/api/detail-page/get-military-career/${mcId}/`));
  }

  getServiceOffering(mcId): Observable<any> {    
    return this._http.get(this._configService.getAwsFullUrl( '/api/detail-page/get-service-offering/' + mcId + '/'));
  }

  getAdditionalInfo(mcId): Observable<any> {
    return this._http.get(this._configService.getAwsFullUrl( '/api/detail-page/get-additional-info/' + mcId + '/'));
  }

  getHotJobTitles(mcId): Observable<any> {
    return this._http.get(this._configService.getAwsFullUrl( '/api/CareerProfile/getHotJobTitle/' + mcId + '/'));
  }

  getHelpfulAttributes(mcId): Observable<any> {
    return this._http.get(this._configService.getAwsFullUrl( '/api/detail-page/get-helpful-attribute/' + mcId + '/'));
  }

  getCareerPathway(mcId): Observable<any> {
    return this._http.get(this._configService.getAwsFullUrl( '/api/detail-page/get-career-path/' + mcId + '/'));
  }

  getSkillRating(mcId): Observable<any> {
    return this._http.get(this._configService.getAwsFullUrl( '/api/detail-page/get-skill-rating/' + mcId + '/'));
  }

  getParentSkillRating(pMcId): Observable<any> {
    return this._http.get(this._configService.getAwsFullUrl( '/api/detail-page/get-skill-rating/' + pMcId + '/'));
  }

  getParentMilitaryCareer(pMcId): Observable<any> {
    return this._http.get(this._configService.getAwsFullUrl( '/api/detail-page/get-military-career/' + pMcId + '/'));
  }

  getOtherTitles(mcId): Observable<any> {
    return this._http.get(this._configService.getAwsFullUrl( '/api/detail-page/get-other-titles/' + mcId + '/'));
  }

  getArmyScores(mcId): Observable<any> {
    return this._http.get(this._configService.getAwsFullUrl( '/api/detail-page/get-see-score-army/' + mcId + '/'));
  }

  getServiceOfferingCompositeScore(mcId): Observable<any> {
    return this._http.get(this._configService.getAwsFullUrl( '/api/detail-page/get-other-titles/' + mcId + '/'));
  }

  getFeaturedProfile(mcId): Observable<any> {
    return this._http.get(this._configService.getAwsFullUrl( '/api/detail-page/get-featured-profiles/' + mcId + '/'));
  }

  getAirForceDetail(mcId, mocId): Observable<any> {
    return this._http.get(this._configService.getAwsFullUrl('/api/detail-page/get-air-force-detail/' + mcId + '/' + mocId + '/'));
  }

  getSpaceForceDetail(mcId, mocId): Observable<any> {
    return this._http.get(this._configService.getAwsFullUrl('/api/detail-page/get-air-force-detail/' + mcId + '/' + mocId + '/'));
  }

  getArmyDetail(mcId, mocId): Observable<any> {
    return this._http.get(this._configService.getAwsFullUrl('/api/detail-page/get-army-detail/' + mcId + '/' + mocId + '/'));
  }

  getCoastGuardDetail(mcId, mocId): Observable<any> {
    return this._http.get(this._configService.getAwsFullUrl('/api/detail-page/get-coast-guard-detail/' + mcId + '/' + mocId + '/'));
  }

  getMarineDetail(mcId, mocId): Observable<any> {
    return this._http.get(this._configService.getAwsFullUrl('/api/detail-page/get-marine-detail/' + mcId + '/' + mocId + '/'));
  }

  getNavyDetail(mcId, mocId): Observable<any> {
    return this._http.get(this._configService.getAwsFullUrl('/api/detail-page/get-navy-detail/' + mcId + '/' + mocId + '/'));
  }

  getProfile(mcId): Observable<any> {
    return this._http.get(this._configService.getAwsFullUrl('/api/detail-page/get-profile/' + mcId + '/'));
  }

  getWorkValues(mcId): Observable<any> {
    return this._http.get(this._configService.getAwsFullUrl( `/api/detail-page/get-work-values/${mcId}/`));
  }

  displayInfoIcon(serviceDetail) {
    if (serviceDetail) {
        switch (serviceDetail.svc) {
            case ('F'):
                // Air Force required courses
                let requiredHSCourses: String = '';
                if (serviceDetail.requirement) {
                  Object.keys(serviceDetail.requirement).forEach((key, index) => {
                      if (key.includes('hs') && serviceDetail.requirement[key] === 'Y') {
                          const courseName = key.split('hs').length === 2 ? key.split('hs')[1] : undefined;
                          if (courseName) {
                              if (!requiredHSCourses) {
                                  requiredHSCourses = courseName;
                              } else {
                                  requiredHSCourses = requiredHSCourses.concat(', ' + courseName)
                              }
                          }
                      }
                  })
                }

                if (!serviceDetail.specialty && !serviceDetail.task &&
                (!serviceDetail.requirement ||
                !serviceDetail.requirement.educationLevel ||
                serviceDetail.requirement.educationLevel === 'NONE' ||
                serviceDetail.requirement.educationLevel === 'NA') && !serviceDetail.basicTraining && 
                !serviceDetail.technicalTraining && 
                !serviceDetail.techSchoolLocation && !requiredHSCourses) {
                    return true;
                }
            break;
            case ('S'):
                if (!serviceDetail.specialty &&  !serviceDetail.task &&
                !serviceDetail.minEducation && !serviceDetail.basicTraining && 
                !serviceDetail.technicalTraining && 
                !serviceDetail.techSchoolLocation && !serviceDetail.desiredHSCourse) {
                    return true;
                }
            break;
            case ('A'):
                if (!serviceDetail.overview) {
                    return true;
                }
            break;
            case ('C'):
                if (!serviceDetail.description && ! serviceDetail.duties &&
                !serviceDetail.training && !serviceDetail.qualifications &&
                ! serviceDetail.specialNotes) {
                    return true;
                }
            break;
            case ('M'):
                if (!serviceDetail.description) {
                    return true;
                }
            break;
            case ('N'):
                if (!serviceDetail.rating && !serviceDetail.description && 
                !serviceDetail.physicalRequirement && !serviceDetail.minEducation &&
                !serviceDetail.basicTraining && !serviceDetail.technicalTrainingDuration &&
                !serviceDetail.techTrainingLocation && !serviceDetail.desiredHighSchoolCourses &&
                !serviceDetail.citizenshipRequirement) {
                    return true;
                }
            break;
        }
    } else {
        return true;
    }
    return false;
  }

  openServiceContactUsDialog(serviceName, serviceContacts, so) {
    let item;

    for (var i = 0; i < serviceContacts.length; i++) {
        if (serviceName == serviceContacts[i].name) {
            item = serviceContacts[i];
            break;
        }
    }

    if (serviceName && so) {
      switch(serviceName) {
        case 'Army':
          this._utility.ga('#CITM_CONTACTRECRUITERSENT_ARMY_'+so.mcId+'_'+so.mocId);
        case 'Army National Guard':
          this._utility.ga('#CITM_CONTACTRECRUITERSENT_ARMY_NATIONAL_GUARD_'+so.mcId+'_'+so.mocId);
        case 'Marine Corps':
          this._utility.ga('#CITM_CONTACTRECRUITERSENT_MARINE_'+so.mcId+'_'+so.mocId);
        case 'Navy':
          this._utility.ga('#CITM_CONTACTRECRUITERSENT_NAVY_'+so.mcId+'_'+so.mocId);
        case 'Air Force':
          this._utility.ga('#CITM_CONTACTRECRUITERSENT_AIR_FORCE_'+so.mcId+'_'+so.mocId);
        case 'Coast Guard':
          this._utility.ga('#CITM_CONTACTRECRUITERSENT_COAST_GUARD_'+so.mcId+'_'+so.mocId);
      }
    }

    this._dialog.open(ServiceContactUsDialogComponent, {
        data: item,
        panelClass: 'custom-dialog'
    });
  }

  checkEligible(mc = null, compositeScores = null, searchList = null, mcId = null, compScoreNum = null, armyScore = null) {
    let eligible = false;

    if (!mc && searchList && mcId) {
        mc = searchList.find(i => i.mcId === mcId);
    }

    if (mc && mc.isOfficer && mc.isOfficer === 'T') {
      return false;
    }

    if (mc && compositeScores && !armyScore) {
      eligible = this._advancedSearchService.checkScore(mc, compositeScores, compScoreNum);
    }

    if (mc && armyScore) {
      eligible = this._advancedSearchService.checkScore(mc, compositeScores, compScoreNum, armyScore);
    }

    return eligible;
  }

  // createMcIdParam(mcId) {
  //   const params = [];
  //   params.push({
  //     key: null,
  //     value: mcId
  //   });
  //   return params;
  // }

  // createServiceParam(mcId, mocId) {
  //   const params = [];
  //   params.push({
  //     key: null,
  //     value: mcId
  //   });
  //   params.push({
  //     key: null,
  //     value: mocId
  //   });
  //   return params;
  // }
}
