import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpHelperService } from './http-helper.service';
import { ConfigService } from './config.service';
import { Observable } from 'rxjs';
import { resolve } from 'q';

// const headers = new HttpHeaders()
// .set('Accept', 'application/json')
// .set('Content-Type', 'application/json');

@Injectable({
    providedIn: 'root'
})
export class PortfolioService {

    constructor(
        private _http: HttpClient,
        private _config: ConfigService,
        private _httpHelper: HttpHelperService,

    ) { }

    getUserSystem(): Observable<any> {
        return this._http.get(this._httpHelper.getFullUrl('portfolio/get-user-system/', null));
    }

    isPortfolioStarted(): Observable<any> {
        return this._http.get(this._httpHelper.getFullUrl('portfolio/PortfolioStarted/', null));
    }

    /**
       * Work Experience
      */
    getWorkExperience(): Observable<any> {
        return this._http.get(this._httpHelper.getFullUrl('portfolio/WorkExperience/', null));
    }

    insertWorkExperience(obj) {
        if (this._config.isLoggedIn()) {
            return this._httpHelper.httpHelper('post', 'portfolio/InsertWorkExperience', null, obj)
                .then((cfgObj) => cfgObj);
        }
    }

    updateWorkExperience(obj) {

        if (this._config.isLoggedIn()) {
            return this._httpHelper.httpHelper('put', 'portfolio/UpdateWorkExperience', null, obj)
                .then((cfgObj) => {
                    resolve(cfgObj);
                });
        }
    }

    deleteWorkExperience(id) {
        const params = [];
        params.push({
            key: null,
            value: id
        });

        return this._httpHelper.httpHelper('Delete', 'portfolio/DeleteWorkExperience/', params, null)
            .then((obj) => {
                resolve(obj);
            });
    }

    /**
      * Education
     */
    getEducation(): Observable<any> {
        return this._http.get(this._httpHelper.getFullUrl('portfolio/Education/', null));
    }

    insertEducation(obj) {
        if (this._config.isLoggedIn()) {
            return this._httpHelper.httpHelper('post', 'portfolio/InsertEducation', null, obj)
                .then((cfgObj) => cfgObj);
        }
    }

    updateWorkEducation(obj) {

        if (this._config.isLoggedIn()) {
            return this._httpHelper.httpHelper('put', 'portfolio/UpdateEducation', null, obj)
                .then((cfgObj) => {
                    resolve(cfgObj);
                });
        }
    }

    deleteEducation(id) {
        const params = [];
        params.push({
            key: null,
            value: id
        });

        return this._httpHelper.httpHelper('Delete', 'portfolio/DeleteEducation/', params, null)
            .then((obj) => {
                resolve(obj);
            });
    }

    /**
      * Achievements
     */
    getAchievements(): Observable<any> {
        return this._http.get(this._httpHelper.getFullUrl('portfolio/Achievement/', null));
    }

    insertAchievements(obj) {

        if (this._config.isLoggedIn()) {
            return this._httpHelper.httpHelper('post', 'portfolio/InsertAchievement', null, obj)
                .then((cfgObj) => cfgObj);
        }
    }

    updateAchievement(obj) {

        if (this._config.isLoggedIn()) {
            return this._httpHelper.httpHelper('put', 'portfolio/UpdateAchievement', null, obj)
                .then((cfgObj) => {
                    resolve(cfgObj);
                });
        }
    }

    deleteAchievement(id) {
        const params = [];
        params.push({
            key: null,
            value: id
        });

        return this._httpHelper.httpHelper('Delete', 'portfolio/DeleteAchievement/', params, null)
            .then((obj) => {
                resolve(obj);
            });
    }

    /**
       * Interests
      */
    getInterests(): Observable<any> {
        return this._http.get(this._httpHelper.getFullUrl('portfolio/Interest/', null));
    }

    insertInterest(obj) {

        if (this._config.isLoggedIn()) {
            return this._httpHelper.httpHelper('post', 'portfolio/InsertInterest', null, obj)
                .then((intObj) => {
                    resolve(intObj);
                });
        }
    }

    updateInterest(obj) {

        if (this._config.isLoggedIn()) {
            return this._httpHelper.httpHelper('put', 'portfolio/UpdateInterest', null, obj)
                .then((intObj) => {
                    resolve(intObj);
                });
        }
    }

    deleteInterest(id) {
        const params = [];
        params.push({
            key: null,
            value: id
        });

        return this._httpHelper.httpHelper('Delete', 'portfolio/DeleteInterest/', params, null)
            .then((obj) => {
                resolve(obj);
            });
    }

    /**
       *Skills
      */
    getSkill(): Observable<any> {
        return this._http.get(this._httpHelper.getFullUrl('portfolio/Skill/', null));
    }

    insertSkill(obj) {

        if (this._config.isLoggedIn()) {
            return this._httpHelper.httpHelper('post', 'portfolio/InsertSkill', null, obj)
                .then((cfgObj) => {
                    resolve(cfgObj);
                });
        }
    }

    updateSkill(obj) {

        if (this._config.isLoggedIn()) {
            return this._httpHelper.httpHelper('put', 'portfolio/UpdateSkill', null, obj)
                .then((cfgObj) => {
                    resolve(cfgObj);
                });
        }
    }

    deleteSkill(id) {
        const params = [];
        params.push({
            key: null,
            value: id
        });

        return this._httpHelper.httpHelper('Delete', 'portfolio/DeleteSkill/', params, null)
            .then((obj) => {
                resolve(obj);
            });
    }

    /**
       * Work values
      */
    getWorkValues(): Observable<any> {
        return this._http.get(this._httpHelper.getFullUrl('portfolio/WorkValue/', null));
    }

    insertWorkValue(obj) {

        if (this._config.isLoggedIn()) {
            return this._httpHelper.httpHelper('post', 'portfolio/InsertWorkValue', null, obj)
                .then((cfgObj) => {
                    resolve(cfgObj);
                });
        }
    }

    updateWorkValue(obj) {

        if (this._config.isLoggedIn()) {
            return this._httpHelper.httpHelper('put', 'portfolio/UpdateWorkValue', null, obj)
                .then((cfgObj) => {
                    resolve(cfgObj);
                });
        }
    }

    deleteWorkValue(id) {
        const params = [];
        params.push({
            key: null,
            value: id
        });

        return this._httpHelper.httpHelper('Delete', 'portfolio/DeleteWorkValue/', params, null)
            .then((obj) => {
                resolve(obj);
            });
    }

    /**
       * Volunteer values
      */
    getVolunteers(): Observable<any> {
        return this._http.get(this._httpHelper.getFullUrl('portfolio/Volunteer/', null));
    }

    insertVolunteer(obj) {

        if (this._config.isLoggedIn()) {
            return this._httpHelper.httpHelper('post', 'portfolio/InsertVolunteer', null, obj)
                .then((cfgObj) => {
                    resolve(cfgObj);
                });
        }
    }

    updateVolunteer(obj) {

        if (this._config.isLoggedIn()) {
            return this._httpHelper.httpHelper('put', 'portfolio/UpdateVolunteer', null, obj)
                .then((cfgObj) => {
                    resolve(cfgObj);
                });
        }
    }

    deleteVolunteer(id) {
        const params = [];
        params.push({
            key: null,
            value: id
        });

        return this._httpHelper.httpHelper('Delete', 'portfolio/DeleteVolunteer/', params, null)
            .then((obj) => {
                resolve(obj);
            });
    }

    /**
       * ACT score  values
      */
    getActScore(): Observable<any> {
        return this._http.get(this._httpHelper.getFullUrl('portfolio/ACT/', null));
    }

    insertActScore(obj) {

        if (this._config.isLoggedIn()) {
            return new Promise((resolve, reject) => {
                this._httpHelper.httpHelper('post', 'portfolio/InsertACT', null, obj)
                    .then((cfgObj) => {
                        resolve(cfgObj);
                    });
            });
        }
    }

    updateActScore(obj) {

        if (this._config.isLoggedIn()) {
            return this._httpHelper.httpHelper('put', 'portfolio/UpdateACT', null, obj)
                .then((cfgObj) => {
                    resolve(cfgObj);
                });
        }
    }

    deleteActScore(id) {
        const params = [];
        params.push({
            key: null,
            value: id
        });

        return this._httpHelper.httpHelper('Delete', 'portfolio/DeleteACT/', params, null)
            .then((obj) => {
                resolve(obj);
            });
    }

    /**
       * ASVAB score  values
      */
    getAsvabScore(): Observable<any> {
        return this._http.get(this._httpHelper.getFullUrl('portfolio/ASVAB/', null));
    }

    insertAsvabScore(obj) {

        if (this._config.isLoggedIn()) {
            return new Promise((resolve, reject) => {
                this._httpHelper.httpHelper('post', 'portfolio/InsertASVAB', null, obj)
                    .then((cfgObj) => {
                        resolve(cfgObj);
                    });
            });
        }
    }

    updateAsvabScore(obj) {

        if (this._config.isLoggedIn()) {
            return this._httpHelper.httpHelper('put', 'portfolio/UpdateASVAB', null, obj)
                .then((cfgObj) => {
                    resolve(cfgObj);
                });
        }
    }

    deleteAsvabScore(id) {
        const params = [];
        params.push({
            key: null,
            value: id
        });

        return this._httpHelper.httpHelper('Delete', 'portfolio/DeleteASVAB/', params, null)
            .then((obj) => {
                resolve(obj);
            });
    }

    /**
       * FYI score  values
      */
    getFyiScore(): Observable<any> {
        return this._http.get(this._httpHelper.getFullUrl('portfolio/FYI/', null));
    }

    insertFyiScore(obj) {

        if (this._config.isLoggedIn()) {
            return this._httpHelper.httpHelper('post', 'portfolio/InsertFYI', null, obj)
                .then((cfgObj) => {
                    resolve(cfgObj);
                });
        }
    }

    updateFyiScore(obj) {

        if (this._config.isLoggedIn()) {
            return this._httpHelper.httpHelper('put', 'portfolio/UpdateFYI', null, obj)
                .then((cfgObj) => {
                    resolve(cfgObj);
                });
        }
    }

    deleteFyiScore(id) {
        const params = [];
        params.push({
            key: null,
            value: id
        });

        return this._httpHelper.httpHelper('Delete', 'portfolio/DeleteFYI/', params, null)
            .then((obj) => {
                resolve(obj);
            });
    }

    /**
       * SAT score  values
      */
    getSatScore(): Observable<any> {
        return this._http.get(this._httpHelper.getFullUrl('portfolio/SAT/', null));
    }

    insertSatScore(obj) {

        if (this._config.isLoggedIn()) {
            return new Promise((resolve, reject) => {
                this._httpHelper.httpHelper('post', 'portfolio/InsertSAT', null, obj)
                    .then((cfgObj) => {
                        resolve(cfgObj);
                    });
            });
        }
    }

    updateSatScore(obj) {

        if (this._config.isLoggedIn()) {
            return this._httpHelper.httpHelper('put', 'portfolio/UpdateSAT', null, obj)
                .then((cfgObj) => {
                    resolve(cfgObj);
                });
        }
    }

    deleteSatScore(id) {
        const params = [];
        params.push({
            key: null,
            value: id
        });

        return this._httpHelper.httpHelper('Delete', 'portfolio/DeleteSAT/', params, null)
            .then((obj) => {
                resolve(obj);
            });
    }

    /**
       * Other score  values
      */
    getOtherScore(): Observable<any> {
        return this._http.get(this._httpHelper.getFullUrl('portfolio/Other/', null));
    }

    insertOtherScore(obj) {

        if (this._config.isLoggedIn()) {
            return new Promise((resolve, reject) => {
                this._httpHelper.httpHelper('post', 'portfolio/InsertOther', null, obj)
                    .then((cfgObj) => {
                        resolve(cfgObj);
                    });
            });
        }
    }

    updateOtherScore(obj) {

        if (this._config.isLoggedIn()) {
            return this._httpHelper.httpHelper('put', 'portfolio/UpdateOther', null, obj)
                .then((cfgObj) => {
                    resolve(cfgObj);
                });
        }
    }

    deleteOtherScore(id) {
        const params = [];
        params.push({
            key: null,
            value: id
        });

        return this._httpHelper.httpHelper('Delete', 'portfolio/DeleteOther', params, null)
            .then((obj) => {
                resolve(obj);
            });
    }

    /**
       *  Plan of action
      */
    getPlan(): Observable<any> {
        return this._http.get(this._httpHelper.getFullUrl('portfolio/Plan/', null));
    }

    insertPlan(obj) {

        if (this._config.isLoggedIn()) {
            return this._httpHelper.httpHelper('post', 'portfolio/InsertPlan', null, obj)
                .then((cfgObj) => {
                    resolve(cfgObj);
                });
        }
    }

    updatePlan(obj) {

        if (this._config.isLoggedIn()) {
            return this._httpHelper.httpHelper('put', 'portfolio/UpdatePlan', null, obj)
                .then((cfgObj) => {
                    resolve(cfgObj);
                });
        }
    }

    deletePlan(id) {
        const params = [];
        params.push({
            key: null,
            value: id
        });

        return this._httpHelper.httpHelper('Delete', 'portfolio/Plan', params, null)
            .then((obj) => {
                resolve(obj);
            });
    }

    selectNameGrade(): Observable<any> {
        const params = [];

        return this._http.get(this._httpHelper.getFullUrl('portfolio/selectNameGrade/', params));
    }

    updateInsertUserInfo(obj) {

        if (this._config.isLoggedIn()) {
            return this._httpHelper.httpHelper('put', 'portfolio/UpdateInsertUserInfo', null, obj)
                .then((cfgObj) => {
                    resolve(cfgObj);
                });
        }
    }

    getUserInfo(): Observable<any> {
        return this._http.get(this._httpHelper.getFullUrl('portfolio/get-user-info/', null));
    }

    /**
       *  Physical Fitness
      */
    getPhysicalFitness(): Observable<any> {
        return this._http.get(this._httpHelper.getFullUrl('portfolio/PhysicalFitness/', null));
    }

    insertPhysicalFitness(obj) {

        if (this._config.isLoggedIn()) {
            return new Promise((resolve, reject) => {
                this._httpHelper.httpHelper('post', 'portfolio/InsertPhysicalFitness', null, obj)
                    .then((cfgObj) => {
                        console.log(cfgObj);
                        resolve(cfgObj);
                    });
            });
        }
    }

    updatePhysicalFitness(obj) {

        if (this._config.isLoggedIn()) {
            return this._httpHelper.httpHelper('put', 'portfolio/UpdatePhysicalFitness', null, obj)
                .then((cfgObj) => {
                    resolve(cfgObj);
                });
        }
    }

    deletePhysicalFitness(id) {
        const params = [];
        params.push({
            key: null,
            value: id
        });

        return this._httpHelper.httpHelper('Delete', 'portfolio/DeletePhysicalFitness', params, null)
            .then((obj) => {
                resolve(obj);
            });
    }
    //
    getScoreSummary(): Observable<any> {
        return this._http.get(this._httpHelper.getFullUrl('testScore/get-test-score/', null));
    }

    getAccessCode(): Observable<any> {
        return this._http.get(this._httpHelper.getFullUrl('testScore/get-access-code/', null));
    }

    getScoreSummarybyAccessCode(accessCode): Observable<any> {
        const params = this.createIdParam(accessCode);

        return this._http.get(this._httpHelper.getFullUrl('testScore/get-test-score-access-code/', params));
    }

    getCepFavoriteOccupations(): Observable<any> {
        return this._http.get(this._httpHelper.getFullUrl('portfolio/getFavoriteOccupation/', null));
    }

    getRelatedCareerList(): Observable<any> {
        return this._http.get(this._httpHelper.getFullUrl('portfolio/get-related-career-list/', null));
    }

    getResourceSpecific(): Observable<any> {

        return this._http.get(this._httpHelper.getFullUrl('portfolio/get-specific-resource', null));
    }

    getResourceGeneric(): Observable<any> {

        return this._http.get(this._httpHelper.getFullUrl('portfolio/get-generic-resource', null));
    }

    createIdParam(id) {
        const params = [];
        params.push({
            key: null,
            value: id
        });
        return params;
    }

    getFromUrl(url): Observable<any> {
        return this._http.get(url);
    }

}
