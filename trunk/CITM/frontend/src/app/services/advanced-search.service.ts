import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material';
import { MessageDialogComponent } from 'app/core/dialogs/message-dialog/message-dialog.component';
import { SaveSearchComponent } from 'app/core/dialogs/save-search/save-search.component';
import { ScoreService } from 'app/services/score.service';

@Injectable({
  providedIn: 'root'
})
export class AdvancedSearchService {
  currentSearchSelections: any;

  constructor(
    private _dialog: MatDialog,
    private _scoreService: ScoreService,
  ) { }

  verifySaveSearch(valuesList, saveSearchList) {
    if (saveSearchList.length === 5) {
      const data = {
        'title': 'Alert',
        'message': 'You have reached the saved search limit.'
      };
      const dialogRef = this._dialog.open(MessageDialogComponent, {
        data: data,
        hasBackdrop: true,
        disableClose: true
      });
      return false;
    }

    if (valuesList.length < 1) {
      const data = {
        'title': 'Alert',
        'message': 'There are no selections to save. Make selections and try again.'
      };
      const dialogRef = this._dialog.open(MessageDialogComponent, {
        data: data,
      });
      return false;
    }
    return true;
  }

  saveSearch(searchValues) {

    const dialogRef2 = this._dialog.open(SaveSearchComponent, {
      data: { searchValues: searchValues, success: false }
    });

    let isSuccess = false;
    dialogRef2.afterClosed().subscribe(results => {
      isSuccess = results;
    });

    return isSuccess;
  }

  filterList(searchCriteria, searchListObject, skill, tabName, compositeScores = {}) {
    const filteredList = [];
    const filteredInterests = this.getFilteredInterest(searchCriteria);
    const filteredWorkValues = this.getFilteredWorkValues(searchCriteria);

    for (let i = 0; i < searchListObject.length; i++) {
      let addItem = false;
      const item = searchListObject[i];
      const searchWord = searchCriteria.keyword ? searchCriteria['keyword'] : '';
      addItem = this.checkKeyword(item, searchWord);
      if (!addItem) {
        console.debug('Failed Keyword for ' + item.title);
        continue;
      }
      addItem = this.checkServices(searchCriteria, item);
      if (!addItem) {
        console.debug('Failed Service for ' + item.title);
        continue;
      }
      addItem = this.checkType(searchCriteria, item, compositeScores);
      if (!addItem) {
        console.debug('Failed Type for ' + item.title);
        continue;
      }
      addItem = this.checkInterests(searchCriteria, item, filteredInterests)
      if (!addItem) {
        console.debug('Failed Interests for ' + item.title);
        continue;
      }
      addItem = this.checkWorkValues(searchCriteria, item, filteredWorkValues)
      if (!addItem) {
        console.debug('Failed Work Value for ' + item.title);
        continue;
      }
      addItem = this.checkFeaturedCategory(searchCriteria, item);
      if (!addItem) {
        console.debug('Failed FC.');
        continue;
      }

      addItem = this.checkSkillImportance(skill, tabName, item);

      if (addItem) {
        filteredList.push(item);
      }
    }
    return filteredList;
  }

  checkKeyword(item, searchword) {
    if (searchword) {
      searchword = searchword.toLowerCase();
    if (!searchword ||
      item.title.toLowerCase().indexOf(searchword) !== -1 ||
      (item.mocTitle && item.mocTitle.toLowerCase().indexOf(searchword) !== -1) ||
      (this.checkOtherTitles(item.mocs, searchword)) ||
      (this.checkOtherTitles(item.alsoCalled, searchword))) {
      return true;
    }
  } else {
    return true;
  }
    return false;
  }

  checkOtherTitles(titles, searchword) {
    if (titles) {
      if (!(titles instanceof Array)) {
        titles = titles.split(';');
      }
      for (let i = 0; i < titles.length; i++) {
        if ((titles[i].mocTitle && (titles[i].mocTitle.toLowerCase()).indexOf(searchword) !== -1) ||
        (titles[i].alsoCalled && (titles[i].alsoCalled.toLowerCase()).indexOf(searchword) !== -1) ||
        (titles[i].mocId && (titles[i].mocId.toLowerCase()).indexOf(searchword) !== -1) ||
        (titles[i] instanceof String && (titles[i].toLowerCase()).indexOf(searchword) !== -1)) {
          return true;
        }
      }
    }
    return false;
  }

  checkServices(searchCriteria, item) {
    if ((!searchCriteria.army && !searchCriteria.marines && !searchCriteria.airForce
      && !searchCriteria.navy && !searchCriteria.coastGuard && !searchCriteria.spaceForce) ||
      (searchCriteria.army && item.isArmy === 'T') ||
      (searchCriteria.marines && item.isMarines === 'T') ||
      (searchCriteria.airForce && item.isAirForce === 'T') ||
      (searchCriteria.navy && item.isNavy === 'T') ||
      (searchCriteria.coastGuard && item.isCoastGuard === 'T') ||
      (searchCriteria.spaceForce && item.isSpaceForce === 'T')) {
      return true;
    }
    return false;
  }

  checkAdditionalRequirementScore(moc, compositeScores, std) {
    if (moc.svc === 'F' && moc.compScoreTwo) {
      let userScore = this._scoreService.afUserCompositeScores(compositeScores, moc.compScoreTwo)
      if (Math.abs(userScore - moc.requiredScoreTwo) <= std) {
        return true;
      }
      return false;
    }

    if(moc.svc === 'C' && moc.additionalRequirementOne) {
      const userScore = parseInt(this._scoreService.getTestScore(compositeScores, this._scoreService.getTestAbbreviation(moc.additionalRequirementOne)));
      const additionalRequirement = moc.additionalRequirementOne.split(' ');
      if (Math.abs(userScore - parseInt(additionalRequirement[additionalRequirement.length - 1])) <= std) {
        return true;
      }
      return false;
    }
    return false;
  }

  getUserScore(item, compositeScores, compScoreNum = null, armyItem = null) {
    if (item.svc === 'N' && (compScoreNum === 1 || !compScoreNum))
      return this._scoreService.navyUserCompositeScores(compositeScores, item.compScoreOne);
    else if (item.svc === 'N' && compScoreNum === 2)
      return this._scoreService.navyUserCompositeScores(compositeScores, item.compScoreTwo);

    if (item.svc === 'F' && (compScoreNum === 1 || !compScoreNum))
      return this._scoreService.afUserCompositeScores(compositeScores, item.compScoreOne);
    else if (item.svc === 'F' && compScoreNum === 2)
      return this._scoreService.afUserCompositeScores(compositeScores, item.compScoreTwo);

    if ((item.svc === 'A' || !item.svc )&& (compScoreNum === 1 || !compScoreNum))
      return this._scoreService.armyUserCompositeScores(compositeScores, armyItem.compositeCd);

    if (item.svc === 'C' && (compScoreNum === 1 || !compScoreNum))
      return this._scoreService.coastGuardUserCompositeScores(compositeScores, item.compScoreOne);
    else if (item.svc === 'C' && compScoreNum === 2)
      return this._scoreService.coastGuardUserCompositeScores(compositeScores, item.compScoreTwo);
  }

  checkScoreWithCondition(item, compScoreNum, compositeScores, std, armyScore = null) {

    let qualified = false;

    if (item.compScoreOne && (compScoreNum === 1 || !compScoreNum)) {
      const userScore = this.getUserScore(item, compositeScores, compScoreNum);
      if (Math.abs(userScore - item.requiredScoreOne) <= std) {
        qualified = true;
      }
    }

    if (item.compScoreTwo && !item.condition && compScoreNum === 2) {
      const userScore = this.getUserScore(item, compositeScores, compScoreNum);
      if (Math.abs(userScore - item.requiredScoreTwo) <= std) {
        qualified = true;
      }
    }

    if (qualified && item.condition && item.condition.toLowerCase() === 'and' 
      || !qualified && item.condition && item.condition.toLowerCase() === 'or' ) {
        qualified = this.checkAdditionalRequirementScore(item, compositeScores, std);
    }

    if ((item.svc === 'A' || !item.svc) && armyScore) {
      const armyItem = armyScore.filter(as => as.mocId === item.mocId && as.mcId === item.mcId && as.score === item.score)[0];
      if (armyItem) {
        const userScore = this.getUserScore(item, compositeScores, compScoreNum, armyItem);
        if (Math.abs(userScore - armyItem.score) <= std) {
          qualified = true;
        }
      }
    }
    
    return qualified;
  }


  checkScore(item, compositeScores, compScoreNum = null, armyScore = null) {

    let qualified = false;
    const std = 10;

    if(!item) {
      return qualified;
    }

    if (item.isAirForce === 'T') {
      item.mocs.forEach(moc => {
        if (!qualified && moc.svc === 'F' && moc.compScoreOne) {
          qualified = this.checkScoreWithCondition(moc, compScoreNum, compositeScores, std);
        }
      }) 
    } else {
      if (!qualified && item.svc === 'F' && item.compScoreOne) {
        qualified = this.checkScoreWithCondition(item, compScoreNum, compositeScores, std);
      }
    }
    
    if (item.isArmy === 'T' && !qualified) {
      item.mocs.forEach(moc => {
        if (!qualified && moc.svc === 'A' && moc.compScoreOne) {
          qualified = this.checkScoreWithCondition(moc, compScoreNum, compositeScores, std);
        }
      })
    } else if (!qualified && item.svc === 'A' && item.compScoreOne) {
      qualified = this.checkScoreWithCondition(item, compScoreNum, compositeScores, std);
    } else if (!qualified && (item.svc === 'A' || !item.svc) && armyScore) {
      qualified = this.checkScoreWithCondition(item, compScoreNum, compositeScores, std, armyScore);
    }
    
    if (item.isCoastGuard === 'T' && !qualified) {
      item.mocs.forEach(moc => {
        if (!qualified && moc.svc === 'C' && moc.compScoreOne) {
          qualified = this.checkScoreWithCondition(moc, compScoreNum, compositeScores, std);
        }
      })  
    } else {
      if (!qualified && item.svc === 'C' && item.compScoreOne) {
        qualified = this.checkScoreWithCondition(item, compScoreNum, compositeScores, std);
      }
    }

    if (item.isNavy === 'T' && !qualified) {
      item.mocs.forEach(moc => {
        if (!qualified && moc.svc === 'N' && moc.compScoreOne) {
          qualified = this.checkScoreWithCondition(moc, compScoreNum, compositeScores, std);
        }
      })
    } else {
      if (!qualified && item.svc === 'N' && item.compScoreOne) {
        qualified = this.checkScoreWithCondition(item, compScoreNum, compositeScores, std);
      }
    }

    return qualified;
  }

  checkType(searchCriteria, item, compositeScores = {}) {
    if ((!searchCriteria.enlisted && !searchCriteria.officer) ||
      (searchCriteria.enlisted && item.isEnlisted === 'T' && !searchCriteria.enlistedEligible) ||
      (searchCriteria.officer && item.isOfficer === 'T') ||
      (searchCriteria.enlisted
        && item.isEnlisted === 'T'
        && searchCriteria.enlistedEligible
        && this.checkScore(item, compositeScores))) {
      return true;
    } else {
      return false;
    }
  }

  checkInterests(searchCriteria, item, filteredInterests) {
    // console.debug('filteredInterests: ' + JSON.stringify(filteredInterests))
    // console.debug(`Title ${item.title}  1: ${item.interestOne} 2: ${item.interestTwo} 3: ${item.interestThree}`)

    let itemInterests = [];
    if (item.interestOne !== null) {
      itemInterests.push(item.interestOne);
    }
    if (item.interestTwo !== null) {
      itemInterests.push(item.interestTwo);
    }
    if (item.interestThree !== null) {
      itemInterests.push(item.interestThree);
    }

    let containsAll = true;
    filteredInterests.some(interest => {
      if (itemInterests.indexOf(interest) <= -1) {
        containsAll = containsAll && false;
        return !containsAll;
      }
      return !containsAll;
    })

    if (!filteredInterests || filteredInterests.length === 0 || containsAll) {
      return true;
    }
    return false;
  }

  checkWorkValues(searchCriteria, item, filteredWorkValues) {
    let itemWorkValues = [];
    
    if (item.workValueOne > 0) {
      itemWorkValues.push(item.workValueOne);
    }
    if (item.workValueTwo > 0) {
      itemWorkValues.push(item.workValueTwo);
    }
    if (item.workValueThree > 0) {
      itemWorkValues.push(item.workValueThree);
    }

    let containsAll = true;
    filteredWorkValues.some(value => {
      if (itemWorkValues.indexOf(value) <= -1) {
        containsAll = containsAll && false;
        return !containsAll;
      }
      return !containsAll;
    })

    if (!filteredWorkValues || filteredWorkValues.length === 0 || containsAll) {
      return true;
    }
    return false;
  }

  checkCareerCluster(searchCriteria, item, filteredCC) {
    if ((!filteredCC || filteredCC.length === 0) || filteredCC.indexOf(item.ccId) > -1) {
      // Remove these code checking in the work_environment table
    // (filteredCC.indexOf(item.ccId) > -1 || filteredCC.indexOf(item.codeOne) > -1 ||
    // filteredCC.indexOf(item.codeTwo) > -1 || filteredCC.indexOf(item.codeThree) > -1 ||
    // filteredCC.indexOf(item.codeFour) > -1 )) {
   return true;
 }
 return false;

 // Left this in in case they want to go to searching for occupations with all Career clusters 
    // if (!filteredCC || filteredCC.length === 0) {
    //   return true;
    // } else {
    //   for (let i = 0; i < filteredCC.length; i++) {
    //     if ()
        // if (filteredCC[i] !== item.ccId && filteredCC[i] !== item.codeOne &&
        // filteredCC[i] !== item.codeTwo && filteredCC[i] !== item.codeThree &&
        // filteredCC[i] !== item.codeFour) {
    //       return false;
    //     }
    //   }
    // }
    // return true;
  }

  checkFeaturedCategory(searchCriteria, item) {
    if (!searchCriteria.hotJob || (searchCriteria.hotJob &&  item.isHotJob === 'T'))  {
      return true;
    }
    return false;
  }

  checkSkillImportance(skill, tabName, item) {
    switch (tabName) {
      case ('Most'):
        if (skill === 'math' && (item.math >= 4 && item.math <= 5)) {
          return true;
        }
        if (skill === 'verbal' && (item.verbal >= 4 && item.verbal <= 5)) {
          return true;
        }
        if (skill === 'science' && (item.science >= 4 && item.science <= 5)) {
          return true;
        }
        return false;

      case ('Moderate'):
        if (skill === 'math' && (item.math >= 2.5 && item.math <= 3.5)) {
          return true;
        }
        if (skill === 'verbal' && (item.verbal >= 2.5 && item.verbal <= 3.5)) {
          return true;
        }
        if (skill === 'science' && (item.science >= 2.5 && item.science <= 3.5)) {
          return true;
        }
        return false;

      case ('Least'):
        if (skill === 'math' && (item.math >= 0 && item.math <= 2)) {
          return true;
        }
        if (skill === 'verbal' && (item.verbal >= 0 && item.verbal <= 2)) {
          return true;
        }
        if (skill === 'science' && (item.science >= 0 && item.science <= 2)) {
          return true;
        }
        return false;
      default:
        return true;
    }
  }

  getFilteredInterest(searchCriteria) {
    const filteredInterests = [];

    if (searchCriteria.realistic) {
      filteredInterests.push('R');
    }
    if (searchCriteria.investigative) {
      filteredInterests.push('I');
    }
    if (searchCriteria.artistic) {
      filteredInterests.push('A');
    }
    if (searchCriteria.social) {
      filteredInterests.push('S');
    }
    if (searchCriteria.enterprising) {
      filteredInterests.push('E');
    }
    if (searchCriteria.conventional) {
      filteredInterests.push('C');
    }
    return filteredInterests;
  }

  getFilteredWorkValues(searchCriteria) {
    const filteredWorkValues = [];

    if (searchCriteria.achievement) {
      filteredWorkValues.push(1);
    }
    if (searchCriteria.independence) {
      filteredWorkValues.push(2);
    }
    if (searchCriteria.recognition) {
      filteredWorkValues.push(3);
    }
    if (searchCriteria.relationships) {
      filteredWorkValues.push(4);
    }
    if (searchCriteria.support) {
      filteredWorkValues.push(5);
    }
    if (searchCriteria.workingconditions) {
      filteredWorkValues.push(6);
    }
    return filteredWorkValues;
  }
 
  getFilteredCC(searchCriteria) {
    const filteredCC = [];

    if (searchCriteria.ccOne) {
      filteredCC.push(1);
    }
    if (searchCriteria.ccTwo) {
      filteredCC.push(2);
    }
    if (searchCriteria.ccThree) {
      filteredCC.push(3);
    }
    if (searchCriteria.ccFour) {
      filteredCC.push(4);
    }
    if (searchCriteria.ccFive) {
      filteredCC.push(5);
    }
    if (searchCriteria.ccSix) {
      filteredCC.push(6);
    }
    if (searchCriteria.ccSeven) {
      filteredCC.push(7);
    }
    if (searchCriteria.ccEight) {
      filteredCC.push(8);
    }
    if (searchCriteria.ccNine) {
      filteredCC.push(9);
    }
    if (searchCriteria.ccTen) {
      filteredCC.push(10);
    }
    if (searchCriteria.ccEleven) {
      filteredCC.push(11);
    }
    if (searchCriteria.ccTwelve) {
      filteredCC.push(12);
    }
    if (searchCriteria.ccThirteen) {
      filteredCC.push(13);
    }
    if (searchCriteria.ccFourteen) {
      filteredCC.push(14);
    }
    if (searchCriteria.ccFifteen) {
      filteredCC.push(15);
    }
    if (searchCriteria.ccSixteen) {
      filteredCC.push(16);
    }
    return filteredCC;
  }

}