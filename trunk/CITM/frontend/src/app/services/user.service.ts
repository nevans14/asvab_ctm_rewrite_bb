import { Injectable } from '@angular/core';
import { HttpHelperService } from './http-helper.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

const headers = new HttpHeaders()
  .set('Accept', 'application/json')
  .set('Content-Type', 'application/json');

@Injectable({
  providedIn: 'root'
})

export class UserService {
  public favoritesListObject: any;
  public hiddenListObject: any;
  public favoriteCareerClusterLIstObject: any;
  public savedSearchesLIstObject: any;
  public notesLIstObject: any;
  public careerClusterNotesListObject: any;

  constructor(
    private _http: HttpClient,
    private _httpHelper: HttpHelperService,
  ) { }

  /**
 * Get Favorites
*/
  getFavoritesList(): Observable<any> {
    return this._http.get(this._httpHelper.getFullUrl('favorite/getFavoriteOccupation/', null));
  }

  getFavoriteServices(): Observable<any> {

    return this._http.get(this._httpHelper.getFullUrl('favorite/get-favorite-service/', null));
  }

  getFavoriteCareerCluster(): Observable<any> {
    return this._http.get(this._httpHelper.getFullUrl('favorite/getFavoriteCareerCluster/', null));
  }

  getFavoriteListPromise() {
    return new Promise((resolve, reject) => {
      const self = this;
      return this._httpHelper.httpHelper('get', 'favorite/getFavoriteOccupation/', null, null)
      .then((favorites) => {
        self.favoritesListObject = favorites;
        resolve();
      })
      .catch((error) => {
        reject(error);
      });
    });
  }

  /**
 * Insert Favorites
*/
  insertFavorite(mcId, title, isHotJob) {
    return new Promise((resolve, reject) => {
      const favoriteObject = {
        'mcId': mcId
      };

      return this._httpHelper.httpHelper('post', 'favorite/insertFavoriteOccupation', null, favoriteObject)
        .then((data) => {
          if (!this.favoritesListObject) {
            this.favoritesListObject = [];
          }

          // data.title = title;
          // data.idHotJob = isHotJob;
          console.log(data);
          this.favoritesListObject.push(data);
          resolve(this.favoritesListObject);
        });
    });
  }

  insertFavoriteService(id) {
    const favoriteObject = {
      'svcId': id,
    };

    return this._httpHelper.httpHelper('post', 'favorite/insert-favorite-service', null, favoriteObject)
      .then((data) => {
        console.log(data);
        return data;
      });
  }

  insertFavoriteCareerCluster(id) {
    const favoriteObject = {
      'ccId': id,
    };

    return this._httpHelper.httpHelper('post', 'favorite/insertFavoriteCareerCluster', null, favoriteObject)
      .then((obj) => {
        return obj;
      });
  }

  /**
* Delete Favorites
*/
  deleteFavorite(mcId) {
    return new Promise((resolve, reject) => {

      let id;
      for (let i = 0; i < this.favoritesListObject.length; i++) {
        if (this.favoritesListObject[i].mcId === mcId) {
          id = this.favoritesListObject[i].id;
          break;
        }
      }
      const params = this.createIdParam(id);

      return this._httpHelper.httpHelper('Delete', 'favorite/deleteFavoriteOccupation/', params, null)
        .then((obj) => {
          for (let i = 0; i < this.favoritesListObject.length; i++) {
            if (this.favoritesListObject[i] === id) {
              this.favoritesListObject.splice(i, 1);
            }
          }
          resolve(this.favoritesListObject);
        });
    });
  }

  deleteFavoriteService(id) {
    return new Promise((resolve, reject) => {

      const params = this.createIdParam(id);

      return this._httpHelper.httpHelper('Delete', 'favorite/delete-favorite-service/', params, null)
        .then((obj) => {
          resolve(obj);
        });
    });
  }

  deleteFavoriteCareerCluster(id) {
    return new Promise((resolve, reject) => {
      const params = this.createIdParam(id);

      return this._httpHelper.httpHelper('Delete', 'favorite/deleteFavoriteCareerCluster/', params, null)
        .then((obj) => {
          resolve(obj);
        });
    });
  }

  /**
	 * Hidden
	*/
  getHiddenList(): Observable<any> {

    return this._http.get(this._httpHelper.getFullUrl('detail-page/get-hide-job-list/', null));

  }

  getHiddenListPromise() {
    return new Promise((resolve, reject) => {

      const self = this;
      return this._httpHelper.httpHelper('get', 'detail-page/get-hide-job-list/', null, null)
      .then((hiddenList) => {
        self.hiddenListObject = hiddenList;
        resolve(hiddenList);
      })
      .catch((error) => {
        reject(error);
      });
    });
  }

  setHidden(obj) {
    return new Promise((resolve, reject) => {
      return this._httpHelper.httpHelper('post', 'detail-page/set-hide-job-list', null, obj)
        .then((responseObj) => {
          resolve(responseObj);
        });
    });
  }

  /**
	 * Saved Searches
	*/
  getSavedSearches(): Observable<any> {

    return this._http.get(this._httpHelper.getFullUrl('search/get-save-search-list/', null));
  }

  insertSavedSearch(obj) {
    return new Promise((resolve, reject) => {

    return this._httpHelper.httpHelper('post', 'search/insert-save-search/', null, obj)
      .then((cfgObj) => {
        resolve(cfgObj);
      });
    });
  }

  deleteSavedSearch(id) {
    return new Promise((resolve, reject) => {

      const params = [];
      params.push({
        key: null,
        value: id
      });

      return this._httpHelper.httpHelper('Delete', 'search/delete-save-search/', params, null)
        .then((obj) => {
          resolve(obj);
        });
    });
  }

  /**
* Occupation Notes
*/
  getNotes(): Observable<any> {

    return this._http.get(this._httpHelper.getFullUrl('notes/getNotes/', null));
  }

  getNote(mcId): Observable<any> {
    const params = [];
    params.push({
      key: null,
      value: mcId
    });

    return this._http.get(this._httpHelper.getFullUrl('notes/getNote/', params));
  }


  insertNotes(obj) {
    return new Promise((resolve, reject) => {

    return this._httpHelper.httpHelper('post', 'notes/insertNote', null, obj)
      .then((cfgObj) => {
        resolve(cfgObj);
      });
    });
  }

  updateNotes(obj) {
    return new Promise((resolve, reject) => {

    if (obj.noteId && !obj.notes) {
      const promise = this.deleteNote(obj.noteId);
      resolve();
    }
    return this._httpHelper.httpHelper('put', 'notes/updateNote', null, obj)
      .then((cfgObj) => {
        resolve(cfgObj);
      });
    });
  }

  deleteNote(id) {
    return new Promise((resolve, reject) => {

      const params = [];
      params.push({
        key: null,
        value: id
      });

      return this._httpHelper.httpHelper('Delete', 'notes/delete-note/', params, null)
        .then((obj) => {
          resolve(obj);
        });
    });
  }

  /**
	 * Career Cluster Notes
  */
 getCareerClusterNotes(): Observable<any> {

  return this._http.get(this._httpHelper.getFullUrl('notes/getCareerClusterNotes/', null));
}

  getCareerClusterNote(socId): Observable<any> {
    const params = [];
    params.push({
      key: null,
      value: socId
    });

    return this._http.get(this._httpHelper.getFullUrl('notes/getCareerClusterNote/', params));
  }

  insertCareerClusterNotes(obj) {
    return new Promise((resolve, reject) => {

    return this._httpHelper.httpHelper('post', 'notes/insertCareerClusterNote', null, obj)
      .then((cfgObj) => {
        resolve(cfgObj);
      });
    });
  }

  updateCareerClusterNotes(obj) {
    return new Promise((resolve, reject) => {

    return this._httpHelper.httpHelper('put', 'notes/updateCareerClusterNote', null, obj)
      .then((cfgObj) => {
        resolve(cfgObj);
      });
    });
  }

  deleteCareerClusterNote(socId) {
    return new Promise((resolve, reject) => {

      const params = [];
      params.push({
        key: null,
        value: socId
      });

      return this._httpHelper.httpHelper('Delete', 'notes/delete-note/', params, null)
        .then((obj) => {
          resolve(obj);
        });
    });
  }

  createIdParam(id) {
    const params = [];
    params.push({
      key: null,
      value: id
    });
    return params;
  }
}
