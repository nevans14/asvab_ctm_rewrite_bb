import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ConfigService } from './config.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';

  const headers = new HttpHeaders()
  .set('Accept', 'application/json')
  .set('Content-Type', 'application/json');

@Injectable({
  providedIn: 'root'
})
export class ContentManagementService {

  constructor(
    private _configService: ConfigService,
    private _http: HttpClient
  ) { }

  public getPageById(pageId): Observable<any> {
    return this._http.get(this._configService.getFullUrl('/pages/get-by-page-id/' + pageId + '/', null));
  }

  public getPageByPageName(pageName): Observable<any> {
    return this._http.get(this._configService.getAwsFullUrl('/api/pages/get-by-page-name-system/' + pageName + '/CITM/'));
  }

  public getDbinfoByName(name): Observable<any> {
    return this._http.get(this._configService.getAwsFullUrl('/api/db-info/get-by-name/' + name + '/'));
  }

}
