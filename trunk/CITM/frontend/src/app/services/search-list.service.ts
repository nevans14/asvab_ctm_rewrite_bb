import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { tap } from 'rxjs/operators';
import { HttpHelperService } from './http-helper.service';
import { UserService } from '../services/user.service';
import { ConfigService } from './config.service';
import { UtilityService } from './utility.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { nextContext } from '@angular/core/src/render3';

  const headers = new HttpHeaders()
  .set('Accept', 'application/json')
  .set('Content-Type', 'application/json');

@Injectable({
  providedIn: 'root'
})
export class SearchListService  {
  public searchListObject: any;
  public careerProfileListObject: any;
  public scoreListObject: any;
  public jobFamilyListObject: any;
  public currentSearchCriteria: any;
  hiddenListObject: any;
  processingSearchListObject: any;
  processingSearchList = false;
  refreshSearchListInterval: any;
  slugMapping: any = [];

  constructor(
    private _httpHelper: HttpHelperService,
    private _userService: UserService,
    private _configService: ConfigService,
    private _http: HttpClient,
    private _utilityService: UtilityService,
  ) { 
    this._utilityService.clearSessionStorage();
  }


  getSearchListObsv(): Observable<any>  {
    return this._http.get(this._configService.getAwsFullUrl('/api/search/get-search-list'));
  }

  getCareerProfileListObsv(): Observable<any>  {
    return this._http.get(this._configService.getAwsFullUrl('/api/CareerProfile/getCareerProfileList/'));
  }

  getJobFamily (): Observable<any>  {
    return this._http.get(this._configService.getAwsFullUrl('/api/search/get-job-family/'));
  }

  addSlugToCareerProfileList(profile = undefined) {

    if (!profile && this.careerProfileListObject) {
      this.careerProfileListObject.forEach(i => {
        let lastName = i.lastName.toLowerCase();
        if(i.toMilitaryCareerList) {
          i.toMilitaryCareerList.forEach(j => {
            // For career profile
            if (j && j.customTitle) {
              j['customTitleSlug'] = j.customTitle.trim().split(' ').join('-').toLowerCase() + '-' + lastName;
            }
            // For career detail
            if (j && j.title) {
              j['slug'] = j.title.trim().split(' ').join('-').toLowerCase();
            } 
          })
        }
      })
    } else if (profile && profile.toMilitaryCareerList) {
        let lastName = profile.lastName.toLowerCase();
        profile.toMilitaryCareerList.forEach(i => {
          // For career profile
          if (i && i.customTitle) {
            i['customTitleSlug'] = i.customTitle.trim().split(' ').join('-').toLowerCase() + '-' + lastName;
          }
        })
    }
  }

  getCareerProfileList() {
    this.careerProfileListObject = JSON.parse(sessionStorage.getItem('careerProfileList'));

    if (!this.careerProfileListObject) {
      return this.getCareerProfileListObsv().pipe(
        tap(data => {
          if (data) {
            this.careerProfileListObject = data;
            this.addSlugToCareerProfileList();
            sessionStorage.setItem('careerProfileList', JSON.stringify(this.careerProfileListObject));
          }
        }),
      );
    } else {
      return of(this.careerProfileListObject);
    }
  }

  refreshSearchList() {
    const self = this;
    if (this.processingSearchList) {
      this.refreshSearchListInterval = setInterval(function () {
        console.log('Wait to process search list refresh.');
        if (!self.processingSearchList) {
          self.processRefreshSearchList();
          clearInterval(self.refreshSearchListInterval);
        }
      }, 500);
    } else {
      this.processRefreshSearchList();
    }
  }

  processRefreshSearchList() {
    this.processingSearchList = true;
    this._userService.favoritesListObject = null;
    this._userService.hiddenListObject = null;
    this.obtainSearchList(false);
  }

  getSearchList() {
    this.searchListObject = JSON.parse(sessionStorage.getItem('searchList'));

    if (!this.searchListObject || this.processingSearchList) {
      if (!this.processingSearchList) {
        this.processingSearchList = true;
        this.obtainSearchList(true);
      }
      return null;
    } else {
      return this.searchListObject;
    }
  }

  getSlugMapping() {
    return this.slugMapping;
  }

  setSlugMapping() {
    if (this.searchListObject && this.slugMapping.length === 0) {
      this.searchListObject.forEach(i => {
        if (i && i.title) {
          this.slugMapping.push({mcId: i.mcId, slug: i.title.trim().split(' ').join('-').toLowerCase()});
        } 
      })
    }
  }

  addSlugToSearchList() {
    if (this.searchListObject) {
      this.searchListObject.forEach(i => {
        if (i && i.title) {
          i['slug'] = i.title.trim().split(' ').join('-').toLowerCase();
        } 
      })
    }
  }

  obtainSearchList(getSearchList = true) {

    if (getSearchList) {
      this.getSearchListObsv().subscribe(
        data => {
          this.processingSearchListObject = data;
          this.processSearchList();
        });
    } else {
      this.processingSearchListObject = this.searchListObject;
      this.processSearchList();
    }

    if (this._configService.isLoggedIn()) {
      this._userService.getFavoritesList().subscribe(
        data => {
          this._userService.favoritesListObject = data ? data : [];
          this.processSearchList();
        });

      this._userService.getHiddenList().subscribe(
        data => {
          this._userService.hiddenListObject = data ? data : [];
          this.processSearchList();
        });
    } else {
      this._userService.favoritesListObject = [];
      this._userService.hiddenListObject = [];
      this.processSearchList();
    }
  }

  processSearchList() {
    if (this.processingSearchListObject != null &&
      this._userService.favoritesListObject != null &&
      this._userService.hiddenListObject != null) {
      const imageUrl = this._configService.getImageUrl();

      // This is for when we do an SSO redirect we need to finish the refresh before doing the redirect or iOS throws an error
      if (!this.processingSearchListObject) {
        this.processingSearchListObject = this.searchListObject;
      }

      for (let x = 0; x < this.processingSearchListObject.length; x++) {
        this.processingSearchListObject[x].show = true;
        this.processingSearchListObject[x].favorited = false;
        this.processingSearchListObject[x].imageUrl = imageUrl + 'citm-images/' + this.processingSearchListObject[x].mcId + '.jpg';
        this.processingSearchListObject[x].backgroundImageUrl =
          'url(\'' + imageUrl + 'citm-images/' + this.processingSearchListObject[x].mcId + '.jpg\')';
      }

      if (this._userService.hiddenListObject != null) {
        for (let i = 0; i < this._userService.hiddenListObject.length; i++) {
          const mcId = this._userService.hiddenListObject[i];
          for (let x = 0; x < this.processingSearchListObject.length; x++) {
            if (this.processingSearchListObject[x].mcId === mcId) {
              this.processingSearchListObject[x].show = false;
              break;
            }
          }
        }
      }

      if (this._userService.favoritesListObject != null) {
        for (let i = 0; i < this._userService.favoritesListObject.length; i++) {
          const mcId = this._userService.favoritesListObject[i].mcId;
          for (let x = 0; x < this.processingSearchListObject.length; x++) {
            if (this.processingSearchListObject[x].mcId === mcId) {
              this.processingSearchListObject[x].favorited = true;
              break;
            }
          }
        }
      }
      this.searchListObject = this.processingSearchListObject;
      this.processingSearchList = false;
      this.processingSearchListObject = null;

      // this.setSlugMapping();
      this.addSlugToSearchList();

      sessionStorage.setItem('searchList', JSON.stringify(this.searchListObject));
    }
  }

  getProcessingSearchList() {
    return this.processingSearchList;
  }
}
