import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material';
import { UserService } from 'app/services/user.service';
import { ConfigService } from 'app/services/config.service';
import { HttpHelperService } from 'app/services/http-helper.service';
import { NeedToLoginDialogComponent } from 'app/core/dialogs/need-to-login-dialog/need-to-login-dialog.component';
import { MessageDialogComponent } from 'app/core/dialogs/message-dialog/message-dialog.component';
import { UndoDialogComponent } from 'app/core/dialogs/undo-dialog/undo-dialog.component';

@Injectable({
  providedIn: 'root'
})
export class FavoritesService {
  errorContext: string;

  constructor(
    private _userService: UserService,
    private _http: HttpHelperService,
    private _dialog: MatDialog,
    private _config: ConfigService
  ) { }

  //  OCCUPATION FAVORITE

  onFavoriteClick(item) {
    return new Promise((resolve, reject) => {


      if (this._config.isLoggedIn()) {

        if (item.favorited) {
          this.deleteFavorite(item) 
          .then (() => {
            resolve();
          });
        } else {
          // Check for the limit being reached
          if (this._userService.favoritesListObject.length < 10) {
            this.insertFavorite(item)
            .then (() => {
              resolve();
            });
          } else {
            this.displayError(false, null, 'Your favorite occupation list is full.  This occupation was not added.');
            reject();
          }
        }
      } else {
        const dialogRef2 = this._dialog.open(NeedToLoginDialogComponent, {
        });
        resolve();
      }
    });
  }

  deleteFavorite(item) {
    return new Promise((resolve, reject) => {

      const self = this;
      return this._userService.deleteFavorite(item.mcId)
      .then((obj) => {
        item.favorited = false;
        self.getFavoriteList()
        .then (() => {
          item.favorited = false;
          resolve();
        });
      })
      .catch ((error) => {
        self.displayError(false, error, null);
        resolve();
      });
    });
  }

  insertFavorite(item) {
    return new Promise((resolve, reject) => {
      const self = this;
      return this._userService.insertFavorite(item.mcId, item.title, item.isHotJob)
      .then((obj) => {
        self.getFavoriteList()
        .then (() => {
          item.favorited = true;
          resolve();
        });
      })
      .catch ((error) => {
        self.displayError(true, error, null);
        resolve();
      });
    });
  }

  getFavoriteList() {
    return new Promise((resolve, reject) => {
      if (this._config.isLoggedIn()) {
        const self = this;
        return this._http.httpHelper('get', 'favorite/getFavoriteOccupation/', null, null)
          .then((favorites) => {
            self._userService.favoritesListObject = favorites;
            resolve();
          })
          .catch((error) => {
            self.displayError(null, error, 'Your session has expired.');
            resolve();
          });
      } else {
        resolve();
      }
    });
  }

  displayError(add, error, message) {
    let data;
    if (message) {
      data = {
        'title': 'Favorites',
        'message': message
      };
    } else if (add) {
      data = {
        'title': 'Favorites',
        'message': 'Your favorite choice was not saved.  Please log out and log back in and try again.  <br> <br> '
      };

    } else if (!add) {
      data = {
        'title': 'Favorites',
        'message': 'Your favorite choice was not removed.  Please log out and log back in and try again.  <br> <br> '
      };
  }

    if (error) {
      // data.message += 'Error Code: ' + error.status + '\nMessage:  ' + error.message;
      console.log(error);
    }

    this._dialog.open(MessageDialogComponent, {
      data: data,
      hasBackdrop: true,
      disableClose: true
    });
  }

// CAREER CLUSTER FAVORITE

  onCareerClusterFavoriteClick(item) {
    return new Promise((resolve, reject) => {

      if (this._config.isLoggedIn()) {
        if (item.favorited) {
          this.deleteFavoriteCareerCluster(item)
          .then ((obj) => {
            this.getCareerClusterFavoriteList()
            .then ((obj2) => {
              resolve(obj2);
            });
          });
        } else {
          if (this._userService.favoriteCareerClusterLIstObject.length < 2) {
            this.insertFavoriteCareerCluster(item)
            .then ((obj) => {
              this.getCareerClusterFavoriteList()
              .then ((obj2) => {
                resolve(obj2);
              });
            });
          } else {
            this.displayError(false, null, 'Your favorite career cluster list is full.  This career cluster was not added.');
            resolve();
          }
        }
      } else {
        const dialogRef = this._dialog.open(NeedToLoginDialogComponent, {
          panelClass: 'custom-dialog'
        });
        resolve();
      }
    });
  }

  // Pull out in separate method so will not continue on
  deleteFavoriteCareerCluster(item) {
    return new Promise((resolve, reject) => {

      const self = this;
      this._userService.deleteFavoriteCareerCluster(item.favoritedId)
        .then((obj) => {
          item.favoritedId = null;
          item.favorited = false;
          const careerClusterFavoriteList = self.getCareerClusterFavoriteList();
          resolve(careerClusterFavoriteList);
        })
        .catch((error) => {
          self.displayError(false, error, null);
          resolve();
        });
    });
  }

  // Pull out in separate method so will not continue on
  insertFavoriteCareerCluster(item) {
    return new Promise((resolve, reject) => {
      const self = this;
      this._userService.insertFavoriteCareerCluster(item.ccId)
        .then((obj) => {
          item.favorited = true;
          item.favoritedId = obj['ccId'];
          const careerClusterFavoriteList = self.getCareerClusterFavoriteList();
          resolve(careerClusterFavoriteList);
        })
        .catch((error) => {
          self.displayError(true, error, null);
          resolve();
        });
    });
  }

  getCareerClusterFavoriteList() {
    return new Promise((resolve, reject) => {
      const self = this;
      this._http.httpHelper('get', 'favorite/getFavoriteCareerCluster/', null, null)
        .then((favorites) => {
          self._userService.favoriteCareerClusterLIstObject = favorites;
          resolve(favorites ? favorites : []);
        })
        .catch((error) => {
          self.displayError(null, error, 'Your session has expired.');
          resolve();
        });
    });
  }

  // FAVORITE SERVICE

  onFavoriteServiceClick(item, id) {
    return new Promise((resolve, reject) => {

      if (this._config.isLoggedIn()) {
        if (id) {
          this.deleteFavoriteService(id)
          .then ((obj) => {
            this.getFavoriteServicesList()
            .then ((obj2) => {
              resolve(obj2);
            });
          });
        } else {
          this.insertFavoriteService(item)
          .then ((obj) => {
            this.getFavoriteServicesList()
            .then ((obj2) => {
              resolve(obj2);
            });
          });
        }
      } else {
        const dialogRef2 = this._dialog.open(NeedToLoginDialogComponent, {
        });
        resolve();
      }
    });
  }

  // Pull out in separate method so will not continue on
  deleteFavoriteService(id) {
    return new Promise((resolve, reject) => {

      const self = this;
      this._userService.deleteFavoriteService(id)
        .then((obj) => {
          resolve();
        })
        .catch((error) => {
          self.displayError(false, error, null);
          resolve();
        });
    });
  }

  // Pull out into promise so will not continue on
  insertFavoriteService(item) {
    return new Promise((resolve, reject) => {
      const self = this;
      this._userService.insertFavoriteService(item)
        .then((obj) => {
          resolve();
        })
        .catch((error) => {
          self.displayError(true, error, null);
          resolve();
        });
    });
  }

  // FAVORITED SERVICES

  getFavoriteServicesList(): any {
    return new Promise((resolve, reject) => {
      const self = this;
      this._http.httpHelper('get', 'favorite/get-favorite-service/', null, null)
        .then((favorites) => {
          resolve(favorites ? favorites : []);
        })
        .catch((error) => {
          self.displayError(null, error, 'Your session has expired.');
          resolve();
        });
    });
  }

  setFavoritedServices(serviceObjects, servicesFavorited) {
    if (serviceObjects) {
      for (let k = 0; k < serviceObjects.length; k++) {
        serviceObjects[k].favoriteId = null;
        if (this._config.isLoggedIn()) {
          for (let i = 0; i < servicesFavorited.length; i++) {
            if (servicesFavorited[i].svcId === serviceObjects[k].svcId) {
              serviceObjects[k].favoriteId = servicesFavorited[i].id;
            }
          }
        }
      }
    }
  }

  // HIDDEN OCCUPATION

  onHiddenClick(item) {
    return new Promise((resolve, reject) => {

      if (this._config.isLoggedIn()) {
        for (let i = 0; i < this._userService.hiddenListObject.length; i++) {
          if (this._userService.hiddenListObject[i] === item.mcId) {
            resolve();
          }
        }
        this._userService.hiddenListObject.push(item.mcId);
        const obj = {  };
        obj['jsonString'] = JSON.stringify(this._userService.hiddenListObject);
        return this._userService.setHidden(obj)
          .then((retObj) => {
            item.show = false;
            resolve(item);
          });
      } else {
        const dialogRef = this._dialog.open(NeedToLoginDialogComponent, {
          panelClass: 'custom-dialog'
        });
        resolve();
      }
    });
  }

  onUndoClick(searchList) {
    return new Promise((resolve, reject) => {

      if (this._config.isLoggedIn()) {
        // Already have search list set so just need to pass it
        const dialogRef = this._dialog.open(UndoDialogComponent, {
          data: searchList
        });

        dialogRef.afterClosed().subscribe(result => {
          resolve();
        });

      } else {
        const dialogRef = this._dialog.open(NeedToLoginDialogComponent, {
          panelClass: 'custom-dialog'
        });

        dialogRef.afterClosed().subscribe(result => {
          resolve();
        });
      }
    });

  }

  // METHODS

  private createIdParam(id) {
    const params = [];
    params.push({
      key: null,
      value: id
    });
    return params;
  }
}
