import { Component, OnInit, AfterViewInit, AfterViewChecked } from '@angular/core';
import { Router } from '@angular/router';
import { PopupPlayerComponent } from 'app/core/dialogs/popup-player/popup-player.component';
import { MatDialog } from '@angular/material';
import { Masonry } from 'ng-masonry-grid'; // import necessary datatypes
import { GoogleAnalyticsService } from 'app/services/google-analytics.service';
import { Title, Meta } from '@angular/platform-browser';

@Component({
  selector: 'app-overview',
  templateUrl: './overview.component.html',
  styleUrls: ['./overview.component.scss']
})
export class OverviewComponent implements OnInit, AfterViewInit, AfterViewChecked{
  _masonry: Masonry;
  showGrid = true;
  // ng-masonry-grid-item requires an array or else the animation doesn't work correctly.
  masonryItems: any[];

  constructor(
    private _router: Router,
    private videoDialog: MatDialog,
    private _meta: Meta,
    private _titleTag: Title,
    private _googleAnalyticsService: GoogleAnalyticsService
  ) { }

  ngOnInit() {
  }

  ngAfterViewChecked() {
    //        this.cdr.detectChanges();
  }

  ngAfterViewInit() {
    const fullUrl = location.href;
    const overviewPageTitle = 'Learn About Your Options | Careers in the Military';
    const overviewPageDescription = 'Need Description';

    this._meta.updateTag({property: 'og:title', content: overviewPageTitle});
    this._meta.updateTag({property: 'og:description', content: overviewPageDescription});
    this._meta.updateTag({name: 'twitter:title', content: overviewPageTitle});
    this._meta.updateTag({name: 'twitter:description', content: overviewPageDescription});
    this._titleTag.setTitle(overviewPageTitle);
    this._meta.updateTag({name: 'description', content: overviewPageDescription});
  }

  refreshMasonryGrid() {
    // this._masonry.removeAllItems()
    //   .subscribe( (items: MasonryGridItem) => {
    this.masonryItems = [
      { item: '' }
    ];
    // });
    if (this._masonry) {
      this._masonry.reloadItems();
      setTimeout(() => this._masonry.reOrderItems(), 500);
    }
    console.debug('force refresh');
  }

  onNgMasonryInit($event: Masonry) {
    this._masonry = $event;
    this.showGrid = true;
    setTimeout(() => { this.refreshMasonryGrid() }, 100);;
  }

  /**
   * Open video player dialog.
   */
  openPlayer(videoId) {
    this.videoDialog.open(PopupPlayerComponent, {
      data: { videoId: videoId }
    });
  }

  redirect(path) {
    this._router.navigate([path]);
  }

  ga(param) {
    this._googleAnalyticsService.trackClick(param);
  }

  gaSocialShare(socialPlug, value) {
    this._googleAnalyticsService.trackSocialShare(socialPlug, value);
  }

  // go to page and set accord panel to open
  navTo(url) {
    const optionsShowPanelID = {
      url: '/' + url
    }

    window.sessionStorage.setItem('optionsShowPanelID', JSON.stringify(optionsShowPanelID));
    this._router.navigate([url]);
  }

}
