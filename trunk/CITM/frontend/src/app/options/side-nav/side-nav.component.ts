import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

export interface Option {
  value: string;
  displayValue: string;
}

@Component({
  selector: 'app-side-nav',
  templateUrl: './side-nav.component.html',
  styleUrls: ['./side-nav.component.scss']
})

export class SideNavComponent implements OnInit {
  menuOptions: Option[] = [
    {value: '/options', displayValue: 'Overview'},
    {value: '/options-military', displayValue: 'The Military'},
    {value: '/options-types-of-service', displayValue: 'Types of Service'},
    {value: '/options-boot-camp-services', displayValue: 'Boot Camp by Service'},
    {value: '/options-enlisted-vs-officer', displayValue: 'Enlisted VS. Officer'},
    {value: '/options-enlistment-requirements', displayValue: 'Enlistment Requirements'},
    {value: '/options-enlistment-process', displayValue: 'Enlistment Process'},
    {value: '/options-becoming-an-officer', displayValue: 'Becoming a Military Officer'},
    {value: '/options-pay', displayValue: 'Pay'},
    {value: '/options-benefits', displayValue: 'Benefits'},
    {value: '/options-reasons-to-consider', displayValue: 'Reasons To Consider The Military'},
    {value: '/options-contact-recruiter', displayValue: 'Why Should I Contact A Recruiter'},
  ];

  selectedValue: string;
  path: string;

  constructor(
    private _router: Router
  ) { }

  ngOnInit() {
    this.path = this._router.url;
    this.selectedValue = this.path;
  }

  isCurentPage(page) {
    if (page === this._router.url) {
      return true;
    }
    return false;
  }

  redirect() {
    this.path = this.selectedValue;
    if (this.path !== '/options') {
      this._router.navigate([this.selectedValue]);
    } else {
      window.location.assign(this.path);
    }
  }

  getDisplayValue() {
    return (this.menuOptions.find(obj => obj.value === this.selectedValue)).displayValue;
  }

  isSelected(lineValue) {
    if (this.selectedValue === lineValue) {
      return true;
    }
    return false;
  }
}
