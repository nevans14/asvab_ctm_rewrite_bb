import { Component, OnInit, AfterViewInit, AfterViewChecked, ChangeDetectorRef, Optional, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ContentManagementService  } from 'app/services/content-management.service';
import { Title, Meta } from '@angular/platform-browser';
import { MatDialog } from '@angular/material';
import { MilitaryInfoDialogComponent } from 'app/core/dialogs/military-info-dialog/military-info-dialog.component'
import { ConfigService } from 'app/services/config.service';
import { UserService } from 'app/services/user.service';
import { FavoritesService } from 'app/services/favorites.service';
import {GoogleAnalyticsService} from 'app/services/google-analytics.service';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RankService } from 'app/services/rank.service';
import { PayService } from 'app/services/pay.service';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MaterialModule } from 'app/material.module';
import { LoginDialogComponent } from 'app/core/dialogs/login-dialog/login-dialog.component';
import { PopupPlayerComponent } from 'app/core/dialogs/popup-player/popup-player.component';
import { NeedToLoginDialogComponent } from 'app/core/dialogs/need-to-login-dialog/need-to-login-dialog.component';
import { FitnessAirForceDialogComponent } from 'app/core/dialogs/fitness-air-force-dialog/fitness-air-force-dialog.component';
import { FitnessArmyDialogComponent } from 'app/core/dialogs/fitness-army-dialog/fitness-army-dialog.component';
import { FitnessCoastGuardDialogComponent } from 'app/core/dialogs/fitness-coast-guard-dialog/fitness-coast-guard-dialog.component';
import { FitnessMarineDialogComponent } from 'app/core/dialogs/fitness-marine-dialog/fitness-marine-dialog.component';
import { FitnessNavyDialogComponent } from 'app/core/dialogs/fitness-navy-dialog/fitness-navy-dialog.component';
import { MessageDialogComponent } from 'app/core/dialogs/message-dialog/message-dialog.component';
import { RankDialogComponent } from 'app/core/dialogs/rank-dialog/rank-dialog.component';
import { PayDialogComponent } from 'app/core/dialogs/pay-dialog/pay-dialog.component';
import { UtilityService } from 'app/services/utility.service';

@Component( {
    selector: 'app-options',
    templateUrl: './options.component.html',
    styleUrls: ['./options.component.scss'],
//    changeDetection: ChangeDetectionStrategy.OnPush
} )

export class OptionsComponent implements OnInit, AfterViewInit, AfterViewChecked, OnDestroy {
    useDynamicContent = false;
    showContactServices = false;
    page: any;
    rankData: any;
    payData: any;
    payInfo: any;
    serviceObjects: any;
    pageHtml: any = [];
    private id: string;
    path;
    option: String = null;
    myStyle = { 'cursor': 'pointer' };
    disabled = false;
    showMilitary = false;
    showTypesOfServices = false;
    showBootCampServices = false;
    showEnlistedVsOfficer = false;
    showEnlistmentRequirements = false;
    showEnlistmentProcess = false;
    showBecomingAnOfficer = false;
    showPay = false;
    showBenefits = false;
    showReasonsToConsider = false;

    text: any;
    resource = this._config.getImageUrl();
    resources = this.resource + 'static/';
    documents = this.resource + 'pdf/';
    citmimages = this.resource + 'citm-images/';
    selectedTabId = 'first';
    panelOpenState = false;
    mapView: Boolean = true;
    aText: String = 'List View';
    step: String;

    constructor(
        @Optional() private cdr: ChangeDetectorRef,
        private _router: Router,
        private _meta: Meta,
        private _titleTag: Title,
        private _contentManagementService: ContentManagementService,
        private _activatedRoute: ActivatedRoute,
        private _dialog: MatDialog,
        private _config: ConfigService,
        private _userService: UserService,
        private _favoriteService: FavoritesService,
        private _googleAnalyticsService: GoogleAnalyticsService,
        private _rankService: RankService,
        private _payService: PayService,
        private _utility: UtilityService,
    ) {
      this._activatedRoute.params.subscribe(routeParams => {
        this.option = routeParams.option;
        });
    }

    ngOnInit() {
      this.path = this._router.url;

      if (this.path !== '/options' && this.path !== 'options') {
          this.useDynamicContent = true;

          if (this.option && 
            (this.option.toLowerCase() === 'rotc'
            || this.option.toLowerCase() === 'service-academies')) {
            this.path = this.path.replace('/' + this.option, '')
          }
          this.getPageByName(this.path);
      }

      if (this.path.indexOf('benefits') > -1 || this.path.indexOf('becoming-an-officer') > -1) {
        this.openPanel();
      }

      if (this.path.indexOf('enlisted-vs-officer') > -1) {
        this._payService.getRank().subscribe(
          data => {
            this.rankData = data;
          });
        this.showEnlistedVsOfficer = true;
      }

      if (this.path.indexOf('pay') > -1) {
        var rank, pay, payChart;
        this._payService.getPay().subscribe(
          data => {
            pay = data;
            this.payData = pay && rank && payChart ? [pay, rank, payChart] : null;
          });
        this._payService.getRank().subscribe(
          data => {
            rank = data;
            this.payData = pay && rank && payChart ? [pay, rank, payChart] : null;
          });
        this._payService.getPayChart().subscribe(
          data => {
            payChart = data;
            this.payData = pay && rank && payChart ? [pay, rank, payChart] : null;
          });
        this._payService.getPayInfo().subscribe(
          data => {
            this.payInfo = data;
          });
      }

      if (this.path.indexOf('contact-recruiter') > -1) {
          this.showContactServices = true;
      }

      if (this.path.indexOf('military') > -1) {
        this.getFavoritedServices();
        this.serviceObjects = [
          { svcId: 'A', name: 'ARMY', favoriteId: null, imageName: 'army' },
          { svcId: 'M', name: 'MARINE CORPS', favoriteId: null, imageName: 'marines' },
          { svcId: 'N', name: 'NAVY', favoriteId: null, imageName: 'navy' },
          { svcId: 'F', name: 'AIR FORCE', favoriteId: null, imageName: 'airforce' },
          { svcId: 'S', name: 'SPACE FORCE', favoriteId: null, imageName: 'spaceforce' },
          { svcId: 'C', name: 'COAST GUARD', favoriteId: null, imageName: 'coastguard' },
        ];
        this.showMilitary = true;
      }

      if (this.path.indexOf('types-of-service') > -1) {
        this.showTypesOfServices = true;
      }

      if (this.path.indexOf('boot-camp-service') > -1) {
        this.showBootCampServices = true;
      }

      if (this.path.indexOf('boot-camp-service') > -1) {
        this.showBootCampServices = true;
      }

      if (this.path.indexOf('enlistment-requirements') > -1) {
        this.showEnlistmentRequirements = true;
      }

      if (this.path.indexOf('enlistment-process') > -1) {
        this.showEnlistmentProcess = true;
      }

      if (this.path.indexOf('becoming-an-officer') > -1) {
        this.showBecomingAnOfficer = true;
      }

      if (this.path.indexOf('pay') > -1) {
        this.showPay = true;
      }

      if (this.path.indexOf('benefits') > -1) {
        this.showBenefits = true;
      }

      if (this.path.indexOf('reasons-to-consider') > -1) {
        this.showReasonsToConsider = true;
      }
    }

    ngAfterViewChecked() {
    }

    ngAfterViewInit() {
        window.scrollTo(0, 0);
    }

    isLoggedIn() {
      return this._config.isLoggedIn();
    }

    getFavoritedServices() {
      if (this.isLoggedIn()) {
        this._userService.getFavoriteServices().subscribe(data => {
          this._favoriteService.setFavoritedServices(this.serviceObjects, data);
        });
      }
    }

  /***
   * Get content from DB
   */
  getPageByName(pageName) {
    // Remove leading slash before lookup
    if (pageName.indexOf('/') === 0) {
      pageName = pageName.substring(1);
    }
    console.log('pageName:', pageName);
    this._contentManagementService.getPageByPageName(pageName).subscribe(
      data => {
        this.page = data;
        this._utility.parseCms(this.page.pageHtml, this.pageHtml);

        this._meta.updateTag({property: 'og:title', content: this.page.pageTitle});
        this._meta.updateTag({property: 'og:description', content: this.page.pageDescription});
        this._meta.updateTag({ property: 'og:image', content: 'http://www.asvabprogram.com/media-center-content/thumbnails/social-share/CITM_Webshare.jpg' });
        this._meta.updateTag({name: 'twitter:title', content: this.page.pageTitle});
        this._meta.updateTag({name: 'twitter:description', content: this.page.pageDescription});
        this._meta.updateTag({ name: 'twitter:image', content: 'http://www.asvabprogram.com/media-center-content/thumbnails/social-share/CITM_Webshare.jpg' });
        this._titleTag.setTitle(this.page.pageTitle);
        this._meta.updateTag({name: 'description', content: this.page.pageDescription});
      });
  }

  openPanel() {
    if (window.sessionStorage.getItem("optionsShowPanelID") &&
      window.sessionStorage.getItem("optionsShowPanelID") !== "undefined") {
      this.step = (this.path.indexOf('benefits') > -1) ? 'panel3' : 'panel2';
      window.sessionStorage.setItem('optionsShowPanelID', "undefined");
    } else {
      if (this.option && this.option.toLowerCase() === 'rotc') {
        this.step = 'panel2';
      } else if (this.option && this.option.toLowerCase() === 'service-academies') {
        this.step = 'panel1';
      } else {
        this.step = null;
      }
      window.sessionStorage.setItem('optionsShowPanelID', "undefined");
    }
  }

  openLoginDialog() {
    this._dialog.open(LoginDialogComponent, { hasBackdrop: true, disableClose: true, autoFocus: true });
  }

  redirect(path) {
    this._router.navigate([path]);
  }

  loginRedirect(path) {
    if (this.isLoggedIn()) {
      this.redirect(path);
    } else {
      this._dialog.open( NeedToLoginDialogComponent, { hasBackdrop: true, disableClose: true, autoFocus: true } );
    }
  }

  showFitnessArmyDialogComponent() {
    this._dialog.open(FitnessArmyDialogComponent, {
      panelClass: 'custom-dialog'
    });
  }
  showFitnessMarineDialogComponent() {
    this._dialog.open(FitnessMarineDialogComponent, {
      panelClass: 'custom-dialog'
    });
  }
  showFitnessNavyDialogComponent() {
    this._dialog.open(FitnessNavyDialogComponent, {
      panelClass: 'custom-dialog'
    });
  }
  showFitnessAirForceDialogComponent() {
    this._dialog.open(FitnessAirForceDialogComponent, {
      panelClass: 'custom-dialog'
    });
  }
  showFitnessCoastGuardDialogComponent() {
    this._dialog.open(FitnessCoastGuardDialogComponent, {
      panelClass: 'custom-dialog'
    });
  }

  /**
   * Open video player dialog.
   */
  openPlayer(videoId) {
    this._dialog.open(PopupPlayerComponent, {
      width: '700px',
      data: { videoId: videoId }
    });
  }

  onClickBootCamp() {
    this.mapView = !this.mapView;
    if (this.mapView) {
      this.aText = 'List View';
    } else {
      this.aText = 'Map View';
    }
  }

  getDisplayMapBootCamp() {
    return this.mapView;
  }

  // Dialog opens with special parameters
  openRankDialog(data) {
    this._dialog.open(RankDialogComponent, {
      width: '700px',
      data: data,
      panelClass: 'custom-dialog'
    });
  }

  // Dialog opens with special parameters
  openPayDialog() {
    this._dialog.open(PayDialogComponent, {
      width: '700px',
      data: {...this.payData, ...this.payInfo},
      panelClass: 'custom-dialog'
    });
  }

  getFavoriteImage(svcObject, favoritId) {
    return favoritId ? '/assets/images/' + svcObject.imageName + '_fav_yes.png' :
      '/assets/images/' + svcObject.imageName + '_fav.png';
  }

  onFavoriteClick(svcObject) {
console.log("svcObject: "+svcObject);
    if (this.disabled) {
      return;
    }
    this.disabled = true;
    this.myStyle = { 'cursor': 'wait' };
    this._favoriteService.onFavoriteServiceClick(svcObject.svcId, svcObject.favoriteId)
      .then((obj) => {
        this._favoriteService.setFavoritedServices(this.serviceObjects, obj);
        this.myStyle = { 'cursor': 'pointer' };
        this.disabled = false;
      });
  }

  openServiceScoreInfo() {
    this._dialog.open(MessageDialogComponent, {
      data: {
        title: 'Service Composite Categories',
        message: '<ul><li>Each military job has unique composite score requirements</li>' +
          '<li>Requirements are subject to change without notice</li>' +
          '<li>For the most up-to-date requirements, <a href="/options-contact-recruiter">contact a recruiter</a></li></ul>'
      }
    });
  }

  trackClick(param) {
    console.log("param: "+param);
    this._googleAnalyticsService.trackClick(param);
  }

  gaSocialShare(socialPlug, value) {
    this._googleAnalyticsService.trackSocialShare(socialPlug, value);
  }

  openMilitaryInfoDialog(service) {
    this._dialog.open(MilitaryInfoDialogComponent, {
      data: service,
      panelClass: 'custom-dialog'
    });
  }

  ngOnDestroy() {
    // if (this.useDynamicContent && this.pageHtml) {
    //   (document.getElementById('dir')[0] as Component)  = null;
    // }
  }
}
