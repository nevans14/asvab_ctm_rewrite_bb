import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { OptionsComponent } from './options/options.component';
import { CareerProfileComponent } from './career-profile/career-profile.component';
import { CareerDetailComponent } from './career-detail/career-detail.component';
import { CareerPathwayComponent } from './career-pathway/career-pathway.component';
import { CareerDetailServiceComponent } from './career-detail-service/career-detail-service.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { ParentsComponent } from './parents/parents.component';
import { FaqComponent } from './faq/faq.component';
import { SiteMapComponent } from './site-map/site-map.component';
import { GuidedExplorationComponent } from './guided-exploration/guided-exploration.component';
import { JobCompareComponent } from './job-compare/job-compare.component';
import { JobFamilyComponent } from './job-family/job-family.component';
import { CareerClusterComponent } from './career-cluster/career-cluster.component';
import { CareerClusterPathwayComponent } from './career-cluster-pathway/career-cluster-pathway.component';
import { AdvancedSearchComponent } from './advanced-search/advanced-search.component';
import { GuidedExplorationResolveService } from './services/guided-exploration-resolve.service';
import { ProfileComponent } from './profile/profile.component';
import { CompositeScoresComponent } from './composite-scores/composite-scores.component';
import { SsoLoginComponent } from 'app/sso-login/sso-login.component';
import { PasswordResetComponent } from './password-reset/password-reset.component';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full'  },
  { path: 'home', component: HomeComponent, runGuardsAndResolvers: 'always'  },
  { path: 'options', component: OptionsComponent },
  { path: 'options-benefits', component: OptionsComponent },
  { path: 'options-boot-camp-services', component: OptionsComponent },
  { path: 'options-reasons-to-consider', component: OptionsComponent },
  { path: 'options-enlisted-vs-officer', component: OptionsComponent },
  { path: 'options-enlistment-process', component: OptionsComponent },
  { path: 'options-enlistment-requirements', component: OptionsComponent },
  { path: 'options-military', component: OptionsComponent },
  { path: 'options-becoming-an-officer', component: OptionsComponent },
  { path: 'options-becoming-an-officer/:option', component: OptionsComponent },
  { path: 'options-pay', component: OptionsComponent },
  { path: 'options-contact-recruiter', component: OptionsComponent },
  { path: 'options-types-of-service', component: OptionsComponent },
  { path: 'career-profile/:profileId', component: CareerProfileComponent},
  { path: 'career-detail/:mcId', component: CareerDetailComponent, runGuardsAndResolvers: 'always'  },
  { path: 'career-pathway/:mcId', component: CareerPathwayComponent },
  { path: 'career-detail-service/:svc/:mcId/:mocId', component: CareerDetailServiceComponent, runGuardsAndResolvers: 'always'  },
  { path: 'service-career-detail/:svc/:mcId/:mocId', component: CareerDetailServiceComponent, runGuardsAndResolvers: 'always'  },
  { path: 'contact-us', component: ContactUsComponent },
  { path: 'parent', component: ParentsComponent },
  { path: 'faq', component: FaqComponent },
  { path: 'site-map', component: SiteMapComponent },
  { path: 'guided-exploration', component: GuidedExplorationComponent, runGuardsAndResolvers: 'always' },
  { path: 'job-compare/:mcIdOne/:mcIdTwo/:mcIdThree/:mcIdFour', component: JobCompareComponent,
       resolve: { searchListObject: GuidedExplorationResolveService }, runGuardsAndResolvers: 'always' },
  { path: 'job-family', component: JobFamilyComponent },
  { path: 'career-cluster', component: CareerClusterComponent },
  { path: 'career-cluster-pathway/:ccId', component: CareerClusterPathwayComponent, runGuardsAndResolvers: 'always'  },
  { path: 'advanced-search', component: AdvancedSearchComponent, runGuardsAndResolvers: 'always'  },
  { path: 'advanced-search/:filter', component: AdvancedSearchComponent, runGuardsAndResolvers: 'always' },
  { path: 'composite-scores', component: CompositeScoresComponent},
  { path: 'profile', component: ProfileComponent },
  { path: 'sso-login/:redirect/:data', component: SsoLoginComponent},
  { path: 'sso-logoff', component: SsoLoginComponent},
  { path: 'passwordReset', component: PasswordResetComponent},
];
 
@NgModule({
  imports: [RouterModule.forRoot(routes, {
    useHash: true,
    scrollPositionRestoration: 'enabled',
    onSameUrlNavigation: 'reload',
    anchorScrolling: 'enabled',
  })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
