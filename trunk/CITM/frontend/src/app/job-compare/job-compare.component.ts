import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CareerDetailService } from 'app/services/career-detail.service';
import { UserService } from 'app/services/user.service';
import { Title, Meta } from '@angular/platform-browser';
import $ from 'jquery';
import html2canvas from 'html2canvas';

declare var $: $

declare var pdfMake: any;

@Component( {
    selector: 'app-job-compare',
    templateUrl: './job-compare.component.html',
    styleUrls: ['./job-compare.component.scss']
} )
export class JobCompareComponent implements OnInit {

    @ViewChild( 'printElement' ) printElement: ElementRef;
    @ViewChild( 'printMobileElementOne' ) printMobileElementOne: ElementRef;
    @ViewChild( 'printMobileElementTwo' ) printMobileElementTwo: ElementRef;
    @ViewChild( 'printMobileElementThree' ) printMobileElementThree: ElementRef;
    @ViewChild( 'printMobileElementFour' ) printMobileElementFour: ElementRef;

    public allJobList: any;

    public mcIdOne: string;
    public mcIdTwo: string;
    public mcIdThree: string;
    public mcIdFour: string;

    public jobOne: any;
    public jobTwo: any;
    public jobThree: any;
    public jobFour: any;

    public jobOneHelpfulAttributes: any;
    public jobTwoHelpfulAttributes: any;
    public jobThreeHelpfulAttributes: any;
    public jobFourHelpfulAttributes: any;

    public jobOneNote: any = {notes: ''};
    public jobTwoNote: any = {notes: ''};
    public jobThreeNote: any = {notes: ''};
    public jobFourNote: any = {notes: ''};

    constructor( private route: ActivatedRoute,
        private _meta: Meta,
        private _titleTag: Title,
        private _careerDetailService: CareerDetailService,
        private _userService: UserService,
    ) { }

    ngOnInit() {
        this._titleTag.setTitle('Job Compare | Careers in the Military');
        this._meta.updateTag({ property: 'og:title', content: 'Job Compare | Careers in the Military' });
        this._meta.updateTag({name: 'description', content: 'Compare up to four different  jobs in your favorites.'});

        this._meta.updateTag({property: 'og:title', content: 'Job Compare | Careers in the Military'});
        this._meta.updateTag({property: 'og:description', content: 'Compare up to four different  jobs in your favorites.'});
        this._meta.updateTag({ property: 'og:image', content: 'http://www.asvabprogram.com/media-center-content/thumbnails/social-share/CITM_Webshare.jpg' });
  
        this._meta.updateTag({name: 'twitter:title', content:  'Job Compare | Careers in the Military' });
        this._meta.updateTag({name: 'twitter:description', content: 'Compare up to four different  jobs in your favorites.'});
        this._meta.updateTag({ name: 'twitter:image', content: 'http://www.asvabprogram.com/media-center-content/thumbnails/social-share/CITM_Webshare.jpg' });
  
        this.route.params.subscribe( res => {
            this.mcIdOne = res.mcIdOne;
            this.mcIdTwo = res.mcIdTwo;
            this.mcIdThree = res.mcIdThree;
            this.mcIdFour = res.mcIdFour;
        } );


        this.allJobList = this.route.snapshot.data.searchListObject;

        this.jobOne = this.allJobList.filter( obj => {
            return obj.mcId == this.mcIdOne;
        } )[0];

        this.jobTwo = this.allJobList.filter( obj => {
            return obj.mcId == this.mcIdTwo;
        } )[0];

        if ( this.mcIdThree != undefined ) {
            this.jobThree = this.allJobList.filter( obj => {
                return obj.mcId == this.mcIdThree;
            } )[0];
        }

        if ( this.mcIdFour != undefined ) {
            this.jobFour = this.allJobList.filter( obj => {
                return obj.mcId == this.mcIdFour;
            } )[0];
        }

        this._careerDetailService.getHelpfulAttributes( this.mcIdOne ).subscribe( data => {
            this.jobOneHelpfulAttributes = data;
        } );

        this._careerDetailService.getHelpfulAttributes( this.mcIdTwo ).subscribe( data => {
            this.jobTwoHelpfulAttributes = data;
        } );

        this._careerDetailService.getHelpfulAttributes( this.mcIdThree ).subscribe( data => {
            this.jobThreeHelpfulAttributes = data;
        } );

        this._careerDetailService.getHelpfulAttributes( this.mcIdFour ).subscribe( data => {
            this.jobFourHelpfulAttributes = data;
        } );

        this._userService.getNotes().subscribe(data => {
            const oneNote = data.filter(d => d.mcId === this.mcIdOne)[0];
            this.jobOneNote = oneNote ? oneNote : {notes: ''};

            const twoNote = data.filter(d => d.mcId === this.mcIdTwo)[0]
            this.jobTwoNote = twoNote ? twoNote : {notes: ''};

            if ( this.mcIdThree != undefined ) {
                const threeNote = data.filter(d => d.mcId === this.mcIdThree)[0];
                this.jobThreeNote = threeNote ? threeNote : {notes: ''};
            }

            if ( this.mcIdFour != undefined ) {
                const fourNote = data.filter(d => d.mcId === this.mcIdFour);
                this.jobFourNote = fourNote ? fourNote : {notes: ''}
            }
        })
    }


    ngAfterViewInit() {

        $( function() {
            $( '.ratebox' ).raterater( {

                // allow the user to change their mind after they have submitted a rating
                allowChange: false,

                // width of the stars in pixels
                starWidth: 12,

                // spacing between stars in pixels
                spaceWidth: 0,

                numStars: 5,
                isStatic: true,
                step: false,
            } );

        } );

    }


    /**
     * Print and download PDF Desktop version..
     */
    getCanvas() {
        return new Promise(( resolve, reject ) => {
            html2canvas(this.printElement.nativeElement, {
                imageTimeout: 2000,
                removeContainer: true,
                backgroundColor: '#fff',
            } ).then( function( canvas ) {
                var img = canvas.toDataURL( "image/jpeg" );
                resolve( img );
            }, function( error ) {
                reject( error );
            } );
        } );
    }

    geMobiletCanvas( mobileElement ) {

        if ( mobileElement == undefined ) {
            return;
        }

        return new Promise(( resolve, reject ) => {

            html2canvas( mobileElement.nativeElement, {
                imageTimeout: 2000,
                removeContainer: true,
                backgroundColor: '#fff'
            } ).then( function( canvas ) {
                var img = canvas.toDataURL( "image/jpeg" );
                resolve( img );
            }, function( error ) {
                reject( error );
            } );
        } );
    }

    printPdf = function() {

        var t = this.getCanvas();

        // IE support
        if ( navigator.appName == 'Microsoft Internet Explorer' || !!( navigator.userAgent.match( /Trident/ ) || navigator.userAgent.match( /rv 11/ ) ) ) {
            this.printIE( t );
            return false;
        }

        t.then( function( s ) {

            var docDefinition = {
                content: [{
                    text: 'Occupation Comparison\n\n',
                    alignment: 'center',
                    fontSize: 18,
                    bold: true
                }, {
                    image: s,
                    width: 520
                }]
            };

            pdfMake.createPdf( docDefinition ).print();
        }, function( error ) {
            console.log( error );
            alert( error );
        } );

    };


    printPdfMobile = function( functionType ) {

        var printMobileElementOne = this.geMobiletCanvas( this.printMobileElementOne );
        var printMobileElementTwo = this.geMobiletCanvas( this.printMobileElementTwo );
        var printMobileElementThree = this.geMobiletCanvas( this.printMobileElementThree );
        var printMobileElementFour = this.geMobiletCanvas( this.printMobileElementFour );

        var allElement = Promise.all( [printMobileElementOne, printMobileElementTwo, printMobileElementThree, printMobileElementThree] );

        allElement.then( function( s ) {

            var docDefinition = {
                content: [{
                    text: 'Occupation Comparison\n\n',
                    alignment: 'center',
                    fontSize: 18,
                    bold: true
                }, {
                    image: s[0],
                    width: 520,
                    height: 520
                }, {
                    image: s[1],
                    width: 520,
                    height: 520
                }]
            };

            if ( s[2] ) {
                docDefinition.content.push( {
                    image: s[2],
                    width: 520,
                    height: 520
                } );
            }

            if ( s[3] ) {
                docDefinition.content.push( {
                    image: s[3],
                    width: 520,
                    height: 520
                } );
            }

            if ( functionType == 'print' ) {
                pdfMake.createPdf( docDefinition ).print();
            } else if ( functionType == 'download' ) {
                pdfMake.createPdf( docDefinition ).download( 'compare.pdf' );
            }

        }, function( error ) {
            console.log( error );
            alert( error );
        } );

    };


    /**
     * IE support for printing element.
     */
    printIE( canvas ) {

        canvas.then( function( img ) {
            var popup = window.open();
            popup.document.write( '<img src=' + img + '>' );
            popup.document.close();
            popup.focus();
            popup.print();
            popup.close();

        }, function( error ) {

            alert( 'Printing failed.' );
        } );

    }

    downloadPdf = function() {
        var t = this.getCanvas();

        t.then(( s ) => {
            var docDefinition = {
                content: [{
                    text: 'Occupation Comparison\n\n',
                    alignment: 'center',
                    fontSize: 18,
                    bold: true
                }, {
                    image: s,
                    width: 520
                }]
            };

            pdfMake.createPdf( docDefinition ).download( 'compare.pdf' );
        }, ( error ) => {
            console.log( error );
            alert( error );
        } );
    };

}
