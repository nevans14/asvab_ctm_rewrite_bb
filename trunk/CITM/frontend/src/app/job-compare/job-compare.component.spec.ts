import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JobCompareComponent } from './job-compare.component';

describe('JobCompareComponent', () => {
  let component: JobCompareComponent;
  let fixture: ComponentFixture<JobCompareComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JobCompareComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JobCompareComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
