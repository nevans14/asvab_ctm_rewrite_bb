import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GuidedExplorationComponent } from './guided-exploration.component';

describe('GuidedExplorationComponent', () => {
  let component: GuidedExplorationComponent;
  let fixture: ComponentFixture<GuidedExplorationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GuidedExplorationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GuidedExplorationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
