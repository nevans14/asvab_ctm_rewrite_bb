import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CareerPathwayComponent } from './career-pathway.component';

describe('CareerPathwayComponent', () => {
  let component: CareerPathwayComponent;
  let fixture: ComponentFixture<CareerPathwayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CareerPathwayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CareerPathwayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
