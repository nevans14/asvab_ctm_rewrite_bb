import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ConfigService  } from 'app/services/config.service';
import { CareerDetailService } from 'app/services/career-detail.service';
import { CareerSearchService } from 'app/services/career-search.service';
import { NgxSpinnerService } from 'ngx-spinner';
import {GoogleAnalyticsService} from '../services/google-analytics.service';
import { SearchListService } from 'app/services/search-list.service';
import { Title, Meta } from '@angular/platform-browser';
import $ from 'jquery'
declare var $: $

@Component({
  selector: 'app-career-pathway',
  templateUrl: './career-pathway.component.html',
  styleUrls: ['./career-pathway.component.scss']
})
export class CareerPathwayComponent implements OnInit, AfterViewInit {
  mcId: any;
  careerPathwayInfo: any;
  milCareer: any;
  profile: any;
  displayCircle = true;
  imageUrl: string;
  searchList: any;

  constructor(
    private _route: ActivatedRoute,
    private _router: Router,
    private _config: ConfigService,
    private _careerDetailService: CareerDetailService,
    private _careerSearchService: CareerSearchService,
    private _meta: Meta,
    private _titleTag: Title,
    private _spinner: NgxSpinnerService,
    private _googleAnalyticsService: GoogleAnalyticsService,
    private _searchService: SearchListService,
  ) { }

  ngOnInit() {
    this._spinner.show();
    this.imageUrl = this._config.getImageUrl();

    this._route.params.subscribe(res => {
      this.mcId = res.mcId;
      this.loadData();
    });

    window.scroll(0, 0);

  }

  translateSlug() {
    if (this.searchList) {
      const career: any = this.searchList.find(i => i.slug === this.mcId);
      this.mcId = career && career.mcId ? career.mcId : '';
    }
  }

  translateMcId() {
    if (this.searchList) {
      const career: any = this.searchList.find(i => i.mcId === this.mcId);
      this.mcId = career && career.slug ? career.slug : '';
    }
  }

  loadDetailData() {
    this._careerDetailService.getMilitaryCareer(this.mcId).subscribe(data => {
      this.milCareer = data;
      if (data) {
        this._titleTag.setTitle(this.milCareer.title + ' | Careers in the Military');
        this._meta.updateTag({name: 'description', content: 'Careers in the Military powered by ASVAB Career Exploration Program allows students to explore different jobs across Services in the context of their skills and interests.'});

        this._meta.updateTag({property: 'og:title', content: this.milCareer.title  + ' | Careers in the Military'});
        this._meta.updateTag({ property: 'og:description', content: 'Careers in the Military powered by ASVAB Career Exploration Program allows students to explore different jobs across Services in the context of their skills and interests.' });
        this._meta.updateTag({ property: 'og:image', content: 'http://www.asvabprogram.com/media-center-content/thumbnails/social-share/CITM_Webshare.jpg' });
        this._meta.updateTag({name: 'twitter:title', content: this.milCareer.title  + ' | Careers in the Military'});
        this._meta.updateTag({ name: 'twitter:description', content: 'Careers in the Military powered by ASVAB Career Exploration Program allows students to explore different jobs across Services in the context of their skills and interests.' });
        this._meta.updateTag({ name: 'twitter:image', content: 'http://www.asvabprogram.com/media-center-content/thumbnails/social-share/CITM_Webshare.jpg' });
      }
    });

    this._careerSearchService.getCareerPath(this.mcId).subscribe(data => {
      for (let i = 0; i < data.length; i++) {
        data[i].imageUrl = this.imageUrl  + 'citm-images/pathways/' + this.mcId + '_' + (i + 1) + '.jpg'
      }
      this.careerPathwayInfo = data;
      this._spinner.hide();
      if (data) {
        this._meta.updateTag({name: 'description', content: this.careerPathwayInfo[0].description});

        this._meta.updateTag({property: 'og:description', content: this.careerPathwayInfo[0].description});
        this._meta.updateTag({property: 'og:image', content: this.careerPathwayInfo[0].imageUrl});
  
        this._meta.updateTag({name: 'twitter:description', content: this.careerPathwayInfo[0].description});
        this._meta.updateTag({name: 'twitter:image', content: this.careerPathwayInfo[0].imageUrl});
      }
    });

    this._careerDetailService.getProfile(this.mcId).subscribe(data => {
      this.profile = data;
    });
  }

  loadData() {
    this.searchList = this._searchService.getSearchList();
    if (!this.searchList) {
        const searchListInterval = setInterval(() => {
            if (this._searchService.searchListObject) {
                clearInterval(searchListInterval);
                this.searchList = this._searchService.searchListObject;

                if (!isNaN(this.mcId)) {
                  this.translateMcId();
                  this._router.navigate(['career-pathway/' + this.mcId]);
                }

                if (isNaN(this.mcId)) {
                  this.translateSlug();
                  this.loadDetailData();
                }
            }
        }, 200);
    } else {
      if (!isNaN(this.mcId)) {
        this.translateMcId();
        this._router.navigate(['career-pathway/' + this.mcId]);
      }
      
      if (isNaN(this.mcId)) {
        this.translateSlug();
        this.loadDetailData();
      }
    }
  }

  ga(param) {
        this._googleAnalyticsService.trackClick(param);
    }

    gaSocialShare(socialPlug, value) {
        this._googleAnalyticsService.trackSocialShare(socialPlug, value);
    }
    
    ngAfterViewInit() {
        
        $(document).ready(function () {
            function viewport() {
              var e = window, a = 'inner';
              if (!('innerWidth' in window )) {
                a = 'client';
                var htmlEl = document.documentElement || document.body;
                return { width : htmlEl[ a+'Width' ] , height : htmlEl[ a+'Height' ] };
              }
              return { width : e[ a+'Width' ] , height : e[ a+'Height' ] };
            }
        
            var vpWidth = viewport().width; // This should match your media query

            if (vpWidth > 639) {
              $('.msCircle').resizeOnApproach({elementDefault: 40, elementClosest: 70,triggerDistance:200, setWidthAndHeight: true});  
            } else {    
              dotabs();
            }

            $( window ).resize(function() {
                  var vpWidth = viewport().width;
                  if (vpWidth > 639) {
                    $('#msTabs').remove();
                    if (!$('.msCircle').hasClass('expanded')) {
                      $('.msCircle').resizeOnApproach({elementDefault: 40, elementClosest: 70,triggerDistance:200, setWidthAndHeight: true});
                    }
                  } else {
                    if ($('#msTabs').length == 0) dotabs();
                    $('#msJoin').unbind('mousemove');
                    //reset
                    $('.msCircle').removeClass('expanded').removeClass('collapsed').removeAttr('style');
                    $('#joinNav').remove();
                    $('.msCircle span').show();
                    $('.msCircle .description').hide();
                  }
                });
                var mkCircleInterval = setInterval(function(){
                if($('.msCircle').length>0){
                    clearInterval(mkCircleInterval);
                    
                    $('.msCircle').on('click', function(){
                      var vpWidth = viewport().width;
                      if (vpWidth > 639) {
                        if(!$(this).hasClass('expanded')) expandCircle($(this));      
                      } else {
                        var id = 'tab-'+$(this).attr('id');
                        $('.msCircle').removeClass('selected active');
                        $(this).addClass('selected');
                        $('.msContainer.active').removeClass('active').fadeOut(200, function() {
                          $('#'+id).addClass('active').fadeIn(200);  
                        });

                      }

                    });     
                }

            }, 500);

            // arrow navigation
            $('body').unbind().on('click', '#joinNav > a',  function(){
              var $current, ind;

              //close the current pathway
              if ($(this).attr('id') == 'joinClose'){
                $('.msCircle').removeClass('expanded collapsed transition').addClass('close-transition').removeAttr('style');
                $('#joinNav').remove();
                $('.msCircle span').show();
                $('.msCircle .description').hide();
                $('.msCircle').resizeOnApproach({elementDefault: 40, elementClosest: 70,triggerDistance:200, setWidthAndHeight: true});
               setTimeout(function(){$('.msCircle').removeClass('close-transition');},1000);
               return false;
              }

              if ($(this).attr('id') == 'joinLeft') {
                $current = $('.expanded').prev('.msCircle');
                ind = 3;
              } else {
                $current = $('.expanded').next('.msCircle');
                ind = 0;
              }

              if ( $current.length ) { 
                expandCircle($current);
              } else  { 
                expandCircle($( '.msCircle' ).eq(ind));
              }
              return false;
            });

            // expand/collapse animations
            function expandCircle($selected) {
              $('#msJoin').unbind('mousemove');
              $('#joinNav').remove();
              $('.description, .msCircle span').hide();
              $('.msCircle').removeClass('expanded').addClass('transition').addClass('collapsed').removeAttr('style').css('float','left');
              var color = $selected.css('backgroundColor');
              var image = 'url(' + $selected.find('img').eq(0).attr('src') + ')'; //data attr
              var joinNav = '<div id="joinNav"><a href="#" id="joinLeft" class="transition" style="background-color:'+color+'"></a><a href="#" id="joinRight" class="transition" style="background-color:'+color+'"></a><a href="#" id="joinClose" class="transition" style="background-color:'+color+'">&times;</a></div>'; 
              $selected.removeClass('collapsed').addClass('expanded').css({'border-color':color, 'background-image':image}).prepend(joinNav);
              $('.description, span', $selected).delay(500).fadeIn( 400 );
            }

            // scripts for mobile version
            function dotabs() { 
              $('#msJoin').append('<div id="msTabs"></div>');
              $('.msContent').each(function(i){
                var image = '<img src="' + $(this).parent().find('img').eq(0).attr('src') + '">'; //data attr
                var desc = $('.description', this).html() + image;
                var show = (i == 0) ? 'style="display:block"' : '';
                var active = (i != 0) ? '' : 'active';
                $('#msTabs').append('<div id="tab-'+$(this).closest('.msCircle').attr('id')+'" class="msContainer '+active+'" '+show+'>'+desc+'</div>');
                $('.msCircle').removeClass('selected').eq(0).addClass('selected');
              });
            }

            if ($('.bxslider3wide').length || $('.bxslider2wide').length) {
                $(".bx-default-pager").addClass("tag-home");
            }
            
        });
      }

}
