import { Component, OnInit, Renderer2, AfterViewInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { SearchListService } from 'app/services/search-list.service';
import { UserService } from 'app/services/user.service';
import { ConfigService } from 'app/services/config.service';
import { MatDialog } from '@angular/material';
import { PrivacySecurityDialogComponent } from 'app/core/dialogs/privacy-security-dialog/privacy-security-dialog.component';
import { NeedToLoginDialogComponent } from 'app/core/dialogs/need-to-login-dialog/need-to-login-dialog.component';
import { FavoritesService } from 'app/services/favorites.service';
import {GoogleAnalyticsService} from '../services/google-analytics.service';
import { PopupPlayerComponent } from 'app/core/dialogs/popup-player/popup-player.component';
import { Title, Meta } from '@angular/platform-browser';
import { ContentManagementService } from 'app/services/content-management.service';
import { UtilityService} from 'app/services/utility.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit, AfterViewInit, OnDestroy {
  navigationSubscription;
  fullpageApi: any;
  fullpageConfig;
  jobList = [];
  allJobList = [];
  hotJobList = [];
  searchList: any;
  careerProfileList: any;
  item: any;
  item2: any;
  item3: any;
  displayHotJobs = true;
  displayList = true;
  currentJobListPos = 0;
  jobsInList = 0;
  showMore = true;
  routeParam: String;
  slides: any;
  currentSlide = 1;
  numberOfSlides;
  currentSlideNumber;
  displayPosition;
  lastCareerOnPage;
  versionText;
  favoriteCount = 0;
  myStyle =  {'cursor': 'pointer'};
  disabled = false;
  searchListInterval;
  numberOfTicks = 0;
  customOptions: any = {  // used for owl carousel before, but owl stops scroll after second image on Home's Browse All
    loop: true,
    mouseDrag: true,
    touchDrag: true,
    pullDrag: false,
    autoplayHoverPause: true,
    dots: true,
    dotsData: false,
    navSpeed: 700,
    autoplay: true,
    autoplaySpeed: 2000,
    lazyLoad: true,
    responsive: {
      0: {
        items: 1,
      },
      420: {
        items: 2,
     },
      640: {
        items: 3,
      }
    },
    nav: false,
    navText: ['',
              ''
   ]
  };
  customOptions2: any = { // used for owl carousel before, but owl stops scroll after second image on Home's Browse All
    loop: false,
    margin: 10,
    mouseDrag: true,
    touchDrag: true,
    pullDrag: false,
    dots: false,
    dotsData: false,
    autoplay: true,
    navSpeed: 700,
    autoplaySpeed: 1500,
    nav: false,
    slideBy: 1,
    responsive: {
	  0: {
        items: 1
      },
      480: {
        items: 2
      },
      640: {
        nav: true,
		items: 1
      }
    }
  };

  /* ngx slick carousel config */
  slideConfig = {
    dots: false,
    infinite: false,
    speed: 300,
    slidesToShow: 4,
    slidesToScroll: 4,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
          infinite: true,
          dots: false
        }
      }, {
        breakpoint: 639,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }, {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ],
    "autoplay": true,
    "autoplaySpeed": 3000,
  };
  displayBanner = false;
  webpEnabled = false;

  constructor(
    private _renderer2: Renderer2,
    private _route: ActivatedRoute,
    private _router: Router,
    private _config: ConfigService,
    private _searchListService: SearchListService,
    private _userService: UserService,
    private _favoritesService: FavoritesService,
    private _meta: Meta,
    private _titleTag: Title,
    private _dialog: MatDialog,
    private _googleAnalyticsService: GoogleAnalyticsService,
    private _cmsService: ContentManagementService,
    private _utilityService: UtilityService,
  ) {
    this.fullpageConfig = {
      licenseKey: '8AB8593E-25F04198-AABED26B-4FCED4CB',
      controlArrows: false,
      scrollOverflow: false,
    };
    // Subscribe to router event Navigation End so can refresh data after log in
    this.navigationSubscription = this._router.events.subscribe(event => {
      if (event instanceof NavigationEnd && event.urlAfterRedirects.indexOf('/home') > -1) {
        this.loadData();
      }
    });

    this._cmsService.getDbinfoByName('show_maintenance_banner')
    .subscribe(data => {
      if (data.value.toLowerCase() === 'true') {
        this.displayBanner = true;
        this._renderer2.addClass(document.body, 'banner-active');
      }
    });

    this.webpEnabled = this._utilityService.canUseWebP();
  }

  ngOnInit() {
    this.versionText = 'v ' + this._config.getVersion() + '  Last Updated:  ' + this._config.getVersionDate();

    this._meta.updateTag({ property: 'og:title', content: "Careers in the Military" });
    this._meta.updateTag({ property: 'og:description', content: "Careers in the Military powered by ASVAB Career Exploration Program allows students to explore different jobs across Services in the context of their skills and interests." });
    this._meta.updateTag({property: 'og:image', content: 'http://www.asvabprogram.com/media-center-content/thumbnails/social-share/CITM_Webshare.jpg'});

    this._meta.updateTag({ name: 'twitter:title', content: "Careers in the Military" });
    this._meta.updateTag({ name: 'twitter:description', content: "Careers in the Military powered by ASVAB Career Exploration Program allows students to explore different jobs across Services in the context of their skills and interests." });
    this._meta.updateTag({name: 'twitter:image', content: 'http://www.asvabprogram.com/media-center-content/thumbnails/social-share/CITM_Webshare.jpg'});

    this._titleTag.setTitle("Careers in the Military");
    this._meta.updateTag({ name: 'description', content:  "Careers in the Military powered by ASVAB Career Exploration Program allows students to explore different jobs across Services in the context of their skills and interests." });

    // NOTE: Commented out because of Angular bug on scroll to anchor/long time before scroll
    // this._route.fragment.subscribe((fragment: string) => { 
    //   if (fragment && document.getElementById(fragment) != null) {
    //     document.getElementById(fragment).scrollIntoView({ behavior: "smooth" });
    //   }
    // });
  }

  loadData() {
    this._renderer2.addClass(document.body, 'home');
    this._route.params.subscribe(res => {
      this.routeParam = res.section;
    });

    const searchObject = this._searchListService.getSearchList();
    const self = this;
    if (!searchObject) {
      if (!this.searchListInterval) {
        this.searchListInterval = setInterval(function () {
          if (!self._searchListService.getProcessingSearchList()) {
            clearInterval(self.searchListInterval);
            self.searchList = self._searchListService.searchListObject;
            this.favoriteCount = self._userService.favoritesListObject ? self._userService.favoritesListObject.length : 0;
            self.refreshPage();
          }
        }, 200);
      }
    } else {
      this.searchList = searchObject;
      this.favoriteCount = self._userService.favoritesListObject ? self._userService.favoritesListObject.length : 0;
      this.refreshPage();
    }

    // Returns observable that contains list from session otherwise from API
    this._searchListService.getCareerProfileList().subscribe(data => {
      this.careerProfileList = data;
      this.refreshPage();
    })
  }

  ngAfterViewInit() {
    if (this.routeParam && this.routeParam.toUpperCase() === 'BROWSE') {
      const el = document.getElementById('boxes');
      el.scrollIntoView();
      this.fullpageApi.moveSectionDown();
    }
  }

  getRef(fullPageRef) {
    this.fullpageApi = fullPageRef;
  }

    ga(param) {
        this._googleAnalyticsService.trackClick(param);
    }

    gaSocialShare(socialPlug, value) {
        this._googleAnalyticsService.trackSocialShare(socialPlug, value);
    }

  refreshPage() {
    if (this.searchList != null &&
      this._userService.favoritesListObject != null &&
      this._userService.hiddenListObject != null) {

      this.allJobList = this._searchListService.searchListObject;
      if (this._searchListService.careerProfileListObject) {
        this.slides = this._searchListService.careerProfileListObject;
      }

      if (this.allJobList != null) {
        this.allJobList = this.searchList.filter(job => job.show === true);
        this.hotJobList = this.allJobList.filter(job => job.isHotJob === 'T');
        this.displayHotJobs = true;
        this.displayList = true;
        this.getJobList();
        this.getJobItem();
      }

      this.currentJobListPos = 0;
      this.displayPosition = this.currentJobListPos + 1;
      this.lastCareerOnPage = this.displayList ? this.displayPosition : this.displayPosition + 2;
      this.lastCareerOnPage = this.lastCareerOnPage > this.jobsInList ? this.jobsInList : this.lastCareerOnPage;
    }

    if (this.careerProfileList != null) {
      this.slides = this.careerProfileList;
      if (this.slides != null) {
        for (let i = 0; i < this.slides.length; i++) {
          this.slides[i].snapshotQuestionList[0].showMore = true;
          this.slides[i].imageList[0].imageUrl = this.getProfileImage(this.slides[i].imageList[0].imageName);
        }
      }
    }
  }
  onClickListType(type) {
    this.displayHotJobs = type === 'Hot' ? true : false;
    this.getJobList();
    this.getJobItem();
  }

  onFavoriteClick(item) {
    if (this.disabled) {
      return;
    }
    this.disabled = true;
    this.myStyle =  {'cursor': 'wait'};
    this._favoritesService.onFavoriteClick(item)
    .then (() => {
      this.favoriteCount = this._userService.favoritesListObject.length;
      this.disabled = false;
      this.myStyle =  {'cursor': 'pointer'};
    })
    .catch(() => {
      this.disabled = false;
      this.myStyle =  {'cursor': 'pointer'};
    });
  }

  onHiddenClick(item) {
    const self = this;
    return this._favoritesService.onHiddenClick(item)
      .then((obj) => {
        self.refreshList();
        return;
      });
  }

  onUndoClick() {
    const itemId = this.item.mcId;
    const self = this;
    return this._favoritesService.onUndoClick(this._searchListService.searchListObject)
    .then (() => {
      self.refreshList(itemId);
    });
  }

  refreshList(itemId = -1) {
    this.allJobList = this.searchList.filter(job => job.show === true);
    this.hotJobList = this.allJobList.filter(job => job.isHotJob === 'T');
    this.jobList = this.displayHotJobs ? this.hotJobList : this.allJobList;
    this.jobsInList = this.displayHotJobs ? this.hotJobList.length : this.allJobList.length;
    if (itemId > -1) {
      this.currentJobListPos = this.jobList.findIndex(job => job.mcId === itemId);
    }
    this.displayPosition = this.currentJobListPos + 1;
    this.lastCareerOnPage = this.displayList ? this.displayPosition : this.displayPosition + 2;
    this.lastCareerOnPage = this.lastCareerOnPage > this.jobsInList ? this.jobsInList : this.lastCareerOnPage;

    this.getJobItem();
  }

  onGridListClick(type) {
    this.displayList = type === 'List' ? true : false;
    this.getJobList();
    this.getJobItem();
  }

  nextJob() {
    if (this.currentJobListPos + 1 < this.jobsInList) {
      this.currentJobListPos = this.displayList ? this.currentJobListPos + 1 : this.currentJobListPos + 3;
      this.displayPosition = this.currentJobListPos + 1;
      this.lastCareerOnPage = this.displayList ? this.displayPosition : this.displayPosition + 2;
      this.lastCareerOnPage = this.lastCareerOnPage > this.jobsInList ? this.jobsInList : this.lastCareerOnPage;
      this.getJobItem();
    }
  }

  previousJob() {
    if (this.currentJobListPos > 0) {
      this.currentJobListPos = this.displayList ? this.currentJobListPos - 1 : this.currentJobListPos - 3;
      this.displayPosition = this.currentJobListPos + 1;
      this.lastCareerOnPage = this.displayList ? this.displayPosition : this.displayPosition + 2;
      this.lastCareerOnPage = this.lastCareerOnPage > this.jobsInList ? this.jobsInList : this.lastCareerOnPage;
      this.getJobItem();
    }
  }

  getJobList() {
    this.currentJobListPos = 0;
    this.displayPosition = this.currentJobListPos + 1;
    this.lastCareerOnPage = this.displayList ? this.displayPosition : this.displayPosition + 2;
    this.lastCareerOnPage = this.lastCareerOnPage > this.jobsInList ? this.jobsInList : this.lastCareerOnPage;

    this.jobList = this.displayHotJobs ? this.hotJobList : this.allJobList;
    this.jobsInList = this.displayHotJobs ? this.hotJobList.length : this.allJobList.length;
  }

  getJobItem() {
    this.item = null;
    this.item2 = null;
    this.item3 = null;
    this.item = this.displayHotJobs ? this.hotJobList[this.currentJobListPos] : this.jobList[this.currentJobListPos];
    if (this.currentJobListPos < this.jobsInList) {
      this.item2 = this.displayHotJobs ? this.hotJobList[this.currentJobListPos + 1] : this.jobList[this.currentJobListPos + 1];
    }
    if ((this.currentJobListPos + 1) < this.jobsInList) {
      this.item3 = this.displayHotJobs ? this.hotJobList[this.currentJobListPos + 2] : this.jobList[this.currentJobListPos + 2];
    }
  }

  getAnswer(value, showMore) {
    const re = new RegExp(String.fromCharCode(160), 'g');
    let text = value.replace(re, ' ');
    text = text.replace(/<[^>]*>/g, '');
    return showMore ? text.substring(0, 50) : value;
  }

  getItemMcId(itemNum) {
    let itemMcId;
    switch (itemNum) {
      case (1):
        itemMcId = this.item.mcId;
        break;
      case (2):
        itemMcId = this.item2.mcId;
        break;
      case (3):
        itemMcId = this.item3.mcId;
        break;
    }
    return itemMcId;
  }

  getProfileImage(imageName) {
    const imageUrl = this._config.getImageUrl();

    return imageUrl + 'profile-images/' + imageName;
  }

  showPrivacySecurity() {
    this._dialog.open(PrivacySecurityDialogComponent, {});
  }

  openNeedToLoginDialog() {
    this._dialog.open(NeedToLoginDialogComponent, {
      panelClass: 'custom-dialog'
    });
  }

  /**
   * Open video player dialog.
   */
  openPlayer(videoId) {
      this._dialog.open(PopupPlayerComponent, {
          data: { videoId: videoId }
      });
  }

  ngOnDestroy() {
    if (this.fullpageApi) {
      this.fullpageApi.destroy();
    }
    this._renderer2.removeClass(document.body, 'home');
    if (this.displayBanner) {
      this._renderer2.removeClass(document.body, 'banner-active');
    }

    if (this.navigationSubscription) {
      this.navigationSubscription.unsubscribe();
    }
  }

  slideChanged(owlEvent) {
    // console.debug('slide:', owlEvent);
    this.currentSlide = owlEvent.startPosition + 1;
  }
  
  isLoggedIn() {
    return this._config.isLoggedIn();
  }

  slickInit(e) {
    console.debug( 'slick initialized' );
  }

  breakpoint(e) {
    console.debug( 'breakpoint' );
  }

  afterChange(e) {
    console.debug( 'afterChange' );
  }

  beforeChange(e) {
    console.debug( 'beforeChange' );
  }
}
