import { Component, OnInit, Input } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ConfigService } from 'app/services/config.service';
import { PortfolioService } from 'app/services/portfolio.service';
import * as cloneDeep from 'lodash/cloneDeep';
import { ScoreService } from '../services/score.service';
import { MessageDialogComponent } from 'app/core/dialogs/message-dialog/message-dialog.component';

declare var pdfMake: any;

export interface ParentType {
    cepFavoriteOccupationsList: [any],
    userProfileInfo: any,
    favoriteList: [any],
    careerClusterFavoriteList: [any],
    serviceFavoriteList: [any]
}

@Component({
    selector: 'app-portfolio-tab',
    templateUrl: './portfolio.component.html',
    styleUrls: ['./profile.component.scss']
})
export class PortfolioComponent implements OnInit {
    // Don't know if we need this
    myStyle = {};
    removeItem = false;
    ssoUrl = this._configService.getSsoUrl();

    workExperience: any;
    workExperienceObject = {
        companyName: undefined,
        jobTitle: undefined,
        startDate: undefined,
        endDate: undefined,
        jobDescription: undefined,
        id: undefined
    };

    updateWorkExperienceObject = {
        companyName: undefined,
        jobTitle: undefined,
        startDate: undefined,
        endDate: undefined,
        jobDescription: undefined,
        id: undefined
    };
    haveTestScores: any;
    scoreSummaryList: any;
    a_af: any;
    adm_navy: any;
    afqt: any;
    ao: any;
    ar: any;
    as: any;
    bee_navy: any;
    cl_army: any;
    cl_mc: any;
    co_army: any;
    e_af: any;
    el: any;
    el_army: any;
    el_mc: any;
    el_navy: any;
    eng_navy: any;
    fa_army: any;
    g_af: any;
    gm_army: any;
    gs: any;
    gt_army: any;
    gt_mc: any;
    gt_navy: any;
    hm_navy: any;
    m_af: any;
    mc: any;
    mec2_navy: any;
    mec_navy: any;
    mk: any;
    mm_army: any;
    mm_mc: any;
    nuc_navy: any;
    of_army: any;
    ops_navy: any;
    pc: any;
    sc_army: any;
    st_army: any;
    testCompletionCd: any;
    ve: any;
    wk: any;
    el_cg: any;
    mr_cg: any;
    gt_cg: any;
    cl_cg: any;
    education: any;
    compositeFavoritesFlags: any;

    // Added for production build
    composite: any;
    scores: any;

    /**
       * User's selection for sections to include in print.
       */
    workExperiencePrint = true;
    educationPrint = true;
    achievementPrint = true;
    testScorePrint = true;
    compositeScoreFavoritesPrint = true;
    physicalFitnessPrint = true;
    favoriteCareerClusterPrint = true;
    favoriteServicePrint = true;
    favoriteOccupationPrint = true;
    favoriteCepOccupationPrint = true;
    fyiScorePrint = true;

    /*
     * Work experience
     */
    showWorkExperienceInputs = false;
    showUpdateWorkExperienceInputs = false;
    workExperienceEndDateRequired = true;
    updateWorkExperienceEndDateDisabled = true;
    deleteRecord = false;
    savingWorkExperience = false;
    selectedWorkEditIndex = undefined;

    // resourceSpecific = resourceSpecific.data;
    // resourceGeneric = resourceGeneric.data;
    // ssoUrl = ssoUrl;
    // favoriteList = favoriteList;
    // saveSearchList = saveSearchList.data;
    // userProfileInfo = userProfileInfo.data[0];
    // data = {};
    isOnlyCitm = true; // TODO: what is this? (userSystemInfo.this.results ===  'CITM' ? true : false);
    portfolioInfoOpen = true;
    // weNoticedOpen = true;
    // serviceContacts = serviceContacts.data;
    // favoriteCompositeScoresList = favoriteCompositeScoresList;
    scoreSummary: any; // = scoreSummary ? scoreSummary.data : null;


    /**
     * Education
     */
    gpaRegEx = '\^((\d+\.?|\.(?=\d))?\d{0,3})$';
    showEducationInputs = false;
    savingEducation = false;
    educationObject = {
        schoolName: undefined,
        gpa: undefined,
        startDate: undefined,
        endDate: undefined,
        activities: undefined,
        achievements: undefined,
        id: undefined
    };
    updateEducationObject = {
        schoolName: undefined,
        gpa: undefined,
        startDate: undefined,
        endDate: undefined,
        activities: undefined,
        achievements: undefined,
        id: undefined
    };
    selectedEducationEditIndex = undefined;
    showUpdateEducationInputs = false;

    /**
     * Achievements
     */
    achievement: any;
    showAchievementInputs = false;
    savingAchievement = false;
    achievementObject = {
        description: undefined,
        id: undefined,
    };
    updateAchievementObject: any;
    selectedAchievementEditIndex = undefined;
    showUpdateAchievementInputs = false;

    @Input() parent: ParentType;
    constructor(
        private _portfolioService: PortfolioService,
        private _configService: ConfigService,
        private _scoreService: ScoreService,
        private _dialog: MatDialog,
    ) {

    }

    /**
     * Test scores.
     */
    actScore: any;
    asvabScore: any;
    fyiScore: any;
    satScore: any;
    otherScore: any;

    showScoreInputs = false; // used for all scores
    // console.log(actScore.length);
    testSelected = this.actScore && this.actScore.length === 0 ? 'ACT' :
        this.asvabScore && this.asvabScore.length === 0 ? 'ASVAB' :
            this.satScore && this.satScore.length === 0 ? 'SAT' :
                'OTHER'; // ACT
    // default
    // selection
    testEditDisable = false;

    /**
     * ACT
     */
    // actScore = actScore.data;
    savingActScore = false;
    actObject = {
        readingScore: undefined,
        mathScore: undefined,
        writingScore: undefined,
        englishScore: undefined,
        scienceScore: undefined
    };
    updateActObject: any;

    showUpdateFyiScoreInputs = false;
    showUpdateAsvabScoreInputs = false;
    showUpdateActScoreInputs = false;
    showUpdateSatScoreInputs = false;
    showUpdateOtherScoreInputs = false;

    selectedACTEditIndex = undefined;

    /**
     * ASVAB
     */
    savingAsvabScore = false;
    asvabObject = {
        verbalScore: undefined,
        mathScore: undefined,
        afqtScore: undefined,
        scienceScore: undefined,
        id: undefined,
    };
    updateAsvabObject: any;
    selectedASVABEditIndex = undefined;

    /**
   * SAT
   */
    savingSatScore = false;
    satObject = {
        readingScore: undefined,
        mathScore: undefined,
        writingScore: undefined,
        subject: undefined
    };
    updateSatObject: any;
    selectedSATEditIndex = undefined;
    showSatInputs = false;

    /**
   * FYI. If user has FYI scores then use it as default.
   */

    hideFyi = false;

    savingFyiScore = false;
    fyiObject = {
        interest_code_one: undefined,
        interest_code_two: undefined,
        interest_code_three: undefined
    };
    updateFyiObject: any;
    selectedFYIEditIndex = undefined;
    showInterestInputs = false;
    interestSelected = undefined;
    showFyiInputs = false;

    /**
   * Other test scores.
   */
    savingOtherScore = false;
    otherObject = {
        description: undefined
    };
    updateOtherObject: any;
    selectedOtherEditIndex = undefined;
    showOtherInputs = false;

    /**
     * Physical Fitness Implementation.
     */
    physicalFitness: any;
    showPhysicalFitnessInputs = false;
    //physicalFitness = physicalFitness.data;
    //console.log(physicalFitness);
    savingPhysicalFitness = false;
    physicalFitnessObject;
    updatePhysicalFitnessObject: any;
    showUpdatePhysicalFitnessInputs = false;
    selectedPhysicalFitnessEditIndex = undefined;


    ngOnInit() {
        this._portfolioService.getUserSystem().subscribe(
            data => {
                this.isOnlyCitm = data.results ===  'CITM' ? true : false;
            }
        )

        this._portfolioService.getWorkExperience().subscribe(
            data => {
                this.workExperience = data;
                console.debug('workExperience:', this.workExperience);
            });

        this._portfolioService.getEducation().subscribe(
            data => {
                this.education = data;
                console.debug('education:', this.education);
            });

        this._portfolioService.getAchievements().subscribe(
            data => {
                this.achievement = data;
                console.debug('achievement:', this.achievement);
            });

        this._portfolioService.getActScore().subscribe(
            data => {
                this.actScore = data;
                console.debug('actScore:', this.actScore);
            });

        this._portfolioService.getAsvabScore().subscribe(
            data => {
                this.asvabScore = data;
                console.debug('asvabScore:', this.asvabScore);
            });

        this._portfolioService.getFyiScore().subscribe(
            data => {
                this.fyiScore = data;
                console.debug('fyiScore:', this.fyiScore);
            });

        this._portfolioService.getSatScore().subscribe(
            data => {
                this.satScore = data;
                console.debug('satScore:', this.satScore);
            });

        this._portfolioService.getOtherScore().subscribe(
            data => {
                this.otherScore = data;
                console.debug('otherScore:', this.otherScore);
            });

        this._portfolioService.getScoreSummary().subscribe(
            data => {
                this.scoreSummary = data;
                if (this.scoreSummary && this.scoreSummary.aFQTRawSSComposites) {
                    this.scoreSummary.aFQTRawSSComposites['el_cg'] = this.scoreSummary.aFQTRawSSComposites['el_navy'];
                    this.scoreSummary.aFQTRawSSComposites['mr_cg'] = this.scoreSummary.aFQTRawSSComposites['mec_navy'];
                    this.scoreSummary.aFQTRawSSComposites['gt_cg'] = this.scoreSummary.aFQTRawSSComposites['gt_navy'];
                    this.scoreSummary.aFQTRawSSComposites['cl_cg'] = this.scoreSummary.aFQTRawSSComposites['adm_navy'];
                }
                console.debug('scoreSummary:', this.scoreSummary);
            });

        // Physical Fitness
        this.physicalFitnessObject = {
            pullUps: undefined,
            curlUps: undefined,
            vSitReach: undefined,
            shuttleRun: undefined,
            mileRun: undefined,
            mileRunMin: '',
            mileRunSec: '',
            otherOne: undefined,
            otherTwo: undefined,
            otherThree: undefined,
            otherFour: undefined,
            id: undefined
        };
        this._portfolioService.getPhysicalFitness().subscribe(
            data => {
                this.physicalFitness = data;
                console.debug('physicalFitness:', this.physicalFitness);
            });

        this._scoreService.getCompositeFavoritesFlags().subscribe(flagList => {
            this.afqt = flagList.afqt;
            this.a_af = flagList.a_af;
            this.adm_navy = flagList.adm_navy;
            this.ao = flagList.ao;
            this.ar = flagList.ar;
            this.as = flagList.as;
            this.bee_navy = flagList.bee_navy;
            this.cl_army = flagList.cl_army;
            this.cl_mc = flagList.cl_mc;
            this.co_army = flagList.co_army;
            this.e_af = flagList.e_af;
            this.el = flagList.el;
            this.el_army = flagList.el_army;
            this.el_mc = flagList.el_mc;
            this.el_navy = flagList.el_navy;
            this.eng_navy = flagList.eng_navy;
            this.fa_army = flagList.fa_army;
            this.g_af = flagList.g_af;
            this.gm_army = flagList.gm_army;
            this.gt_army = flagList.gt_army;
            this.gt_navy = flagList.gt_navy;
            this.hm_navy = flagList.hm_navy;
            this.m_af = flagList.m_af;
            this.mc = flagList.mc;
            this.mec2_navy = flagList.mec2_navy;
            this.mec_navy = flagList.mec_navy;
            this.mk = flagList.mk;
            this.mm_army = flagList.mm_army;
            this.mm_mc = flagList.mm_mc;
            this.nuc_navy = flagList.nuc_navy;
            this.of_army = flagList.of_army;
            this.pc = flagList.pc;
            this.sc_army = flagList.sc_army;
            this.st_army = flagList.st_army;
            this.ve = flagList.ve;
            this.wk = flagList.wk;
            this.el_cg = flagList.el_navy;
            this.mr_cg = flagList.mr_cg;
            this.gt_cg = flagList.gt_cg;
            this.cl_cg = flagList.cl_cg;
            this.gt_mc = flagList.gt_mc;
        });
        this._scoreService.getScore().subscribe(scoreData => {
            if (scoreData) {
                this.scoreSummaryList = scoreData['aFQTRawSSComposites'];
                this._scoreService.setScoreObject(this.scoreSummaryList);
                this.scoreSummaryList.el_cg = this.scoreSummaryList.el_navy;
                this.scoreSummaryList.mr_cg = this.scoreSummaryList.mec_navy;
                this.scoreSummaryList.gt_cg = this.scoreSummaryList.gt_navy;
                this.scoreSummaryList.cl_cg = this.scoreSummaryList.adm_navy;
                this.haveTestScores = true;
            } else {
                this.haveTestScores = false;
            }
        });


        this._scoreService.getCompositeFavoritesFlags().subscribe(flagData => {
            this.compositeFavoritesFlags = flagData;
        });

    }

    /**
     * Check all sections to print.
     */
    selectAll = function () {
        this.workExperiencePrint = true;
        this.educationPrint = true;
        this.achievementPrint = true;
        this.testScorePrint = true;
        this.compositeScoreFavoritesPrint = true;
        this.physicalFitnessPrint = true;
        this.favoriteCareerClusterPrint = true;
        this.favoriteServicePrint = true;
        this.favoriteOccupationPrint = true;
        this.favoriteCepOccupationPrint = true;
        this.fyiScorePrint = true;
    };

    /**
     * Uncheck all sections.
     */
    selectNone = function () {
        this.workExperiencePrint = false;
        this.educationPrint = false;
        this.achievementPrint = false;
        this.testScorePrint = false;
        this.compositeScoreFavoritesPrint = false;
        this.physicalFitnessPrint = false;
        this.favoriteCareerClusterPrint = false;
        this.favoriteServicePrint = false;
        this.favoriteOccupationPrint = false;
        this.favoriteCepOccupationPrint = false;
        this.fyiScorePrint = false;
    };

    stripLeadingZero(score) {
        let strippedScore = '';
        strippedScore = parseInt(score, 10).toString();
        strippedScore = strippedScore === '0' ? 'NA' : strippedScore;
        return strippedScore;
    }

    /**
     * Setup for updating work experience input.
     */
    setupUpdateWorkExperience(id, index) {
        this.myStyle = {
            'cursor': 'not-allowed'
        };
        this.selectedWorkEditIndex = index;
        this.showWorkExperienceInputs = true;
        this.showUpdateWorkExperienceInputs = true;
        const length = this.workExperience.length;
        for (let i = 0; i < length; i++) {
            if (this.workExperience[i].id === id) {
                this.updateWorkExperienceObject = this.workExperience[i];
            }
        }
        this.updateWorkExperienceEndDateDisabled = this.updateWorkExperienceObject.endDate === 'Present' ? true : false;
        console.log(this.updateWorkExperienceObject);
    }

    toggleEndDatePresent(dateDisabled, endDateField) {
        if (dateDisabled === true) {
            endDateField = '';
        } else {
            endDateField = 'Present';
        }
        return endDateField;
    }

    deleteWorkExperience(id) {
        this.myStyle = {
            'cursor': 'not-allowed'
        };
        this.deleteRecord = true;

        this._portfolioService.deleteWorkExperience(id)
            .then((val) => {
                this.myStyle = {
                    'cursor': 'pointer'
                };
                this.deleteRecord = false;
                // remove work experience locally
                const length = this.workExperience.length;
                for (let i = 0; i < length; i++) {
                    if (this.workExperience[i].id === id) {
                        this.workExperience.splice(i, 1);
                        break;
                    }
                }

                // if user has inputs open and tries to delete then close inputs
                // after delete.
                this.showUpdateWorkExperienceInputs = false;
                // this.showWorkExperienceInputs = false;
            })
            .catch(err => {
                console.log(err);
                this.myStyle = {
                    'cursor': 'pointer'
                };
                this.deleteRecord = false;
                alert('Failed to delete work experience. Try again. If error persist, please contact site administrator.');
            });
    }

    /**
     * Update work experience.
     */
    updateWorkExperience() {
        console.log(this.updateWorkExperienceObject);
        this.savingWorkExperience = true;

        this._portfolioService.updateWorkExperience(this.updateWorkExperienceObject)
            .then((val) => {
                console.debug('workExperience updated', val);

                this.savingWorkExperience = false;
                this.workExperienceEndDateRequired = true;
                this.updateWorkExperienceEndDateDisabled = true;
                this.showWorkExperienceInputs = false;
                this.showUpdateWorkExperienceInputs = false;
                this.selectedWorkEditIndex = undefined;
                this.myStyle = {
                    'cursor': 'pointer'
                };
                const length = this.workExperience.length;
                for (let i = 0; i < length; i++) {
                    if (this.workExperience[i].id === this.updateWorkExperienceObject.id) {
                        this.workExperience[i] = this.updateWorkExperienceObject;
                    }
                }
                // clear form data
                this.updateWorkExperienceObject = {
                    companyName: undefined,
                    jobTitle: undefined,
                    startDate: undefined,
                    endDate: undefined,
                    jobDescription: undefined,
                    id: undefined
                };
            })
            .catch(err => {
                this.savingWorkExperience = false;
                console.log(err);
                alert('Failed to update work experience. Try again. If error persist, please contact site administrator.');
            });
    }

    /*
     * Insert work experience.
     */
    insertWorkExperience() {
        console.log(this.workExperienceObject);
        this.savingWorkExperience = true;
        const newObject = this.workExperienceObject;
        this._portfolioService.insertWorkExperience(newObject)
            .then((response) => {
                this.savingWorkExperience = false;
                this.workExperienceEndDateRequired = true;
                this.showWorkExperienceInputs = false;
                this.workExperience.push(response);
                console.log(this.showWorkExperienceInputs);
                // clear form data
                this.workExperienceObject = {
                    companyName: undefined,
                    jobTitle: undefined,
                    startDate: undefined,
                    endDate: undefined,
                    jobDescription: undefined,
                    id: undefined
                };
            })
            .catch(err => {
                this.savingWorkExperience = false;
                console.log(err);
                alert('Failed to save work experience. Try again. If error persist, please contact site administrator.');
            });
    }

    /**
     * Education
     */

    /**
     * Setup for updating education input.
     */
    setupUpdateEducation(id, index) {
        this.myStyle = {
            'cursor': 'not-allowed'
        };
        this.selectedEducationEditIndex = index;
        this.showEducationInputs = true;
        this.showUpdateEducationInputs = true;
        const length = this.education.length;
        for (let i = 0; i < length; i++) {
            console.log(this.education[i]);
            if (this.education[i].id === id) {
                this.updateEducationObject = this.education[i];
            }
        }
        console.log(this.updateEducationObject);
    }

    /**
     * Update education.
     */
    updateEducation() {
        this.savingEducation = true;
        this._portfolioService.updateWorkEducation(this.updateEducationObject)
            .then((response) => {
                this.savingEducation = false;
                // workExperienceEndDateRequired = true;
                this.showEducationInputs = false;
                this.showUpdateEducationInputs = false;
                this.selectedEducationEditIndex = undefined;
                this.myStyle = {
                    'cursor': 'pointer'
                };

                const length = this.education.length;
                for (let i = 0; i < length; i++) {
                    if (this.education[i].id === this.updateEducationObject.id) {
                        this.education[i] = this.updateEducationObject;
                    }
                }
            })
            .catch(reason => {
                this.savingEducation = false;
                console.log(reason);
                alert('Failed to update education. Try again. If error persist, please contact site administrator.');
            });
    }

    /*
     * Insert education.
     */
    insertEducation() {
        this.savingEducation = true;
        const newObject = this.educationObject;
        this._portfolioService.insertEducation(newObject)
            .then((response) => {
                console.log(response);
                this.savingEducation = false;
                this.showEducationInputs = false;
                this.education.push(response);

                // clear form data
                this.educationObject = {
                    schoolName: undefined,
                    gpa: undefined,
                    startDate: undefined,
                    endDate: undefined,
                    activities: undefined,
                    achievements: undefined,
                    id: undefined
                };

            })
            .catch(reason => {
                this.savingEducation = false;
                console.log(reason);
                alert('Failed to save education data. Try again. If error persist, please contact site administrator.');
            });
    }

    /*
     * Delete education
     */
    deleteEducation(id) {
        this.myStyle = {
            'cursor': 'not-allowed'
        };
        this.deleteRecord = true;
        const promise = this._portfolioService.deleteEducation(id);
        promise.then((response) => {
            this.myStyle = {
                'cursor': 'pointer'
            };
            this.deleteRecord = false;
            // remove education locally
            const length = this.education.length;
            for (let i = 0; i < length; i++) {
                if (this.education[i].id === id) {
                    this.education.splice(i, 1);
                    break;
                }
            }

            // if user has inputs open and tries to delete then close inputs
            // after delete.
            this.showUpdateEducationInputs = false;
            this.showEducationInputs = false;
        }, (reason) => {
            console.log(reason);
            this.myStyle = {
                'cursor': 'pointer'
            };
            this.deleteRecord = false;
            alert('Failed to delete education data. Try again. If error persist, please contact site administrator.');
        });
    }

    /**
     * Achievements
     */

    /**
     * Setup for updating achievement input.
     */
    setupUpdateAchievement(id, index) {
        this.myStyle = {
            'cursor': 'not-allowed'
        };
        this.selectedAchievementEditIndex = index;
        this.showAchievementInputs = true;
        this.showUpdateAchievementInputs = true;
        const length = this.achievement.length;
        for (let i = 0; i < length; i++) {
            console.log(this.achievement[i]);
            if (this.achievement[i].id === id) {
                this.updateAchievementObject = this.achievement[i];
            }
        }
        console.log(this.updateAchievementObject);
    }

    /**
     * Update achievement.
     */
    updateAchievement() {
        this.savingAchievement = true;
        this._portfolioService.updateAchievement(this.updateAchievementObject)
            .then((response) => {
                this.savingAchievement = false;
                this.showAchievementInputs = false;
                this.showUpdateAchievementInputs = false;
                this.selectedAchievementEditIndex = undefined;
                this.myStyle = {
                    'cursor': 'pointer'
                };

                const length = this.achievement.length;
                for (let i = 0; i < length; i++) {
                    if (this.achievement[i].id === this.updateAchievementObject.id) {
                        this.achievement[i] = this.updateAchievementObject;
                    }
                }
            })
            .catch(reason => {
                this.savingAchievement = false;
                console.log(reason);
                alert('Failed to update achievement. Try again. If error persist, please contact site administrator.');
            });
    }

    /*
     * Insert achievement.
     */
    insertAchievement() {
        this.savingAchievement = true;
        const newObject = this.achievementObject;
        this._portfolioService.insertAchievements(newObject)
            .then((response) => {
                console.log(response);
                this.savingAchievement = false;
                this.showAchievementInputs = false;
                this.achievement.push(response);

                // clear form data
                this.achievementObject = {
                    description: undefined,
                    id: undefined,
                };
            })
            .catch(reason => {
                this.savingAchievement = false;
                console.log(reason);
                alert('Failed to save achievement data. Try again. If error persist, please contact site administrator.');
                // }, function(update) {
                //   alert('Got notification: ' + update);
            });
    }

    /*
     * Delete achievement
     */
    deleteAchievement(id) {
        this.myStyle = {
            'cursor': 'not-allowed'
        };
        this.deleteRecord = true;
        this._portfolioService.deleteAchievement(id)
            .then((response) => {
                this.myStyle = {
                    'cursor': 'pointer'
                };
                this.deleteRecord = false;
                // remove achievement locally
                const length = this.achievement.length;
                for (let i = 0; i < length; i++) {
                    if (this.achievement[i].id === id) {
                        this.achievement.splice(i, 1);
                        break;
                    }
                }

                // if user has inputs open and tries to delete then close inputs
                // after delete.
                this.showUpdateAchievementInputs = false;
                this.showAchievementInputs = false;
            })
            .catch(reason => {
                console.log(reason);
                this.myStyle = {
                    'cursor': 'pointer'
                };
                this.deleteRecord = false;
                alert('Failed to delete achievement data. Try again. If error persist, please contact site administrator.');
                // }, function(update) {
                //   alert('Got notification: ' + update);
            });
    }

    /**
     * Test scores.
     */
    // console.log(actScore.length);
    // this.testSelected = this.actScore.length === 0 ? 'ACT' :
    //                    this.asvabScore.length === 0 ? 'ASVAB' :
    //                    this.satScore.length === 0 ? 'SAT' :
    //                     'OTHER'; // ACT

    // reset so only one section is open at a time.
    resetScoreEditFlags() {
        this.showUpdateFyiScoreInputs = false;
        this.showUpdateAsvabScoreInputs = false;
        this.showUpdateActScoreInputs = false;
        this.showUpdateSatScoreInputs = false;
        this.showUpdateOtherScoreInputs = false;
    }

    /**
     * ACT
     */

    /**
    * Setup for updating ACT input.
    */
    setupUpdateActScore(id, index) {
        this.myStyle = {
            'cursor': 'not-allowed'
        };
        this.selectedACTEditIndex = index;
        this.testEditDisable = true;
        this.resetScoreEditFlags();
        this.showScoreInputs = false;
        this.showUpdateActScoreInputs = true;
        const length = this.actScore.length;
        for (let i = 0; i < length; i++) {
            console.log(this.actScore[i]);
            if (this.actScore[i].id === id) {
                this.updateActObject = this.actScore[i];
                break;
            }
        }
        console.log(this.updateActObject);
    }

    /**
     * Update ACT.
     */
    updateActScore() {
        this.savingActScore = true;
        this._portfolioService.updateActScore(this.updateActObject)
            .then((response) => {
                this.savingActScore = false;
                this.showScoreInputs = false;
                this.showUpdateActScoreInputs = false;
                this.selectedACTEditIndex = undefined;
                this.myStyle = {
                    'cursor': 'pointer'
                };
                this.testEditDisable = false;

                const length = this.actScore.length;
                for (let i = 0; i < length; i++) {
                    if (this.actScore[i].id === this.updateActObject.id) {
                        this.actScore[i] = this.updateActObject;
                        break;
                    }
                }
            })
            .catch(reason => {
                this.savingActScore = false;
                console.log(reason);
                alert('Failed to update ACT score. Try again. If error persist, please contact site administrator.');
                // }, function(update) {
                //   alert('Got notification: ' + update);
            });
    }

    /*
     * Insert ACT score.
     */
    insertActScore() {
        this.savingActScore = true;
        const newObject = this.actObject;
        this._portfolioService.insertActScore(newObject)
            .then((response) => {
                console.log(response);
                this.savingActScore = false;
                this.showScoreInputs = false;
                this.actScore.push(response);

                // clear form data
                this.actObject = {
                    readingScore: undefined,
                    mathScore: undefined,
                    writingScore: undefined,
                    englishScore: undefined,
                    scienceScore: undefined
                };

                this.testSelected = 'OTHER';
            })
            .catch(reason => {
                this.savingActScore = false;
                console.log(reason);
                alert('Failed to save ACT score. Try again. If error persist, please contact site administrator.');
                // }, function(update) {
                //   alert('Got notification: ' + update);
            });
    }

    /*
     * Delete ACT score.
     */
    deleteActScore(id) {
        console.log(id);
        this._portfolioService.deleteActScore(id)
            .then((response) => {
                console.log(response);
                // remove work value locally
                const length = this.actScore.length;
                for (let i = 0; i < length; i++) {
                    if (this.actScore[i].id === id) {
                        this.actScore.splice(i, 1);
                        break;
                    }
                }
            })
            .catch(reason => {
                console.log(reason);
                alert('Failed to delete ACT score. Try again. If error persist, please contact site administrator.');
                // }, function(update) {
                //   alert('Got notification: ' + update);
            });
    }

    /**
     * ASVAB
     */

    /**
     * Setup for updating ASVAB input.
     */
    setupUpdateAsvabScore(id, index) {
        this.myStyle = {
            'cursor': 'not-allowed'
        };
        this.selectedASVABEditIndex = index;
        this.testEditDisable = true;
        this.resetScoreEditFlags();
        this.showScoreInputs = false;
        this.showUpdateAsvabScoreInputs = true;
        const length = this.asvabScore.length;
        for (let i = 0; i < length; i++) {
            console.log(this.asvabScore[i]);
            if (this.asvabScore[i].id === id) {
                this.updateAsvabObject = this.asvabScore[i];
                break;
            }
        }
        console.log(this.updateAsvabObject);
    }

    /**
     * Update ASVAB.
     */
    updateAsvabScore() {
        this.savingAsvabScore = true;
        this._portfolioService.updateAsvabScore(this.updateAsvabObject)
            .then((response) => {
                this.savingAsvabScore = false;
                this.showScoreInputs = false;
                this.showUpdateAsvabScoreInputs = false;
                this.selectedASVABEditIndex = undefined;
                this.myStyle = {
                    'cursor': 'pointer'
                };
                this.testEditDisable = false;

                const length = this.asvabScore.length;
                for (let i = 0; i < length; i++) {
                    if (this.asvabScore[i].id === this.updateAsvabObject.id) {
                        this.asvabScore[i] = this.updateAsvabObject;
                        break;
                    }
                }
            })
            .catch(reason => {
                this.savingAsvabScore = false;
                console.log(reason);
                alert('Failed to update ASVAB score. Try again. If error persist, please contact site administrator.');
                // }, function(update) {
                //   alert('Got notification: ' + update);
            });
    }

    /*
     * Insert ASVAB score.
     */
    insertAsvabScore() {
        this.savingAsvabScore = true;
        const newObject = this.asvabObject;
        this._portfolioService.insertAsvabScore(newObject)
            .then((response) => {
                console.log(response);
                this.savingAsvabScore = false;
                this.showScoreInputs = false;
                this.asvabScore.push(response);

                // clear form data
                this.asvabObject = {
                    verbalScore: undefined,
                    mathScore: undefined,
                    afqtScore: undefined,
                    scienceScore: undefined,
                    id: undefined,
                };

                this.testSelected = 'OTHER';

            })
            .catch(reason => {
                this.savingAsvabScore = false;
                console.log(reason);
                alert('Failed to save ASVAB score. Try again. If error persist, please contact site administrator.');
                // }, function(update) {
                //   alert('Got notification: ' + update);
            });
    }

    /*
     * Delete ASVAB score.
     */
    deleteAsvabScore(id) {
        console.log(id);
        this._portfolioService.deleteAsvabScore(id)
            .then((response) => {
                console.log(response);
                // remove work value locally
                const length = this.asvabScore.length;
                for (let i = 0; i < length; i++) {
                    if (this.asvabScore[i].id === id) {
                        this.asvabScore.splice(i, 1);
                        break;
                    }
                }
            })
            .catch(reason => {
                console.log(reason);
                alert('Failed to delete ASVAB score. Try again. If error persist, please contact site administrator.');
                // }, function(update) {
                //   alert('Got notification: ' + update);
            });
    }

    /**
     * SAT
     */

    /**
     * Setup for updating SAT input.
     */
    setupUpdateSatScore(id, index) {
        this.myStyle = {
            'cursor': 'not-allowed'
        };
        this.selectedSATEditIndex = index;
        this.testEditDisable = true;
        this.resetScoreEditFlags();
        this.showScoreInputs = false;
        this.showUpdateSatScoreInputs = true;
        const length = this.satScore.length;
        for (let i = 0; i < length; i++) {
            console.log(this.satScore[i]);
            if (this.satScore[i].id === id) {
                this.updateSatObject = this.satScore[i];
                break;
            }
        }
        console.log(this.updateSatObject);
    }

    /**
     * Update SAT.
     */
    updateSatScore() {
        this.savingSatScore = true;
        this._portfolioService.updateSatScore(this.updateSatObject)
            .then((response) => {
                this.savingSatScore = false;
                this.showSatInputs = false;
                this.showUpdateSatScoreInputs = false;
                this.selectedSATEditIndex = undefined;
                this.myStyle = {
                    'cursor': 'pointer'
                };
                this.testEditDisable = false;

                const length = this.satScore.length;
                for (let i = 0; i < length; i++) {
                    if (this.satScore[i].id === this.updateSatObject.id) {
                        this.satScore[i] = this.updateSatObject;
                        break;
                    }
                }
            })
            .catch(reason => {
                this.savingSatScore = false;
                console.log(reason);
                alert('Failed to update SAT score. Try again. If error persist, please contact site administrator.');
                // }, function(update) {
                //   alert('Got notification: ' + update);
            });
    }

    /*
     * Insert SAT score.
     */
    insertSatScore() {
        this.savingSatScore = true;
        const newObject = this.satObject;
        this._portfolioService.insertSatScore(newObject)
            .then((response) => {
                console.log(response);
                this.savingSatScore = false;
                this.showScoreInputs = false;
                this.satScore.push(response);

                // clear form data
                this.satObject = {
                    readingScore: undefined,
                    mathScore: undefined,
                    writingScore: undefined,
                    subject: undefined
                };

                this.testSelected = 'OTHER';

            })
            .catch(reason => {
                this.savingSatScore = false;
                console.log(reason);
                alert('Failed to save SAT score. Try again. If error persist, please contact site administrator.');
                // }, function(update) {
                //   alert('Got notification: ' + update);
            });
    }

    /*
     * Delete SAT score.
     */
    deleteSatScore(id) {
        console.log(id);
        this._portfolioService.deleteSatScore(id)
            .then((response) => {
                console.log(response);
                // remove work value locally
                const length = this.satScore.length;
                for (let i = 0; i < length; i++) {
                    if (this.satScore[i].id === id) {
                        this.satScore.splice(i, 1);
                        break;
                    }
                }
            })
            .catch(reason => {
                console.log(reason);
                alert('Failed to delete SAT score. Try again. If error persist, please contact site administrator.');
                // }, function(update) {
                //   alert('Got notification: ' + update);
            });
    }

    /**
     * FYI. If user has FYI scores then use it as default.
     */

    /*// Use FYI test results if exists.
    UserFactory.getUserInterestCodes().then((response) => {
  
      if (response.length == 0) {
        hideFyi = false;
        fyiScore = fyiScore.data;
      } else {
        hideFyi = true; // hide fyi inputs
        let array = [];
        array.push(response);
        fyiScore = array;
      }
  
    }, function(reason) {
      hideFyi = false;
      if (reason.length == 0) {
        fyiScore = fyiScore.data;
      } else {
        alert("Error fetching FYI test result." + reason);
      }
    });*/


    /**
     * Setup for updating FYI input.
     */
    setupUpdateFyiScore(id, index) {
        this.myStyle = {
            'cursor': 'not-allowed'
        };
        this.selectedFYIEditIndex = index;
        this.testEditDisable = true;
        this.resetScoreEditFlags();
        this.showScoreInputs = false;
        this.showUpdateFyiScoreInputs = true;
        const length = this.fyiScore.length;
        for (let i = 0; i < length; i++) {
            console.log(this.fyiScore[i]);
            if (this.fyiScore[i].id === id) {
                this.updateFyiObject = this.fyiScore[i];
                break;
            }
        }
        console.log(this.updateFyiObject);
    }

    /**
     * Update FYI.
     */
    updateFyiScore() {
        this.savingFyiScore = true;
        this._portfolioService.updateFyiScore(this.updateFyiObject)
            .then((response) => {
                this.savingFyiScore = false;
                this.showFyiInputs = false;
                this.showUpdateFyiScoreInputs = false;
                this.selectedFYIEditIndex = undefined;
                this.myStyle = {
                    'cursor': 'pointer'
                };
                this.testEditDisable = false;

                const length = this.fyiScore.length;
                for (let i = 0; i < length; i++) {
                    if (this.fyiScore[i].id === this.updateFyiObject.id) {
                        this.fyiScore[i] = this.updateFyiObject;
                        break;
                    }
                }
            })
            .catch(reason => {
                this.savingFyiScore = false;
                console.log(reason);
                alert('Failed to update FYI score. Try again. If error persist, please contact site administrator.');
                // }, function(update) {
                //   alert('Got notification: ' + update);
            });
    }

    /*
     * Insert FYI score.
     */
    insertFyiScore() {
        this.savingFyiScore = true;
        const newObject = this.fyiObject;
        this._portfolioService.insertFyiScore(newObject)
            .then((response) => {
                console.log(response);
                this.savingFyiScore = false;
                this.showInterestInputs = false;
                this.interestSelected = 'interest';
                this.fyiScore.push(response);

                // clear form data
                this.fyiObject = {
                    interest_code_one: undefined,
                    interest_code_two: undefined,
                    interest_code_three: undefined
                };

                this.testSelected = 'OTHER';

            })
            .catch(reason => {
                this.savingFyiScore = false;
                console.log(reason);
                alert('Failed to save FYI score. Try again. If error persist, please contact site administrator.');
                // }, function(update) {
                //   alert('Got notification: ' + update);
            });
    }

    /*
     * Delete FYI score.
     */
    deleteFyiScore(id) {
        console.log(id);
        this._portfolioService.deleteFyiScore(id)
            .then((response) => {
                console.log(response);
                // remove work value locally
                const length = this.fyiScore.length;
                for (let i = 0; i < length; i++) {
                    if (this.fyiScore[i].id === id) {
                        this.fyiScore.splice(i, 1);
                        break;
                    }
                }
            })
            .catch(reason => {
                console.log(reason);
                alert('Failed to delete FYI score. Try again. If error persist, please contact site administrator.');
                // }, function(update) {
                //   alert('Got notification: ' + update);
            });
    }

    showInterestCode(code) {
        switch (code) {
            case 'S':
                return 'Social';
            case 'A':
                return 'Artistic';
            case 'C':
                return 'Conventional';
            case 'E':
                return 'Enterprising';
            case 'R':
                return 'Realistic';
            case 'I':
                return 'Investigative';
            default:
                return '';
        }
    }

    /**
     * Other test scores.
     */

    /**
     * Setup for updating other input.
     */
    setupUpdateOtherScore(id, index) {
        this.myStyle = {
            'cursor': 'not-allowed'
        };
        this.selectedOtherEditIndex = index;
        this.testEditDisable = true;
        this.resetScoreEditFlags();
        this.showScoreInputs = false;
        this.showUpdateOtherScoreInputs = true;
        const length = this.otherScore.length;
        for (let i = 0; i < length; i++) {
            console.log(this.otherScore[i]);
            if (this.otherScore[i].id === id) {
                this.updateOtherObject = this.otherScore[i];
                break;
            }
        }
        console.log(this.updateOtherObject);
    }

    /**
     * Update other scores.
     */
    updateOtherScore() {
        this.savingOtherScore = true;
        this._portfolioService.updateOtherScore(this.updateOtherObject)
            .then((response) => {
                this.savingOtherScore = false;
                this.showOtherInputs = false;
                this.showUpdateOtherScoreInputs = false;
                this.selectedOtherEditIndex = undefined;
                this.myStyle = {
                    'cursor': 'pointer'
                };
                this.testEditDisable = false;

                const length = this.otherScore.length;
                for (let i = 0; i < length; i++) {
                    if (this.otherScore[i].id === this.updateOtherObject.id) {
                        this.otherScore[i] = this.updateOtherObject;
                        break;
                    }
                }
            })
            .catch(reason => {
                this.savingOtherScore = false;
                console.log(reason);
                alert('Failed to update other score. Try again. If error persist, please contact site administrator.');
                // }, function(update) {
                //   alert('Got notification: ' + update);
            });
    }

    /*
     * Insert other scores.
     */
    insertOtherScore() {
        this.savingOtherScore = true;
        const newObject = this.otherObject;
        this._portfolioService.insertOtherScore(newObject)
            .then((response) => {
                console.log(response);
                this.savingOtherScore = false;
                this.showScoreInputs = false;
                this.otherScore.push(response);

                // clear form data
                this.otherObject = {
                    description: undefined
                };

                this.testSelected = 'OTHER';

            })
            .catch(reason => {
                this.savingOtherScore = false;
                console.log(reason);
                alert('Failed to save score. Try again. If error persist, please contact site administrator.');
                // }, function(update) {
                //   alert('Got notification: ' + update);
            });
    }

    /*
     * Delete other score.
     */
    deleteOtherScore(id) {
        console.log(id);
        this._portfolioService.deleteOtherScore(id)
            .then((response) => {
                console.log(response);
                // remove work value locally
                const length = this.otherScore.length;
                for (let i = 0; i < length; i++) {
                    if (this.otherScore[i].id === id) {
                        this.otherScore.splice(i, 1);
                        break;
                    }
                }
            })
            .catch(reason => {
                console.log(reason);
                alert('Failed to delete other score. Try again. If error persist, please contact site administrator.');
                // }, function(update) {
                //   alert('Got notification: ' + update);
            });
    }

    /**
    * Physical Fitness Implementation.
    */

    /**
    * Setup for updating physical fitness input.
    */
    setupUpdatePhysicalFitness = function (id, index) {
        this.myStyle = {
            'cursor': 'not-allowed'
        };
        this.selectedPhysicalFitnessEditIndex = index;
        this.showPhysicalFitnessInputs = true;
        this.showUpdatePhysicalFitnessInputs = true;
        var length = this.physicalFitness.length;
        for (var i = 0; i < length; i++) {
            console.log(this.physicalFitness[i]);
            if (this.physicalFitness[i].id == id) {
                this.updatePhysicalFitnessObject = cloneDeep(this.physicalFitness[i]);
                console.log(this.updatePhysicalFitnessObject);
                if (this.updatePhysicalFitnessObject.mileRun) {
                    var splitArray = this.updatePhysicalFitnessObject.mileRun.split(":");
                    this.updatePhysicalFitnessObject.mileRunMin = Number(splitArray[0]);
                    this.updatePhysicalFitnessObject.mileRunSec = Number(splitArray[1]);
                }

                console.log(this.updatePhysicalFitnessObject);
            }
        }
        console.log(this.updatePhysicalFitnessObject);
    };

    /**
     * Update physical fitness.
     */
    updatePhysicalFitness = function () {
        this.savingPhysicalFitness = true;
        var promise = this._portfolioService.updatePhysicalFitness(this.updatePhysicalFitnessObject);
        promise.then((response) => {
            this.savingPhysicalFitness = false;
            // this.workExperienceEndDateRequired = true;
            this.showPhysicalFitnessInputs = false;
            this.showUpdatePhysicalFitnessInputs = false;
            this.selectedPhysicalFitnessEditIndex = undefined;
            this.myStyle = {
                'cursor': 'pointer'
            };

            var length = this.physicalFitness.length;
            for (var i = 0; i < length; i++) {
                if (this.physicalFitness[i].id == this.updatePhysicalFitnessObject.id) {
                    this.physicalFitness[i] = this.updatePhysicalFitnessObject;
                }
            }
        }, (reason) => {
            this.savingPhysicalFitness = false;
            console.log(reason);
            alert("Failed to update physical fitness. Try again. If error persist, please contact site administrator.");
        });
    };

    /*
     * Insert physical fitness.
     */
    insertPhysicalFitness = function () {
        this.savingPhysicalFitness = true;
        var newObject = cloneDeep(this.physicalFitnessObject);
        console.log(newObject);
        this._portfolioService.insertPhysicalFitness(newObject).then((response) => {
            console.log(response);
            this.savingPhysicalFitness = false;
            this.showPhysicalFitnessInputs = false;
            console.log(response);
            this.physicalFitness.push(response);

            // clear form data
            this.physicalFitnessObject = {
                pullUps: undefined,
                curlUps: undefined,
                vSitReach: undefined,
                shuttleRun: undefined,
                mileRun: undefined,
                mileRunMin: '',
                mileRunSec: '',
                otherOne: undefined,
                otherTwo: undefined,
                otherThree: undefined,
                otherFour: undefined,
                id: undefined
            };

        }, (reason) => {
            this.savingPhysicalFitness = false;
            console.log(reason);
            alert("Failed to save physical fitness data. Try again. If error persist, please contact site administrator.");
        });
    };

    /*
     * Delete physical fitness
     */
    deletePhysicalFitness(id) {
        this.myStyle = {
            'cursor': 'not-allowed'
        };
        this.deleteRecord = true;
        var promise = this._portfolioService.deletePhysicalFitness(id);
        promise.then((response) => {
            this.myStyle = {
                'cursor': 'pointer'
            };
            this.deleteRecord = false;
            // remove physical fitness locally
            var length = this.physicalFitness.length;
            for (var i = 0; i < length; i++) {
                if (this.physicalFitness[i].id == id) {
                    this.physicalFitness.splice(i, 1);
                    break;
                }
            }

            // if user has inputs open and tries to delete then close inputs
            // after delete.
            this.showUpdatePhysicalFitnessInputs = false;
            this.showPhysicalFitnessInputs = false;
        }, (reason) => {
            console.log(reason);
            this.myStyle = {
                'cursor': 'pointer'
            };
            this.deleteRecord = false;
            alert("Failed to delete physical fitness data. Try again. If error persist, please contact site administrator.");
        });
    }


    // Extra code
    test() {
        const x = 0;
    }


    /**
     * Print page as pdf. Utilizes pdfmake library.
     */
    docDefinition;

    /*
     * function toDataUrl(url, callback, outputFormat){ var img = new Image();
     * img.crossOrigin = 'Anonymous'; img.onload = function(){ var canvas =
     * document.createElement('CANVAS'); var ctx = canvas.getContext('2d'); var
     * dataURL; canvas.height = this.height; canvas.width = this.width;
     * ctx.drawImage(this, 0, 0); dataURL = canvas.toDataURL(outputFormat);
     * callback(dataURL); canvas = null; }; img.src = url; }
     * 
     * toDataUrl('images/logo.png', function(base64Img){ logoImg = base64Img;
     * console.log(logoImg); });
     */

    /**
     * Setup PDF and print content.
     */
    setupContent() {

        console.log(this.parent.userProfileInfo);
        var content = [];

        // User input for name and grade.
        var nameAndGrade = [{
            columns: [{
                text: this.parent.userProfileInfo ? 'Name: ' + this.parent.userProfileInfo.name : '',
                //text : 'Name: ' + this.parent.fullName,
                alignment: 'left',
                fontSize: 18,
                bold: true
            }, {
                text: this.parent.userProfileInfo ? 'Graduation Year: ' + this.parent.userProfileInfo.graduationYear : '',
                //text : 'Grade: ' + this.parent.grade,
                alignment: 'right',
                fontSize: 18,
                bold: true
            }]
        }, {
            canvas: [{
                type: 'line',
                x1: 0,
                y1: 5,
                x2: 595 - 2 * 40,
                y2: 5,
                lineWidth: 1
            }],
            style: 'horizontalLine'
        }];
        content.push(nameAndGrade);



        // Work experience content.
        if (this.workExperiencePrint) {
            var len: any = this.workExperience.length;

            if (len > 0) {

                // section title
                content.push({
                    text: 'Work Experience\n',
                    style: 'sectionTitle'
                })
                for (var i = 0; i < len; i++) {

                    content.push([{
                        text: [{
                            text: '\n' + this.workExperience[i].jobTitle + ', ',
                            bold: true
                        }, this.workExperience[i].companyName]
                    }, {
                        text: this.workExperience[i].startDate + ' - ' + this.workExperience[i].endDate
                    }, {
                        text: this.workExperience[i].jobDescription == '' ? '' : 'Job Tasks: ' + this.workExperience[i].jobDescription
                    }]);
                }

                content.push({
                    canvas: [{
                        type: 'line',
                        x1: 0,
                        y1: 5,
                        x2: 595 - 2 * 40,
                        y2: 5,
                        lineWidth: 1
                    }],
                    style: 'horizontalLine'
                })
            }
        }

        // Education content.
        if (this.educationPrint) {
            var len: any = this.education.length;

            if (len > 0) {

                // section title
                content.push({
                    text: 'Education\n',
                    style: 'sectionTitle'
                })
                for (var i = 0; i < len; i++) {
                    content.push([{
                        text: '\n' + this.education[i].schoolName + ', ',
                        bold: true
                    }, {
                        text: this.education[i].startDate + ' - ' + this.education[i].endDate
                    }, {
                        text: this.education[i].gpa == 0 ? '' : 'GPA: ' + this.education[i].gpa
                    }, {
                        text: this.education[i].activities == '' ? '' : 'Activities: ' + this.education[i].activities
                    }, {
                        text: this.education[i].achievements == '' ? '' : 'Honor, AP Courses and AP tests :\n' + this.education[i].achievements
                    }]);
                }

                content.push({
                    canvas: [{
                        type: 'line',
                        x1: 0,
                        y1: 5,
                        x2: 595 - 2 * 40,
                        y2: 5,
                        lineWidth: 1
                    }],
                    style: 'horizontalLine'
                })
            }
        }

        // Achievement content.
        if (this.achievementPrint) {
            var len: any = this.achievement.length;

            if (len > 0) {

                // section title
                content.push({
                    text: 'Achievement\n',
                    style: 'sectionTitle'
                })
                for (var i = 0; i < len; i++) {

                    content.push([{
                        text: '\nDescription:',
                        bold: true
                    }, {
                        text: this.achievement[i].description == null ? '' : this.achievement[i].description
                    }]);
                }

                content.push({
                    canvas: [{
                        type: 'line',
                        x1: 0,
                        y1: 5,
                        x2: 595 - 2 * 40,
                        y2: 5,
                        lineWidth: 1
                    }],
                    style: 'horizontalLine'
                })
            }
        }

        if (this.fyiScorePrint) {
            // FYI score content
            var fyiLen = this.fyiScore.length;
            if (fyiLen > 0) {

                content.push({
                    text: 'FYI Score\n\n',
                    style: 'sectionTitle'
                })

                for (var i = 0; i < fyiLen; i++) {

                    /*
                     * content.push([ { text : 'Interest Code One:' +
                     * fyiScore[i].interestCodeOne },{ text : 'Interest
                     * Code Two:' + fyiScore[i].interestCodeTwo },{ text :
                     * 'Interest Code Three:' +
                     * fyiScore[i].interestCodeThree } ]);
                     */

                    var scoreTextOne = this.fyiScore[i].interestCodeOne == 'E' ? 'Enterprising' : this.fyiScore[i].interestCodeOne == 'C' ? 'Conventional' : this.fyiScore[i].interestCodeOne == 'A' ? 'Artistic' : this.fyiScore[i].interestCodeOne == 'S' ? 'Social' : this.fyiScore[i].interestCodeOne == 'R' ? 'Realistic' : this.fyiScore[i].interestCodeOne == 'I' ? 'Investigative' : '';
                    var scoreTextTwo = this.fyiScore[i].interestCodeTwo == 'E' ? 'Enterprising' : this.fyiScore[i].interestCodeTwo == 'C' ? 'Conventional' : this.fyiScore[i].interestCodeTwo == 'A' ? 'Artistic' : this.fyiScore[i].interestCodeTwo == 'S' ? 'Social' : this.fyiScore[i].interestCodeTwo == 'R' ? 'Realistic' : this.fyiScore[i].interestCodeTwo == 'I' ? 'Investigative' : '';
                    var scoreTextThree = this.fyiScore[i].interestCodeThree == 'E' ? 'Enterprising' : this.fyiScore[i].interestCodeThree == 'C' ? 'Conventional' : this.fyiScore[i].interestCodeThree == 'A' ? 'Artistic' : this.fyiScore[i].interestCodeThree == 'S' ? 'Social' : this.fyiScore[i].interestCodeThree == 'R' ? 'Realistic' : this.fyiScore[i].interestCodeThree == 'I' ? 'Investigative' : '';

                    content.push([{
                        text: this.fyiScore[i].interestCodeOne + ' | ' + this.fyiScore[i].interestCodeTwo + ' | ' + this.fyiScore[i].interestCodeThree + ' (' + scoreTextOne + ' | ' + scoreTextTwo + ' | ' + scoreTextThree + ')',
                        style: 'tableStyle'
                    }]);
                }

                content.push({
                    canvas: [{
                        type: 'line',
                        x1: 0,
                        y1: 5,
                        x2: 595 - 2 * 40,
                        y2: 5,
                        lineWidth: 1
                    }],
                    style: 'horizontalLine'
                })
            }
        }

        // Test score content.
        if (this.testScorePrint) {
            var actLen = this.actScore.length;
            var asvabLen = this.asvabScore.length;
            var satLen = this.satScore.length;
            var fyiLen = this.fyiScore.length;
            var otherLen = this.otherScore.length;

            if (actLen > 0 || asvabLen > 0 || satLen > 0 || fyiLen > 0 || otherLen > 0) {
                // section title
                content.push({
                    text: 'Test Score\n',
                    style: 'sectionTitle'
                });
            }

            // ACT score content
            if (actLen > 0) {

                for (var i = 0; i < actLen; i++) {

                    content.push([{
                        style: 'scoreTable',
                        table: {
                            widths: [150, '*'],
                            headerRows: 1,
                            keepWithHeaderRows: 1,
                            dontBreakRows: true,
                            body: [[{
                                text: 'ACT Score',
                                colSpan: 2,
                                alignment: 'left',
                                bold: true
                            }, {}], [{
                                text: 'Reading Score',
                                style: 'tableStyle'
                            }, {
                                text: '' + this.actScore[i].readingScore,
                                style: 'tableStyle'
                            }], [{
                                text: 'Math Score',
                                style: 'tableStyle'
                            }, {
                                text: '' + this.actScore[i].mathScore,
                                style: 'tableStyle'
                            }], [{
                                text: 'Writing Score',
                                style: 'tableStyle'
                            }, {
                                text: '' + this.actScore[i].writingScore,
                                style: 'tableStyle'
                            }], [{
                                text: 'Science Score',
                                style: 'tableStyle'
                            }, {
                                text: '' + this.actScore[i].scienceScore,
                                style: 'tableStyle'
                            }], [{
                                text: 'English Score',
                                style: 'tableStyle'
                            }, {
                                text: '' + this.actScore[i].englishScore,
                                style: 'tableStyle'
                            }]]
                        },
                        layout: 'noBorders'
                    }]);
                }
            }

            // ASVAB score content
            if (asvabLen > 0) {

                for (var i = 0; i < asvabLen; i++) {

                    /*
                     * content.push([ { text : 'Verbal Score:' +
                     * asvabScore[i].verbalScore },{ text : 'Math Score:' +
                     * asvabScore[i].mathScore },{ text : 'Science
                     * Score:' + asvabScore[i].scienceScore },{ text :
                     * 'AFQT Score:' + asvabScore[i].afqtScore } ]);
                     */

                    let bodyContent = null;
                    if (!this.haveAsvabTestScores()) {
                        bodyContent = [[{
                            text: 'ASVAB Score',
                            colSpan: 2,
                            alignment: 'left',
                            bold: true
                        }, {}], [{
                            text: 'Verbal Skills Score',
                            style: 'tableStyle'
                        }, {
                            text: '' + this.asvabScore[i].verbalScore,
                            style: 'tableStyle'
                        }], [{
                            text: 'Math Skills Score',
                            style: 'tableStyle'
                        }, {
                            text: '' + this.asvabScore[i].mathScore,
                            style: 'tableStyle'
                        }], [{
                            text: 'Science and Technical Skills Score',
                            style: 'tableStyle'
                        }, {
                            text: '' + this.asvabScore[i].scienceScore,
                            style: 'tableStyle'
                        }], [{
                            text: 'AFQT Score',
                            style: 'tableStyle'
                        }, {
                            text: '' + this.asvabScore[i].afqtScore,
                            style: 'tableStyle'
                        }]]
                    } else {
                        bodyContent = [[{
                            text: 'ASVAB Score',
                            colSpan: 2,
                            alignment: 'left',
                            bold: true
                        }, {}], [{
                            text: 'AFQT Score',
                            style: 'tableStyle'
                        }, {
                            text: '' + this.scoreSummaryList.afqt,
                            style: 'tableStyle'
                        }]]
                    }

                    content.push([{
                        style: 'scoreTable',
                        table: {
                            widths: [150, '*'],
                            headerRows: 1,
                            keepWithHeaderRows: 1,
                            dontBreakRows: true,
                            body: bodyContent,
                        },
                        layout: 'noBorders'
                    }]);
                }
            }

            // SAT score content
            if (satLen > 0) {

                for (var i = 0; i < satLen; i++) {

                    /*
                     * content.push([ { text : 'Reading Score:' +
                     * satScore[i].readingScore },{ text : 'Math Score:' +
                     * satScore[i].mathScore },{ text : 'Writing Score:' +
                     * satScore[i].writingScore },{ text :
                     * satScore[i].subject != null ? 'Subject Score:' +
                     * satScore[i].subject : '' } ]);
                     */

                    content.push([{
                        style: 'scoreTable',
                        table: {
                            widths: [150, '*'],
                            headerRows: 1,
                            keepWithHeaderRows: 1,
                            dontBreakRows: true,
                            body: [[{
                                text: 'SAT Score',
                                colSpan: 2,
                                alignment: 'left',
                                bold: true
                            }, {}], [{
                                text: 'Critical Reading',
                                style: 'tableStyle'
                            }, {
                                text: '' + this.satScore[i].readingScore,
                                style: 'tableStyle'
                            }], [{
                                text: 'Mathematics',
                                style: 'tableStyle'
                            }, {
                                text: '' + this.satScore[i].mathScore,
                                style: 'tableStyle'
                            }], [{
                                text: 'Writing',
                                style: 'tableStyle'
                            }, {
                                text: parseInt(this.satScore[i].writingScore) != 0 ? + this.satScore[i].writingScore : 'None',
                                style: 'tableStyle'
                            }], [{
                                text: 'Other Subject Score',
                                style: 'tableStyle'
                            }, {
                                text: this.satScore[i].subject != null ? 'Subject Score:' + this.satScore[i].subject : 'None',
                                style: 'tableStyle'
                            }]]
                        },
                        layout: 'noBorders'
                    }]);
                }
            }

            // Other score content
            if (otherLen > 0) {

                for (var i = 0; i < otherLen; i++) {

                    content.push([{
                        style: 'scoreTable',
                        table: {
                            widths: [150, '*'],
                            headerRows: 1,
                            keepWithHeaderRows: 1,
                            dontBreakRows: true,
                            body: [[{
                                text: 'Other Scores',
                                colSpan: 2,
                                alignment: 'left',
                                bold: true
                            }, {}], [{
                                text: 'Other Scores',
                                style: 'tableStyle'
                            }, {
                                text: '' + this.otherScore[i].description,
                                style: 'tableStyle'
                            }]]
                        },
                        layout: 'noBorders'
                    }]);
                }
            }

            content.push({
                canvas: [{
                    type: 'line',
                    x1: 0,
                    y1: 5,
                    x2: 595 - 2 * 40,
                    y2: 5,
                    lineWidth: 1
                }],
                style: 'horizontalLine'
            })

        }

        // Composite Scores Favorites content.
                if (this.compositeScoreFavoritesPrint) {
                    var csFavs = this.compositeFavoritesFlags;
                    delete csFavs.userId; // no needed to print 
                    var dataToPrint = false;
                    var bodyData;
                    var serv = "";
                    var csFavsToPrint= { afqt: [], army: [], mc: [], navy: [], af: [], cg: [] };
                    for (var key in csFavs) {
                        if (csFavs[key]) {
                            serv = key.substring(key.indexOf("_")+1, key.length) // i.e. gt_army
                            csFavsToPrint[serv].push({
                                text: document.getElementById(key).innerHTML, 
                                score: this.stripLeadingZero(this.scoreSummary.aFQTRawSSComposites[key])
                            });
                            dataToPrint =true;
                        }
                    }
        
                    if (dataToPrint) {
                        // section title
                        content.push({
                            text : 'Composite Scores Favorites\n',
                            style : 'sectionTitle'
                        })
        
                        // add afqt to print if exists
                        if (csFavsToPrint.afqt.length > 0) {
                            bodyData = [];
                            bodyData.push([
                                {text : 'AFQT', colSpan : 2, alignment : 'left', bold : true}, 
                                {}
                            ]);
        
                            for (var i = 0; i < csFavsToPrint.afqt.length; i++) {
                                bodyData.push([
                                    {text : csFavsToPrint.afqt[i].text, style : 'tableStyle'}, 
                                    {text : ''+ csFavsToPrint.afqt[i].score, style : 'tableStyle'}
                                ])
                            }
                            
                            content.push([ {
                                style : 'scoreTable',
                                table : {
                                    widths : [ 250, '*' ],
                                    headerRows : 1,
                                    keepWithHeaderRows : 1,
                                    dontBreakRows : true,
                                    body : bodyData
                                },
                                layout : 'noBorders'
                            } ]);
                        }
        
                        // add army to print if exists
                        if (csFavsToPrint.army.length > 0) {
                            bodyData = [];
                            bodyData.push([
                                {text : 'Army', colSpan : 2, alignment : 'left', bold : true}, 
                                {}
                            ]);
        
                            for (var i = 0; i < csFavsToPrint.army.length; i++) {
                                bodyData.push([
                                    {text : csFavsToPrint.army[i].text, style : 'tableStyle'}, 
                                    {text : ''+ csFavsToPrint.army[i].score, style : 'tableStyle'}
                                ])
                            }
                            
                            content.push([ {
                                style : 'scoreTable',
                                table : {
                                    widths : [ 250, '*' ],
                                    headerRows : 1,
                                    keepWithHeaderRows : 1,
                                    dontBreakRows : true,
                                    body : bodyData
                                },
                                layout : 'noBorders'
                            } ]);
                        }
        
                        // add marines to print if exists
                        if (csFavsToPrint.mc.length > 0) {
                            bodyData = [];
                            bodyData.push([
                                {text : 'Marines', colSpan : 2, alignment : 'left', bold : true}, 
                                {}
                            ]);
        
                            for (var i = 0; i < csFavsToPrint.mc.length; i++) {
                                bodyData.push([
                                    {text : csFavsToPrint.mc[i].text, style : 'tableStyle'}, 
                                    {text : ''+ csFavsToPrint.mc[i].score, style : 'tableStyle'}
                                ])
                            }
                            
                            content.push([ {
                                style : 'scoreTable',
                                table : {
                                    widths : [ 250, '*' ],
                                    headerRows : 1,
                                    keepWithHeaderRows : 1,
                                    dontBreakRows : true,
                                    body : bodyData
                                },
                                layout : 'noBorders'
                            } ]);
                        }
        
                        // add navy to print if exists
                        if (csFavsToPrint.navy.length > 0) {
                            bodyData = [];
                            bodyData.push([
                                {text : 'Navy', colSpan : 2, alignment : 'left', bold : true}, 
                                {}
                            ]);
        
                            for (var i = 0; i < csFavsToPrint.navy.length; i++) {
                                bodyData.push([
                                    {text : csFavsToPrint.navy[i].text, style : 'tableStyle'}, 
                                    {text : ''+ csFavsToPrint.navy[i].score, style : 'tableStyle'}
                                ])
                            }
                            
                            content.push([ {
                                style : 'scoreTable',
                                table : {
                                    widths : [ 250, '*' ],
                                    headerRows : 1,
                                    keepWithHeaderRows : 1,
                                    dontBreakRows : true,
                                    body : bodyData
                                },
                                layout : 'noBorders'
                            } ]);
                        }
        
                        // add air force to print if exists
                        if (csFavsToPrint.af.length > 0) {
                            bodyData = [];
                            bodyData.push([
                                {text : 'Air Force', colSpan : 2, alignment : 'left', bold : true}, 
                                {}
                            ]);
        
                            for (var i = 0; i < csFavsToPrint.af.length; i++) {
                                bodyData.push([
                                    {text : csFavsToPrint.af[i].text, style : 'tableStyle'}, 
                                    {text : ''+ csFavsToPrint.af[i].score, style : 'tableStyle'}
                                ])
                            }
                            
                            content.push([ {
                                style : 'scoreTable',
                                table : {
                                    widths : [ 250, '*' ],
                                    headerRows : 1,
                                    keepWithHeaderRows : 1,
                                    dontBreakRows : true,
                                    body : bodyData
                                },
                                layout : 'noBorders'
                            } ]);
                        }
        
                        // add coast gaurd to print if exists
                        if (csFavsToPrint.cg.length > 0) {
                            bodyData = [];
                            bodyData.push([
                                {text : 'Coast Guard', colSpan : 2, alignment : 'left', bold : true}, 
                                {}
                            ]);
        
                            for (var i = 0; i < csFavsToPrint.cg.length; i++) {
                                bodyData.push([
                                    {text : csFavsToPrint.cg[i].text, style : 'tableStyle'}, 
                                    {text : ''+ csFavsToPrint.cg[i].score, style : 'tableStyle'}
                                ])
                            }
                            
                            content.push([ {
                                style : 'scoreTable',
                                table : {
                                    widths : [ 250, '*' ],
                                    headerRows : 1,
                                    keepWithHeaderRows : 1,
                                    dontBreakRows : true,
                                    body : bodyData
                                },
                                layout : 'noBorders'
                            } ]);
                        }
                        
                        content.push({
                            canvas : [ {
                                type : 'line',
                                x1 : 0,
                                y1 : 5,
                                x2 : 595 - 2 * 40,
                                y2 : 5,
                                lineWidth : 1
                            } ],
                            style : 'horizontalLine'
                        })
                    }
                }


        // Physical fitness content.
        if (this.physicalFitnessPrint) {
            var len: any = this.physicalFitness.length;

            if (len > 0) {

                // section title
                content.push({
                    text: 'Physical Fitness\n',
                    style: 'sectionTitle'
                })

                for (var i = 0; i < len; i++) {

                    content.push([{
                        style: 'scoreTable',
                        table: {
                            widths: [150, '*'],
                            headerRows: 1,
                            keepWithHeaderRows: 1,
                            dontBreakRows: true,
                            body: [[{
                                text: 'Curl-ups (# 1min)',
                                style: 'tableStyle'
                            }, {
                                text: '' + this.physicalFitness[i].curlUps,
                                style: 'tableStyle'
                            }], [{
                                text: 'V-Sit Reach (in)',
                                style: 'tableStyle'
                            }, {
                                text: '' + this.physicalFitness[i].vSitReach,
                                style: 'tableStyle'
                            }], [{
                                text: 'Pull-ups (# 1min)',
                                style: 'tableStyle'
                            }, {
                                text: this.physicalFitness[i].pullUps != null ? this.physicalFitness[i].pullUps.toString() : '0',
                                style: 'tableStyle'
                            }], [{
                                text: 'Shuttle Run (sec)',
                                style: 'tableStyle'
                            }, {
                                text: '' + this.physicalFitness[i].shuttleRun,
                                style: 'tableStyle'
                            }], [{
                                text: '1 Mile Run (min:sec)',
                                style: 'tableStyle'
                            }, {
                                text: this.physicalFitness[i].mileRun != null ? this.physicalFitness[i].mileRun : '0:0',
                                style: 'tableStyle'
                            }], [{
                                text: this.physicalFitness[i].otherOne != null && this.physicalFitness[i].otherOne != '' ? 'Other' : '',
                                style: 'tableStyle'
                            }, {
                                text: this.physicalFitness[i].otherOne != null ? this.physicalFitness[i].otherOne : '',
                                style: 'tableStyle'
                            }], [{
                                text: this.physicalFitness[i].otherTwo != null && this.physicalFitness[i].otherOne != '' ? 'Other' : '',
                                style: 'tableStyle'
                            }, {
                                text: this.physicalFitness[i].otherTwo != null ? this.physicalFitness[i].otherTwo : '',
                                style: 'tableStyle'
                            }], [{
                                text: this.physicalFitness[i].otherThree != null && this.physicalFitness[i].otherOne != '' ? 'Other' : '',
                                style: 'tableStyle'
                            }, {
                                text: this.physicalFitness[i].otherThree != null ? this.physicalFitness[i].otherThree : '',
                                style: 'tableStyle'
                            }], [{
                                text: this.physicalFitness[i].otherFour != null && this.physicalFitness[i].otherOne != '' ? 'Other' : '',
                                style: 'tableStyle'
                            }, {
                                text: this.physicalFitness[i].otherFour != null ? this.physicalFitness[i].otherFour : '',
                                style: 'tableStyle'
                            }]]
                        },
                        layout: 'noBorders'
                    }]);

                }

                content.push({
                    canvas: [{
                        type: 'line',
                        x1: 0,
                        y1: 5,
                        x2: 595 - 2 * 40,
                        y2: 5,
                        lineWidth: 1
                    }],
                    style: 'horizontalLine'
                })
            }
        }

        // Favorite occupation content.
        //        if (this.favoriteOccupationPrint) {
        //            var len = favoriteList.length;
        //
        //            if (len > 0) {
        //
        //                // section title
        //                content.push({
        //                    text : len == 1 ? 'Favorite Military Occupation:\n\n' : 'Favorite Military Occupations\n\n',
        //                    style : 'sectionTitle'
        //                })
        //                for (var i = 0; i < len; i++) {
        //
        //                    content.push([ {
        //                        text : favoriteList[i].title == null ? '' : favoriteList[i].title
        //                    } ]);
        //                }
        //
        //                content.push({
        //                    canvas : [ {
        //                        type : 'line',
        //                        x1 : 0,
        //                        y1 : 5,
        //                        x2 : 595 - 2 * 40,
        //                        y2 : 5,
        //                        lineWidth : 1
        //                    } ],
        //                    style : 'horizontalLine'
        //                })
        //            }
        //        }
        //        
        // CEP favorite occupation.
        if (this.favoriteOccupationPrint && this.parent.cepFavoriteOccupationsList) {
            var len: any = this.parent.cepFavoriteOccupationsList.length;

            if (len > 0) {

                // section title
                content.push({
                    text : len == 1 ? 'Favorite ASVAB CEP Occupation:\n\n' : 'Favorite ASVAB CEP Occupations\n\n',
                    style: 'sectionTitle'
                })
                for (var i = 0; i < len; i++) {

                    content.push([{
                        text: this.parent.cepFavoriteOccupationsList[i].title == null ? '' : this.parent.cepFavoriteOccupationsList[i].title
                    }]);
                }

                content.push({
                    canvas: [{
                        type: 'line',
                        x1: 0,
                        y1: 5,
                        x2: 595 - 2 * 40,
                        y2: 5,
                        lineWidth: 1
                    }],
                    style: 'horizontalLine'
                })
            }
        }

        // Favorite military occupations
        if (this.favoriteOccupationPrint && this.parent.favoriteList) {
            var len: any = this.parent.favoriteList.length;

            if (len > 0) {

                // section title
                content.push({
                    text : len == 1 ? 'Favorite Military Occupation:\n\n' : 'Favorite Military Occupations\n\n',
                    style: 'sectionTitle'
                })
                for (var i = 0; i < len; i++) {

                    content.push([{
                        text: this.parent.favoriteList[i].title == null ? '' : this.parent.favoriteList[i].title
                    }]);
                }

                content.push({
                    canvas: [{
                        type: 'line',
                        x1: 0,
                        y1: 5,
                        x2: 595 - 2 * 40,
                        y2: 5,
                        lineWidth: 1
                    }],
                    style: 'horizontalLine'
                })
            }
        }
        //
        //        // Favorite career cluster content.
        if (this.favoriteCareerClusterPrint && this.parent.careerClusterFavoriteList) {
            var len: any = this.parent.careerClusterFavoriteList.length;

            if (len > 0) {

                // section title
                content.push({
                    text: len == 1 ? 'Favorite Career Cluster\n\n' : 'Favorite Career Clusters\n\n',
                    style: 'sectionTitle'
                })
                for (var i = 0; i < len; i++) {

                    content.push([{
                        text: this.parent.careerClusterFavoriteList[i].title == null ? '' : this.parent.careerClusterFavoriteList[i].title
                    }]);
                }

                content.push({
                    canvas: [{
                        type: 'line',
                        x1: 0,
                        y1: 5,
                        x2: 595 - 2 * 40,
                        y2: 5,
                        lineWidth: 1
                    }],
                    style: 'horizontalLine'
                })
            }
        }

        // Favorite service content.
        if (this.favoriteServicePrint && this.parent.serviceFavoriteList) {
            var len: any = this.parent.serviceFavoriteList.length;

            if (len > 0) {

                // section title
                content.push({
                    text: len == 1 ? 'Favorite Service\n\n' : 'Favorite Services\n\n',
                    style: 'sectionTitle'
                })
                for (var i = 0; i < len; i++) {

                    content.push([{
                        text: this.parent.serviceFavoriteList[i].svcId == null ? '' : this.parent.serviceFavoriteList[i].svcId == 'A' ? 'Army' : this.parent.serviceFavoriteList[i].svcId == 'M' ? 'Marines' : this.parent.serviceFavoriteList[i].svcId == 'F' ? 'Air Force' : this.parent.serviceFavoriteList[i].svcId == 'N' ? 'Navy' : this.parent.serviceFavoriteList[i].svcId == 'C' ? 'Coast Guard' : this.parent.serviceFavoriteList[i].svcId == 'S' ? 'Space Force' : ''
                    }]);
                }
            }
        }

        /**
         * Set content.
         */
        // [{text: 'ASVAB Portfolio', style:
        // 'headerTitle', margin: [72,40]},
        this.docDefinition = {
            /*
             * header : [ { image : 'logo', margin : [ 0, 20, 0, 20 ], alignment :
             * 'center' }, headerContent ],
             */
            content: content,

            styles: {
                headerTitle: {
                    fontSize: 22,
                    bold: true,
                    alignment: 'center'
                },
                sectionTitle: {
                    margin: [0, 0, 0, 0],
                    bold: true,
                    alignment: 'left',
                    fontSize: 12
                },
                sectionContent: {
                    margin: [0, 0, 0, 20]
                },
                tableStyle: {
                    alignment: 'left'
                },
                scoreTable: {
                    margin: [0, 30, 0, 0]
                },
                horizontalLine: {
                    margin: [0, 20, 0, 0]
                }
            },
            pageMargins: [ 40, 60, 40, 100 ],
        };
    }


    printPdf = function () {
        this.setupContent();

        // IE support
        if (navigator.appName == 'Microsoft Internet Explorer' || !!(navigator.userAgent.match(/Trident/) || navigator.userAgent.match(/rv 11/))) {

            // TODO: create a custom alert box. Is one available to use?
            alert('Your browser does not support this feature. Alternatively, you can download as PDF and then print.');

            //            var modalOptions = {
            //                    headerText : 'Alert',
            //                    bodyText : 'Your browser does not support this feature. Alternatively, you can download as PDF and then print.'
            //                };
            //
            //                modalService.showModal({
            //                    size : 'sm'
            //                }, modalOptions);
            return false;
        }

        pdfMake.createPdf(this.docDefinition).print();
    };

    downloadPdf = function () {
        this.setupContent();
        pdfMake.createPdf(this.docDefinition).download('ASVAB_Portfolio.pdf');
    };
    haveAsvabTestScores() {
        return this.haveTestScores;
    }

    // initial checkbox values for removing comp scores
    cl_cg_isChecked: boolean = false;
    gt_cg_isChecked: boolean = false;
    mr_cg_isChecked: boolean = false;
    el_cg_isChecked: boolean = false;
    e_af_isChecked: boolean = false;
    g_af_isChecked: boolean = false;
    a_af_isChecked: boolean = false;
    m_af_isChecked: boolean = false;
    adm_navy_isChecked: boolean = false;
    hm_navy_isChecked: boolean = false;
    ops_navy_isChecked: boolean = false;
    nuc_navy_isChecked: boolean = false;
    mec2_navy_isChecked: boolean = false;
    mec_navy_isChecked: boolean = false;
    eng_navy_isChecked: boolean = false;
    bee_navy_isChecked: boolean = false;
    el_navy_isChecked: boolean = false;
    gt_navy_isChecked: boolean = false;
    cl_mc_isChecked: boolean = false;
    el_mc_isChecked: boolean = false;
    gt_mc_isChecked: boolean = false;
    mm_mc_isChecked: boolean = false;
    st_army_isChecked: boolean = false;
    sc_army_isChecked: boolean = false;
    of_army_isChecked: boolean = false;
    mm_army_isChecked: boolean = false;
    gm_army_isChecked: boolean = false;
    fa_army_isChecked: boolean = false;
    el_army_isChecked: boolean = false;
    co_army_isChecked: boolean = false;
    cl_army_isChecked: boolean = false;
    gt_army_isChecked: boolean = false;
    afqt_isChecked: boolean = false;

    removeCompFavorite( value, itemCode ) {
        if ( value == true ) {
            this.compositeFavoritesFlags[itemCode] = 0;
        } else {
            this.compositeFavoritesFlags[itemCode] = 1;
        }
    }

    submitCompFavoriteList() {
        this.removeItem = true;
        this._scoreService.setCompositeFavoritesFlags( this.compositeFavoritesFlags ).then( results => {
            // if successful update view
            if ( results == 1 ) {
                this.removeItem = false;
                this.updateCompositeScoreView( this.compositeFavoritesFlags );
            }
        } )
            .catch( reason => {
                this.removeItem = false;
                console.log( reason );
            } );
    }

    updateCompositeScoreView( compositeFlags ) {
        this.afqt = compositeFlags.afqt;
        this.a_af = compositeFlags.a_af;
        this.adm_navy = compositeFlags.adm_navy;
        this.ao = compositeFlags.ao;
        this.ar = compositeFlags.ar;
        this.as = compositeFlags.as;
        this.bee_navy = compositeFlags.bee_navy;
        this.cl_army = compositeFlags.cl_army;
        this.cl_mc = compositeFlags.cl_mc;
        this.co_army = compositeFlags.co_army;
        this.e_af = compositeFlags.e_af;
        this.el = compositeFlags.el;
        this.el_army = compositeFlags.el_army;
        this.el_mc = compositeFlags.el_mc;
        this.el_navy = compositeFlags.el_navy;
        this.eng_navy = compositeFlags.eng_navy;
        this.fa_army = compositeFlags.fa_army;
        this.g_af = compositeFlags.g_af;
        this.gm_army = compositeFlags.gm_army;
        this.gt_army = compositeFlags.gt_army;
        this.gt_navy = compositeFlags.gt_navy;
        this.hm_navy = compositeFlags.hm_navy;
        this.m_af = compositeFlags.m_af;
        this.mc = compositeFlags.mc;
        this.mec2_navy = compositeFlags.mec2_navy;
        this.mec_navy = compositeFlags.mec_navy;
        this.mk = compositeFlags.mk;
        this.mm_army = compositeFlags.mm_army;
        this.mm_mc = compositeFlags.mm_mc;
        this.gt_mc= compositeFlags.gt_mc;
        this.nuc_navy = compositeFlags.nuc_navy;
        this.of_army = compositeFlags.of_army;
        this.pc = compositeFlags.pc;
        this.sc_army = compositeFlags.sc_army;
        this.st_army = compositeFlags.st_army;
        this.ve = compositeFlags.ve;
        this.wk = compositeFlags.wk;
        this.el_cg = compositeFlags.el_cg;
        this.mr_cg = compositeFlags.mr_cg;
        this.gt_cg = compositeFlags.gt_cg;
        this.cl_cg = compositeFlags.cl_cg;
    }

    openNewTab(url) {
        window.open(url, '_blank');
    }

    openTip() {
        let bodyText = "<p>NOT SURE WHAT TO INCLUDE? CONSIDER THESE TIPS WHEN WRITING YOUR EXPERIENCE DESCRIPTION:<p>" +
                "<ol><li>Begin every statement with an action verb in the proper tense based on your employment status at the job.</li>" +
                "If you currently work there use present tense: " +
                "<br><b>Example</b>" +
                "<ul><li>Construct oral arrangements including boutonnieres and corsages</li>" +
                "<li>Inventory stock to determine whether additional supplies are needed</li></ul>" +
                "<br>" +
                "Did you used to work there? Use past tense:" +
                "<br><b>Example</b>" +
                "<ul><li>Managed incoming calls and accurately documented customer orders</li>" +
                "<li>Received shipments and verified ordered amount matched received amount and confirmed appropriate quality</li></ul>" +
                "<br>" +
                "<li>Focus on what you do or did and be specific. Use clear language and avoid writing more than necessary. </li>" +
                "<br>" +
                "Avoid"+
                "<ul><li>Greet customers</li></ul>"+
                "Try"+
                "<ul><li>Welcome guests upon arrival, answering any questions they may have while escorting them to their seats</li></ul>"+
                "<br>" +
                "<li>Never underestimate the value of 21st century life skills. Call attention to time management, work ethic, and communication skills where possible. e.g. Filled in for absent employees in other departments and gained additional experience related to..." +
                "<li>Use formatting elements wisely. You can use bullets to create list (CTRL 8) and call attention to certain information selectively with bold face (CTRL B) and italics (CTRL I)." +
                "<li>Most importantly double check your spelling and grammar, then check again. And then have someone else look at it. Errors on a resume are simply unacceptable.</li></ol>";

        const data = {
            'title': '<div class="modal-options"><h4>WRITING TIPS</h4></div>',
            'message': bodyText,
          };

        const dialogRef1 = this._dialog.open(MessageDialogComponent, {
            data: data,
            hasBackdrop: true,
            disableClose: true,
            maxWidth: '400px'
        });
    }
}
