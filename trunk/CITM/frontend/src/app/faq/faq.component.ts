import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { ContentManagementService } from 'app/services/content-management.service';
import { Title, Meta } from '@angular/platform-browser';
import { UtilityService } from 'app/services/utility.service';

@Component({
    selector: 'app-faq',
    templateUrl: './faq.component.html',
    styleUrls: ['./faq.component.scss']
})
export class FaqComponent implements OnInit, OnDestroy {
    path;
    pageHtml: any = [];
    page: any;

    constructor(
        private _router: Router,
        private _meta: Meta,
        private _titleTag: Title,
        private _contentManagementService: ContentManagementService,
        private _utility: UtilityService,
    ) { }

    ngOnInit() {
        this.path = this._router.url;

        window.scroll(0, 0);
        this.getPageByName(this.path);
    }

    /***
     * Get content from DB
     */
    getPageByName(pageName) {
        // Remove leading slash before lookup
        if (pageName.indexOf('/') === 0) {
            pageName = pageName.substring(1);
        }
        console.log('pageName:', pageName);
        this._contentManagementService.getPageByPageName(pageName).subscribe(
            data => {
                this.page = data;
                this._utility.parseCms(this.page.pageHtml, this.pageHtml);

                this._meta.updateTag({ property: 'og:title', content: this.page.pageTitle });
                this._meta.updateTag({ property: 'og:description', content: this.page.pageDescription });
                this._meta.updateTag({ property: 'og:image', content: 'http://www.asvabprogram.com/media-center-content/thumbnails/social-share/CITM_Webshare.jpg' });
                this._meta.updateTag({ name: 'twitter:title', content: this.page.pageTitle });
                this._meta.updateTag({ name: 'twitter:description', content: this.page.pageDescription });
                this._meta.updateTag({ name: 'twitter:image', content: 'http://www.asvabprogram.com/media-center-content/thumbnails/social-share/CITM_Webshare.jpg' });
 
                this._titleTag.setTitle(this.page.pageTitle);
                this._meta.updateTag({ name: 'description', content: this.page.pageDescription });
            });
    }

    ngOnDestroy() {
    }
}
