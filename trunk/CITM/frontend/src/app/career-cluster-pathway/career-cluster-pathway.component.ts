import { Component, OnInit, OnDestroy  } from '@angular/core';
import { ConfigService } from 'app/services/config.service';
import { CareerSearchService } from 'app/services/career-search.service';
import { SearchListService } from 'app/services/search-list.service';
import { UserService } from 'app/services/user.service';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { FavoritesService } from 'app/services/favorites.service';
import {GoogleAnalyticsService} from '../services/google-analytics.service';
import { Title, Meta } from '@angular/platform-browser';

@Component({
  selector: 'app-career-cluster-pathway',
  templateUrl: './career-cluster-pathway.component.html',
  styleUrls: ['./career-cluster-pathway.component.scss']
})
export class CareerClusterPathwayComponent implements OnInit, OnDestroy {
  navigationSubscription;
  ccId: any;
  searchList: any;
  pathwayList: any;
  sampleJobs: any;
  careerClusterList: any;
  careerCluster: any;
  favoriteCareerCluster: any;
  clusterSearchListInterval: any;
  disabled = false;
  myStyle =  {'cursor': 'pointer'};

  constructor(
    private _activatedRoute: ActivatedRoute,
    private _router: Router,
    private _config: ConfigService,
    private _userService: UserService,
    private _searchListService: SearchListService,
    private _favoriteService: FavoritesService,
    private _careerSearchService: CareerSearchService,
    private _meta: Meta,
    private _titleTag: Title,
    private _googleAnalyticsService: GoogleAnalyticsService
  ) {
     // Subscribe to router event Navigation End so can refresh data after log in
     this.navigationSubscription = this._router.events.subscribe(event => {
      if (event instanceof NavigationEnd && event.urlAfterRedirects.indexOf('/career-cluster-pathway') > -1) {
        this._activatedRoute.params.subscribe(routeParams => {
          this.ccId = parseInt(routeParams.ccId, 10);
         });
         this.loadData();
      }
    });
  }

  ngOnInit() {
    window.scroll(0, 0);
  }

  loadData() {
    const searchObject = this._searchListService.getSearchList();
    const self = this;
    if (!searchObject) {
      if (!this.clusterSearchListInterval) {
        this.clusterSearchListInterval = setInterval(function () {
          if (!self._searchListService.getProcessingSearchList()) {
            clearInterval(self.clusterSearchListInterval);
            self.searchList = self._searchListService.searchListObject;
          }
        }, 200);
      }
    } else {
      this.searchList = searchObject;
    }

    this._careerSearchService.getCareerClusterPathway( this.ccId ).subscribe( data => {
        this.pathwayList = data;

        this.pathwayList.forEach(i => {
          if (i && i.pathwayTitle) {
            i['slug'] = i.pathwayTitle.split(' ').join('-').toLowerCase();
          } 

          if(i.sampleJobs) {
            i.sampleJobs.forEach(j => {
              if (j && j.title) {
                j['slug'] = j.title.split(' ').join('-').toLowerCase();
              } 
            })
          }
        })

        this._careerSearchService.getCareerCluster().subscribe( data => {
            this.careerClusterList = data;
            this._careerSearchService.setCareerCluster( data );

            for ( let i = 0; i < this.careerClusterList.length; i++ ) {
                if ( this.careerClusterList[i].ccId === this.ccId ) {
                    this.careerCluster = this.careerClusterList[i];
                    if ( this.careerCluster && this.favoriteCareerCluster ) {
                        this.setFavorited();
                    }
                }
            }
            this.setData();
        } );

    } );
    

    this.getFavoriteCareerCluster();
  }

  getFavoriteCareerCluster() {
    if (this.isLoggedIn()) {
      this._userService.getFavoriteCareerCluster().subscribe(data => {
        this.favoriteCareerCluster = data ? data : [];
        this._userService.favoriteCareerClusterLIstObject = this.favoriteCareerCluster;
        if (this.careerCluster && this.favoriteCareerCluster) {
          this.setFavorited();
        }
      });
    } else {
      this.favoriteCareerCluster = [];
    }

  }

  setFavorited() {
    this.careerCluster.favorited = false;
    for (let i = 0; i < this.favoriteCareerCluster.length; i++) {
      if (this.careerCluster.ccId === this.favoriteCareerCluster[i].ccId) {
        this.careerCluster.favoritedId = this.favoriteCareerCluster[i].id;
        this.careerCluster.favorited = true;
        break;
      }
    }
  }

    ga(param) {
        this._googleAnalyticsService.trackClick(param);
    }

    gaSocialShare(socialPlug, value) {
        this._googleAnalyticsService.trackSocialShare(socialPlug, value);
    }

  setData() {
    const baseImageUrl = this._config.getImageUrl();

    if (this.pathwayList) {
      for (let i = 0; i < this.pathwayList.length; i++) {
        if (this.pathwayList[i].imageId) {
        this.pathwayList[i].imageUrl = baseImageUrl + 'citm-images/' + this.pathwayList[i].imageId.split(' ').join('') + '.jpg';
        this.pathwayList[i].mcId = this.pathwayList[i].imageId.split(' ').join('');
        } else {
          this.pathwayList[i].imageUrl = 'none';
        }
             // this._recentlyViewedService.setRecentlyViewed(this.militaryCareer.mcId, this.militaryCareer.title);
      this._titleTag.setTitle(this.careerCluster.title + ' | Careers in the Military');
      this._meta.updateTag({name: 'description', content: this.pathwayList[0].description});

      this._meta.updateTag( {property: 'og:image:width', content: '1200'} );
      this._meta.updateTag( {property: 'og:image:height', content: '627'} );
      this._meta.updateTag({property: 'og:title', content: this.careerCluster.title  + ' | Careers in the Military'});
      this._meta.updateTag({property: 'og:description', content: this.pathwayList[0].description});
      this._meta.updateTag({property: 'og:image', content: this.pathwayList[0].imageUrl});
      this._meta.updateTag({name: 'og:image:alt', content: 'Pathway ' + this.pathwayList[0].pathwayId + ': ' +
       this.pathwayList[0].pathwayTitle});

      this._meta.updateTag({name: 'twitter:title', content: this.careerCluster.title  + ' | Careers in the Military'});
      this._meta.updateTag({name: 'twitter:description', content: this.pathwayList[0].description});
      this._meta.updateTag({name: 'twitter:image', content: this.pathwayList[0].imageUrl});
      this._meta.updateTag({name: 'twitter:image:alt', content: 'Pathway ' + this.pathwayList[0].pathwayId + ': ' +
       this.pathwayList[0].pathwayTitle});

      }
    }
  }

  setSampleJobs(index) {
    if (this.pathwayList[index].sampleJobs.length > 0) {
    this.sampleJobs = this.pathwayList[index].sampleJobs;
    } else {
      this.sampleJobs = [];
      this.sampleJobs.push ( {
            'title': 'There are no military careers assigned to this pathway at this time.'
      });
    }
  }

  getPhotoCredit(id) {
    if (this.searchList) {
      const index = this.searchList.map(function (item) {
        return item.mcId;
      }).indexOf(id);
      if (index !== -1) {
        return this.searchList[index].photoCredit;
      } else {
        return '';
      }
    }

  }

  onFavoriteClick() {
    if (this.disabled) {
      return;
    }
    this.disabled = true;
    this.myStyle =  {'cursor': 'wait'};
    return this._favoriteService.onCareerClusterFavoriteClick(this.careerCluster)
    .then ((obj) => {
      this.favoriteCareerCluster = obj;
      this.setFavorited();
      this.disabled = false;
      this.myStyle =  {'cursor': 'pointer'};
      return;
    })
    .catch(() => {
      this.disabled = false;
      this.myStyle =  {'cursor': 'pointer'};
      return;
    });
  }

  isLoggedIn() {
    return this._config.isLoggedIn();
  }

    scrollTop = function() {
    window.scrollTo(0, 0);
    };

    ngOnDestroy() {
      if (this.navigationSubscription) {
        this.navigationSubscription.unsubscribe();
      }
    }
}
