import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CareerClusterPathwayComponent } from './career-cluster-pathway.component';

describe('CareerClusterPathwayComponent', () => {
  let component: CareerClusterPathwayComponent;
  let fixture: ComponentFixture<CareerClusterPathwayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CareerClusterPathwayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CareerClusterPathwayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
