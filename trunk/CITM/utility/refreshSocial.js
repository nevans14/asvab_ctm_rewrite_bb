// Pathway starts from 1 to 16
const pathwayStart = 1;
const pathwayEnd = 16;  // Total 16 pathways

let env = 0;  // 0. dev  |  1. staging  |  2. production:
if (env === 0) {
  subDomain = 'dev-www'
  console.log("Clearing cache for dev.")
} else if (env === 1) {
  subDomain = 'stg-www'
  console.log("Clearing cache for stg.")
} else if (env === 2) {
  subDomain = 'www'
  console.log("Clearing cache for production.")
} else {
  console.log("Please retry with a valid entry.")
  return;
}


//  http://dev-www.careersinthemilitary.com/CITM/rest/CareerProfile/getCareerProfileList
const careerProfilesUrl = "http://legacy-dev.careersinthemilitary.com/CITM/rest/CareerProfile/getCareerProfileList"
const basePathwayUrl = "https://legacy-dev.careersinthemilitary.com/CITM/rest/search/get-career-cluster-pathway/"
const baseCareerDetailUrl = "https://" + subDomain +".careersinthemilitary.com/career-detail/"
const baseCareerProfileUrl = "https://" + subDomain + ".careersinthemilitary.com/career-profile/"

const axios = require('axios')

let urls = []
let urlsSaved = []

const fs = require('fs')
if (fs.existsSync("output.txt")) {
  fs.unlinkSync("output.txt")
}

let promises = []
let pathways = []
let careerProfiles = []
for (i = pathwayStart; i <= pathwayEnd; ++i) {
  promises.push(axios.get(basePathwayUrl + i + '/'))
}
promises.push(axios.get(careerProfilesUrl))

Promise.all(promises).then(res => {
  for (j = 0; j < (pathwayEnd - pathwayStart + 1); ++j) {
    pathways = [...pathways, ...res[j].data]
  }
  careerProfiles = res[j].data;

  for (j = 0; j < pathways.length; ++j) {
    const pathway = pathways[j];
    for (z = 0; z < pathway.sampleJobs.length; ++z) {
      const sampleJob = pathway.sampleJobs[z];
      if (sampleJob.mcId) {
        let careerNum = sampleJob.mcId.trim();

        urlsSaved.push(baseCareerDetailUrl + careerNum);
        urls.push(baseCareerDetailUrl + careerNum + '?reset=1');
      }
    }
  }

  for (profile of careerProfiles) {
    urlsSaved.push(baseCareerProfileUrl + profile.mcId);
    urls.push(baseCareerProfileUrl + profile.mcd + '?reset=1');
  }

  urlsSaved = urlsSaved.join('\n')
  fs.appendFileSync("output.txt", urlsSaved, "UTF-8", { 'flags': 'a' });
}).then(res => {
  promises = [];
  for (url of urls) {
    promises.push(axios.get(url))
  }

  Promise.all(promises).then(res => {
    console.log("Update Completed, all except Facebook. Please follow readme on how to update Facebook")
  })
  .catch(err => {
    console.log(err.message)
  })
})
.catch(err => {
  console.log(err.message)
})