# Readme for clearing social networks shared image

## Setup
* Install node https://nodejs.org/en/
* In utility folder,
```
npm i
```
* In refreshSocial.js, update env with the numerical number corresponding to the environment you want to clear cache on.
Env mapping is the following: 0 is dev, 1 is staging, and 2 is production.

## Execution
This refreshes all social networks image cache except for Facebook. See following section for Facebook.
```
node refreshSocial.js
```

## Bulk refresh Facebook
Open the output.txt file, and go to https://developers.facebook.com/tools/debug/sharing/batch/. 
Since there is a 50 URLs limit for each bulk, please copy paste 50 URLs from the above list. 
Then click on Debug. For the ones that has a red explanation mark, click on View Details, and Fetch or Scrape Again.
Keep clicking on this until you see the correct image shows up under Link Preview. 

## Manually refresh other social networks
If for some reason the shared image is not coming up correctly, shared image could be manually refreshed by going to each of the social networks own site. Copy and paste in the page URL where share originated and refetch. 
* Facebook https://developers.facebook.com/tools/debug/sharing/batch/
* Twitter https://cards-dev.twitter.com/validator
* Linkedin https://www.linkedin.com/post-inspector/inspect/
