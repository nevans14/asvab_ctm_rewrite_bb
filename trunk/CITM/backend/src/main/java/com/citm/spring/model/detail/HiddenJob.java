package com.citm.spring.model.detail;

import java.io.Serializable;

public class HiddenJob implements Serializable {

	private static final long serialVersionUID = 5875166107249198339L;
	int userId;
	String jsonString;

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getJsonString() {
		return jsonString;
	}

	public void setJsonString(String jsonString) {
		this.jsonString = jsonString;
	}
}
