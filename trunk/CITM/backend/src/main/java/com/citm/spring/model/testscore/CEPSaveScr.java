package com.citm.spring.model.testscore;

public class CEPSaveScr {

	private	int	resultId;
	private	int	formId;
	private	int	score;
	private	int	scoreSection;
	private	int	scoreType;
	/**
	 * @return the resultId
	 */
	public int getResultId() {
		return resultId;
	}
	/**
	 * @param resultId the resultId to set
	 */
	public void setResultId(int resultId) {
		this.resultId = resultId;
	}
	/**
	 * @return the formId
	 */
	public int getFormId() {
		return formId;
	}
	/**
	 * @param formId the formId to set
	 */
	public void setFormId(int formId) {
		this.formId = formId;
	}
	/**
	 * @return the score
	 */
	public int getScore() {
		return score;
	}
	/**
	 * @param score the score to set
	 */
	public void setScore(int score) {
		this.score = score;
	}
	/**
	 * @return the scoreSection
	 */
	public int getScoreSection() {
		return scoreSection;
	}
	/**
	 * @param scoreSection the scoreSection to set
	 */
	public void setScoreSection(int scoreSection) {
		this.scoreSection = scoreSection;
	}
	/**
	 * @return the scoreType
	 */
	public int getScoreType() {
		return scoreType;
	}
	/**
	 * @param scoreType the scoreType to set
	 */
	public void setScoreType(int scoreType) {
		this.scoreType = scoreType;
	}
}
