package com.citm.spring.model.detail;

import java.io.Serializable;

public class HelpfulAttribute implements Serializable {

	private static final long serialVersionUID = -9178253678044390297L;
	String helpfulAttribute, mcId;

	public String getHelpfulAttribute() {
		return helpfulAttribute;
	}

	public void setHelpfulAttribute(String helpfulAttribute) {
		this.helpfulAttribute = helpfulAttribute;
	}

	public String getMcId() {
		return mcId;
	}

	public void setMcId(String mcId) {
		this.mcId = mcId;
	}
}
