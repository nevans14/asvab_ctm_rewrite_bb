package com.citm.spring.dao.mybatis.service;

import java.util.ArrayList;

import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.citm.spring.dao.mybatis.mapper.CareerSearchMapper;
import com.citm.spring.model.search.CareerCluster;
import com.citm.spring.model.search.CareerClusterPathway;
import com.citm.spring.model.search.JobFamily;
import com.citm.spring.model.search.SaveSearch;
import com.citm.spring.model.search.Search;
import com.citm.spring.model.search.ServiceContacts;

@Service
public class CareerSearchService {

	@Autowired
	CareerSearchMapper mapper;

	public ArrayList<Search> getSearchList() {
		return mapper.getSearchList();
	}

	public int insertSaveSearch(SaveSearch saveSearchObject) {
		return mapper.insertSaveSearch(saveSearchObject);
	}

	public ArrayList<SaveSearch> getSaveSearch(int userId) {
		return mapper.getSaveSearch(userId);
	}

	public int deleteSaveSearch(Integer userId, int id) {
		return mapper.deleteSaveSearch(userId, id);
	}

	public ArrayList<CareerCluster> getCareerCluster() {
		return mapper.getCareerCluster();
	}

	public ArrayList<ServiceContacts> getServiceContacts() {
		return mapper.getServiceContacts();
	}

	public ArrayList<CareerClusterPathway> getCareerClusterPathway(int ccId) {
		return mapper.getCareerClusterPathway(ccId);
	}

	public ArrayList<JobFamily> getJobFamily() {
		return mapper.getJobFamily();
	}
}
