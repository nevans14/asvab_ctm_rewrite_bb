package com.citm.spring.model.detail;

import java.io.Serializable;

public class ArmyDetail implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6538557421863755190L;
	String mcId, socId, svc, mpc, title, motdTitle, mocId, activeDutyLink, reserveLink, nationalGuardLink, overview, youTubeLink;

	Integer motdId;

	public String getMcId() {
		return mcId;
	}

	public void setMcId(String mcId) {
		this.mcId = mcId;
	}

	public String getSocId() {
		return socId;
	}

	public void setSocId(String socId) {
		this.socId = socId;
	}

	public String getSvc() {
		return svc;
	}

	public void setSvc(String svc) {
		this.svc = svc;
	}

	public String getMpc() {
		return mpc;
	}

	public void setMpc(String mpc) {
		this.mpc = mpc;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getMotdTitle() {
		return motdTitle;
	}

	public void setMotdTitle(String motdTitle) {
		this.motdTitle = motdTitle;
	}

	public String getMocId() {
		return mocId;
	}

	public void setMocId(String mocId) {
		this.mocId = mocId;
	}

	public String getActiveDutyLink() {
		return activeDutyLink;
	}

	public void setActiveDutyLink(String activeDutyLink) {
		this.activeDutyLink = activeDutyLink;
	}

	public String getReserveLink() {
		return reserveLink;
	}

	public void setReserveLink(String reserveLink) {
		this.reserveLink = reserveLink;
	}
	
	public String getNationalGuardLink() {
		return nationalGuardLink;
	}

	public void setNationalGuardLink(String nationalGuardLink) {
		this.nationalGuardLink = nationalGuardLink;
	}

	public String getOverview() {
		return overview;
	}

	public void setOverview(String overview) {
		this.overview = overview;
	}

	public Integer getMotdId() {
		return motdId;
	}

	public void setMotdId(Integer motdId) {
		this.motdId = motdId;
	}
	
	public String getYouTubeLink() {
		return youTubeLink;
	}
	
	public void setYouTubeLink(String youTubeLink) {
		this.youTubeLink = youTubeLink;
	}
}
