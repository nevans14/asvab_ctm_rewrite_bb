package com.citm.spring.dao.mybatis.mapper;

import java.util.ArrayList;

import org.apache.ibatis.annotations.Param;

import com.citm.spring.model.contact.EmailContactUs;

public interface ContactUsMapper {

	ArrayList<String> getEmail(@Param("roleId") int roleId);

	int insertGeneralContactUs(@Param("emailObject") EmailContactUs emailObject);
	
	int insertServiceContactUs(@Param("emailObject") EmailContactUs emailObject);
	
	int insertContactUsByService(@Param("svc") String svc, @Param("dateContacted") String dateContacted);
}
