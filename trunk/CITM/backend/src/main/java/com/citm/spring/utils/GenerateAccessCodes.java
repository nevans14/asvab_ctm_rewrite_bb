package com.citm.spring.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

import org.apache.poi.util.SystemOutLogger;

import com.citm.spring.model.login.AccessCode;


/**
 * 
 * @author Dave Springer
 * 
 * Based on the document "2016 ASVAB Website Passcode System"
 * This class generates Access Codes in the following types and formats.
 * 
 * Marketing Codes   Ex.  ###[A-Z][A-Z] 
 * Counselor Codes   Ex.  C#####
 * Reserved Codes    Ex.  #####[A-E]  Depends on year 
 * Three Week Codes  Ex.  [A-Z][A-Z][A-Z][A-Z][A-Z]
 * 
 * Note: [A-Z] is letters from A to Z minus L, I, O!
 * 
 */
public class GenerateAccessCodes {
	// Not synchronized !!!!!
	private static Random random = new Random();
	
	
	/**
	 * generateMarketingCodes : 
	 * 
	 * Marketing Codes   Ex.  ###[A-Z][A-Z] 
	 * 
	 * @param setSize  The number of ids to generate.
	 * @return A set of unique access codes of the size setSize.
	 */
	public static Set<String> generateMarketingCodes(Integer setSize) {
		Set<String> set = new HashSet<String>();
		while ( set.size() < setSize ){
			set.add(getNextRandomNumber(1, 999, "%03d") + getNextRandomCharacters(2)   );
		}
		return set;
	}

	
	/**
	 * generateCounselorCodes
	 * 
	 *  * Counselor Codes   Ex.  C#####
	 * 
	 * @param setSize  The number of ids to generate.
	 * @return A set of unique access codes of the size setSize.
	 */
	public static Set<String> generateCounselorCodes(Integer setSize) {
		Set<String> set = new HashSet<String>();
		while ( set.size() < setSize ){
			if ( set.size() == 10000) break;
			set.add("C" +  getNextRandomNumber(1, 10000, "%05d")  );
		}
		return set;
	}

	/**
	 * generateReservedCodes
	 * Reserved Codes    Ex.  #####[A-E]  Depends on year 
	 * 
	 * @param setSize
	 * @return
	 */
	public static Set<String> generateReservedCodes(Integer setSize) {
		Set<String> set = new HashSet<String>();
		// Determine the suffix 
		String suffix1 = "";
		String suffix2 = "";
		Calendar date = new GregorianCalendar();
		int year = date.get(Calendar.YEAR);  // 2012
		
		if ( year % 3 == 0 ) {
			suffix1 = "E";
			suffix2 = "A";
		} else if ( year % 3 == 1 ) {
			suffix1 = "A";
			suffix2 = "B";
		} else if ( year % 3 == 2 ) {
			suffix1 = "C";
			suffix2 = "D";	
		} 
		while ( set.size() < setSize ){
			if ( set.size() == 20000) break;
			if ( set.size() % 2 == 0 ) 
				set.add( getNextRandomNumber(1, 10000, "%05d") + suffix1 );
			else 
				set.add( getNextRandomNumber(1, 10000, "%05d") + suffix2 );

		}
		return set;
	}
	
	/**
	 * generateThreeWeekCodes 
	 *  
	 * Three Week Codes  Ex.  [A-Z][A-Z][A-Z][A-Z][A-Z]
	 * 
	 * @param setSize
	 * @return
	 */
	public static Set<String> generateThreeWeekCodes(Integer setSize) {
		Set<String> set = new HashSet<String>();
		while ( set.size() < setSize ){
			set.add( getNextRandomCharacters(5) );
		}
		return set;
	}
	
	/**
	 * Generates a new CITM Access Code based on the last one that was generated
	 * @param lastAccessCodeUsed
	 * @return
	 */
	public static String generateCitmCode(AccessCode lastAccessCodeUsed) {
		String accessCode = "";
		if (lastAccessCodeUsed != null) {
			String lastUsedAccessCodeStr = lastAccessCodeUsed.getAccessCode().substring(0, 6);
			int lastUsedAccessCodeNum = Integer.parseInt(lastUsedAccessCodeStr);
			accessCode = String.format("%06d", lastUsedAccessCodeNum + 1);	
		} else {
			accessCode = String.format("%06d", 1);
		}
		accessCode += "CITM";
		return accessCode;
	}

	/**
	 * Creates a new AccessCode instance with the given parameters and an expiration date
	 * @param accessCode
	 * @param role
	 * @param unlimitedFyi
	 * @return
	 */
	public static AccessCode createAccessCode(String accessCode, String role, Integer unlimitedFyi) {
		// set expire date
		Integer schoolYear = (determineSchoolYear() + 2);
		String expireDate = schoolYear.toString().trim() + "-06-30 00:00:00.000";
		// returns a new access code
		return new AccessCode(accessCode, role, SimpleUtils.getDateFromStringInStandardFormat(expireDate), unlimitedFyi);
	}
	
	
	
	/* Helper Functions */
	/**
	 * getNextRandomNumber - Generates a random number between the min and max.
	 * 
	 * 
	 * @param min    Low end of range.
	 * @param max    High end of range.
	 * @param format "%05d"
	 * @return
	 */
	public static String getNextRandomNumber(Integer min, Integer max, String format) {
		return String.format(format, random.nextInt(max - min + 1) + min );
	}
	
	/**
	 * getNextRandomCharacters - gets a random character string with out I,L,O
	 * 
	 * @param size   Number of random characters to return.
	 * @return
	 */
	private static String getNextRandomCharacters(Integer size) {
		StringBuffer sb = new StringBuffer(); 
		int ii = 'I'; // 73 
		int ll = 'L';  // 76
		int oo = 'O';  // 79		
		int a = 65;
		int z = 90;
		int count = 0;
		
		while (count < size ) {
			int randC = random.nextInt(z - a + 1) + a ;
			if ( randC != ii || 
				 randC != ll ||
				 randC != oo ) {
				sb.append( (char)  randC);
			}
			count++;
		}
		  
		return sb.toString() ;
	}
	
	   /**
     * generateNewMarketingCodes 
     *  
     * New Marketing codes  [00000-99999]+ "MARK"
     * 
     * @param setSize
     * @return
     */
    public static Set<String> generateNewMarketingCodes(Integer setSize) {
        Set<String> set = new HashSet<String>();
/*        while ( set.size() < setSize ){
            set.add( getNextRandomCharacters(5) + "MARK" );
        }
*/        

        for ( int i = 1 ; i <= setSize; i++) {
            set.add( getNextRandomCharacters(5) + "MARK" );            
        }
        return set;
    }
    
    
    /** Given todays date determine what the Modulus year would be.
     * 
     * Modulus 0 SchoolYear2016 = "F,G,H,J,K,L"  
     * Modulus 1 SchoolYear2017 = "M,N,P,Q,R,S" 
     * Modulus 2 SchoolYear2018 = "T,U,V,W,X,Y" 
     * 
     * @param schoolYear 
     * @return
     */
    public static List<String> getSchoolYearSuffixes(Integer schoolYear ) {
        ArrayList<String> list = null;
        int remainder = schoolYear % 3;
        
        if ( remainder == 0 ) {
            list = new ArrayList<String>() {{
                add("F");
                add("G");
                add("H");
                add("J");
                add("K");
                add("L");
                add("f");
                add("g");
                add("h");
                add("j");
                add("k");
                add("l");         }};
        } else if ( remainder == 1 ) {
            list = new ArrayList<String>() {{
                add("M");
                add("N");
                add("P");
                add("Q");
                add("R");
                add("S");
                add("m");
                add("n");
                add("p");
                add("q");
                add("r");
                add("s");
         }};
        } else if ( remainder == 2 ) {
            list = new ArrayList<String>() {{
                add("T");
                add("U");
                add("V");
                add("W");
                add("X");
                add("Y");
                add("t");
                add("u");
                add("v");
                add("w");
                add("x");
                add("y");
         }};
        }
        return list;
    }
        
    
    
    public static boolean isSuffixInCurrentYear(String suffix){
        Integer  year = determineSchoolYear(); 
        List<String> ls = getSchoolYearSuffixes(year % 3);
         if ( ls.contains(suffix) ) {
             return true;
         }
        
        return false;
    }
    
    public static Integer schoolYearOfSuffix(String suffix){
        //  Today is in what school year?
        Integer  thisYear = determineSchoolYear(); 
        Integer  lastYear = thisYear - 1 ;
        Integer  twoYearsAgo = thisYear - 2; 
        Integer yearMod = thisYear % 3; 

        if (getSchoolYearSuffixes(thisYear ).contains(suffix)) return thisYear;
        if (getSchoolYearSuffixes(lastYear ).contains(suffix)) return lastYear;
        if (getSchoolYearSuffixes(twoYearsAgo ).contains(suffix)) return twoYearsAgo;


        return null;
    }
    
    
    
    
    /**
     * Returns what the school year.
     * 
     * 
     * 
     * @return
     */
    public static Integer determineSchoolYear(){
        String yearMonth = SimpleUtils.getCurrentYearMonth();
        String [] yearMonthComponents = yearMonth.split("-");
        Integer year = Integer.parseInt(yearMonthComponents[0]);
        Integer month = Integer.parseInt(yearMonthComponents[1]);
        
        // If month is greater than six then the school year is equal to year + 1.
        // If month is seven or more then the school year is equal to year.
        if ( month > 6 ){ 
            year++;
        }
        
        return year;
    }

    
    
}
