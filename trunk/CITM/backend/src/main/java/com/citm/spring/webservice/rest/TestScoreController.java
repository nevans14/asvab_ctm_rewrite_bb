package com.citm.spring.webservice.rest;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import com.citm.spring.utils.UserManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;

import com.citm.spring.dao.mybatis.impl.TestScoreDAOImpl;
import com.citm.spring.model.testscore.CompositeFavoritesFlags;
import com.citm.spring.model.testscore.ManualTestScore;
import com.citm.spring.model.testscore.StudentTestingProgam;
import com.citm.spring.model.testscore.StudentTestingProgamSelects;

@Controller
@RequestMapping("/testScore")
public class TestScoreController {

	private Logger logger = LogManager.getLogger(this.getClass().getName());

	@Autowired
	UserManager userManager;
	@Autowired
	TestScoreDAOImpl dao;

	@RequestMapping(value = "/InsertTestScore", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<Integer> insertTestScore(
			@RequestBody List<StudentTestingProgam> testScoreObject) {
		logger.info("insertTestScore");
		int rowsInserted = 0;
		try {
			rowsInserted = dao.insertTestScore(testScoreObject);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<Integer>(HttpStatus.BAD_REQUEST);
		}
		if (rowsInserted != 1) {
			logger.error("Unexpected Response");
			return new ResponseEntity<Integer>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<Integer>(rowsInserted, HttpStatus.OK);
		}
	}
	
	@RequestMapping(value = "/get-test-score", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<StudentTestingProgamSelects> getTestScore() {
	    logger.info("getTestScore");
		if (!userManager.isAuthenticated()) {
			return new ResponseEntity<>(HttpStatus.FORBIDDEN);
		}
	    StudentTestingProgamSelects results = null;
	    String role = userManager.getRole();
	    String accessCode = userManager.getAccessCode();
		try {
			if (role.toUpperCase().equals("T")) {
				// see if the access contains the promo code "CEP123"
				if (accessCode.toUpperCase().contains("CEP123")) {
					// get mock test score based on SHANNON-A
					results = dao.getTestScore("SHANNON-A");
				} else {
					// get mock test score based on KATE-A
					results = dao.getTestScore("KATE-A");
				}
			} else if (role.toUpperCase().equals("P")) {
				// get mock test scores for PTI
				results = dao.getTestScore("KATE-A");
			} else {
				// anyone else
				results = dao.getTestScore(userManager.getUserId());
			}
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<StudentTestingProgamSelects>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<StudentTestingProgamSelects>(results, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/get-access-code", method = RequestMethod.GET)
	public @ResponseBody Map<String, String> getAccessCode() {
		logger.info("getAccessCode");
		return Collections.singletonMap("r", userManager.getAccessCode());
	}
	
	@RequestMapping(value = "/get-test-score-access-code", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<StudentTestingProgamSelects> getTestScoreByAccessCode() {
	    logger.info("getTestScore");
	    StudentTestingProgamSelects studentTestingProgramObject = null;
		try {
			studentTestingProgramObject = dao.getTestScore(userManager.getAccessCode());
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<StudentTestingProgamSelects>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<StudentTestingProgamSelects>(studentTestingProgramObject, HttpStatus.OK);
	}

	@RequestMapping(value = "/getManualTestScore", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<ManualTestScore> getManualTestScore() {
		logger.info("getManualTestScore");
		ManualTestScore manualTestScoreObj = null;
		try {
			manualTestScoreObj = dao.getManualTestScore(userManager.getUserId());
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<ManualTestScore>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<ManualTestScore>(manualTestScoreObj, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/insertManualTestScore", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<Integer> insertManualTestScore(
			@RequestBody ManualTestScore manualTestScoreObj) {
		logger.info("insertManualTestScore");
		int rowsInserted = 0;
		manualTestScoreObj.setUserId(userManager.getUserId());
		try {
			rowsInserted = dao.insertManualTestScore(manualTestScoreObj);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<Integer>(HttpStatus.BAD_REQUEST);
		}
		if (rowsInserted != 1) {
			logger.error("Unexpected Response");
			return new ResponseEntity<Integer>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<Integer>(rowsInserted, HttpStatus.OK);
		}
	}
	
	@RequestMapping(value = "/updateManualTestScore", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<Integer> updateManualTestScore(
			@RequestBody ManualTestScore manualTestScoreObj) {
		logger.info("updateManualTestScore");
		int rowUpdated = 0;
		try {
			rowUpdated = dao.updateManualTestScore(manualTestScoreObj);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<Integer>(HttpStatus.BAD_REQUEST);
		}
		if (rowUpdated != 1) {
			logger.error("Unexpected Response");
			return new ResponseEntity<Integer>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<Integer>(rowUpdated, HttpStatus.OK);
		}
	}
	
	@RequestMapping(value = "/deleteManualTestScore", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<Integer> deleteManualTestScore() {
	    logger.info("deleteManaulTestScore");
	    int rowdeleted = 0;
		try {
			rowdeleted = dao.deleteManualTestScore(userManager.getUserId());
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<Integer>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<Integer>(rowdeleted, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/getCompositeFavoritesFlags", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<CompositeFavoritesFlags> getCompositeFavoritesFlags() {
	    logger.info("getCompositeFavoritesFlags");
	    CompositeFavoritesFlags compositeFavoritesFlagsObj;
		try {
			compositeFavoritesFlagsObj = dao.getCompositeFavoritesFlags(userManager.getUserId());
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<CompositeFavoritesFlags>(HttpStatus.BAD_REQUEST);
		}
		// Front end code expects default object when there is no record found.
		if(compositeFavoritesFlagsObj == null){
			return new ResponseEntity<CompositeFavoritesFlags>(new CompositeFavoritesFlags(), HttpStatus.OK);
		}
		return new ResponseEntity<CompositeFavoritesFlags>(compositeFavoritesFlagsObj, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/setCompositeFavoritesFlags", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<Integer> setCompositeFavoritesFlags(
			@RequestBody CompositeFavoritesFlags compositeFavoritesFlagsObj) {
		logger.info("updateCompositeFavoritesFlags");
		int rows = 0;
		compositeFavoritesFlagsObj.setUserId(userManager.getUserId());
		try {
			if(dao.compositeFavoritesFlagsExists(compositeFavoritesFlagsObj.getUserId()))
				rows = dao.updateCompositeFavoritesFlags(compositeFavoritesFlagsObj);
			else
				rows = dao.setCompositeFavoritesFlags(compositeFavoritesFlagsObj);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<Integer>(HttpStatus.BAD_REQUEST);
		}
		if (rows != 1) {
			logger.error("Unexpected Response");
			return new ResponseEntity<Integer>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<Integer>(rows, HttpStatus.OK);
		}
	}
	
	@RequestMapping(value = "/deleteCompositeFavoritesFlags", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<Integer> deleteCompositeFavoritesFlags() {
	    logger.info("deleteCompositeFavoritesFlags");
	    int rowdeleted = 0;
		try {
			rowdeleted = dao.deleteCompositeFavoritesFlags(userManager.getUserId());
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<Integer>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<Integer>(rowdeleted, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/emailExists/{userName}/", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<Boolean> emailExists(@PathVariable String userName) {
	    logger.info("emailExists");
	    boolean exists;
		try {
			exists = dao.emailExists(userName);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<Boolean>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<Boolean>(exists, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/latestTestDate", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<String> latestTestDate() {
	    logger.info("deleteCompositeFavoritesFlags");
	    String date;
		try {
			date = dao.latestTestDate(userManager.getAccessCode());
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<String>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<String>(date, HttpStatus.OK);
	}

	public StudentTestingProgam wsClient(){
		RestTemplate restTemplate = new RestTemplate();
		StudentTestingProgam testScoreObject = restTemplate.getForObject("http://gturnquist-quoters.cfapps.io/ap", StudentTestingProgam.class);
		return testScoreObject;
	}
}
