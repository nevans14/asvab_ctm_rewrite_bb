package com.citm.spring.webservice.rest;

import java.util.ArrayList;

import com.citm.spring.utils.UserManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.support.CustomSQLErrorCodesTranslation;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.citm.spring.dao.mybatis.impl.CareerDetailDAOImpl;
import com.citm.spring.model.detail.WhatTheyDo;
import com.citm.spring.model.detail.RelatedCareer;
import com.citm.spring.model.detail.SeeScoreArmy;
import com.citm.spring.model.detail.ServiceOffering;
import com.citm.spring.model.detail.SkillRating;
import com.citm.spring.model.detail.TrainingProvided;
import com.citm.spring.model.detail.AdditionalInfo;
import com.citm.spring.model.detail.AirForceDetail;
import com.citm.spring.model.detail.ArmyDetail;
import com.citm.spring.model.detail.CareerPath;
import com.citm.spring.model.detail.CoastGuardDetail;
import com.citm.spring.model.detail.HelpfulAttribute;
import com.citm.spring.model.detail.HiddenJob;
import com.citm.spring.model.detail.MarineDetail;
import com.citm.spring.model.detail.MilitaryCareer;
import com.citm.spring.model.detail.NavyDetail;
import com.citm.spring.model.detail.Profile;
import com.citm.spring.model.detail.RecentlyViewed;
import com.citm.spring.model.careerprofile.CareerProfile;

@Controller
@RequestMapping("/detail-page")
public class CareerDetailController {
	
	private Logger logger = LogManager.getLogger(this.getClass().getName());
	@Autowired
	UserManager userManager;
	@Autowired
	CareerDetailDAOImpl dao;

	@RequestMapping(value = "/get-military-career/{mcId}", method = RequestMethod.GET)
	public @ResponseBody MilitaryCareer getMilitaryCareer(@PathVariable String mcId) {
		MilitaryCareer results = new MilitaryCareer();

		try {
			results = dao.getMilitaryCareer(mcId);
		} catch (Exception e) {
			logger.error("Error", e);
		}
		return results;
	}

	@RequestMapping(value = "/get-what-they-do/{mcId}", method = RequestMethod.GET)
	public @ResponseBody ArrayList<WhatTheyDo> getWhatTheyDo(@PathVariable String mcId) {
		ArrayList<WhatTheyDo> results = new ArrayList<WhatTheyDo>();

		try {
			results = dao.getWhatTheyDo(mcId);
		} catch (Exception e) {
			logger.error("Error", e);
		}
		return results;
	}

	@RequestMapping(value = "/get-training-provided/{mcId}", method = RequestMethod.GET)
	public @ResponseBody ArrayList<TrainingProvided> getTrainingProvided(@PathVariable String mcId) {
		ArrayList<TrainingProvided> results = new ArrayList<TrainingProvided>();

		try {
			results = dao.getTrainingProvided(mcId);
		} catch (Exception e) {
			logger.error("Error", e);
		}
		return results;
	}

	@RequestMapping(value = "/get-related-career/{mcId}", method = RequestMethod.GET)
	public @ResponseBody ArrayList<RelatedCareer> getRelatedCareers(@PathVariable String mcId) {
		ArrayList<RelatedCareer> results = new ArrayList<RelatedCareer>();

		try {
			results = dao.getRelatedCareers(mcId);
		} catch (Exception e) {
			logger.error("Error", e);
		}
		return results;
	}
	
	@RequestMapping(value = "/get-related-civilian-career/{mcId}", method = RequestMethod.GET)
	public @ResponseBody ArrayList<RelatedCareer> getRelatedCivilianCareers(@PathVariable String mcId) {
		ArrayList<RelatedCareer> results = new ArrayList<RelatedCareer>();

		try {
			results = dao.getRelatedCivilianCareers(mcId);
		} catch (Exception e) {
			logger.error("Error", e);
		}
		return results;
	}
	
	@RequestMapping(value = "/get-additional-info/{mcId}", method = RequestMethod.GET)
	public @ResponseBody ArrayList<AdditionalInfo> getAdditionalCareerInfo(@PathVariable String mcId) {
		ArrayList<AdditionalInfo> results = new ArrayList<AdditionalInfo>();

		try {
			results = dao.getAdditionalCareerInfo(mcId);
		} catch (Exception e) {
			logger.error("Error", e);
		}
		return results;
	}

	@RequestMapping(value = "/get-profile/{mcId}", method = RequestMethod.GET)
	public @ResponseBody ArrayList<Profile> getProfiles(@PathVariable String mcId) {
		ArrayList<Profile> results = new ArrayList<Profile>();

		try {
			results = dao.getProfiles(mcId);
		} catch (Exception e) {
			logger.error("Error", e);
		}
		return results;
	}

	@RequestMapping(value = "/get-hot-job/{mcId}", method = RequestMethod.GET)
	public @ResponseBody Integer getHotJob(@PathVariable String mcId) {
		Integer results = null;

		try {
			results = dao.getHotJob(mcId);
		} catch (Exception e) {
			logger.error("Error", e);
		}
		return results;
	}

	@RequestMapping(value = "/get-helpful-attribute/{mcId}", method = RequestMethod.GET)
	public @ResponseBody ArrayList<HelpfulAttribute> getHelpfulAttribute(@PathVariable String mcId) {
		ArrayList<HelpfulAttribute> results = new ArrayList<HelpfulAttribute>();

		try {
			results = dao.getHelpfulAttribute(mcId);
		} catch (Exception e) {
			logger.error("Error", e);
		}
		return results;
	}

	@RequestMapping(value = "/get-service-offering/{mcId}", method = RequestMethod.GET)
	public @ResponseBody ArrayList<ServiceOffering> getServiceOffering(@PathVariable String mcId) {
		ArrayList<ServiceOffering> results = new ArrayList<ServiceOffering>();

		try {
			mcId = mcId + "%";
			results = dao.getServiceOffering(mcId);
		} catch (Exception e) {
			logger.error("Error", e);
		}
		return results;
	}
	
	@RequestMapping(value = "/get-service-offering-composite", method = RequestMethod.GET)
	public @ResponseBody ArrayList<ServiceOffering> getServiceOfferingCompositeScores() {
		ArrayList<ServiceOffering> results = new ArrayList<ServiceOffering>();

		try {
			results = dao.getServiceOfferingCompositeScores();
		} catch (Exception e) {
			logger.error("Error", e);
		}
		return results;
	}
	
	@RequestMapping(value = "/get-skill-rating/{mcId}", method = RequestMethod.GET)
	public @ResponseBody SkillRating getSkillRating(@PathVariable String mcId) {
		SkillRating results = new SkillRating();

		try {
			results = dao.getSkillRating(mcId);
		} catch (Exception e) {
			logger.error("Error", e);
		}
		return results;
	}

	@RequestMapping(value = "/get-hide-job-list", method = RequestMethod.GET)
	public @ResponseBody String getHideJobList() {
		if (!userManager.isAuthenticated()) {
			return null;
		}
		String results = null;
		try {
			results = dao.getHideJobList(userManager.getUserId());
		} catch (Exception e) {
			logger.error("Error", e);
		}
		return results;
	}

	@RequestMapping(value = "/set-hide-job-list", method = RequestMethod.POST)
	public @ResponseBody int setHideJobList(@RequestBody HiddenJob obj) {
		if (!userManager.isAuthenticated()) {
			return 0;
		}
		int results = 0;
		obj.setUserId(userManager.getUserId());
		try {
			results = dao.setHideJobList(obj);
		} catch (Exception e) {
			logger.error("Error", e);
		}
		return results;
	}

	@RequestMapping(value = "/get-career-path/{mcId}", method = RequestMethod.GET)
	public @ResponseBody ArrayList<CareerPath> getCareerPath(@PathVariable String mcId) {
		ArrayList<CareerPath> results = new ArrayList<CareerPath>();

		try {
			results = dao.getCareerPath(mcId);
		} catch (Exception e) {
			logger.error("Error", e);
		}
		return results;
	}
	
	@RequestMapping(value ="/get-featured-profiles/{mcId}", method = RequestMethod.GET)
	public @ResponseBody ArrayList<CareerProfile> getFeaturedProfiles(@PathVariable String mcId) {
		ArrayList<CareerProfile> results = new ArrayList<CareerProfile>();
		try {
			results = dao.getFeaturedProfiles(mcId);
		} catch (Exception e) {
			logger.error("Error", e);
		}
		return results;
	}

	@RequestMapping(value = "/get-recently-viewed", method = RequestMethod.GET)
	public @ResponseBody String getRecentlyViewed() {
		if (!userManager.isAuthenticated()) {
			return null;
		}
		String results = null;
		try {
			results = dao.getRecentlyViewed(userManager.getUserId());
		} catch (Exception e) {
			logger.error("Error", e);
		}
		return results;
	}

	@RequestMapping(value = "/set-recently-viewed", method = RequestMethod.POST)
	public @ResponseBody int setRecentlyViewed(@RequestBody RecentlyViewed obj) {
		if (!userManager.isAuthenticated()) {
			return 0;
		}
		int results = 0;
		obj.setUserId(userManager.getUserId());
		try {
			results = dao.setRecentlyViewed(obj);
		} catch (Exception e) {
			logger.error("Error", e);
		}
		return results;
	}

	@RequestMapping(value = "/get-other-titles/{mcId}", method = RequestMethod.GET)
	public @ResponseBody ArrayList<String> getOtherTitles(@PathVariable String mcId) {
		ArrayList<String> results = null;

		try {
			results = dao.getOtherTitles(mcId);
		} catch (Exception e) {
			logger.error("Error", e);
		}
		return results;
	}

	@RequestMapping(value = "/get-air-force-detail/{mcId}/{mocId}", method = RequestMethod.GET)
	public @ResponseBody AirForceDetail getAirForceDetail(@PathVariable String mcId, @PathVariable String mocId) {
		AirForceDetail results = null;

		try {
			results = dao.getAirForceDetail(mcId, mocId);
		} catch (Exception e) {
			logger.error("Error", e);
		}
		return results;
	}
	
	@RequestMapping(value = "/get-army-detail/{mcId}/{mocId}", method = RequestMethod.GET)
	public @ResponseBody ArmyDetail getArmyDetail(@PathVariable String mcId, @PathVariable String mocId) {
		ArmyDetail results = null;

		try {
			results = dao.getArmyDetail(mcId, mocId);
		} catch (Exception e) {
			logger.error("Error", e);
		}
		return results;
	}
	
	@RequestMapping(value = "/get-coast-guard-detail/{mcId}/{mocId}", method = RequestMethod.GET)
	public @ResponseBody CoastGuardDetail getCoastGuardDetail(@PathVariable String mcId, @PathVariable String mocId) {
		CoastGuardDetail results = null;

		try {
			results = dao.getCoastGuardDetail(mcId, mocId);
		} catch (Exception e) {
			logger.error("Error", e);
		}
		return results;
	}
	
	@RequestMapping(value = "/get-marine-detail/{mcId}/{mocId}", method = RequestMethod.GET)
	public @ResponseBody MarineDetail getMarineDetail(@PathVariable String mcId, @PathVariable String mocId) {
		MarineDetail results = null;

		try {
			results = dao.getMarineDetail(mcId, mocId);
		} catch (Exception e) {
			logger.error("Error", e);
		}
		return results;
	}
	
	@RequestMapping(value = "/get-navy-detail/{mcId}/{mocId}", method = RequestMethod.GET)
	public @ResponseBody NavyDetail getNavyDetail(@PathVariable String mcId, @PathVariable String mocId) {
		NavyDetail results = null;

		try {
			results = dao.getNavyDetail(mcId, mocId);
		} catch (Exception e) {
			logger.error("Error", e);
		}
		return results;
	}
	
	@RequestMapping(value = "/get-see-score-army/{mcId}", method = RequestMethod.GET)
	public @ResponseBody ArrayList<SeeScoreArmy> getSeeScoreArmy(@PathVariable String mcId) {
		ArrayList<SeeScoreArmy> results = new ArrayList<SeeScoreArmy>();

		try {
			mcId = mcId + "%";
			results = dao.getSeeScoreArmy(mcId);
		} catch (Exception e) {
			logger.error("Error", e);
		}
		return results;
	}
	

}
