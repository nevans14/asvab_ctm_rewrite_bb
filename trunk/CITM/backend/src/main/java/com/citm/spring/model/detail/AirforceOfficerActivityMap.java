package com.citm.spring.model.detail;

import java.io.Serializable;

public class AirforceOfficerActivityMap implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 3602324070375716375L;
	Integer officerActivitiestId;
	String officerActivitiesTitle, officerActivitiesDefinition;

	public Integer getOfficerActivitiestId() {
		return officerActivitiestId;
	}

	public void setOfficerActivitiestId(Integer officerActivitiestId) {
		this.officerActivitiestId = officerActivitiestId;
	}

	public String getOfficerActivitiesTitle() {
		return officerActivitiesTitle;
	}

	public void setOfficerActivitiesTitle(String officerActivitiesTitle) {
		this.officerActivitiesTitle = officerActivitiesTitle;
	}

	public String getOfficerActivitiesDefinition() {
		return officerActivitiesDefinition;
	}

	public void setOfficerActivitiesDefinition(String officerActivitiesDefinition) {
		this.officerActivitiesDefinition = officerActivitiesDefinition;
	}
}
