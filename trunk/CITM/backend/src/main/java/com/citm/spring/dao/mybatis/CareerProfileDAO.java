package com.citm.spring.dao.mybatis;

import java.util.List;

import com.citm.spring.model.careerprofile.CareerProfile;
import com.citm.spring.model.careerprofile.HotJobs;

public interface CareerProfileDAO {

	public List<CareerProfile> selectCareerProfiles();
		
	public List<HotJobs> getHotJobTitle(String mcId);
	
}
