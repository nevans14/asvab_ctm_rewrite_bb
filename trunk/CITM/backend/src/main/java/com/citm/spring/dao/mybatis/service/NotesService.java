package com.citm.spring.dao.mybatis.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.citm.spring.dao.mybatis.mapper.NotesMapper;
import com.citm.spring.model.notes.CareerClusterNotes;
import com.citm.spring.model.notes.Notes;

@Service
public class NotesService {

	@Autowired
	NotesMapper mapper;

	public ArrayList<Notes> getNotes(Integer userId) {
		return mapper.getNotes(userId);
	}

	public int insertNote(Notes noteObject) {
		return mapper.insertNote(noteObject);
	}

	public int updateNote(Notes noteObject) {
		return mapper.updateNote(noteObject);
	}
	
	public int deleteNote(Integer userId, int id) {
		return mapper.deleteNote(userId, id);
	}

	public String getOccupationNotes(Integer userId, String socId) {
		return mapper.getOccupationNotes(userId, socId);
	}
	
	public ArrayList<CareerClusterNotes> getCareerClusterNotes(Integer userId) {
		return mapper.getCareerClusterNotes(userId);
	}

	public int insertCareerClusterNote(CareerClusterNotes noteObject) {
		return mapper.insertCareerClusterNote(noteObject);
	}

	public int updateCareerClusterNote(CareerClusterNotes noteObject) {
		return mapper.updateCareerClusterNote(noteObject);
	}
	
	public int deleteCareerClusterNote(Integer userId, Integer ccNoteId) {
		return mapper.deleteCareerClusterNote(userId, ccNoteId);
	}

	public String getCareerClusterNote(Integer userId, Integer ccId) {
		return mapper.getCareerClusterNote(userId, ccId);
	}

}
