package com.citm.spring.dao.mybatis.mapper;

import java.util.ArrayList;

import org.apache.ibatis.annotations.Param;

import com.citm.spring.model.option.Pay;
import com.citm.spring.model.option.PayChart;

public interface OptionMapper {
	
	ArrayList<Pay> getRank();

	ArrayList<Pay> getPay();
	
	ArrayList<PayChart> getPayChart();

}
