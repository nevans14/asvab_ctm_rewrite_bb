package com.citm.spring.dao.mybatis.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.citm.spring.dao.mybatis.mapper.TestScoreMapper;
import com.citm.spring.model.testscore.CompositeFavoritesFlags;
import com.citm.spring.model.testscore.ManualTestScore;
import com.citm.spring.model.testscore.StudentTestingProgam;
import com.citm.spring.model.testscore.StudentTestingProgamSelects;

@Service
public class TestScoreService {
	
	@Autowired
	TestScoreMapper mapper;
	
	public int insertTestScore(List<StudentTestingProgam> testScoreObject) {
		return mapper.insertTestScore(testScoreObject);
	}
	
	public String getAccessCode(Integer userId) {
		return mapper.getAccessCode(userId);
	}
	
	public StudentTestingProgamSelects getTestScore(Integer userId) {
		StudentTestingProgamSelects testScore = mapper.getTestScore(userId);
		if (testScore == null)
			testScore = mapper.getMockTestScoreWithUserId(userId);
		return testScore;
	}
	
	public StudentTestingProgamSelects getTestScore(String accessCode) {
		StudentTestingProgamSelects testScore = mapper.getTestScoreByAccessCode(accessCode);
		if (testScore == null)
			testScore = mapper.getMockTestScoreByAccessCode(accessCode);
		return testScore;
	}

	public int insertManualTestScore(ManualTestScore manualTestScoreObj) {
		return mapper.insertManualTestScore(manualTestScoreObj);
	}

	public ManualTestScore getManualTestScore(int userId) {
		return mapper.getManualTestScore(userId);
	}

	public int updateManualTestScore(ManualTestScore manualTestScoreObj) {
		return mapper.updateManualTestScore(manualTestScoreObj);
	}

	public int deleteManualTestScore(int userId) {
		return mapper.deleteManualTestScore(userId);
	}

	public int setCompositeFavoritesFlags(CompositeFavoritesFlags compositeFavoritesFlagsObj) {
		return mapper.setCompositeFavoritesFlags(compositeFavoritesFlagsObj);
	}

	public CompositeFavoritesFlags getCompositeFavoritesFlags(int userId) {
		return mapper.getCompositeFavoritesFlags(userId);
	}

	public int updateCompositeFavoritesFlags(CompositeFavoritesFlags compositeFavoritesFlagsObj) {
		return mapper.updateCompositeFavoritesFlags(compositeFavoritesFlagsObj);
	}

	public int deleteCompositeFavoritesFlags(int userId) {
		return mapper.deleteCompositeFavoritesFlags(userId);
	}
	
	public boolean compositeFavoritesFlagsExists(int userId) {
		return mapper.compositeFavoritesFlagsExists(userId);
	}
	
	public boolean emailExistsI(String userName) {
		return mapper.emailExists(userName);
	}
	
	public String latestTestDate(String accessCode) {
		return mapper.latestTestDate(accessCode);
	}
}
