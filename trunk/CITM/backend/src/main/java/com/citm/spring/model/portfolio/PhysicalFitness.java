package com.citm.spring.model.portfolio;

import java.io.Serializable;

public class PhysicalFitness implements Serializable {

	private static final long serialVersionUID = 7787849821103501651L;
	private Integer id, userId, pullUps;
	private float curlUps, vSitReach, shuttleRun;
	private String otherOne, otherTwo, otherThree, otherFour, mileRun;

	public String getMileRun() {
		return mileRun;
	}

	public void setMileRun(String mileRun) {
		this.mileRun = mileRun;
	}

	public float getShuttleRun() {
		return shuttleRun;
	}

	public void setShuttleRun(float shuttleRun) {
		this.shuttleRun = shuttleRun;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Integer getPullUps() {
		return pullUps;
	}

	public void setPullUps(Integer pullUps) {
		this.pullUps = pullUps;
	}

	public float getCurlUps() {
		return curlUps;
	}

	public void setCurlUps(float curlUps) {
		this.curlUps = curlUps;
	}

	public float getvSitReach() {
		return vSitReach;
	}

	public void setvSitReach(float vSitReach) {
		this.vSitReach = vSitReach;
	}

	public String getOtherOne() {
		return otherOne;
	}

	public void setOtherOne(String otherOne) {
		this.otherOne = otherOne;
	}

	public String getOtherTwo() {
		return otherTwo;
	}

	public void setOtherTwo(String otherTwo) {
		this.otherTwo = otherTwo;
	}

	public String getOtherThree() {
		return otherThree;
	}

	public void setOtherThree(String otherThree) {
		this.otherThree = otherThree;
	}

	public String getOtherFour() {
		return otherFour;
	}

	public void setOtherFour(String otherFour) {
		this.otherFour = otherFour;
	}
}
