package com.citm.spring.model.portfolio;

import java.io.Serializable;

public class UserInfo implements Serializable {

	private static final long serialVersionUID = 175665755724508717L;
	String name, schoolName, photo, fileName;
	Integer userId, id, graduationYear;

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSchoolName() {
		return schoolName;
	}

	public void setSchoolName(String schoolName) {
		this.schoolName = schoolName;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getGraduationYear() {
		return graduationYear;
	}

	public void setGraduationYear(Integer graduationYear) {
		this.graduationYear = graduationYear;
	}

	public String getFileName() {
		return fileName;
	}

	public void setfileName(String fileName) {
		this.fileName = fileName;
	}
	
	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

}
