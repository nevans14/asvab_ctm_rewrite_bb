package com.citm.spring.model.detail;

import java.io.Serializable;

public class airforceActivityMap implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5119159684712448105L;
	Integer activityId;
	String activityTitle, activityDefinition;

	public Integer getActivityId() {
		return activityId;
	}

	public void setActivityId(Integer activityId) {
		this.activityId = activityId;
	}

	public String getActivityTitle() {
		return activityTitle;
	}

	public void setActivityTitle(String activityTitle) {
		this.activityTitle = activityTitle;
	}

	public String getActivityDefinition() {
		return activityDefinition;
	}

	public void setActivityDefinition(String activityDefinition) {
		this.activityDefinition = activityDefinition;
	}
}
