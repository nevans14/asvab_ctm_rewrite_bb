package com.citm.spring.dao.mybatis.impl;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.citm.spring.model.detail.WhatTheyDo;
import com.citm.spring.model.detail.RelatedCareer;
import com.citm.spring.model.detail.SeeScoreArmy;
import com.citm.spring.model.detail.ServiceOffering;
import com.citm.spring.model.detail.SkillRating;
import com.citm.spring.model.detail.TrainingProvided;
import com.citm.spring.dao.mybatis.service.CareerDetailService;
import com.citm.spring.model.detail.AdditionalInfo;
import com.citm.spring.model.detail.AirForceDetail;
import com.citm.spring.model.detail.ArmyDetail;
import com.citm.spring.model.detail.CareerPath;
import com.citm.spring.model.detail.CoastGuardDetail;
import com.citm.spring.model.detail.HelpfulAttribute;
import com.citm.spring.model.detail.HiddenJob;
import com.citm.spring.model.detail.MarineDetail;
import com.citm.spring.model.detail.MilitaryCareer;
import com.citm.spring.model.detail.NavyDetail;
import com.citm.spring.model.detail.Profile;
import com.citm.spring.model.detail.RecentlyViewed;
import com.citm.spring.model.careerprofile.*;

@Repository
public class CareerDetailDAOImpl {

	@Autowired
	CareerDetailService service;

	public MilitaryCareer getMilitaryCareer(String mcId) throws Exception {
		return service.getMilitaryCareer(mcId);
	}

	public ArrayList<WhatTheyDo> getWhatTheyDo(String mcId) throws Exception {
		return service.getWhatTheyDo(mcId);
	}

	public ArrayList<TrainingProvided> getTrainingProvided(String mcId) throws Exception {
		return service.getTrainingProvided(mcId);
	}

	public ArrayList<RelatedCareer> getRelatedCareers(String mcId) throws Exception {
		return service.getRelatedCareers(mcId);
	}

	public ArrayList<RelatedCareer> getRelatedCivilianCareers(String mcId) throws Exception {
		return service.getRelatedCivilianCareers(mcId);
	}

	public ArrayList<AdditionalInfo> getAdditionalCareerInfo(String mcId) throws Exception {
		return service.getAdditionalCareerInfo(mcId);
	}

	public ArrayList<Profile> getProfiles(String mcId) throws Exception {
		return service.getProfiles(mcId);
	}

	public int getHotJob(String mcId) throws Exception {
		return service.getHotJob(mcId);
	}

	public ArrayList<HelpfulAttribute> getHelpfulAttribute(String mcId) throws Exception {
		return service.getHelpfulAttribute(mcId);
	}

	public ArrayList<ServiceOffering> getServiceOffering(String mcId) throws Exception {
		return service.getServiceOffering(mcId);
	}

	public ArrayList<ServiceOffering> getServiceOfferingCompositeScores() throws Exception {
		return service.getServiceOfferingCompositeScores();
	}

	public SkillRating getSkillRating(String mcId) throws Exception {
		return service.getSkillRating(mcId);
	}

	public String getHideJobList(int userId) throws Exception {
		return service.getHideJobList(userId);
	}

	public int setHideJobList(HiddenJob obj) throws Exception {
		return service.setHideJobList(obj);
	}

	public ArrayList<CareerPath> getCareerPath(String mcId) throws Exception {
		return service.getCareerPath(mcId);
	}

	public String getRecentlyViewed(int userId) throws Exception {
		return service.getRecentlyViewed(userId);
	}

	public int setRecentlyViewed(RecentlyViewed obj) throws Exception {
		return service.setRecentlyViewed(obj);
	}

	public ArrayList<String> getOtherTitles(String mcId) throws Exception {
		return service.getOtherTitles(mcId);
	}

	public AirForceDetail getAirForceDetail(String mcId, String mocId) throws Exception {
		return service.getAirForceDetail(mcId, mocId);
	}

	public ArmyDetail getArmyDetail(String mcId, String mocId) throws Exception {
		return service.getArmyDetail(mcId, mocId);
	}

	public CoastGuardDetail getCoastGuardDetail(String mcId, String mocId) throws Exception {
		return service.getCoastGuardDetail(mcId, mocId);
	}

	public MarineDetail getMarineDetail(String mcId, String mocId) throws Exception {
		return service.getMarineDetail(mcId, mocId);
	}

	public NavyDetail getNavyDetail(String mcId, String mocId) throws Exception {
		return service.getNavyDetail(mcId, mocId);
	}

	public ArrayList<SeeScoreArmy> getSeeScoreArmy(String mcId) throws Exception {
		return service.getSeeScoreArmy(mcId);
	}
	
	public ArrayList<CareerProfile> getFeaturedProfiles(String mcId) throws Exception {
		return service.getFeaturedProfiles(mcId);
	}
}
