package com.citm.spring.dao.mybatis.impl;

import java.util.ArrayList;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.citm.spring.dao.mybatis.service.FavoriteOccupationService;
import com.citm.spring.model.favorites.FavoriteCareerCluster;
import com.citm.spring.model.favorites.FavoriteOccupation;
import com.citm.spring.model.favorites.FavoriteService;

@Repository
public class FavoriteOccupationDAOImpl {
    static Logger logger = LogManager.getLogger("FavoriteOccupationDAOImpl");


	@Autowired
	FavoriteOccupationService service;

	public ArrayList<FavoriteOccupation> getFavoriteOccupation(Integer userId) throws Exception {
		ArrayList<FavoriteOccupation> results = new ArrayList<FavoriteOccupation>();

		results = service.getFavoriteOccupation(userId);

		return results;
	}

	public int insertFavoriteOccupation(FavoriteOccupation favoriteObject) throws Exception {
		int results = 0;

		results = service.insertFavoriteOccupation(favoriteObject);

		return results;
	}

	public int deleteFavoriteOccupation(Integer id) throws Exception {
		int results = 0;

		results = service.deleteFavoriteOccupation(id);

		return results;
	}
	
	public ArrayList<FavoriteService> getFavoriteService(Integer userId) throws Exception {
		return service.getFavoriteService(userId);
	}

	public int insertFavoriteService(FavoriteService serviceObject) throws Exception {
		return service.insertFavoriteService(serviceObject);
	}

	public int deleteFavoriteService(Integer id) throws Exception {
		return service.deleteFavoriteService(id);
	}
	
	
	public ArrayList<FavoriteCareerCluster> getFavoriteCareerCluster(Integer userId) throws Exception {
		ArrayList<FavoriteCareerCluster> results = new ArrayList<FavoriteCareerCluster>();

		results = service.getFavoriteCareerCluster(userId);

		return results;
	}

	public int insertFavoriteCareerCluster(FavoriteCareerCluster favoriteObject) throws Exception {
		int results = 0;

		results = service.insertFavoriteCareerCluster(favoriteObject);

		return results;
	}

	public int deleteFavoriteCareerCluster(Integer id) throws Exception {
		int results = 0;

		results = service.deleteFavoriteCareerCluster(id);

		return results;
	}

}
