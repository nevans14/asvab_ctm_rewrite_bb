package com.citm.spring.model.portfolio;

import java.io.Serializable;

public class ResourceGeneric implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5563177028172062662L;
	String title, link;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}
}
