package com.citm.spring.dao.mybatis.mapper;

import java.util.List;

import com.citm.spring.model.careerprofile.CareerProfile;
import com.citm.spring.model.careerprofile.HotJobs;

public interface CareerProfileMapper {
	
	List<CareerProfile> selectCareerProfiles();
	
	List<HotJobs> getHotJobTitle(String mcId);

}
