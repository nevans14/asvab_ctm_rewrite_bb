package com.citm.spring.model.portfolio;

import java.io.Serializable;

public class ActScore implements Serializable {

	private static final long serialVersionUID = -5829091338341044769L;
	private Integer id;
	private Integer userId;
	private double readingScore;
	private double mathScore;
	private double writingScore;
	private double scienceScore;
	private double englishScore;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public double getReadingScore() {
		return readingScore;
	}

	public void setReadingScore(double readingScore) {
		this.readingScore = readingScore;
	}

	public double getMathScore() {
		return mathScore;
	}

	public void setMathScore(double mathScore) {
		this.mathScore = mathScore;
	}

	public double getWritingScore() {
		return writingScore;
	}

	public void setWritingScore(double writingScore) {
		this.writingScore = writingScore;
	}

	public double getScienceScore() {
		return scienceScore;
	}

	public void setScienceScore(double scienceScore) {
		this.scienceScore = scienceScore;
	}

	public double getEnglishScore() {
		return englishScore;
	}

	public void setEnglishScore(double englishScore) {
		this.englishScore = englishScore;
	}
}
