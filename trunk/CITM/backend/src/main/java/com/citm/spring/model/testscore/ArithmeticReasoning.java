package com.citm.spring.model.testscore;

public class ArithmeticReasoning {

	private	int	AR_YP_Score;
	private	int	AR_GS_Percentage;
	private	int	AR_GOS_Percentage;
	private	int	AR_COMP_Percentage;
	private	int	AR_TP_Percentage;
	private	int	AR_SGS;
	private	int	AR_USL;
	private	int	AR_LSL;
	/**
	 * @return the aR_YP_Score
	 */
	public int getAR_YP_Score() {
		return AR_YP_Score;
	}
	/**
	 * @param aR_YP_Score the aR_YP_Score to set
	 */
	public void setAR_YP_Score(int aR_YP_Score) {
		AR_YP_Score = aR_YP_Score;
	}
	/**
	 * @return the aR_GS_Percentage
	 */
	public int getAR_GS_Percentage() {
		return AR_GS_Percentage;
	}
	/**
	 * @param aR_GS_Percentage the aR_GS_Percentage to set
	 */
	public void setAR_GS_Percentage(int aR_GS_Percentage) {
		AR_GS_Percentage = aR_GS_Percentage;
	}
	/**
	 * @return the aR_GOS_Percentage
	 */
	public int getAR_GOS_Percentage() {
		return AR_GOS_Percentage;
	}
	/**
	 * @param aR_GOS_Percentage the aR_GOS_Percentage to set
	 */
	public void setAR_GOS_Percentage(int aR_GOS_Percentage) {
		AR_GOS_Percentage = aR_GOS_Percentage;
	}
	/**
	 * @return the aR_COMP_Percentage
	 */
	public int getAR_COMP_Percentage() {
		return AR_COMP_Percentage;
	}
	/**
	 * @param aR_COMP_Percentage the aR_COMP_Percentage to set
	 */
	public void setAR_COMP_Percentage(int aR_COMP_Percentage) {
		AR_COMP_Percentage = aR_COMP_Percentage;
	}
	/**
	 * @return the aR_TP_Percentage
	 */
	public int getAR_TP_Percentage() {
		return AR_TP_Percentage;
	}
	/**
	 * @param aR_TP_Percentage the aR_TP_Percentage to set
	 */
	public void setAR_TP_Percentage(int aR_TP_Percentage) {
		AR_TP_Percentage = aR_TP_Percentage;
	}
	/**
	 * @return the aR_SGS
	 */
	public int getAR_SGS() {
		return AR_SGS;
	}
	/**
	 * @param aR_SGS the aR_SGS to set
	 */
	public void setAR_SGS(int aR_SGS) {
		AR_SGS = aR_SGS;
	}
	/**
	 * @return the aR_USL
	 */
	public int getAR_USL() {
		return AR_USL;
	}
	/**
	 * @param aR_USL the aR_USL to set
	 */
	public void setAR_USL(int aR_USL) {
		AR_USL = aR_USL;
	}
	/**
	 * @return the aR_LSL
	 */
	public int getAR_LSL() {
		return AR_LSL;
	}
	/**
	 * @param aR_LSL the aR_LSL to set
	 */
	public void setAR_LSL(int aR_LSL) {
		AR_LSL = aR_LSL;
	}
	
}
