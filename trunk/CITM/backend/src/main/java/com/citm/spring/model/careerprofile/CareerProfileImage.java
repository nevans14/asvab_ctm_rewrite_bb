package com.citm.spring.model.careerprofile;

public class CareerProfileImage {
	
	private int profileId;
	
	private int imageId;
		
	private String imageName;
	
	private String imageAltText;
	
	private String imageData;

	public int getProfileId() {
		return profileId;
	}

	public void setProfileId(int profileId) {
		this.profileId = profileId;
	}

	/**
	 * @return the imageId
	 */
	public int getImageId() {
		return imageId;
	}

	/**
	 * @param imageId the imageId to set
	 */
	public void setImageId(int imageId) {
		this.imageId = imageId;
	}

	/**
	 * @return the imageName
	 */
	public String getImageName() {
		return imageName;
	}

	/**
	 * @param imageName the imageName to set
	 */
	public void setImageName(String imageName) {
		this.imageName = imageName;
	}

	/**
	 * @return the imageAltText
	 */
	public String getImageAltText() {
		return imageAltText;
	}

	/**
	 * @param imageAltText the imageAltText to set
	 */
	public void setImageAltText(String imageAltText) {
		this.imageAltText = imageAltText;
	}

	/**
	 * @return the imageData
	 */
	public String getImageData() {
		return imageData;
	}

	/**
	 * @param imageData the imageData to set
	 */
	public void setImageData(String imageData) {
		this.imageData = imageData;
	}
}
