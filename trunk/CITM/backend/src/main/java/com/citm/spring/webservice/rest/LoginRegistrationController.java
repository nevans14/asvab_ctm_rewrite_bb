package com.citm.spring.webservice.rest; 

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.MethodParameter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;

import com.citm.spring.CEPMailer;
import com.citm.spring.dao.jdbctemplate.GenericRowMapper;
import com.citm.spring.dao.mybatis.DbInfoDAO;
import com.citm.spring.dao.mybatis.RegistrationDAO;
import com.citm.spring.model.DbInfo;
import com.citm.spring.model.login.AccessCode;
import com.citm.spring.model.login.MEPS;
import com.citm.spring.model.login.Registration;
import com.citm.spring.model.login.UserRole;
import com.citm.spring.model.login.Users;
import com.citm.spring.utils.GenerateAccessCodes;
import com.citm.spring.utils.SimpleUtils;
import java.security.Principal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;


/**
 * Controller that allows for application login and user registration.
 * 
 * This controller has the following end points
 * <ul>
 *   <li>rest/applicationAccess/loginAccessCode</li>
 *   <li>rest/applicationAccess/user</li>
 *   <li>rest/applicationAccess/loginEmail</li>
 *   <li>rest/applicationAccess/register</li>
 *   <li>rest/applicationAccess/passwordResetRequest</li>
 *   <li>rest/applicationAccess/passwordReset</li>
 * </ul>
 * 
 * @author Dave Springer
 *
 */
@Controller
@RequestMapping("/applicationAccess")
@Transactional(propagation=Propagation.REQUIRED) 
public class LoginRegistrationController 
    implements HandlerMethodArgumentResolver 
    { 
	private Logger logger = LogManager.getLogger( this.getClass().getName() );

	@Autowired 
	JdbcTemplate jdbcTemplate;
	@Autowired
	CEPMailer cepMailer;
	@Autowired
	static SecurityContextHolder securityContextHolder;
	@Autowired
	private RegistrationDAO dao;
	@Autowired
	private DbInfoDAO dbInfoDao;
	@Autowired
	BCryptPasswordEncoder encoder;
	
	@Deprecated   // CITM user not allowed to login with access code.
	@RequestMapping(value = "/loginAccessCode", method = RequestMethod.POST)
	public @ResponseBody RegistrationLoginStatus  
				loginAccessCode(@RequestBody Registration registration, HttpServletRequest request) {

		logger.info("loginAccessCode->" );
		logger.info("Access Code:" + registration.getAccessCode() );
		logger.info("Email      :" + registration.getEmail()  );
		logger.info("Password   :" + registration.getPassword()  );
		logger.info("ResetKey   :" + registration.getResetKey()  );
		
		String sql = "SELECT * " +
						" FROM  [ASVAB_CEP_2019].[dbo].access_codes " +
						" WHERE access_code = ?";

		List<Map<String, String>> accessCodeList =  jdbcTemplate.query(sql, new GenericRowMapper(), registration.getAccessCode() );

		logger.info("jdbcTemplate Query executed." );

		RegistrationLoginStatus rls =  new RegistrationLoginStatus();
		if (accessCodeList.size() == 1 ) {
			logger.info("found an access code" );
			// Is expire date null if so set it ....
			if ( accessCodeList.get(0).get("expire_date") == null ) { 
			    String user_id = accessCodeList.get(0).get("user_id");
			    DateFormat dFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);   
			    String updateAccessCode = "UPDATE  [ASVAB_CEP_2019].[dbo].access_codes SET expire_date = '" + dFormat.format(SimpleUtils.datePlusDays(new Date(), 21)) + " 00:00:00.000' WHERE user_id = ?"; 
			    jdbcTemplate.update(updateAccessCode, user_id);   
		    } else {
                // If expire date is not null then determine expired.
    			Date expireDate = SimpleUtils.getDateFromStringInStandardFormat(accessCodeList.get(0).get("expire_date"));
                if ( 0 > SimpleUtils.dateCompare(new Date(), expireDate) ) { 
                    logger.info("Access Code is good!");
                } else {
                    logger.error("Access code has expired:" + registration.getAccessCode() );
                    rls.setStatusNumber(1021); // Error
                    rls.setStatus("Error:Access code has expired:"); 
                    return rls;
                }
            }
	
			
			// set spring security context for sso
			List<GrantedAuthority> authorities = createAuthorityList("ROLE_USER");
			UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(
					registration.getEmail(), registration.getPassword(), authorities);
			SecurityContextHolder.getContext().setAuthentication(authenticationToken);

			// Create a new session and add the security context.
			HttpSession session = request.getSession(true);
			session.setAttribute("SPRING_SECURITY_CONTEXT_KEY", SecurityContextHolder.getContext());
			session.setAttribute("ASVAB_ROLE", accessCodeList.get(0).get("role"));
			session.setAttribute("ASVAB_USER_ID", accessCodeList.get(0).get("user_id"));
			
			 
			// Log 
			String logSql = "INSERT INTO  [ASVAB_CEP_2019].[dbo].[asvab_log] ([user_id],[login_date]) VALUES ( " +
					accessCodeList.get(0).get("user_id") + ",'" + SimpleUtils.getStringInStandardFormatFromDate(new Date() ) + "')";
			logger.info(logSql);
			jdbcTemplate.execute(logSql);
			
			rls.setStatusNumber(0); // Success 
			rls.setStatus("Success"); 
			rls.setRole(accessCodeList.get(0).get("role"));
			rls.setUser_id(accessCodeList.get(0).get("user_id"));
		} else {
			logger.info("Access code not found:" + registration.getAccessCode() );
			// Determine if this is a valid Student code
			
			if ( isThisAValidStudentId(registration.getAccessCode()) ) { // insert to db
			    Integer schoolYear = GenerateAccessCodes.schoolYearOfSuffix(registration.getAccessCode().substring(registration.getAccessCode().length()-1));
			    schoolYear = schoolYear + 2;
			    String expireDate = schoolYear.toString().trim() + "-06-30 00:00:00.000";
			    String insertStudent = "INSERT INTO  [ASVAB_CEP_2019].[dbo].access_codes (access_code, expire_date, role) VALUES ('" + 
			                registration.getAccessCode().trim() + "', '" + expireDate + "','S')";
			     
			    jdbcTemplate.execute(insertStudent);
			    
			    // if we got this far log the student in ...
			    
		         // set spring security context ....
	            List <GrantedAuthority> authorities = createAuthorityList("ROLE_USER"); 
	             UsernamePasswordAuthenticationToken authenticationToken = 
	                     new UsernamePasswordAuthenticationToken(registration.getEmail() , registration.getPassword() , authorities );
	            // authenticationToken.isAuthenticated();
	             SecurityContextHolder.getContext().setAuthentication(authenticationToken);
	             
				// Create a new session and add the security context.
				HttpSession session = request.getSession(true);
				session.setAttribute("SPRING_SECURITY_CONTEXT_KEY", SecurityContextHolder.getContext());
                session.setAttribute("ASVAB_ROLE", "ROLE_USER" );
//				session.setAttribute("ASVAB_ROLE", accessCodeList.get(0).get("role"));
//				session.setAttribute("ASVAB_USER_ID", accessCodeList.get(0).get("user_id"));
	            
	             String user_id = jdbcTemplate.queryForObject("select user_id from  [ASVAB_CEP_2019].[dbo].access_codes where access_code = '" + registration.getAccessCode().trim() + "'"  , String.class);
	             
	            // Log 
	            String logSql = "INSERT INTO  [ASVAB_CEP_2019].[dbo].[asvab_log] ([user_id],[login_date]) VALUES ( " +
	                    user_id + ",'" + SimpleUtils.getStringInStandardFormatFromDate(new Date() ) + "')";
	            logger.info(logSql);
	            jdbcTemplate.execute(logSql);
	            
	            rls.setStatusNumber(0); // Success 
	            rls.setStatus("Success"); 
	            rls.setRole("S");
	            rls.setUser_id(user_id.toString());
	            return rls ;
			} 
		    
		    logger.error("Access code not found:" + registration.getAccessCode() );

		    rls.setStatusNumber(1001); // Error
			rls.setStatus("Error:Access Code Not Found"); 
		}
		
		return rls ;
		
	}

	@RequestMapping("/user")
	public @ResponseBody Principal user(Principal user) {
		logger.info("User url was hit!");
		return user;
	}
	
	@RequestMapping(value = "/loginEmail", method = RequestMethod.POST)
	public @ResponseBody RegistrationLoginStatus  
				loginEmail(@RequestBody Registration registration, HttpServletRequest request) {

		//   REST debug statements data from client ...
		logger.info("loginEmail->" );
		logger.info("Access Code:" + registration.getAccessCode());
		logger.info("Email      :" + registration.getEmail());
		logger.info("Password   :" + registration.getPassword());
		logger.info("ResetKey   :" + registration.getResetKey() );

		RegistrationLoginStatus rls =  new RegistrationLoginStatus();
		Users user = dao.getUser(registration.getEmail());
		if (user == null) {
			// user does not exist
			rls.setStatusNumber(1002); // Error
			rls.setStatus("Username or password does not match.");
			return rls;
		} else if (user.getAccessCode() == null) { 
			// incomplete profile
			rls.setStatusNumber(1023); // Error
            rls.setStatus("Registration incomplete, please register this email again.");
			return rls;
		} else if (user.getAccessCode() != null && 0 < SimpleUtils.dateCompare(new Date(), user.getAccessCode().getExpireDate())) {
			// access code has expired
			rls.setStatusNumber(1021);
			rls.setStatus("Account expired");
			return rls;
		}		
		// check to see if user is enabled
		if (user.getEnabled() == 0) {
			// user is locked
			rls.setStatusNumber(1010); // Error
			rls.setStatus("User account has been locked out!"); 		
			return rls;
		}
		// check password
		if (encoder.matches(registration.getPassword(), user.getPassword())) {
			// set status #
			if (user.getSystem().isEmpty()) {
				rls.setStatusNumber(2323); // Success 
                rls.setStatus("Unknown error!"); 
                return rls;
			} else if (user.getSystem().trim().equals("CEP-CITM")) {
				rls.setStatusNumber(0);
			} else if (user.getSystem().trim().equals("CITM")) {
				rls.setStatusNumber(1);
			}
			rls.setStatus("success");
			rls.setUser_id(String.valueOf(user.getUserId()));
			rls.setRole(user.getAccessCode().getRole());
			rls.setHash(user.getPassword());			
			// check user roles
			if (user.getUserRoles().size() == 0) {
				// not assigned roles, throw error
				rls.setStatusNumber(1009); // Error
				rls.setStatus("A role was not assigned. Contact support."); 	
				return rls;
			}
			setSessionInfo(createAuthorityList(user.getUserRoles()), registration.getEmail(), registration.getPassword(), registration.getAccessCode(), user.getAccessCode().getRole(), request, user.getAccessCode().getRole(), user.getUserId());
			// get MEPS
			MEPS meps = dao.getByAccessCode(user.getAccessCode().getAccessCode());
			// set MEPS
			setMEPSInfo(meps, rls);
			// insert log
			dao.insertAsvabLog(user.getUserId(), new Date());			
		} else {
			rls.setStatusNumber(1008); // Error
			rls.setStatus("Username or password does not match.");
		}		
		return rls;		
	}
	
	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public @ResponseBody RegistrationLoginStatus register(@RequestBody Registration registration, HttpServletRequest request) {
		RegistrationLoginStatus rls =  new RegistrationLoginStatus();
		logger.info("register->" );
		logger.info("Access Code:" + registration.getAccessCode() );
		logger.info("Email      :" + registration.getEmail() );
		logger.info("Password   :" + registration.getPassword() );
		logger.info("ResetKey   :" + registration.getResetKey() );		
		// get last used access code and generate a new access code
		AccessCode lastAccessCodeUsed = dao.getLastUsedAccessCode();
		String generatedAccessCode = GenerateAccessCodes.generateCitmCode(lastAccessCodeUsed);		
		// create a new access code
		AccessCode newCode = GenerateAccessCodes.createAccessCode(generatedAccessCode, "S", 0);
		// now check if the user exists
		Users user = dao.getUser(registration.getEmail());
		if (user != null && user.getUserId() > 0) {
			// username is already registered
			rls.setStatusNumber(1003);
			rls.setStatus("This username is already taken.");
			return rls;
		} 
		// insert access code
		dao.insertAccessCode(newCode);
		// get access code
		newCode = dao.getAccessCode(newCode.getAccessCode());		
		if (user == null) {
			// new registration, add new user account
			user = new Users(newCode.getUserId(), registration.getEmail(), registration.getPassword(), Short.valueOf("1"), "CITM");
			dao.insertUser(user);			
		} else {
			// update user ID and password
			user.setUserId(newCode.getUserId());
			user.setPassword(registration.getPassword());
			dao.updateUser(user);			
		}
		// get roles from db
		List<UserRole> userRoles = dao.getRoles(user.getUsername());
		if (userRoles.isEmpty()) {
			// insert default roles
			UserRole role = new UserRole(registration.getEmail(), "ROLE_USER");
			dao.insertRole(role);
			// add default role to userRoles list
			userRoles.add(role);
		}
		// create session
		setSessionInfo(createAuthorityList(userRoles), registration.getEmail(), registration.getPassword(), newCode.getAccessCode(), newCode.getRole(), request, newCode.getRole(), user.getUserId());
		/***
		 * NOTE: Because the user is registering through CTM, there is no access code and therefore cannot access test scores to get MEPS information
		 ***/
		// log the user
		dao.insertAsvabLog(user.getUserId(), new Date());
		// send email
		String userEmail = registration.getEmail();
		DbInfo websiteUrlInfo = dbInfoDao.getDbInfo("citm_website_url");
		DbInfo messageInfo = dbInfoDao.getDbInfo("citm_registration_email");
        String websiteUrl = websiteUrlInfo.getValue();
        // compose Registration email
        String htmlEmailMessage = messageInfo.getValue2();
	    htmlEmailMessage = htmlEmailMessage.replaceAll("#WEBSITE_URL#", websiteUrl);   
	    htmlEmailMessage = htmlEmailMessage.replaceAll("#NEW_LINE#", "\n");
	    // try to send registration email to new user
	    try {
	        cepMailer.sendResetRegisterHtmlEmail(userEmail, "" , htmlEmailMessage, "Welcome to Careers in the Military");
	    } catch (Exception e) {
	        // log and move on ....  This always fails locally 
	        e.printStackTrace();
	    }	
	    // set up response
		rls.setUser_id(String.valueOf(user.getUserId()));
		rls.setRole(newCode.getRole());
		rls.setStatusNumber(0);
		rls.setStatus("success");
		return rls;
	}
	
	@Transactional
	@RequestMapping(value = "/passwordResetRequest", method = RequestMethod.POST)
	public @ResponseBody RegistrationLoginStatus  
			passwordResetRequest(@RequestBody Registration registration) {
		RegistrationLoginStatus rls =  new RegistrationLoginStatus();
		
		logger.info("passwordResetRequest->" );
		logger.info("Access Code:" + registration.getAccessCode() );
		logger.info("Email      :" + registration.getEmail() );
		logger.info("Password   :" + registration.getPassword() );
		logger.info("ResetKey   :" + registration.getResetKey() );
		
		Users user = dao.getUser(registration.getEmail());
		// check if the email has been register
		if (user == null) {
			rls.statusNumber = 1005;
			rls.status = "Email not registered";
			return rls;
		}		
		// check if registration is complete
		if (user.getUserId() == 0) {
			// incomplete profile
			rls.setStatusNumber(1023); // Error
            rls.setStatus("Registration incomplete, please register this email again.");
			return rls;
		}		
		// set the user to have a new UUID reset key and expire date
		user.setResetKey(UUID.randomUUID().toString());
		user.setResetExpireDate(SimpleUtils.datePlusDays(new Date(), 2));
		user.setEnabled(Short.parseShort("0"));
		// update user
		dao.updateUser(user);
		// compose email
		DbInfo resetUrlInfo = dbInfoDao.getDbInfo("citm_password_reset_url");
		DbInfo messageInfo = dbInfoDao.getDbInfo("citm_password_reset_email_msg");
		String resetUrl = resetUrlInfo.getValue();	
		resetUrl = resetUrl + user.getResetKey() + "+username=" + user.getUsername();		
		// Compose reset email
		String htmlEmailMessage = messageInfo.getValue2();		
		htmlEmailMessage = htmlEmailMessage.replace("#resetUrl#", resetUrl);
		htmlEmailMessage = htmlEmailMessage.replace("#userEmail#", user.getUsername());      
		htmlEmailMessage = htmlEmailMessage.replaceAll("#NEW_LINE#", "\n");		
		try {
		    cepMailer.sendResetRegisterHtmlEmail(user.getUsername(), "" , htmlEmailMessage, "Password Reset ..." );
		} catch (Exception e) {
		    // TODO Auto-generated catch block
		    e.printStackTrace();
		}	
		rls.statusNumber = 0;
		rls.status = "Email sent to complete password reset.";
		return rls;
	}

	@RequestMapping(value = "/passwordReset", method = RequestMethod.POST)
	public @ResponseBody RegistrationLoginStatus  
			passwordReset(@RequestBody Registration registration) {
		
		RegistrationLoginStatus rls =new RegistrationLoginStatus();

		logger.info("passwordReset->" );
		logger.info("Access Code:" + registration.getAccessCode() );
		logger.info("Email      :" + registration.getEmail() );
		logger.info("Password   :" + registration.getPassword() );
		logger.info("ResetKey   :" + registration.getResetKey() );
		// get user by reset key
		Users user = dao.getUserByResetKey(registration.getResetKey());
		// was reset key found?
		if (user == null) {
			rls.setStatusNumber(1006);
			rls.setStatus("A reset request has not been made.");
			return rls;	
		}
		// is reset key still valid?
		if (0 <= SimpleUtils.dateCompare(new Date(), user.getResetExpireDate())) {
			rls.setStatusNumber(1007);
			rls.setStatus("Reset request has expired!");
			return rls;	
		}
		// update user
		user.setEnabled(Short.parseShort("1"));
		user.setPassword(registration.getPassword());
		user.setResetKey(null);
		user.setResetExpireDate(null);
		dao.updateUser(user);
		// return response
		rls.setStatusNumber(0);
		rls.setStatus("Success");
		return rls;		
	}
	
	/**
	 * Inner class for return payload.
	 * 
	 * 
	 * @author Dave Springer
	 *
	 */
	public class RegistrationLoginStatus {
		
		private String 	status = "";
		private Integer statusNumber = 0;
		private String 	color = "";
		private String  role = "";
		private String  user_id = "";
		private String  hash = "";
		private String schoolCode, schoolName, city, state, zipCode, mepsId, mepsName;
		
		public String getStatus() {
			return status;
		}
		public void setStatus(String status) {
			this.status = status;
		}
		public Integer getStatusNumber() {
			return statusNumber;
		}
		public void setStatusNumber(Integer statusNumber) {
			this.statusNumber = statusNumber;
		}
		public String getColor() {
			return color;
		}
		public void setColor(String color) {
			this.color = color;
		}
		public String getRole() {
			return role;
		}
		public void setRole(String role) {
			this.role = role;
		}
		public String getUser_id() {
			return user_id;
		}
		public void setUser_id(String user_id) {
			this.user_id = user_id;
		}
		public String getHash() {
			return hash;
		}
		public void setHash(String hash) {
			this.hash = hash;
		}
		
		/**
		 * @return the schoolCode
		 */
		public String getSchoolCode() {
			return schoolCode;
		}
		/**
		 * @param schoolCode the schoolCode to set
		 */
		public void setSchoolCode(String schoolCode) {
			this.schoolCode = schoolCode;
		}
		/**
		 * @return the schoolName
		 */
		public String getSchoolName() {
			return schoolName;
		}
		/**
		 * @param schoolName the schoolName to set
		 */
		public void setSchoolName(String schoolName) {
			this.schoolName = schoolName;
		}
		/**
		 * @return the city
		 */
		public String getCity() {
			return city;
		}
		/**
		 * @param city the city to set
		 */
		public void setCity(String city) {
			this.city = city;
		}
		/**
		 * @return the state
		 */
		public String getState() {
			return state;
		}
		/**
		 * @param state the state to set
		 */
		public void setState(String state) {
			this.state = state;
		}
		/**
		 * @return the zipCode
		 */
		public String getZipCode() {
			return zipCode;
		}
		/**
		 * @param zipCode the zipCode to set
		 */
		public void setZipCode(String zipCode) {
			this.zipCode = zipCode;
		}
		/**
		 * @return the mepsId
		 */
		public String getMepsId() {
			return mepsId;
		}
		/**
		 * @param mepsId the mepsId to set
		 */
		public void setMepsId(String mepsId) {
			this.mepsId = mepsId;
		}
		/**
		 * @return the mepsName
		 */
		public String getMepsName() {
			return mepsName;
		}
		/**
		 * @param mepsName the mepsName to set
		 */
		public void setMepsName(String mepsName) {
			this.mepsName = mepsName;
		}
		
	}

	/**
	 *  Needed to implement HandlerMethodArgumentResolver interface.
	 */
	@Override
	public boolean supportsParameter(MethodParameter parameter) {
		// Implementation not needed for this use case.
		return false;
	}

	/**
	 *  Needed to implement HandlerMethodArgumentResolver interface.
	 */
	@Override
	public Object resolveArgument(MethodParameter parameter, ModelAndViewContainer mavContainer,
			NativeWebRequest webRequest, WebDataBinderFactory binderFactory) throws Exception {
		// Implementation not needed for this use case.
		return null;
	}
 
	/**
	 * Create authority list with one role.
	 * @param role
	 * @return
	 */
	private  List <GrantedAuthority>  createAuthorityList(String role) {
		 return AuthorityUtils.createAuthorityList(role);
	}

	/**
	 * Return the appropriate security role
	 * @param role
	 * @return
	 */
	private String getSecurityRole(String role) {
		if (role.toUpperCase().equals("A")) {
			return "ROLE_ADMIN";
		}
		return "ROLE_USER";
	}
	
	/**
	 * Creates an authority list based on the roles of the user
	 * @param userRoles
	 * @return
	 */
	private List<GrantedAuthority> createAuthorityList(List<UserRole> userRoles) {
		List<GrantedAuthority> authorities = new ArrayList<>();
		for (UserRole role : userRoles) {
			authorities.add(new SimpleGrantedAuthority(role.getRole()));
		}
		return authorities;
	}
	
	/**
	 * Creates a new Spring Session
	 * @param authorities
	 * @param email
	 * @param password
	 * @param accessCode
	 * @param role
	 * @param request
	 * @param userRole
	 * @param userId
	 */
	private void setSessionInfo(List<GrantedAuthority> authorities, String email, String password, String accessCode, String role, HttpServletRequest request, String userRole, int userId) {
		UsernamePasswordAuthenticationToken authenticationToken = 
				 new UsernamePasswordAuthenticationToken(
						 email, 
						 password, 
						 authorities);			
		SecurityContextHolder.getContext().setAuthentication(authenticationToken);
		// Create a new session and add the security context.
		HttpSession session = request.getSession(true);
		session.setAttribute("SPRING_SECURITY_CONTEXT_KEY", SecurityContextHolder.getContext());
		session.setAttribute("USER_ROLE", role);
		session.setAttribute("USER_ACCESS_CODE", accessCode);
		session.setAttribute("ASVAB_ROLE", getSecurityRole(userRole));
		session.setAttribute("ASVAB_USER_ID", String.valueOf(userId));
	}

	private boolean isThisAValidStudentId(String accessCode) {
	    Integer stringLength = accessCode.length();
	    String suffix = accessCode.substring( accessCode.length() - 1) ;
	    if ( stringLength != 10) return false;
	    
        Integer schoolYear = GenerateAccessCodes.schoolYearOfSuffix(suffix);
	    if ( schoolYear != null ) return true;
	    

	    return false;
	}
	
	private void setMEPSInfo(MEPS meps, RegistrationLoginStatus rls) {
		if (meps != null) {
			rls.setMepsId(meps.getMepsId());
			rls.setMepsName(meps.getName());
			rls.setSchoolCode(meps.getSchoolCode());
			rls.setSchoolName(meps.getSchoolName());
			rls.setCity(meps.getCity());
			rls.setState(meps.getState());
			rls.setZipCode(meps.getZip());
		}
	}
	
}