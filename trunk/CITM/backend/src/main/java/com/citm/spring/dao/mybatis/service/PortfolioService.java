package com.citm.spring.dao.mybatis.service;

import java.util.ArrayList;

import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.citm.spring.model.portfolio.FavoriteOccupation;
import com.citm.spring.dao.mybatis.mapper.PortfolioMapper;
import com.citm.spring.model.portfolio.Achievement;
import com.citm.spring.model.portfolio.ActScore;
import com.citm.spring.model.portfolio.AsvabScore;
import com.citm.spring.model.portfolio.Education;
import com.citm.spring.model.portfolio.FyiScore;
import com.citm.spring.model.portfolio.OtherScore;
import com.citm.spring.model.portfolio.PhysicalFitness;
import com.citm.spring.model.portfolio.RelatedCareer;
import com.citm.spring.model.portfolio.ResourceGeneric;
import com.citm.spring.model.portfolio.ResourceSpecific;
import com.citm.spring.model.portfolio.SatScore;
import com.citm.spring.model.portfolio.UserInfo;
import com.citm.spring.model.portfolio.WorkExperience;

@Service
public class PortfolioService {

	@Autowired
	public PortfolioMapper mapper;

	public int isPortfolioStarted(Integer userId) {
		return mapper.isPortfolioStarted(userId);
	}

	public ArrayList<WorkExperience> getWorkExperience(Integer userId) {
		return mapper.getWorkExperience(userId);
	}

	public int insertWorkExperience(WorkExperience workExperienceObject) {
		return mapper.insertWorkExperience(workExperienceObject);
	}

	public int deleteWorkExperience(Integer userId, Integer id) {
		return mapper.deleteWorkExperience(userId, id);
	}

	public int updateWorkExperience(WorkExperience workExperienceObject) {
		return mapper.updateWorkExperience(workExperienceObject);
	}

	public ArrayList<Education> getEducation(Integer userId) {
		return mapper.getEducation(userId);
	}

	public int insertEducation(Education educationObject) {
		return mapper.insertEducation(educationObject);
	}

	public int deleteEducation(Integer userId, Integer id) {
		return mapper.deleteEducation(userId, id);
	}

	public int updateEducation(Education educationObject) {
		return mapper.updateEducation(educationObject);
	}

	public ArrayList<Achievement> getAchievements(Integer userId) {
		return mapper.getAchievements(userId);
	}

	public int insertAchievement(Achievement achievementObject) {
		return mapper.insertAchievement(achievementObject);
	}

	public int deleteAchievement(Integer userId, Integer id) {
		return mapper.deleteAchievement(userId, id);
	}

	public int updateAchievement(Achievement achievementObject) {
		return mapper.updateAchievement(achievementObject);
	}

	public ArrayList<AsvabScore> getAsvabScore(Integer userId) {
		return mapper.getAsvabScore(userId);
	}

	public int insertAsvabScore(AsvabScore asvabObject) {
		return mapper.insertAsvabScore(asvabObject);
	}

	public int deleteAsvabScore(Integer userId, Integer id) {
		return mapper.deleteAsvabScore(userId, id);
	}

	public int updateAsvabScore(AsvabScore asvabObject) {
		return mapper.updateAsvabScore(asvabObject);
	}

	public ArrayList<ActScore> getActScore(Integer userId) {
		return mapper.getActScore(userId);
	}

	public int insertActScore(ActScore actObject) {
		return mapper.insertActScore(actObject);
	}

	public int deleteActScore(Integer userId, Integer id) {
		return mapper.deleteActScore(userId, id);
	}

	public int updateActScore(ActScore actObject) {
		return mapper.updateActScore(actObject);
	}

	public ArrayList<SatScore> getSatScore(Integer userId) {
		return mapper.getSatScore(userId);
	}

	public int insertSatScore(SatScore satObject) {
		return mapper.insertSatScore(satObject);
	}

	public int deleteSatScore(Integer userId, Integer id) {
		return mapper.deleteSatScore(userId, id);
	}

	public int updateSatScore(SatScore satObject) {
		return mapper.updateSatScore(satObject);
	}

	public ArrayList<FyiScore> getFyiScore(Integer userId) {
		return mapper.getFyiScore(userId);
	}

	public int insertFyiScore(FyiScore fyiObject) {
		return mapper.insertFyiScore(fyiObject);
	}

	public int deleteFyiScore(Integer userId, Integer id) {
		return mapper.deleteFyiScore(userId, id);
	}

	public int updateFyiScore(FyiScore fyiObject) {
		return mapper.updateFyiScore(fyiObject);
	}

	public ArrayList<OtherScore> getOtherScore(Integer userId) {
		return mapper.getOtherScore(userId);
	}

	public int insertOtherScore(OtherScore otherObject) {
		return mapper.insertOtherScore(otherObject);
	}

	public int deleteOtherScore(Integer userId, Integer id) {
		return mapper.deleteOtherScore(userId, id);
	}

	public int updateOtherScore(OtherScore otherObject) {
		return mapper.updateOtherScore(otherObject);
	}
	
	public int updateUserInfo(UserInfo userInfo) {
		return mapper.updateUserInfo(userInfo);
	}

	public ArrayList<UserInfo> getUserInfo(Integer userId) {
		return mapper.getUserInfo(userId);
	}
	
	public ArrayList<PhysicalFitness> getPhysicalFitness(Integer userId) {
		return mapper.getPhysicalFitness(userId);
	}

	public int insertPhysicalFitness(PhysicalFitness physicalFitnessObject) {
		return mapper.insertPhysicalFitness(physicalFitnessObject);
	}

	public int deletePhysicalFitness(Integer userId, Integer id) {
		return mapper.deletePhysicalFitness(userId, id);
	}

	public int updatePhysicalFitness(PhysicalFitness physicalFitnessObject) {
		return mapper.updatePhysicalFitness(physicalFitnessObject);
	}
	
	public ArrayList<FavoriteOccupation> getFavoriteOccupation(Integer userId) {
		return mapper.getFavoriteOccupation(userId);
	}
	
	public ArrayList<RelatedCareer> getRelatedCareerList(Integer userId) {
		return mapper.getRelatedCareerList(userId);
	}
	
	public ArrayList<ResourceSpecific> getResourceSpecific() {
		return mapper.getResourceSpecific();
	}

	public ArrayList<ResourceGeneric> getResourceGeneric() {
		return mapper.getResourceGeneric();
	}
	
	public String getUserSystem(@Param("userId") Integer userId) {
		return mapper.getUserSystem(userId);
	}

	/*public ArrayList<UserInfo> selectNameGrade(Integer userId) {
		return mapper.selectNameGrade(userId);
	}

	public int updateInsertNameGrade(UserInfo userInfo) {
		return mapper.updateInsertNameGrade(userInfo);
	}*/

}
