package com.citm.spring.utils;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import com.citm.spring.dao.mybatis.impl.CareerDetailDAOImpl;
import com.citm.spring.model.detail.MilitaryCareer;

public class CrawlersRedirect extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1328977028542486L;

	static Logger logger = LogManager.getLogger("CrawlersRedirect");
	
	@Autowired
	private CareerDetailDAOImpl careerDAO;

	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		SpringBeanAutowiringSupport.processInjectionBasedOnServletContext(this, config.getServletContext());
	}

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


		String userAgent = request.getHeader("user-agent");

		// Full URL.
		String url = request.getRequestURL().toString();

		if (!StringUtils.isEmpty(userAgent)) {
			
			// Google adwords.
			if (userAgent.toLowerCase().contains("adsbot") || userAgent.toLowerCase().contains("mediapartners")) {
				response.sendError(307);
			}

			// Checks to see if the user is a crawler.
			if (userAgent.toLowerCase().contains("facebookexternalhit")
					|| userAgent.toLowerCase().contains("twitterbot")
					|| userAgent.toLowerCase().contains("linkedinbot")) {

				logger.debug(userAgent + ": This is a bot");

				// Crawler is trying to access Occufind details page.
				if (request.getRequestURI().toLowerCase().contains("career-detail")) {

					// Get the SOC ID from the URL.
					String mcId = url.replaceAll(".*/(\\d+.\\d+).*", "$1");
					MilitaryCareer occupation = new MilitaryCareer();

					try {
						occupation = careerDAO.getMilitaryCareer(mcId);
					} catch (Exception e) {
						logger.error(e);
					}

					// Create and serve HTML page to crawler with all meta tags.
					makeOccufindBotPage(request, response, occupation, url, mcId);
				}

			} else {
				response.sendError(308);
				//response.sendError(HttpServletResponse.SC_TEMPORARY_REDIRECT);
				//throw new ServletException("Not Crawler.");
			}

		} else {
			response.sendError(308);
			//response.sendError(HttpServletResponse.SC_TEMPORARY_REDIRECT);
			//throw new ServletException("Not Crawler.");
		}

	}


	/**
	 * Creates and returns crawler friendly HTML page for occufind sharing.
	 * 
	 * @param request
	 * @param response
	 * @param occupation
	 * @param url
	 * @param mcId
	 * @throws IOException
	 */
	private void makeOccufindBotPage(HttpServletRequest request, HttpServletResponse response,
			MilitaryCareer career, String url, String mcId) throws IOException {
		
		String scheme = request.getScheme();
		// Host URL will be production since Twitter and Linkedin don't work
		// in Dev environment.
		String hostURL = scheme + "://www.careersinthemilitary.com";
		//String hostURL = scheme + "://asvabcitm.humrro.org";
		if(career != null) {
			logger.error("" + career.getTitle() + "\\");
			logger.error("fasebook description: "+career.getDescription());
			logger.error("fasebook url: "+ url);
			logger.error("fasebook image: "+ hostURL + "/citm-images/" + mcId + ".jpg");
			
			logger.error("" + career.getTitle() + "\\");
			logger.error("twitter description: "+career.getDescription());
			logger.error("twitter image: "+ hostURL + "/citm-images/" + mcId + ".jpg");
			
			PrintWriter out = response.getWriter();
			out.println("<!DOCTYPE html>");
			out.println("<html>");
			out.println("<head>");
			
			out.println("<meta charset=\"UTF-8\">");
			out.println("<meta property=\"fb:app_id\" content=\"2055205968038605\" />");
			
			out.println("<meta prefix=\"og: http://ogp.me/ns#\" property=\"og:title\" content=\"" + career.getTitle() + "\" />");
			out.println("<meta prefix=\"og: http://ogp.me/ns#\" property=\"og:description\" content=\"" + career.getDescription() + "\" />");
			out.println("<meta prefix=\"og: http://ogp.me/ns#\" property=\"og:url\" content=\"" + url + "\" />");
			out.println("<meta prefix=\"og: http://ogp.me/ns#\" property=\"og:image\" content=\"" + hostURL + "/citm-images/" + mcId + ".jpg" + "\" />");
			
			out.println("<meta property=\"og:image:width\" content=\"276\" />");
			out.println("<meta property=\"og:image:height\" content=\"182\" />");
			
			out.println("<meta name=\"twitter:card\" content=\"summary_large_image\" />");
			out.println("<meta name=\"twitter:site\" content=\"@asvabcep\" />");
			out.println("<meta name=\"twitter:title\" content=\"" + career.getTitle() + "\" />");
			out.println("<meta name=\"twitter:description\" content=\"" + career.getDescription() + "\" />");
			out.println("<meta name=\"twitter:image\" content=\"" + hostURL + "/citm-images/" + mcId + ".jpg" + "\" />");
			out.println("</head>");
			
			out.println("<body>");
			out.println("<a href=\"\" + url + \"\">Return to home page</a>");
			out.println("</body>");
			out.println("</html>");
		}else {
			response.sendRedirect(response.encodeRedirectURL(request.getContextPath() + "/"));
		}
	}

}
