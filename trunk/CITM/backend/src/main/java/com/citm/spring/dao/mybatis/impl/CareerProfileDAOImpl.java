package com.citm.spring.dao.mybatis.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.citm.spring.dao.mybatis.CareerProfileDAO;
import com.citm.spring.dao.mybatis.service.CareerProfileService;
import com.citm.spring.model.careerprofile.CareerProfile;
import com.citm.spring.model.careerprofile.HotJobs;

@Repository
public class CareerProfileDAOImpl implements CareerProfileDAO {

	
	@Autowired
	private CareerProfileService service;
	
	@Override
	public List<CareerProfile> selectCareerProfiles() {
		return service.selectCareerProfiles();
	}
	
	@Override
	public List<HotJobs> getHotJobTitle(String mcId) {
		return service.getHotJobTitle( mcId);
	}

}
