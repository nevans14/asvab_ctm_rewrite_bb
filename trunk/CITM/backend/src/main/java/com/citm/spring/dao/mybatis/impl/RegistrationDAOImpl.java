package com.citm.spring.dao.mybatis.impl;

import com.citm.spring.model.login.Users;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.citm.spring.dao.mybatis.RegistrationDAO;
import com.citm.spring.dao.mybatis.service.RegistrationService;
import com.citm.spring.model.login.AccessCode;
import com.citm.spring.model.login.MEPS;
import com.citm.spring.model.login.UserRole;

@Repository
public class RegistrationDAOImpl implements RegistrationDAO {
	
	@Autowired
	private RegistrationService service;
	
	public Users getUser(String email) {
		return service.getUser(email);
	}
	
	public Users getUserByResetKey(String resetKey) {
		return service.getUserByResetKey(resetKey);
	}
	
	public Integer insertUser(Users user) {
		return service.insertUser(user);
	}
	
	public Integer updateUser(Users user) {
		return service.updateUser(user);
	}
	
	public AccessCode getLastUsedAccessCode() {
		return service.getLastUsedAccessCode();
	}
	
	public AccessCode getAccessCode(String accessCode) {
		return service.getAccessCode(accessCode);
	}
	
	public Integer insertAccessCode(AccessCode accessCode) {
		return service.insertAccessCode(accessCode);
	}
	
	public MEPS getByAccessCode(String accessCode) {
		return service.getByAccessCode(accessCode);
	}
	
	public int insertAsvabLog(int userId, Date loginDate) {
		return service.insertAsvabLog(userId, loginDate);
	}
	
	public List<UserRole> getRoles(String email) {
		return service.getRoles(email);
	}
	
	public int insertRole(UserRole role) {
		return service.insertRole(role);
	}
}