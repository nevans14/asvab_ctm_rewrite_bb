package com.citm.spring.dao.mybatis.mapper;

import java.util.ArrayList;

import org.apache.ibatis.annotations.Param;

import com.citm.spring.model.detail.AdditionalInfo;
import com.citm.spring.model.detail.AirForceDetail;
import com.citm.spring.model.detail.ArmyDetail;
import com.citm.spring.model.detail.CareerPath;
import com.citm.spring.model.detail.CoastGuardDetail;
import com.citm.spring.model.detail.HelpfulAttribute;
import com.citm.spring.model.detail.HiddenJob;
import com.citm.spring.model.detail.MarineDetail;
import com.citm.spring.model.detail.MilitaryCareer;
import com.citm.spring.model.detail.NavyDetail;
import com.citm.spring.model.detail.Profile;
import com.citm.spring.model.detail.RecentlyViewed;
import com.citm.spring.model.detail.WhatTheyDo;
import com.citm.spring.model.detail.RelatedCareer;
import com.citm.spring.model.detail.SeeScoreArmy;
import com.citm.spring.model.detail.ServiceOffering;
import com.citm.spring.model.detail.SkillRating;
import com.citm.spring.model.detail.TrainingProvided;
import com.citm.spring.model.careerprofile.*;

public interface CareerDetailMapper {
	MilitaryCareer getMilitaryCareer(@Param("mcId") String mcId);

	ArrayList<WhatTheyDo> getWhatTheyDo(@Param("mcId") String mcId);

	ArrayList<TrainingProvided> getTrainingProvided(@Param("mcId") String mcId);

	ArrayList<RelatedCareer> getRelatedCareers(@Param("mcId") String mcId);
	
	ArrayList<RelatedCareer> getRelatedCivilianCareers(@Param("mcId") String mcId);
	
	ArrayList<AdditionalInfo> getAdditionalCareerInfo(@Param("mcId") String mcId);

	ArrayList<Profile> getProfiles(@Param("mcId") String mcId);

	int getHotJob(@Param("mcId") String mcId);

	ArrayList<HelpfulAttribute> getHelpfulAttribute(@Param("mcId") String mcId);

	ArrayList<ServiceOffering> getServiceOffering(@Param("mcId") String mcId);
	
	ArrayList<ServiceOffering> getServiceOfferingCompositeScores();
	
	SkillRating getSkillRating(@Param("mcId") String mcId);

	String getHideJobList(@Param("userId") int userId);

	int setHideJobList(@Param("obj") HiddenJob obj);

	ArrayList<CareerPath> getCareerPath(@Param("mcId") String mcId);

	String getRecentlyViewed(@Param("userId") int userId);

	int setRecentlyViewed(@Param("obj") RecentlyViewed obj);

	ArrayList<String> getOtherTitles(@Param("mcId") String mcId);
	
	AirForceDetail getAirForceDetail(@Param("mcId") String mcId, @Param("mocId") String mocId);
	
	ArmyDetail getArmyDetail(@Param("mcId") String mcId, @Param("mocId") String mocId);
	
	CoastGuardDetail getCoastGuardDetail(@Param("mcId") String mcId, @Param("mocId") String mocId);
	
	MarineDetail getMarineDetail(@Param("mcId") String mcId, @Param("mocId") String mocId);
	
	NavyDetail getNavyDetail(@Param("mcId") String mcId, @Param("mocId") String mocId);
	
	ArrayList<SeeScoreArmy> getSeeScoreArmy(@Param("mcId") String mcId);
	
	ArrayList<CareerProfile> getFeaturedProfiles(@Param("mcId") String mcId);

}
