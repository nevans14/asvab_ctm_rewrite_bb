package com.citm.spring.model.login;

import java.util.Date;
import java.util.List;

public class Users {
	private Integer userId;
	private String username, password, system, resetKey, studentId;
	private Short enabled;
	private Date resetExpireDate;
	
	private AccessCode accessCode;
	private List<UserRole> userRoles;
	
	/*Constructors*/
	public Users() {}
	public Users(Integer userId, String username, String password, Short enabled, String system) {
		this.userId = userId;
		this.username = username;
		this.password = password;
		this.enabled = enabled;
		this.system = system;
	}
	
	public void setUserId(Integer userId) { this.userId = userId; }
	public Integer getUserId() { return userId; }
	
	public void setUsername(String username) { this.username = username; }
	public String getUsername() { return username; }
	
	public void setPassword(String password) { this.password = password; }
	public String getPassword() { return password; }
	
	public void setSystem(String system) { this.system = system; }
	public String getSystem() { return system; }
	
	public void setResetKey(String resetKey) { this.resetKey = resetKey; }
	public String getResetKey() { return resetKey; }
	
	public void setStudentId(String studentId) { this.studentId = studentId; }
	public String getStudentId() { return studentId; }
	
	public void setEnabled(Short enabled) { this.enabled = enabled; }
	public Short getEnabled() { return enabled; }
	
	public void setResetExpireDate(Date resetExpireDate) { this.resetExpireDate = resetExpireDate; }
	public Date getResetExpireDate() { return resetExpireDate; }
	
	public void setAccessCode(AccessCode accessCode) { this.accessCode = accessCode; }
	public AccessCode getAccessCode() { return accessCode; }
	
	public void setUserRoles(List<UserRole> userRoles) { this.userRoles = userRoles; }
	public List<UserRole> getUserRoles() { return userRoles; }
 }