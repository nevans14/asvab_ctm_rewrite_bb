package com.citm.spring.model.testscore;

public class CEPPandPDataReader {

	//Control Information
	private	int	aptCd;
	private	int	statusCd;
	private	int	platform;
	private	int	ansSeqNum;
	private	int	ssnNum;
	private	int	MepsId;
	private	int	metSiteId;
	private	int	lastName;
	private	int	service;
	private	int	testType;
	private	int	gender;
	private	int	educationLevel;
	private	int	certification;
	private	int	administratorSsnNum;
	private	int	testForm;
	private	int	scoringMethod;
	private	int	rpGrp;
	private	int	ethnicGrp;
	private	int	administeredYear;
	private	int	administeredMonth;
	private	int	administeredDay;
	private	int	scoredYear;
	private	int	scoredMonth;
	private	int	socredDay;
	private	int	scoredHour;
	private	int	scoredMinute;
	private	int	scoredSecond;
	private	int	dob;
	
	//SS ASVAB Combine Control
	private	int	seqNum;
	
	//Scoring Software Controls
	private	int	transformationTblVersion;
	private	int	itemParameterTblVersion;
	private	int	priorDistTblVer;
	
	//AFQT, Raw, SS, Composites
	private	int	testCompletionCd;
	private	int	rawScore;
	private	int	standardScores;
	private	int	afqt;
	private	int	armyScore;
	private	int	navyScore;
	private	int	airForceScore;
	private	int	marineScore;
	private	int	bookletNum;
	
	//Theta, Item Data, Scoring
	private	int	GS_CompletionCd;
	private	int	GS_UnroundedTheta;
	private	int	GS_Iterations;
	private	int	GS_ActualItemResponse ;
	private	int	GS_DichotomousItemScoring ;
	private	int	AR_CompletionCd;
	private	int	AR_UnroundedTheta;
	private	int	AR_Iterations;
	private	int	AR_ActualItemResponse ;
	private	int	AR_DichotomousItemScoring ;
	private	int	WK_CompletionCd;
	private	int	WK_UnroundedTheta;
	private	int	WK_Iterations;
	private	int	WK_ActualItemResponse ;
	private	int	WK_DichotomousItemScoring ;
	private	int	PC_CompletionCd;
	private	int	PC_UnroundedTheta;
	private	int	PC_Iterations;
	private	int	PC_ActualItemResponse ;
	private	int	PC_DichotomousItemScoring ;
	private	int	MK_CompletionCd;
	private	int	MK_UnroundedTheta;
	private	int	MK_Iterations;
	private	int	MK_ActualItemResponse ;
	private	int	MK_DichotomousItemScoring;
	private	int	EI_CompletionCd;
	private	int	EI_UnroundedTheta;
	private	int	EI_Iterations;
	private	int	EI_ActualItemResponse;
	private	int	EI_DichotomousItemScoring;
	private	int	AS_CompletionCd;
	private	int	AS_UnroundedTheta;
	private	int	AS_Iterations;
	private	int	AS_ActualItemResponse;
	private	int	AS_DichotomousItemScoring;
	private	int	MC_CompletionCd;
	private	int	MC_UnroundedTheta;
	private	int	MC_Iterations;
	private	int	MC_ActualItemResponse;
	private	int	MC_DichotomousItemScoring;
	private	int	AO_CompletionCd;
	private	int	AO_UnroundedTheta;
	private	int	AO_Iterations;
	private	int	AO_ActualItemResponse;
	private	int	AO_DichotomousItemScoring;
	private	int	VE_CompletionCd;
	private	int	DataEntryType;
	
	//High School Information
	private	int	schoolCode;
	private	int	schoolSessionNbr;
	private	int	schoolSpecialInstructions;
	private	int	releaseToServiceDate;
	private	int	schoolCouncellorCd;
	private	int	serviceResponsibleForScheduling;
	
	//Test Scoring Indicators And Controls
	private	int	shotIndicator;
	private	int	specialStudies;
	private	int	page1Status;
	private	int	page2Status;
	private	int	page3Status;
	
	//Student Information
	private	int	studentName;
	private	int	studentAddress;
	private	int	studentCity;
	private	int	studentState;
	private	int	studentZipCode;
	private	int	studentTelephoneNumber;
	private	int	studentIntentions;
	
	//Student Result SheetScores
	private	int	milCareerScore;
	private	int	milCareerScoreCategory;
	
	//Verbal Ability
	private	int	VA_YP_Score;
	private	int	VA_GS_Percentage;
	private	int	VA_GOS_Percentage;
	private	int	VA_COMP_Percentage;
	private	int	VA_TP_Percentage;
	private	int	VA_SGS;
	private	int	VA_USL;
	private	int	VA_LSL;
	
	//Mathematical Ability
	private	int	MA_YP_Score;
	private	int	MA_GS_Percentage;
	private	int	MA_GOS_Percentage;
	private	int	MA_COMP_Percentage;
	private	int	MA_TP_Percentage;
	private	int	MA_SGS;
	private	int	MA_USL;
	private	int	MA_LSL;
	
	//Science & Technical Ability
	private	int	TEC_YP_Score;
	private	int	TEC_GS_Percentage;
	private	int	TEC_GOS_Percentage;
	private	int	TEC_COMP_Percentage;
	private	int	TEC_TP_Percentage;
	private	int	TEC_SGS;
	private	int	TEC_USL;
	private	int	TEC_LSL;
	
	//General Science
	private	int	GS_YP_Score;
	private	int	GS_GS_Percentage;
	private	int	GS_GOS_Percentage;
	private	int	GS_COMP_Percentage;
	private	int	GS_TP_Percentage;
	private	int	GS_SGS;
	private	int	GS_USL;
	private	int	GS_LSL;
	
	//Arithmetic Reasoning
	private	int	AR_YP_Score;
	private	int	AR_GS_Percentage;
	private	int	AR_GOS_Percentage;
	private	int	AR_COMP_Percentage;
	private	int	AR_TP_Percentage;
	private	int	AR_SGS;
	private	int	AR_USL;
	private	int	AR_LSL;
	
	//Word Knowledge
	private	int	WK_YP_Score;
	private	int	WK_GS_Percentage;
	private	int	WK_GOS_Percentage;
	private	int	WK_COMP_Percentage;
	private	int	WK_TP_Percentage;
	private	int	WK_SGS;
	private	int	WK_USL;
	private	int	WK_LSL;
	
	//Paragraph Comprehension
	private	int	PC_YP_Score;
	private	int	PC_GS_Percentage;
	private	int	PC_GOS_Percentage;
	private	int	PC_COMP_Percentage;
	private	int	PC_TP_Percentage;
	private	int	PC_SGS;
	private	int	PC_USL;
	private	int	PC_LSL;
	
	//Mathematics Knowledge
	private	int	MK_YP_Score;
	private	int	MK_GS_Percentage;
	private	int	MK_GOS_Percentage;
	private	int	MK_COMP_Percentage;
	private	int	MK_TP_Percentage;
	private	int	MK_SGS;
	private	int	MK_USL;
	private	int	MK_LSL;
	
	
	//Electronics Information
	private	int	EI_YP_Score;
	private	int	EI_GS_Percentage;
	private	int	EI_GOS_Percentage;
	private	int	EI_COMP_Percentage;
	private	int	EI_TP_Percentage;
	private	int	EI_SGS;
	private	int	EI_USL;
	private	int	EI_LSL;
	
	//Auto Shop Information
	private	int	AS_YP_Score;
	private	int	AS_GS_Percentage;
	private	int	AS_GOS_Percentage;
	private	int	AS_COMP_Percentage;
	private	int	AS_TP_Percentage;
	private	int	AS_SGS;
	private	int	AS_USL;
	private	int	AS_LSL;
	
	//Mechanical Comprehension
	private	int	MC_YP_Score;
	private	int	MC_GS_Percentage;
	private	int	MC_GOS_Percentage;
	private	int	MC_COMP_Percentage;
	private	int	MC_TP_Percentage;
	private	int	MC_SGS;
	private	int	MC_USL;
	private	int	MC_LSL;
	
	//AssemblingObjects
	private	int	AO_YP_Score;
	private	int	AO_GS_Percentage;
	private	int	AO_GOS_Percentage;
	private	int	AO_COMP_Percentage;
	private	int	AO_TP_Percentage;
	private	int	AO_SGS;
	private	int	AO_USL;
	private	int	AO_LSL;
	
	//Race Fields
	private	int	americanIndian;
	private	int	asian;
	private	int	africanAmerican;
	private	int	nativeHawiian;
	private	int	white;
	private	int	reserved;
	private	int	hispanic;
	private	int	notHispanic;
	/**
	 * @return the aptCd
	 */
	public int getAptCd() {
		return aptCd;
	}
	/**
	 * @param aptCd the aptCd to set
	 */
	public void setAptCd(int aptCd) {
		this.aptCd = aptCd;
	}
	/**
	 * @return the statusCd
	 */
	public int getStatusCd() {
		return statusCd;
	}
	/**
	 * @param statusCd the statusCd to set
	 */
	public void setStatusCd(int statusCd) {
		this.statusCd = statusCd;
	}
	/**
	 * @return the platform
	 */
	public int getPlatform() {
		return platform;
	}
	/**
	 * @param platform the platform to set
	 */
	public void setPlatform(int platform) {
		this.platform = platform;
	}
	/**
	 * @return the ansSeqNum
	 */
	public int getAnsSeqNum() {
		return ansSeqNum;
	}
	/**
	 * @param ansSeqNum the ansSeqNum to set
	 */
	public void setAnsSeqNum(int ansSeqNum) {
		this.ansSeqNum = ansSeqNum;
	}
	/**
	 * @return the ssnNum
	 */
	public int getSsnNum() {
		return ssnNum;
	}
	/**
	 * @param ssnNum the ssnNum to set
	 */
	public void setSsnNum(int ssnNum) {
		this.ssnNum = ssnNum;
	}
	/**
	 * @return the mepsId
	 */
	public int getMepsId() {
		return MepsId;
	}
	/**
	 * @param mepsId the mepsId to set
	 */
	public void setMepsId(int mepsId) {
		MepsId = mepsId;
	}
	/**
	 * @return the metSiteId
	 */
	public int getMetSiteId() {
		return metSiteId;
	}
	/**
	 * @param metSiteId the metSiteId to set
	 */
	public void setMetSiteId(int metSiteId) {
		this.metSiteId = metSiteId;
	}
	/**
	 * @return the lastName
	 */
	public int getLastName() {
		return lastName;
	}
	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(int lastName) {
		this.lastName = lastName;
	}
	/**
	 * @return the service
	 */
	public int getService() {
		return service;
	}
	/**
	 * @param service the service to set
	 */
	public void setService(int service) {
		this.service = service;
	}
	/**
	 * @return the testType
	 */
	public int getTestType() {
		return testType;
	}
	/**
	 * @param testType the testType to set
	 */
	public void setTestType(int testType) {
		this.testType = testType;
	}
	/**
	 * @return the gender
	 */
	public int getGender() {
		return gender;
	}
	/**
	 * @param gender the gender to set
	 */
	public void setGender(int gender) {
		this.gender = gender;
	}
	/**
	 * @return the educationLevel
	 */
	public int getEducationLevel() {
		return educationLevel;
	}
	/**
	 * @param educationLevel the educationLevel to set
	 */
	public void setEducationLevel(int educationLevel) {
		this.educationLevel = educationLevel;
	}
	/**
	 * @return the certification
	 */
	public int getCertification() {
		return certification;
	}
	/**
	 * @param certification the certification to set
	 */
	public void setCertification(int certification) {
		this.certification = certification;
	}
	/**
	 * @return the administratorSsnNum
	 */
	public int getAdministratorSsnNum() {
		return administratorSsnNum;
	}
	/**
	 * @param administratorSsnNum the administratorSsnNum to set
	 */
	public void setAdministratorSsnNum(int administratorSsnNum) {
		this.administratorSsnNum = administratorSsnNum;
	}
	/**
	 * @return the testForm
	 */
	public int getTestForm() {
		return testForm;
	}
	/**
	 * @param testForm the testForm to set
	 */
	public void setTestForm(int testForm) {
		this.testForm = testForm;
	}
	/**
	 * @return the scoringMethod
	 */
	public int getScoringMethod() {
		return scoringMethod;
	}
	/**
	 * @param scoringMethod the scoringMethod to set
	 */
	public void setScoringMethod(int scoringMethod) {
		this.scoringMethod = scoringMethod;
	}
	/**
	 * @return the rpGrp
	 */
	public int getRpGrp() {
		return rpGrp;
	}
	/**
	 * @param rpGrp the rpGrp to set
	 */
	public void setRpGrp(int rpGrp) {
		this.rpGrp = rpGrp;
	}
	/**
	 * @return the ethnicGrp
	 */
	public int getEthnicGrp() {
		return ethnicGrp;
	}
	/**
	 * @param ethnicGrp the ethnicGrp to set
	 */
	public void setEthnicGrp(int ethnicGrp) {
		this.ethnicGrp = ethnicGrp;
	}
	/**
	 * @return the administeredYear
	 */
	public int getAdministeredYear() {
		return administeredYear;
	}
	/**
	 * @param administeredYear the administeredYear to set
	 */
	public void setAdministeredYear(int administeredYear) {
		this.administeredYear = administeredYear;
	}
	/**
	 * @return the administeredMonth
	 */
	public int getAdministeredMonth() {
		return administeredMonth;
	}
	/**
	 * @param administeredMonth the administeredMonth to set
	 */
	public void setAdministeredMonth(int administeredMonth) {
		this.administeredMonth = administeredMonth;
	}
	/**
	 * @return the administeredDay
	 */
	public int getAdministeredDay() {
		return administeredDay;
	}
	/**
	 * @param administeredDay the administeredDay to set
	 */
	public void setAdministeredDay(int administeredDay) {
		this.administeredDay = administeredDay;
	}
	/**
	 * @return the scoredYear
	 */
	public int getScoredYear() {
		return scoredYear;
	}
	/**
	 * @param scoredYear the scoredYear to set
	 */
	public void setScoredYear(int scoredYear) {
		this.scoredYear = scoredYear;
	}
	/**
	 * @return the scoredMonth
	 */
	public int getScoredMonth() {
		return scoredMonth;
	}
	/**
	 * @param scoredMonth the scoredMonth to set
	 */
	public void setScoredMonth(int scoredMonth) {
		this.scoredMonth = scoredMonth;
	}
	/**
	 * @return the socredDay
	 */
	public int getSocredDay() {
		return socredDay;
	}
	/**
	 * @param socredDay the socredDay to set
	 */
	public void setSocredDay(int socredDay) {
		this.socredDay = socredDay;
	}
	/**
	 * @return the scoredHour
	 */
	public int getScoredHour() {
		return scoredHour;
	}
	/**
	 * @param scoredHour the scoredHour to set
	 */
	public void setScoredHour(int scoredHour) {
		this.scoredHour = scoredHour;
	}
	/**
	 * @return the scoredMinute
	 */
	public int getScoredMinute() {
		return scoredMinute;
	}
	/**
	 * @param scoredMinute the scoredMinute to set
	 */
	public void setScoredMinute(int scoredMinute) {
		this.scoredMinute = scoredMinute;
	}
	/**
	 * @return the scoredSecond
	 */
	public int getScoredSecond() {
		return scoredSecond;
	}
	/**
	 * @param scoredSecond the scoredSecond to set
	 */
	public void setScoredSecond(int scoredSecond) {
		this.scoredSecond = scoredSecond;
	}
	/**
	 * @return the dob
	 */
	public int getDob() {
		return dob;
	}
	/**
	 * @param dob the dob to set
	 */
	public void setDob(int dob) {
		this.dob = dob;
	}
	/**
	 * @return the seqNum
	 */
	public int getSeqNum() {
		return seqNum;
	}
	/**
	 * @param seqNum the seqNum to set
	 */
	public void setSeqNum(int seqNum) {
		this.seqNum = seqNum;
	}
	/**
	 * @return the transformationTblVersion
	 */
	public int getTransformationTblVersion() {
		return transformationTblVersion;
	}
	/**
	 * @param transformationTblVersion the transformationTblVersion to set
	 */
	public void setTransformationTblVersion(int transformationTblVersion) {
		this.transformationTblVersion = transformationTblVersion;
	}
	/**
	 * @return the itemParameterTblVersion
	 */
	public int getItemParameterTblVersion() {
		return itemParameterTblVersion;
	}
	/**
	 * @param itemParameterTblVersion the itemParameterTblVersion to set
	 */
	public void setItemParameterTblVersion(int itemParameterTblVersion) {
		this.itemParameterTblVersion = itemParameterTblVersion;
	}
	/**
	 * @return the priorDistTblVer
	 */
	public int getPriorDistTblVer() {
		return priorDistTblVer;
	}
	/**
	 * @param priorDistTblVer the priorDistTblVer to set
	 */
	public void setPriorDistTblVer(int priorDistTblVer) {
		this.priorDistTblVer = priorDistTblVer;
	}
	/**
	 * @return the testCompletionCd
	 */
	public int getTestCompletionCd() {
		return testCompletionCd;
	}
	/**
	 * @param testCompletionCd the testCompletionCd to set
	 */
	public void setTestCompletionCd(int testCompletionCd) {
		this.testCompletionCd = testCompletionCd;
	}
	/**
	 * @return the rawScore
	 */
	public int getRawScore() {
		return rawScore;
	}
	/**
	 * @param rawScore the rawScore to set
	 */
	public void setRawScore(int rawScore) {
		this.rawScore = rawScore;
	}
	/**
	 * @return the standardScores
	 */
	public int getStandardScores() {
		return standardScores;
	}
	/**
	 * @param standardScores the standardScores to set
	 */
	public void setStandardScores(int standardScores) {
		this.standardScores = standardScores;
	}
	/**
	 * @return the afqt
	 */
	public int getAfqt() {
		return afqt;
	}
	/**
	 * @param afqt the afqt to set
	 */
	public void setAfqt(int afqt) {
		this.afqt = afqt;
	}
	/**
	 * @return the armyScore
	 */
	public int getArmyScore() {
		return armyScore;
	}
	/**
	 * @param armyScore the armyScore to set
	 */
	public void setArmyScore(int armyScore) {
		this.armyScore = armyScore;
	}
	/**
	 * @return the navyScore
	 */
	public int getNavyScore() {
		return navyScore;
	}
	/**
	 * @param navyScore the navyScore to set
	 */
	public void setNavyScore(int navyScore) {
		this.navyScore = navyScore;
	}
	/**
	 * @return the airForceScore
	 */
	public int getAirForceScore() {
		return airForceScore;
	}
	/**
	 * @param airForceScore the airForceScore to set
	 */
	public void setAirForceScore(int airForceScore) {
		this.airForceScore = airForceScore;
	}
	/**
	 * @return the marineScore
	 */
	public int getMarineScore() {
		return marineScore;
	}
	/**
	 * @param marineScore the marineScore to set
	 */
	public void setMarineScore(int marineScore) {
		this.marineScore = marineScore;
	}
	/**
	 * @return the bookletNum
	 */
	public int getBookletNum() {
		return bookletNum;
	}
	/**
	 * @param bookletNum the bookletNum to set
	 */
	public void setBookletNum(int bookletNum) {
		this.bookletNum = bookletNum;
	}
	/**
	 * @return the gS_CompletionCd
	 */
	public int getGS_CompletionCd() {
		return GS_CompletionCd;
	}
	/**
	 * @param gS_CompletionCd the gS_CompletionCd to set
	 */
	public void setGS_CompletionCd(int gS_CompletionCd) {
		GS_CompletionCd = gS_CompletionCd;
	}
	/**
	 * @return the gS_UnroundedTheta
	 */
	public int getGS_UnroundedTheta() {
		return GS_UnroundedTheta;
	}
	/**
	 * @param gS_UnroundedTheta the gS_UnroundedTheta to set
	 */
	public void setGS_UnroundedTheta(int gS_UnroundedTheta) {
		GS_UnroundedTheta = gS_UnroundedTheta;
	}
	/**
	 * @return the gS_Iterations
	 */
	public int getGS_Iterations() {
		return GS_Iterations;
	}
	/**
	 * @param gS_Iterations the gS_Iterations to set
	 */
	public void setGS_Iterations(int gS_Iterations) {
		GS_Iterations = gS_Iterations;
	}
	/**
	 * @return the gS_ActualItemResponse
	 */
	public int getGS_ActualItemResponse() {
		return GS_ActualItemResponse;
	}
	/**
	 * @param gS_ActualItemResponse the gS_ActualItemResponse to set
	 */
	public void setGS_ActualItemResponse(int gS_ActualItemResponse) {
		GS_ActualItemResponse = gS_ActualItemResponse;
	}
	/**
	 * @return the gS_DichotomousItemScoring
	 */
	public int getGS_DichotomousItemScoring() {
		return GS_DichotomousItemScoring;
	}
	/**
	 * @param gS_DichotomousItemScoring the gS_DichotomousItemScoring to set
	 */
	public void setGS_DichotomousItemScoring(int gS_DichotomousItemScoring) {
		GS_DichotomousItemScoring = gS_DichotomousItemScoring;
	}
	/**
	 * @return the aR_CompletionCd
	 */
	public int getAR_CompletionCd() {
		return AR_CompletionCd;
	}
	/**
	 * @param aR_CompletionCd the aR_CompletionCd to set
	 */
	public void setAR_CompletionCd(int aR_CompletionCd) {
		AR_CompletionCd = aR_CompletionCd;
	}
	/**
	 * @return the aR_UnroundedTheta
	 */
	public int getAR_UnroundedTheta() {
		return AR_UnroundedTheta;
	}
	/**
	 * @param aR_UnroundedTheta the aR_UnroundedTheta to set
	 */
	public void setAR_UnroundedTheta(int aR_UnroundedTheta) {
		AR_UnroundedTheta = aR_UnroundedTheta;
	}
	/**
	 * @return the aR_Iterations
	 */
	public int getAR_Iterations() {
		return AR_Iterations;
	}
	/**
	 * @param aR_Iterations the aR_Iterations to set
	 */
	public void setAR_Iterations(int aR_Iterations) {
		AR_Iterations = aR_Iterations;
	}
	/**
	 * @return the aR_ActualItemResponse
	 */
	public int getAR_ActualItemResponse() {
		return AR_ActualItemResponse;
	}
	/**
	 * @param aR_ActualItemResponse the aR_ActualItemResponse to set
	 */
	public void setAR_ActualItemResponse(int aR_ActualItemResponse) {
		AR_ActualItemResponse = aR_ActualItemResponse;
	}
	/**
	 * @return the aR_DichotomousItemScoring
	 */
	public int getAR_DichotomousItemScoring() {
		return AR_DichotomousItemScoring;
	}
	/**
	 * @param aR_DichotomousItemScoring the aR_DichotomousItemScoring to set
	 */
	public void setAR_DichotomousItemScoring(int aR_DichotomousItemScoring) {
		AR_DichotomousItemScoring = aR_DichotomousItemScoring;
	}
	/**
	 * @return the wK_CompletionCd
	 */
	public int getWK_CompletionCd() {
		return WK_CompletionCd;
	}
	/**
	 * @param wK_CompletionCd the wK_CompletionCd to set
	 */
	public void setWK_CompletionCd(int wK_CompletionCd) {
		WK_CompletionCd = wK_CompletionCd;
	}
	/**
	 * @return the wK_UnroundedTheta
	 */
	public int getWK_UnroundedTheta() {
		return WK_UnroundedTheta;
	}
	/**
	 * @param wK_UnroundedTheta the wK_UnroundedTheta to set
	 */
	public void setWK_UnroundedTheta(int wK_UnroundedTheta) {
		WK_UnroundedTheta = wK_UnroundedTheta;
	}
	/**
	 * @return the wK_Iterations
	 */
	public int getWK_Iterations() {
		return WK_Iterations;
	}
	/**
	 * @param wK_Iterations the wK_Iterations to set
	 */
	public void setWK_Iterations(int wK_Iterations) {
		WK_Iterations = wK_Iterations;
	}
	/**
	 * @return the wK_ActualItemResponse
	 */
	public int getWK_ActualItemResponse() {
		return WK_ActualItemResponse;
	}
	/**
	 * @param wK_ActualItemResponse the wK_ActualItemResponse to set
	 */
	public void setWK_ActualItemResponse(int wK_ActualItemResponse) {
		WK_ActualItemResponse = wK_ActualItemResponse;
	}
	/**
	 * @return the wK_DichotomousItemScoring
	 */
	public int getWK_DichotomousItemScoring() {
		return WK_DichotomousItemScoring;
	}
	/**
	 * @param wK_DichotomousItemScoring the wK_DichotomousItemScoring to set
	 */
	public void setWK_DichotomousItemScoring(int wK_DichotomousItemScoring) {
		WK_DichotomousItemScoring = wK_DichotomousItemScoring;
	}
	/**
	 * @return the pC_CompletionCd
	 */
	public int getPC_CompletionCd() {
		return PC_CompletionCd;
	}
	/**
	 * @param pC_CompletionCd the pC_CompletionCd to set
	 */
	public void setPC_CompletionCd(int pC_CompletionCd) {
		PC_CompletionCd = pC_CompletionCd;
	}
	/**
	 * @return the pC_UnroundedTheta
	 */
	public int getPC_UnroundedTheta() {
		return PC_UnroundedTheta;
	}
	/**
	 * @param pC_UnroundedTheta the pC_UnroundedTheta to set
	 */
	public void setPC_UnroundedTheta(int pC_UnroundedTheta) {
		PC_UnroundedTheta = pC_UnroundedTheta;
	}
	/**
	 * @return the pC_Iterations
	 */
	public int getPC_Iterations() {
		return PC_Iterations;
	}
	/**
	 * @param pC_Iterations the pC_Iterations to set
	 */
	public void setPC_Iterations(int pC_Iterations) {
		PC_Iterations = pC_Iterations;
	}
	/**
	 * @return the pC_ActualItemResponse
	 */
	public int getPC_ActualItemResponse() {
		return PC_ActualItemResponse;
	}
	/**
	 * @param pC_ActualItemResponse the pC_ActualItemResponse to set
	 */
	public void setPC_ActualItemResponse(int pC_ActualItemResponse) {
		PC_ActualItemResponse = pC_ActualItemResponse;
	}
	/**
	 * @return the pC_DichotomousItemScoring
	 */
	public int getPC_DichotomousItemScoring() {
		return PC_DichotomousItemScoring;
	}
	/**
	 * @param pC_DichotomousItemScoring the pC_DichotomousItemScoring to set
	 */
	public void setPC_DichotomousItemScoring(int pC_DichotomousItemScoring) {
		PC_DichotomousItemScoring = pC_DichotomousItemScoring;
	}
	/**
	 * @return the mK_CompletionCd
	 */
	public int getMK_CompletionCd() {
		return MK_CompletionCd;
	}
	/**
	 * @param mK_CompletionCd the mK_CompletionCd to set
	 */
	public void setMK_CompletionCd(int mK_CompletionCd) {
		MK_CompletionCd = mK_CompletionCd;
	}
	/**
	 * @return the mK_UnroundedTheta
	 */
	public int getMK_UnroundedTheta() {
		return MK_UnroundedTheta;
	}
	/**
	 * @param mK_UnroundedTheta the mK_UnroundedTheta to set
	 */
	public void setMK_UnroundedTheta(int mK_UnroundedTheta) {
		MK_UnroundedTheta = mK_UnroundedTheta;
	}
	/**
	 * @return the mK_Iterations
	 */
	public int getMK_Iterations() {
		return MK_Iterations;
	}
	/**
	 * @param mK_Iterations the mK_Iterations to set
	 */
	public void setMK_Iterations(int mK_Iterations) {
		MK_Iterations = mK_Iterations;
	}
	/**
	 * @return the mK_ActualItemResponse
	 */
	public int getMK_ActualItemResponse() {
		return MK_ActualItemResponse;
	}
	/**
	 * @param mK_ActualItemResponse the mK_ActualItemResponse to set
	 */
	public void setMK_ActualItemResponse(int mK_ActualItemResponse) {
		MK_ActualItemResponse = mK_ActualItemResponse;
	}
	/**
	 * @return the mK_DichotomousItemScoring
	 */
	public int getMK_DichotomousItemScoring() {
		return MK_DichotomousItemScoring;
	}
	/**
	 * @param mK_DichotomousItemScoring the mK_DichotomousItemScoring to set
	 */
	public void setMK_DichotomousItemScoring(int mK_DichotomousItemScoring) {
		MK_DichotomousItemScoring = mK_DichotomousItemScoring;
	}
	/**
	 * @return the eI_CompletionCd
	 */
	public int getEI_CompletionCd() {
		return EI_CompletionCd;
	}
	/**
	 * @param eI_CompletionCd the eI_CompletionCd to set
	 */
	public void setEI_CompletionCd(int eI_CompletionCd) {
		EI_CompletionCd = eI_CompletionCd;
	}
	/**
	 * @return the eI_UnroundedTheta
	 */
	public int getEI_UnroundedTheta() {
		return EI_UnroundedTheta;
	}
	/**
	 * @param eI_UnroundedTheta the eI_UnroundedTheta to set
	 */
	public void setEI_UnroundedTheta(int eI_UnroundedTheta) {
		EI_UnroundedTheta = eI_UnroundedTheta;
	}
	/**
	 * @return the eI_Iterations
	 */
	public int getEI_Iterations() {
		return EI_Iterations;
	}
	/**
	 * @param eI_Iterations the eI_Iterations to set
	 */
	public void setEI_Iterations(int eI_Iterations) {
		EI_Iterations = eI_Iterations;
	}
	/**
	 * @return the eI_ActualItemResponse
	 */
	public int getEI_ActualItemResponse() {
		return EI_ActualItemResponse;
	}
	/**
	 * @param eI_ActualItemResponse the eI_ActualItemResponse to set
	 */
	public void setEI_ActualItemResponse(int eI_ActualItemResponse) {
		EI_ActualItemResponse = eI_ActualItemResponse;
	}
	/**
	 * @return the eI_DichotomousItemScoring
	 */
	public int getEI_DichotomousItemScoring() {
		return EI_DichotomousItemScoring;
	}
	/**
	 * @param eI_DichotomousItemScoring the eI_DichotomousItemScoring to set
	 */
	public void setEI_DichotomousItemScoring(int eI_DichotomousItemScoring) {
		EI_DichotomousItemScoring = eI_DichotomousItemScoring;
	}
	/**
	 * @return the aS_CompletionCd
	 */
	public int getAS_CompletionCd() {
		return AS_CompletionCd;
	}
	/**
	 * @param aS_CompletionCd the aS_CompletionCd to set
	 */
	public void setAS_CompletionCd(int aS_CompletionCd) {
		AS_CompletionCd = aS_CompletionCd;
	}
	/**
	 * @return the aS_UnroundedTheta
	 */
	public int getAS_UnroundedTheta() {
		return AS_UnroundedTheta;
	}
	/**
	 * @param aS_UnroundedTheta the aS_UnroundedTheta to set
	 */
	public void setAS_UnroundedTheta(int aS_UnroundedTheta) {
		AS_UnroundedTheta = aS_UnroundedTheta;
	}
	/**
	 * @return the aS_Iterations
	 */
	public int getAS_Iterations() {
		return AS_Iterations;
	}
	/**
	 * @param aS_Iterations the aS_Iterations to set
	 */
	public void setAS_Iterations(int aS_Iterations) {
		AS_Iterations = aS_Iterations;
	}
	/**
	 * @return the aS_ActualItemResponse
	 */
	public int getAS_ActualItemResponse() {
		return AS_ActualItemResponse;
	}
	/**
	 * @param aS_ActualItemResponse the aS_ActualItemResponse to set
	 */
	public void setAS_ActualItemResponse(int aS_ActualItemResponse) {
		AS_ActualItemResponse = aS_ActualItemResponse;
	}
	/**
	 * @return the aS_DichotomousItemScoring
	 */
	public int getAS_DichotomousItemScoring() {
		return AS_DichotomousItemScoring;
	}
	/**
	 * @param aS_DichotomousItemScoring the aS_DichotomousItemScoring to set
	 */
	public void setAS_DichotomousItemScoring(int aS_DichotomousItemScoring) {
		AS_DichotomousItemScoring = aS_DichotomousItemScoring;
	}
	/**
	 * @return the mC_CompletionCd
	 */
	public int getMC_CompletionCd() {
		return MC_CompletionCd;
	}
	/**
	 * @param mC_CompletionCd the mC_CompletionCd to set
	 */
	public void setMC_CompletionCd(int mC_CompletionCd) {
		MC_CompletionCd = mC_CompletionCd;
	}
	/**
	 * @return the mC_UnroundedTheta
	 */
	public int getMC_UnroundedTheta() {
		return MC_UnroundedTheta;
	}
	/**
	 * @param mC_UnroundedTheta the mC_UnroundedTheta to set
	 */
	public void setMC_UnroundedTheta(int mC_UnroundedTheta) {
		MC_UnroundedTheta = mC_UnroundedTheta;
	}
	/**
	 * @return the mC_Iterations
	 */
	public int getMC_Iterations() {
		return MC_Iterations;
	}
	/**
	 * @param mC_Iterations the mC_Iterations to set
	 */
	public void setMC_Iterations(int mC_Iterations) {
		MC_Iterations = mC_Iterations;
	}
	/**
	 * @return the mC_ActualItemResponse
	 */
	public int getMC_ActualItemResponse() {
		return MC_ActualItemResponse;
	}
	/**
	 * @param mC_ActualItemResponse the mC_ActualItemResponse to set
	 */
	public void setMC_ActualItemResponse(int mC_ActualItemResponse) {
		MC_ActualItemResponse = mC_ActualItemResponse;
	}
	/**
	 * @return the mC_DichotomousItemScoring
	 */
	public int getMC_DichotomousItemScoring() {
		return MC_DichotomousItemScoring;
	}
	/**
	 * @param mC_DichotomousItemScoring the mC_DichotomousItemScoring to set
	 */
	public void setMC_DichotomousItemScoring(int mC_DichotomousItemScoring) {
		MC_DichotomousItemScoring = mC_DichotomousItemScoring;
	}
	/**
	 * @return the aO_CompletionCd
	 */
	public int getAO_CompletionCd() {
		return AO_CompletionCd;
	}
	/**
	 * @param aO_CompletionCd the aO_CompletionCd to set
	 */
	public void setAO_CompletionCd(int aO_CompletionCd) {
		AO_CompletionCd = aO_CompletionCd;
	}
	/**
	 * @return the aO_UnroundedTheta
	 */
	public int getAO_UnroundedTheta() {
		return AO_UnroundedTheta;
	}
	/**
	 * @param aO_UnroundedTheta the aO_UnroundedTheta to set
	 */
	public void setAO_UnroundedTheta(int aO_UnroundedTheta) {
		AO_UnroundedTheta = aO_UnroundedTheta;
	}
	/**
	 * @return the aO_Iterations
	 */
	public int getAO_Iterations() {
		return AO_Iterations;
	}
	/**
	 * @param aO_Iterations the aO_Iterations to set
	 */
	public void setAO_Iterations(int aO_Iterations) {
		AO_Iterations = aO_Iterations;
	}
	/**
	 * @return the aO_ActualItemResponse
	 */
	public int getAO_ActualItemResponse() {
		return AO_ActualItemResponse;
	}
	/**
	 * @param aO_ActualItemResponse the aO_ActualItemResponse to set
	 */
	public void setAO_ActualItemResponse(int aO_ActualItemResponse) {
		AO_ActualItemResponse = aO_ActualItemResponse;
	}
	/**
	 * @return the aO_DichotomousItemScoring
	 */
	public int getAO_DichotomousItemScoring() {
		return AO_DichotomousItemScoring;
	}
	/**
	 * @param aO_DichotomousItemScoring the aO_DichotomousItemScoring to set
	 */
	public void setAO_DichotomousItemScoring(int aO_DichotomousItemScoring) {
		AO_DichotomousItemScoring = aO_DichotomousItemScoring;
	}
	/**
	 * @return the vE_CompletionCd
	 */
	public int getVE_CompletionCd() {
		return VE_CompletionCd;
	}
	/**
	 * @param vE_CompletionCd the vE_CompletionCd to set
	 */
	public void setVE_CompletionCd(int vE_CompletionCd) {
		VE_CompletionCd = vE_CompletionCd;
	}
	/**
	 * @return the dataEntryType
	 */
	public int getDataEntryType() {
		return DataEntryType;
	}
	/**
	 * @param dataEntryType the dataEntryType to set
	 */
	public void setDataEntryType(int dataEntryType) {
		DataEntryType = dataEntryType;
	}
	/**
	 * @return the schoolCode
	 */
	public int getSchoolCode() {
		return schoolCode;
	}
	/**
	 * @param schoolCode the schoolCode to set
	 */
	public void setSchoolCode(int schoolCode) {
		this.schoolCode = schoolCode;
	}
	/**
	 * @return the schoolSessionNbr
	 */
	public int getSchoolSessionNbr() {
		return schoolSessionNbr;
	}
	/**
	 * @param schoolSessionNbr the schoolSessionNbr to set
	 */
	public void setSchoolSessionNbr(int schoolSessionNbr) {
		this.schoolSessionNbr = schoolSessionNbr;
	}
	/**
	 * @return the schoolSpecialInstructions
	 */
	public int getSchoolSpecialInstructions() {
		return schoolSpecialInstructions;
	}
	/**
	 * @param schoolSpecialInstructions the schoolSpecialInstructions to set
	 */
	public void setSchoolSpecialInstructions(int schoolSpecialInstructions) {
		this.schoolSpecialInstructions = schoolSpecialInstructions;
	}
	/**
	 * @return the releaseToServiceDate
	 */
	public int getReleaseToServiceDate() {
		return releaseToServiceDate;
	}
	/**
	 * @param releaseToServiceDate the releaseToServiceDate to set
	 */
	public void setReleaseToServiceDate(int releaseToServiceDate) {
		this.releaseToServiceDate = releaseToServiceDate;
	}
	/**
	 * @return the schoolCouncellorCd
	 */
	public int getSchoolCouncellorCd() {
		return schoolCouncellorCd;
	}
	/**
	 * @param schoolCouncellorCd the schoolCouncellorCd to set
	 */
	public void setSchoolCouncellorCd(int schoolCouncellorCd) {
		this.schoolCouncellorCd = schoolCouncellorCd;
	}
	/**
	 * @return the serviceResponsibleForScheduling
	 */
	public int getServiceResponsibleForScheduling() {
		return serviceResponsibleForScheduling;
	}
	/**
	 * @param serviceResponsibleForScheduling the serviceResponsibleForScheduling to set
	 */
	public void setServiceResponsibleForScheduling(int serviceResponsibleForScheduling) {
		this.serviceResponsibleForScheduling = serviceResponsibleForScheduling;
	}
	/**
	 * @return the shotIndicator
	 */
	public int getShotIndicator() {
		return shotIndicator;
	}
	/**
	 * @param shotIndicator the shotIndicator to set
	 */
	public void setShotIndicator(int shotIndicator) {
		this.shotIndicator = shotIndicator;
	}
	/**
	 * @return the specialStudies
	 */
	public int getSpecialStudies() {
		return specialStudies;
	}
	/**
	 * @param specialStudies the specialStudies to set
	 */
	public void setSpecialStudies(int specialStudies) {
		this.specialStudies = specialStudies;
	}
	/**
	 * @return the page1Status
	 */
	public int getPage1Status() {
		return page1Status;
	}
	/**
	 * @param page1Status the page1Status to set
	 */
	public void setPage1Status(int page1Status) {
		this.page1Status = page1Status;
	}
	/**
	 * @return the page2Status
	 */
	public int getPage2Status() {
		return page2Status;
	}
	/**
	 * @param page2Status the page2Status to set
	 */
	public void setPage2Status(int page2Status) {
		this.page2Status = page2Status;
	}
	/**
	 * @return the page3Status
	 */
	public int getPage3Status() {
		return page3Status;
	}
	/**
	 * @param page3Status the page3Status to set
	 */
	public void setPage3Status(int page3Status) {
		this.page3Status = page3Status;
	}
	/**
	 * @return the studentName
	 */
	public int getStudentName() {
		return studentName;
	}
	/**
	 * @param studentName the studentName to set
	 */
	public void setStudentName(int studentName) {
		this.studentName = studentName;
	}
	/**
	 * @return the studentAddress
	 */
	public int getStudentAddress() {
		return studentAddress;
	}
	/**
	 * @param studentAddress the studentAddress to set
	 */
	public void setStudentAddress(int studentAddress) {
		this.studentAddress = studentAddress;
	}
	/**
	 * @return the studentCity
	 */
	public int getStudentCity() {
		return studentCity;
	}
	/**
	 * @param studentCity the studentCity to set
	 */
	public void setStudentCity(int studentCity) {
		this.studentCity = studentCity;
	}
	/**
	 * @return the studentState
	 */
	public int getStudentState() {
		return studentState;
	}
	/**
	 * @param studentState the studentState to set
	 */
	public void setStudentState(int studentState) {
		this.studentState = studentState;
	}
	/**
	 * @return the studentZipCode
	 */
	public int getStudentZipCode() {
		return studentZipCode;
	}
	/**
	 * @param studentZipCode the studentZipCode to set
	 */
	public void setStudentZipCode(int studentZipCode) {
		this.studentZipCode = studentZipCode;
	}
	/**
	 * @return the studentTelephoneNumber
	 */
	public int getStudentTelephoneNumber() {
		return studentTelephoneNumber;
	}
	/**
	 * @param studentTelephoneNumber the studentTelephoneNumber to set
	 */
	public void setStudentTelephoneNumber(int studentTelephoneNumber) {
		this.studentTelephoneNumber = studentTelephoneNumber;
	}
	/**
	 * @return the studentIntentions
	 */
	public int getStudentIntentions() {
		return studentIntentions;
	}
	/**
	 * @param studentIntentions the studentIntentions to set
	 */
	public void setStudentIntentions(int studentIntentions) {
		this.studentIntentions = studentIntentions;
	}
	/**
	 * @return the milCareerScore
	 */
	public int getMilCareerScore() {
		return milCareerScore;
	}
	/**
	 * @param milCareerScore the milCareerScore to set
	 */
	public void setMilCareerScore(int milCareerScore) {
		this.milCareerScore = milCareerScore;
	}
	/**
	 * @return the milCareerScoreCategory
	 */
	public int getMilCareerScoreCategory() {
		return milCareerScoreCategory;
	}
	/**
	 * @param milCareerScoreCategory the milCareerScoreCategory to set
	 */
	public void setMilCareerScoreCategory(int milCareerScoreCategory) {
		this.milCareerScoreCategory = milCareerScoreCategory;
	}
	/**
	 * @return the vA_YP_Score
	 */
	public int getVA_YP_Score() {
		return VA_YP_Score;
	}
	/**
	 * @param vA_YP_Score the vA_YP_Score to set
	 */
	public void setVA_YP_Score(int vA_YP_Score) {
		VA_YP_Score = vA_YP_Score;
	}
	/**
	 * @return the vA_GS_Percentage
	 */
	public int getVA_GS_Percentage() {
		return VA_GS_Percentage;
	}
	/**
	 * @param vA_GS_Percentage the vA_GS_Percentage to set
	 */
	public void setVA_GS_Percentage(int vA_GS_Percentage) {
		VA_GS_Percentage = vA_GS_Percentage;
	}
	/**
	 * @return the vA_GOS_Percentage
	 */
	public int getVA_GOS_Percentage() {
		return VA_GOS_Percentage;
	}
	/**
	 * @param vA_GOS_Percentage the vA_GOS_Percentage to set
	 */
	public void setVA_GOS_Percentage(int vA_GOS_Percentage) {
		VA_GOS_Percentage = vA_GOS_Percentage;
	}
	/**
	 * @return the vA_COMP_Percentage
	 */
	public int getVA_COMP_Percentage() {
		return VA_COMP_Percentage;
	}
	/**
	 * @param vA_COMP_Percentage the vA_COMP_Percentage to set
	 */
	public void setVA_COMP_Percentage(int vA_COMP_Percentage) {
		VA_COMP_Percentage = vA_COMP_Percentage;
	}
	/**
	 * @return the vA_TP_Percentage
	 */
	public int getVA_TP_Percentage() {
		return VA_TP_Percentage;
	}
	/**
	 * @param vA_TP_Percentage the vA_TP_Percentage to set
	 */
	public void setVA_TP_Percentage(int vA_TP_Percentage) {
		VA_TP_Percentage = vA_TP_Percentage;
	}
	/**
	 * @return the vA_SGS
	 */
	public int getVA_SGS() {
		return VA_SGS;
	}
	/**
	 * @param vA_SGS the vA_SGS to set
	 */
	public void setVA_SGS(int vA_SGS) {
		VA_SGS = vA_SGS;
	}
	/**
	 * @return the vA_USL
	 */
	public int getVA_USL() {
		return VA_USL;
	}
	/**
	 * @param vA_USL the vA_USL to set
	 */
	public void setVA_USL(int vA_USL) {
		VA_USL = vA_USL;
	}
	/**
	 * @return the vA_LSL
	 */
	public int getVA_LSL() {
		return VA_LSL;
	}
	/**
	 * @param vA_LSL the vA_LSL to set
	 */
	public void setVA_LSL(int vA_LSL) {
		VA_LSL = vA_LSL;
	}
	/**
	 * @return the mA_YP_Score
	 */
	public int getMA_YP_Score() {
		return MA_YP_Score;
	}
	/**
	 * @param mA_YP_Score the mA_YP_Score to set
	 */
	public void setMA_YP_Score(int mA_YP_Score) {
		MA_YP_Score = mA_YP_Score;
	}
	/**
	 * @return the mA_GS_Percentage
	 */
	public int getMA_GS_Percentage() {
		return MA_GS_Percentage;
	}
	/**
	 * @param mA_GS_Percentage the mA_GS_Percentage to set
	 */
	public void setMA_GS_Percentage(int mA_GS_Percentage) {
		MA_GS_Percentage = mA_GS_Percentage;
	}
	/**
	 * @return the mA_GOS_Percentage
	 */
	public int getMA_GOS_Percentage() {
		return MA_GOS_Percentage;
	}
	/**
	 * @param mA_GOS_Percentage the mA_GOS_Percentage to set
	 */
	public void setMA_GOS_Percentage(int mA_GOS_Percentage) {
		MA_GOS_Percentage = mA_GOS_Percentage;
	}
	/**
	 * @return the mA_COMP_Percentage
	 */
	public int getMA_COMP_Percentage() {
		return MA_COMP_Percentage;
	}
	/**
	 * @param mA_COMP_Percentage the mA_COMP_Percentage to set
	 */
	public void setMA_COMP_Percentage(int mA_COMP_Percentage) {
		MA_COMP_Percentage = mA_COMP_Percentage;
	}
	/**
	 * @return the mA_TP_Percentage
	 */
	public int getMA_TP_Percentage() {
		return MA_TP_Percentage;
	}
	/**
	 * @param mA_TP_Percentage the mA_TP_Percentage to set
	 */
	public void setMA_TP_Percentage(int mA_TP_Percentage) {
		MA_TP_Percentage = mA_TP_Percentage;
	}
	/**
	 * @return the mA_SGS
	 */
	public int getMA_SGS() {
		return MA_SGS;
	}
	/**
	 * @param mA_SGS the mA_SGS to set
	 */
	public void setMA_SGS(int mA_SGS) {
		MA_SGS = mA_SGS;
	}
	/**
	 * @return the mA_USL
	 */
	public int getMA_USL() {
		return MA_USL;
	}
	/**
	 * @param mA_USL the mA_USL to set
	 */
	public void setMA_USL(int mA_USL) {
		MA_USL = mA_USL;
	}
	/**
	 * @return the mA_LSL
	 */
	public int getMA_LSL() {
		return MA_LSL;
	}
	/**
	 * @param mA_LSL the mA_LSL to set
	 */
	public void setMA_LSL(int mA_LSL) {
		MA_LSL = mA_LSL;
	}
	/**
	 * @return the tEC_YP_Score
	 */
	public int getTEC_YP_Score() {
		return TEC_YP_Score;
	}
	/**
	 * @param tEC_YP_Score the tEC_YP_Score to set
	 */
	public void setTEC_YP_Score(int tEC_YP_Score) {
		TEC_YP_Score = tEC_YP_Score;
	}
	/**
	 * @return the tEC_GS_Percentage
	 */
	public int getTEC_GS_Percentage() {
		return TEC_GS_Percentage;
	}
	/**
	 * @param tEC_GS_Percentage the tEC_GS_Percentage to set
	 */
	public void setTEC_GS_Percentage(int tEC_GS_Percentage) {
		TEC_GS_Percentage = tEC_GS_Percentage;
	}
	/**
	 * @return the tEC_GOS_Percentage
	 */
	public int getTEC_GOS_Percentage() {
		return TEC_GOS_Percentage;
	}
	/**
	 * @param tEC_GOS_Percentage the tEC_GOS_Percentage to set
	 */
	public void setTEC_GOS_Percentage(int tEC_GOS_Percentage) {
		TEC_GOS_Percentage = tEC_GOS_Percentage;
	}
	/**
	 * @return the tEC_COMP_Percentage
	 */
	public int getTEC_COMP_Percentage() {
		return TEC_COMP_Percentage;
	}
	/**
	 * @param tEC_COMP_Percentage the tEC_COMP_Percentage to set
	 */
	public void setTEC_COMP_Percentage(int tEC_COMP_Percentage) {
		TEC_COMP_Percentage = tEC_COMP_Percentage;
	}
	/**
	 * @return the tEC_TP_Percentage
	 */
	public int getTEC_TP_Percentage() {
		return TEC_TP_Percentage;
	}
	/**
	 * @param tEC_TP_Percentage the tEC_TP_Percentage to set
	 */
	public void setTEC_TP_Percentage(int tEC_TP_Percentage) {
		TEC_TP_Percentage = tEC_TP_Percentage;
	}
	/**
	 * @return the tEC_SGS
	 */
	public int getTEC_SGS() {
		return TEC_SGS;
	}
	/**
	 * @param tEC_SGS the tEC_SGS to set
	 */
	public void setTEC_SGS(int tEC_SGS) {
		TEC_SGS = tEC_SGS;
	}
	/**
	 * @return the tEC_USL
	 */
	public int getTEC_USL() {
		return TEC_USL;
	}
	/**
	 * @param tEC_USL the tEC_USL to set
	 */
	public void setTEC_USL(int tEC_USL) {
		TEC_USL = tEC_USL;
	}
	/**
	 * @return the tEC_LSL
	 */
	public int getTEC_LSL() {
		return TEC_LSL;
	}
	/**
	 * @param tEC_LSL the tEC_LSL to set
	 */
	public void setTEC_LSL(int tEC_LSL) {
		TEC_LSL = tEC_LSL;
	}
	/**
	 * @return the gS_YP_Score
	 */
	public int getGS_YP_Score() {
		return GS_YP_Score;
	}
	/**
	 * @param gS_YP_Score the gS_YP_Score to set
	 */
	public void setGS_YP_Score(int gS_YP_Score) {
		GS_YP_Score = gS_YP_Score;
	}
	/**
	 * @return the gS_GS_Percentage
	 */
	public int getGS_GS_Percentage() {
		return GS_GS_Percentage;
	}
	/**
	 * @param gS_GS_Percentage the gS_GS_Percentage to set
	 */
	public void setGS_GS_Percentage(int gS_GS_Percentage) {
		GS_GS_Percentage = gS_GS_Percentage;
	}
	/**
	 * @return the gS_GOS_Percentage
	 */
	public int getGS_GOS_Percentage() {
		return GS_GOS_Percentage;
	}
	/**
	 * @param gS_GOS_Percentage the gS_GOS_Percentage to set
	 */
	public void setGS_GOS_Percentage(int gS_GOS_Percentage) {
		GS_GOS_Percentage = gS_GOS_Percentage;
	}
	/**
	 * @return the gS_COMP_Percentage
	 */
	public int getGS_COMP_Percentage() {
		return GS_COMP_Percentage;
	}
	/**
	 * @param gS_COMP_Percentage the gS_COMP_Percentage to set
	 */
	public void setGS_COMP_Percentage(int gS_COMP_Percentage) {
		GS_COMP_Percentage = gS_COMP_Percentage;
	}
	/**
	 * @return the gS_TP_Percentage
	 */
	public int getGS_TP_Percentage() {
		return GS_TP_Percentage;
	}
	/**
	 * @param gS_TP_Percentage the gS_TP_Percentage to set
	 */
	public void setGS_TP_Percentage(int gS_TP_Percentage) {
		GS_TP_Percentage = gS_TP_Percentage;
	}
	/**
	 * @return the gS_SGS
	 */
	public int getGS_SGS() {
		return GS_SGS;
	}
	/**
	 * @param gS_SGS the gS_SGS to set
	 */
	public void setGS_SGS(int gS_SGS) {
		GS_SGS = gS_SGS;
	}
	/**
	 * @return the gS_USL
	 */
	public int getGS_USL() {
		return GS_USL;
	}
	/**
	 * @param gS_USL the gS_USL to set
	 */
	public void setGS_USL(int gS_USL) {
		GS_USL = gS_USL;
	}
	/**
	 * @return the gS_LSL
	 */
	public int getGS_LSL() {
		return GS_LSL;
	}
	/**
	 * @param gS_LSL the gS_LSL to set
	 */
	public void setGS_LSL(int gS_LSL) {
		GS_LSL = gS_LSL;
	}
	/**
	 * @return the aR_YP_Score
	 */
	public int getAR_YP_Score() {
		return AR_YP_Score;
	}
	/**
	 * @param aR_YP_Score the aR_YP_Score to set
	 */
	public void setAR_YP_Score(int aR_YP_Score) {
		AR_YP_Score = aR_YP_Score;
	}
	/**
	 * @return the aR_GS_Percentage
	 */
	public int getAR_GS_Percentage() {
		return AR_GS_Percentage;
	}
	/**
	 * @param aR_GS_Percentage the aR_GS_Percentage to set
	 */
	public void setAR_GS_Percentage(int aR_GS_Percentage) {
		AR_GS_Percentage = aR_GS_Percentage;
	}
	/**
	 * @return the aR_GOS_Percentage
	 */
	public int getAR_GOS_Percentage() {
		return AR_GOS_Percentage;
	}
	/**
	 * @param aR_GOS_Percentage the aR_GOS_Percentage to set
	 */
	public void setAR_GOS_Percentage(int aR_GOS_Percentage) {
		AR_GOS_Percentage = aR_GOS_Percentage;
	}
	/**
	 * @return the aR_COMP_Percentage
	 */
	public int getAR_COMP_Percentage() {
		return AR_COMP_Percentage;
	}
	/**
	 * @param aR_COMP_Percentage the aR_COMP_Percentage to set
	 */
	public void setAR_COMP_Percentage(int aR_COMP_Percentage) {
		AR_COMP_Percentage = aR_COMP_Percentage;
	}
	/**
	 * @return the aR_TP_Percentage
	 */
	public int getAR_TP_Percentage() {
		return AR_TP_Percentage;
	}
	/**
	 * @param aR_TP_Percentage the aR_TP_Percentage to set
	 */
	public void setAR_TP_Percentage(int aR_TP_Percentage) {
		AR_TP_Percentage = aR_TP_Percentage;
	}
	/**
	 * @return the aR_SGS
	 */
	public int getAR_SGS() {
		return AR_SGS;
	}
	/**
	 * @param aR_SGS the aR_SGS to set
	 */
	public void setAR_SGS(int aR_SGS) {
		AR_SGS = aR_SGS;
	}
	/**
	 * @return the aR_USL
	 */
	public int getAR_USL() {
		return AR_USL;
	}
	/**
	 * @param aR_USL the aR_USL to set
	 */
	public void setAR_USL(int aR_USL) {
		AR_USL = aR_USL;
	}
	/**
	 * @return the aR_LSL
	 */
	public int getAR_LSL() {
		return AR_LSL;
	}
	/**
	 * @param aR_LSL the aR_LSL to set
	 */
	public void setAR_LSL(int aR_LSL) {
		AR_LSL = aR_LSL;
	}
	/**
	 * @return the wK_YP_Score
	 */
	public int getWK_YP_Score() {
		return WK_YP_Score;
	}
	/**
	 * @param wK_YP_Score the wK_YP_Score to set
	 */
	public void setWK_YP_Score(int wK_YP_Score) {
		WK_YP_Score = wK_YP_Score;
	}
	/**
	 * @return the wK_GS_Percentage
	 */
	public int getWK_GS_Percentage() {
		return WK_GS_Percentage;
	}
	/**
	 * @param wK_GS_Percentage the wK_GS_Percentage to set
	 */
	public void setWK_GS_Percentage(int wK_GS_Percentage) {
		WK_GS_Percentage = wK_GS_Percentage;
	}
	/**
	 * @return the wK_GOS_Percentage
	 */
	public int getWK_GOS_Percentage() {
		return WK_GOS_Percentage;
	}
	/**
	 * @param wK_GOS_Percentage the wK_GOS_Percentage to set
	 */
	public void setWK_GOS_Percentage(int wK_GOS_Percentage) {
		WK_GOS_Percentage = wK_GOS_Percentage;
	}
	/**
	 * @return the wK_COMP_Percentage
	 */
	public int getWK_COMP_Percentage() {
		return WK_COMP_Percentage;
	}
	/**
	 * @param wK_COMP_Percentage the wK_COMP_Percentage to set
	 */
	public void setWK_COMP_Percentage(int wK_COMP_Percentage) {
		WK_COMP_Percentage = wK_COMP_Percentage;
	}
	/**
	 * @return the wK_TP_Percentage
	 */
	public int getWK_TP_Percentage() {
		return WK_TP_Percentage;
	}
	/**
	 * @param wK_TP_Percentage the wK_TP_Percentage to set
	 */
	public void setWK_TP_Percentage(int wK_TP_Percentage) {
		WK_TP_Percentage = wK_TP_Percentage;
	}
	/**
	 * @return the wK_SGS
	 */
	public int getWK_SGS() {
		return WK_SGS;
	}
	/**
	 * @param wK_SGS the wK_SGS to set
	 */
	public void setWK_SGS(int wK_SGS) {
		WK_SGS = wK_SGS;
	}
	/**
	 * @return the wK_USL
	 */
	public int getWK_USL() {
		return WK_USL;
	}
	/**
	 * @param wK_USL the wK_USL to set
	 */
	public void setWK_USL(int wK_USL) {
		WK_USL = wK_USL;
	}
	/**
	 * @return the wK_LSL
	 */
	public int getWK_LSL() {
		return WK_LSL;
	}
	/**
	 * @param wK_LSL the wK_LSL to set
	 */
	public void setWK_LSL(int wK_LSL) {
		WK_LSL = wK_LSL;
	}
	/**
	 * @return the pC_YP_Score
	 */
	public int getPC_YP_Score() {
		return PC_YP_Score;
	}
	/**
	 * @param pC_YP_Score the pC_YP_Score to set
	 */
	public void setPC_YP_Score(int pC_YP_Score) {
		PC_YP_Score = pC_YP_Score;
	}
	/**
	 * @return the pC_GS_Percentage
	 */
	public int getPC_GS_Percentage() {
		return PC_GS_Percentage;
	}
	/**
	 * @param pC_GS_Percentage the pC_GS_Percentage to set
	 */
	public void setPC_GS_Percentage(int pC_GS_Percentage) {
		PC_GS_Percentage = pC_GS_Percentage;
	}
	/**
	 * @return the pC_GOS_Percentage
	 */
	public int getPC_GOS_Percentage() {
		return PC_GOS_Percentage;
	}
	/**
	 * @param pC_GOS_Percentage the pC_GOS_Percentage to set
	 */
	public void setPC_GOS_Percentage(int pC_GOS_Percentage) {
		PC_GOS_Percentage = pC_GOS_Percentage;
	}
	/**
	 * @return the pC_COMP_Percentage
	 */
	public int getPC_COMP_Percentage() {
		return PC_COMP_Percentage;
	}
	/**
	 * @param pC_COMP_Percentage the pC_COMP_Percentage to set
	 */
	public void setPC_COMP_Percentage(int pC_COMP_Percentage) {
		PC_COMP_Percentage = pC_COMP_Percentage;
	}
	/**
	 * @return the pC_TP_Percentage
	 */
	public int getPC_TP_Percentage() {
		return PC_TP_Percentage;
	}
	/**
	 * @param pC_TP_Percentage the pC_TP_Percentage to set
	 */
	public void setPC_TP_Percentage(int pC_TP_Percentage) {
		PC_TP_Percentage = pC_TP_Percentage;
	}
	/**
	 * @return the pC_SGS
	 */
	public int getPC_SGS() {
		return PC_SGS;
	}
	/**
	 * @param pC_SGS the pC_SGS to set
	 */
	public void setPC_SGS(int pC_SGS) {
		PC_SGS = pC_SGS;
	}
	/**
	 * @return the pC_USL
	 */
	public int getPC_USL() {
		return PC_USL;
	}
	/**
	 * @param pC_USL the pC_USL to set
	 */
	public void setPC_USL(int pC_USL) {
		PC_USL = pC_USL;
	}
	/**
	 * @return the pC_LSL
	 */
	public int getPC_LSL() {
		return PC_LSL;
	}
	/**
	 * @param pC_LSL the pC_LSL to set
	 */
	public void setPC_LSL(int pC_LSL) {
		PC_LSL = pC_LSL;
	}
	/**
	 * @return the mK_YP_Score
	 */
	public int getMK_YP_Score() {
		return MK_YP_Score;
	}
	/**
	 * @param mK_YP_Score the mK_YP_Score to set
	 */
	public void setMK_YP_Score(int mK_YP_Score) {
		MK_YP_Score = mK_YP_Score;
	}
	/**
	 * @return the mK_GS_Percentage
	 */
	public int getMK_GS_Percentage() {
		return MK_GS_Percentage;
	}
	/**
	 * @param mK_GS_Percentage the mK_GS_Percentage to set
	 */
	public void setMK_GS_Percentage(int mK_GS_Percentage) {
		MK_GS_Percentage = mK_GS_Percentage;
	}
	/**
	 * @return the mK_GOS_Percentage
	 */
	public int getMK_GOS_Percentage() {
		return MK_GOS_Percentage;
	}
	/**
	 * @param mK_GOS_Percentage the mK_GOS_Percentage to set
	 */
	public void setMK_GOS_Percentage(int mK_GOS_Percentage) {
		MK_GOS_Percentage = mK_GOS_Percentage;
	}
	/**
	 * @return the mK_COMP_Percentage
	 */
	public int getMK_COMP_Percentage() {
		return MK_COMP_Percentage;
	}
	/**
	 * @param mK_COMP_Percentage the mK_COMP_Percentage to set
	 */
	public void setMK_COMP_Percentage(int mK_COMP_Percentage) {
		MK_COMP_Percentage = mK_COMP_Percentage;
	}
	/**
	 * @return the mK_TP_Percentage
	 */
	public int getMK_TP_Percentage() {
		return MK_TP_Percentage;
	}
	/**
	 * @param mK_TP_Percentage the mK_TP_Percentage to set
	 */
	public void setMK_TP_Percentage(int mK_TP_Percentage) {
		MK_TP_Percentage = mK_TP_Percentage;
	}
	/**
	 * @return the mK_SGS
	 */
	public int getMK_SGS() {
		return MK_SGS;
	}
	/**
	 * @param mK_SGS the mK_SGS to set
	 */
	public void setMK_SGS(int mK_SGS) {
		MK_SGS = mK_SGS;
	}
	/**
	 * @return the mK_USL
	 */
	public int getMK_USL() {
		return MK_USL;
	}
	/**
	 * @param mK_USL the mK_USL to set
	 */
	public void setMK_USL(int mK_USL) {
		MK_USL = mK_USL;
	}
	/**
	 * @return the mK_LSL
	 */
	public int getMK_LSL() {
		return MK_LSL;
	}
	/**
	 * @param mK_LSL the mK_LSL to set
	 */
	public void setMK_LSL(int mK_LSL) {
		MK_LSL = mK_LSL;
	}
	/**
	 * @return the eI_YP_Score
	 */
	public int getEI_YP_Score() {
		return EI_YP_Score;
	}
	/**
	 * @param eI_YP_Score the eI_YP_Score to set
	 */
	public void setEI_YP_Score(int eI_YP_Score) {
		EI_YP_Score = eI_YP_Score;
	}
	/**
	 * @return the eI_GS_Percentage
	 */
	public int getEI_GS_Percentage() {
		return EI_GS_Percentage;
	}
	/**
	 * @param eI_GS_Percentage the eI_GS_Percentage to set
	 */
	public void setEI_GS_Percentage(int eI_GS_Percentage) {
		EI_GS_Percentage = eI_GS_Percentage;
	}
	/**
	 * @return the eI_GOS_Percentage
	 */
	public int getEI_GOS_Percentage() {
		return EI_GOS_Percentage;
	}
	/**
	 * @param eI_GOS_Percentage the eI_GOS_Percentage to set
	 */
	public void setEI_GOS_Percentage(int eI_GOS_Percentage) {
		EI_GOS_Percentage = eI_GOS_Percentage;
	}
	/**
	 * @return the eI_COMP_Percentage
	 */
	public int getEI_COMP_Percentage() {
		return EI_COMP_Percentage;
	}
	/**
	 * @param eI_COMP_Percentage the eI_COMP_Percentage to set
	 */
	public void setEI_COMP_Percentage(int eI_COMP_Percentage) {
		EI_COMP_Percentage = eI_COMP_Percentage;
	}
	/**
	 * @return the eI_TP_Percentage
	 */
	public int getEI_TP_Percentage() {
		return EI_TP_Percentage;
	}
	/**
	 * @param eI_TP_Percentage the eI_TP_Percentage to set
	 */
	public void setEI_TP_Percentage(int eI_TP_Percentage) {
		EI_TP_Percentage = eI_TP_Percentage;
	}
	/**
	 * @return the eI_SGS
	 */
	public int getEI_SGS() {
		return EI_SGS;
	}
	/**
	 * @param eI_SGS the eI_SGS to set
	 */
	public void setEI_SGS(int eI_SGS) {
		EI_SGS = eI_SGS;
	}
	/**
	 * @return the eI_USL
	 */
	public int getEI_USL() {
		return EI_USL;
	}
	/**
	 * @param eI_USL the eI_USL to set
	 */
	public void setEI_USL(int eI_USL) {
		EI_USL = eI_USL;
	}
	/**
	 * @return the eI_LSL
	 */
	public int getEI_LSL() {
		return EI_LSL;
	}
	/**
	 * @param eI_LSL the eI_LSL to set
	 */
	public void setEI_LSL(int eI_LSL) {
		EI_LSL = eI_LSL;
	}
	/**
	 * @return the aS_YP_Score
	 */
	public int getAS_YP_Score() {
		return AS_YP_Score;
	}
	/**
	 * @param aS_YP_Score the aS_YP_Score to set
	 */
	public void setAS_YP_Score(int aS_YP_Score) {
		AS_YP_Score = aS_YP_Score;
	}
	/**
	 * @return the aS_GS_Percentage
	 */
	public int getAS_GS_Percentage() {
		return AS_GS_Percentage;
	}
	/**
	 * @param aS_GS_Percentage the aS_GS_Percentage to set
	 */
	public void setAS_GS_Percentage(int aS_GS_Percentage) {
		AS_GS_Percentage = aS_GS_Percentage;
	}
	/**
	 * @return the aS_GOS_Percentage
	 */
	public int getAS_GOS_Percentage() {
		return AS_GOS_Percentage;
	}
	/**
	 * @param aS_GOS_Percentage the aS_GOS_Percentage to set
	 */
	public void setAS_GOS_Percentage(int aS_GOS_Percentage) {
		AS_GOS_Percentage = aS_GOS_Percentage;
	}
	/**
	 * @return the aS_COMP_Percentage
	 */
	public int getAS_COMP_Percentage() {
		return AS_COMP_Percentage;
	}
	/**
	 * @param aS_COMP_Percentage the aS_COMP_Percentage to set
	 */
	public void setAS_COMP_Percentage(int aS_COMP_Percentage) {
		AS_COMP_Percentage = aS_COMP_Percentage;
	}
	/**
	 * @return the aS_TP_Percentage
	 */
	public int getAS_TP_Percentage() {
		return AS_TP_Percentage;
	}
	/**
	 * @param aS_TP_Percentage the aS_TP_Percentage to set
	 */
	public void setAS_TP_Percentage(int aS_TP_Percentage) {
		AS_TP_Percentage = aS_TP_Percentage;
	}
	/**
	 * @return the aS_SGS
	 */
	public int getAS_SGS() {
		return AS_SGS;
	}
	/**
	 * @param aS_SGS the aS_SGS to set
	 */
	public void setAS_SGS(int aS_SGS) {
		AS_SGS = aS_SGS;
	}
	/**
	 * @return the aS_USL
	 */
	public int getAS_USL() {
		return AS_USL;
	}
	/**
	 * @param aS_USL the aS_USL to set
	 */
	public void setAS_USL(int aS_USL) {
		AS_USL = aS_USL;
	}
	/**
	 * @return the aS_LSL
	 */
	public int getAS_LSL() {
		return AS_LSL;
	}
	/**
	 * @param aS_LSL the aS_LSL to set
	 */
	public void setAS_LSL(int aS_LSL) {
		AS_LSL = aS_LSL;
	}
	/**
	 * @return the mC_YP_Score
	 */
	public int getMC_YP_Score() {
		return MC_YP_Score;
	}
	/**
	 * @param mC_YP_Score the mC_YP_Score to set
	 */
	public void setMC_YP_Score(int mC_YP_Score) {
		MC_YP_Score = mC_YP_Score;
	}
	/**
	 * @return the mC_GS_Percentage
	 */
	public int getMC_GS_Percentage() {
		return MC_GS_Percentage;
	}
	/**
	 * @param mC_GS_Percentage the mC_GS_Percentage to set
	 */
	public void setMC_GS_Percentage(int mC_GS_Percentage) {
		MC_GS_Percentage = mC_GS_Percentage;
	}
	/**
	 * @return the mC_GOS_Percentage
	 */
	public int getMC_GOS_Percentage() {
		return MC_GOS_Percentage;
	}
	/**
	 * @param mC_GOS_Percentage the mC_GOS_Percentage to set
	 */
	public void setMC_GOS_Percentage(int mC_GOS_Percentage) {
		MC_GOS_Percentage = mC_GOS_Percentage;
	}
	/**
	 * @return the mC_COMP_Percentage
	 */
	public int getMC_COMP_Percentage() {
		return MC_COMP_Percentage;
	}
	/**
	 * @param mC_COMP_Percentage the mC_COMP_Percentage to set
	 */
	public void setMC_COMP_Percentage(int mC_COMP_Percentage) {
		MC_COMP_Percentage = mC_COMP_Percentage;
	}
	/**
	 * @return the mC_TP_Percentage
	 */
	public int getMC_TP_Percentage() {
		return MC_TP_Percentage;
	}
	/**
	 * @param mC_TP_Percentage the mC_TP_Percentage to set
	 */
	public void setMC_TP_Percentage(int mC_TP_Percentage) {
		MC_TP_Percentage = mC_TP_Percentage;
	}
	/**
	 * @return the mC_SGS
	 */
	public int getMC_SGS() {
		return MC_SGS;
	}
	/**
	 * @param mC_SGS the mC_SGS to set
	 */
	public void setMC_SGS(int mC_SGS) {
		MC_SGS = mC_SGS;
	}
	/**
	 * @return the mC_USL
	 */
	public int getMC_USL() {
		return MC_USL;
	}
	/**
	 * @param mC_USL the mC_USL to set
	 */
	public void setMC_USL(int mC_USL) {
		MC_USL = mC_USL;
	}
	/**
	 * @return the mC_LSL
	 */
	public int getMC_LSL() {
		return MC_LSL;
	}
	/**
	 * @param mC_LSL the mC_LSL to set
	 */
	public void setMC_LSL(int mC_LSL) {
		MC_LSL = mC_LSL;
	}
	/**
	 * @return the aO_YP_Score
	 */
	public int getAO_YP_Score() {
		return AO_YP_Score;
	}
	/**
	 * @param aO_YP_Score the aO_YP_Score to set
	 */
	public void setAO_YP_Score(int aO_YP_Score) {
		AO_YP_Score = aO_YP_Score;
	}
	/**
	 * @return the aO_GS_Percentage
	 */
	public int getAO_GS_Percentage() {
		return AO_GS_Percentage;
	}
	/**
	 * @param aO_GS_Percentage the aO_GS_Percentage to set
	 */
	public void setAO_GS_Percentage(int aO_GS_Percentage) {
		AO_GS_Percentage = aO_GS_Percentage;
	}
	/**
	 * @return the aO_GOS_Percentage
	 */
	public int getAO_GOS_Percentage() {
		return AO_GOS_Percentage;
	}
	/**
	 * @param aO_GOS_Percentage the aO_GOS_Percentage to set
	 */
	public void setAO_GOS_Percentage(int aO_GOS_Percentage) {
		AO_GOS_Percentage = aO_GOS_Percentage;
	}
	/**
	 * @return the aO_COMP_Percentage
	 */
	public int getAO_COMP_Percentage() {
		return AO_COMP_Percentage;
	}
	/**
	 * @param aO_COMP_Percentage the aO_COMP_Percentage to set
	 */
	public void setAO_COMP_Percentage(int aO_COMP_Percentage) {
		AO_COMP_Percentage = aO_COMP_Percentage;
	}
	/**
	 * @return the aO_TP_Percentage
	 */
	public int getAO_TP_Percentage() {
		return AO_TP_Percentage;
	}
	/**
	 * @param aO_TP_Percentage the aO_TP_Percentage to set
	 */
	public void setAO_TP_Percentage(int aO_TP_Percentage) {
		AO_TP_Percentage = aO_TP_Percentage;
	}
	/**
	 * @return the aO_SGS
	 */
	public int getAO_SGS() {
		return AO_SGS;
	}
	/**
	 * @param aO_SGS the aO_SGS to set
	 */
	public void setAO_SGS(int aO_SGS) {
		AO_SGS = aO_SGS;
	}
	/**
	 * @return the aO_USL
	 */
	public int getAO_USL() {
		return AO_USL;
	}
	/**
	 * @param aO_USL the aO_USL to set
	 */
	public void setAO_USL(int aO_USL) {
		AO_USL = aO_USL;
	}
	/**
	 * @return the aO_LSL
	 */
	public int getAO_LSL() {
		return AO_LSL;
	}
	/**
	 * @param aO_LSL the aO_LSL to set
	 */
	public void setAO_LSL(int aO_LSL) {
		AO_LSL = aO_LSL;
	}
	/**
	 * @return the americanIndian
	 */
	public int getAmericanIndian() {
		return americanIndian;
	}
	/**
	 * @param americanIndian the americanIndian to set
	 */
	public void setAmericanIndian(int americanIndian) {
		this.americanIndian = americanIndian;
	}
	/**
	 * @return the asian
	 */
	public int getAsian() {
		return asian;
	}
	/**
	 * @param asian the asian to set
	 */
	public void setAsian(int asian) {
		this.asian = asian;
	}
	/**
	 * @return the africanAmerican
	 */
	public int getAfricanAmerican() {
		return africanAmerican;
	}
	/**
	 * @param africanAmerican the africanAmerican to set
	 */
	public void setAfricanAmerican(int africanAmerican) {
		this.africanAmerican = africanAmerican;
	}
	/**
	 * @return the nativeHawiian
	 */
	public int getNativeHawiian() {
		return nativeHawiian;
	}
	/**
	 * @param nativeHawiian the nativeHawiian to set
	 */
	public void setNativeHawiian(int nativeHawiian) {
		this.nativeHawiian = nativeHawiian;
	}
	/**
	 * @return the white
	 */
	public int getWhite() {
		return white;
	}
	/**
	 * @param white the white to set
	 */
	public void setWhite(int white) {
		this.white = white;
	}
	/**
	 * @return the reserved
	 */
	public int getReserved() {
		return reserved;
	}
	/**
	 * @param reserved the reserved to set
	 */
	public void setReserved(int reserved) {
		this.reserved = reserved;
	}
	/**
	 * @return the hispanic
	 */
	public int getHispanic() {
		return hispanic;
	}
	/**
	 * @param hispanic the hispanic to set
	 */
	public void setHispanic(int hispanic) {
		this.hispanic = hispanic;
	}
	/**
	 * @return the notHispanic
	 */
	public int getNotHispanic() {
		return notHispanic;
	}
	/**
	 * @param notHispanic the notHispanic to set
	 */
	public void setNotHispanic(int notHispanic) {
		this.notHispanic = notHispanic;
	}
	
}
