package com.citm.spring.model.detail;

import java.io.Serializable;

public class RecentlyViewed implements Serializable{

	private static final long serialVersionUID = -2709797879301972638L;
	int userId;
	String jsonString;

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getJsonString() {
		return jsonString;
	}

	public void setJsonString(String jsonString) {
		this.jsonString = jsonString;
	}
}
