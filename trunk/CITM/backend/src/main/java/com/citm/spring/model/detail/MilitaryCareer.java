package com.citm.spring.model.detail;

import java.io.Serializable;

public class MilitaryCareer implements Serializable {

	private static final long serialVersionUID = 6096451295984833263L;
	String mcId, title, description, photoCredit, statement, careerClusterTitle, workEnvironmentDescription;
	Integer interestOne, interestTwo, interestThree, careerClusterId;
	char officer, enlisted, activeDuty, reserve, openToWomen, entryLevel;

	public String getCareerClusterTitle() {
		return careerClusterTitle;
	}

	public void setCareerClusterTitle(String careerClusterTitle) {
		this.careerClusterTitle = careerClusterTitle;
	}

	public Integer getCareerClusterId() {
		return careerClusterId;
	}

	public void setCareerClusterId(Integer careerClusterId) {
		this.careerClusterId = careerClusterId;
	}

	public String getWorkEnvironmentDescription() {
		return workEnvironmentDescription;
	}

	public void setWorkEnvironmentDescription(String workEnvironmentDescription) {
		this.workEnvironmentDescription = workEnvironmentDescription;
	}

	public String getMcId() {
		return mcId;
	}

	public void setMcId(String mcId) {
		this.mcId = mcId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the photoCredit
	 */
	public String getPhotoCredit() {
		return photoCredit;
	}

	/**
	 * @param photoCredit the photoCredit to set
	 */
	public void setPhotoCredit(String photoCredit) {
		this.photoCredit = photoCredit;
	}

	public String getStatement() {
		return statement;
	}

	public void setStatement(String statement) {
		this.statement = statement;
	}

	public Integer getInterestOne() {
		return interestOne;
	}

	public void setInterestOne(Integer interestOne) {
		this.interestOne = interestOne;
	}

	public Integer getInterestTwo() {
		return interestTwo;
	}

	public void setInterestTwo(Integer interestTwo) {
		this.interestTwo = interestTwo;
	}

	public Integer getInterestThree() {
		return interestThree;
	}

	public void setInterestThree(Integer interestThree) {
		this.interestThree = interestThree;
	}

	public char getOfficer() {
		return officer;
	}

	public void setOfficer(char officer) {
		this.officer = officer;
	}

	public char getEnlisted() {
		return enlisted;
	}

	public void setEnlisted(char enlisted) {
		this.enlisted = enlisted;
	}

	public char getActiveDuty() {
		return activeDuty;
	}

	public void setActiveDuty(char activeDuty) {
		this.activeDuty = activeDuty;
	}

	public char getReserve() {
		return reserve;
	}

	public void setReserve(char reserve) {
		this.reserve = reserve;
	}

	public char getOpenToWomen() {
		return openToWomen;
	}

	public void setOpenToWomen(char openToWomen) {
		this.openToWomen = openToWomen;
	}

	public char getEntryLevel() {
		return entryLevel;
	}

	public void setEntryLevel(char entryLevel) {
		this.entryLevel = entryLevel;
	}

}
