package com.citm.spring.model.favorites;

import java.io.Serializable;

public class FavoriteOccupation implements Serializable {

	private static final long serialVersionUID = -1060134172068193023L;
	private Integer id;
	private Integer userId;
	private String mcId;
	private String title;
	private String isHotJob;

	public String getMcId() {
		return mcId;
	}

	public void setMcId(String mcId) {
		this.mcId = mcId;
	}

	public String getIsHotJob() {
		return isHotJob;
	}

	public void setIsHotJob(String isHotJob) {
		this.isHotJob = isHotJob;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

}
