package com.citm.spring.model.search;

import java.io.Serializable;

public class CareerCluster implements Serializable {

	private static final long serialVersionUID = -1076319582981254787L;

	Integer id;
	String title, description;

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
}
