package com.citm.spring.model.testscore;

public class GeneralScience {

	private	int	GS_YP_Score;
	private	int	GS_GS_Percentage;
	private	int	GS_GOS_Percentage;
	private	int	GS_COMP_Percentage;
	private	int	GS_TP_Percentage;
	private	int	GS_SGS;
	private	int	GS_USL;
	private	int	GS_LSL;
	/**
	 * @return the gS_YP_Score
	 */
	public int getGS_YP_Score() {
		return GS_YP_Score;
	}
	/**
	 * @param gS_YP_Score the gS_YP_Score to set
	 */
	public void setGS_YP_Score(int gS_YP_Score) {
		GS_YP_Score = gS_YP_Score;
	}
	/**
	 * @return the gS_GS_Percentage
	 */
	public int getGS_GS_Percentage() {
		return GS_GS_Percentage;
	}
	/**
	 * @param gS_GS_Percentage the gS_GS_Percentage to set
	 */
	public void setGS_GS_Percentage(int gS_GS_Percentage) {
		GS_GS_Percentage = gS_GS_Percentage;
	}
	/**
	 * @return the gS_GOS_Percentage
	 */
	public int getGS_GOS_Percentage() {
		return GS_GOS_Percentage;
	}
	/**
	 * @param gS_GOS_Percentage the gS_GOS_Percentage to set
	 */
	public void setGS_GOS_Percentage(int gS_GOS_Percentage) {
		GS_GOS_Percentage = gS_GOS_Percentage;
	}
	/**
	 * @return the gS_COMP_Percentage
	 */
	public int getGS_COMP_Percentage() {
		return GS_COMP_Percentage;
	}
	/**
	 * @param gS_COMP_Percentage the gS_COMP_Percentage to set
	 */
	public void setGS_COMP_Percentage(int gS_COMP_Percentage) {
		GS_COMP_Percentage = gS_COMP_Percentage;
	}
	/**
	 * @return the gS_TP_Percentage
	 */
	public int getGS_TP_Percentage() {
		return GS_TP_Percentage;
	}
	/**
	 * @param gS_TP_Percentage the gS_TP_Percentage to set
	 */
	public void setGS_TP_Percentage(int gS_TP_Percentage) {
		GS_TP_Percentage = gS_TP_Percentage;
	}
	/**
	 * @return the gS_SGS
	 */
	public int getGS_SGS() {
		return GS_SGS;
	}
	/**
	 * @param gS_SGS the gS_SGS to set
	 */
	public void setGS_SGS(int gS_SGS) {
		GS_SGS = gS_SGS;
	}
	/**
	 * @return the gS_USL
	 */
	public int getGS_USL() {
		return GS_USL;
	}
	/**
	 * @param gS_USL the gS_USL to set
	 */
	public void setGS_USL(int gS_USL) {
		GS_USL = gS_USL;
	}
	/**
	 * @return the gS_LSL
	 */
	public int getGS_LSL() {
		return GS_LSL;
	}
	/**
	 * @param gS_LSL the gS_LSL to set
	 */
	public void setGS_LSL(int gS_LSL) {
		GS_LSL = gS_LSL;
	}

	
}
