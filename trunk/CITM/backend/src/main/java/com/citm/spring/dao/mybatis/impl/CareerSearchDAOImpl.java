package com.citm.spring.dao.mybatis.impl;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.citm.spring.dao.mybatis.service.CareerSearchService;
import com.citm.spring.model.search.CareerCluster;
import com.citm.spring.model.search.CareerClusterPathway;
import com.citm.spring.model.search.JobFamily;
import com.citm.spring.model.search.SaveSearch;
import com.citm.spring.model.search.Search;
import com.citm.spring.model.search.ServiceContacts;

@Repository
public class CareerSearchDAOImpl {

	@Autowired
	CareerSearchService service;

	public ArrayList<Search> getSearchList() throws Exception {
		return service.getSearchList();
	}

	public int insertSaveSearch(SaveSearch saveSearchObject) throws Exception {
		return service.insertSaveSearch(saveSearchObject);
	}

	public ArrayList<SaveSearch> getSaveSearch(Integer userId) throws Exception {
		return service.getSaveSearch(userId);
	}

	public int deleteSaveSearch(Integer userId, int id) throws Exception {
		return service.deleteSaveSearch(userId, id);
	}

	public ArrayList<CareerCluster> getCareerCluster() throws Exception {
		return service.getCareerCluster();
	}

	public ArrayList<ServiceContacts> getServiceContacts() throws Exception {
		return service.getServiceContacts();
	}

	public ArrayList<CareerClusterPathway> getCareerClusterPathway(int ccId) throws Exception {
		return service.getCareerClusterPathway(ccId);
	}

	public ArrayList<JobFamily> getJobFamily() throws Exception {
		return service.getJobFamily();
	}
}
