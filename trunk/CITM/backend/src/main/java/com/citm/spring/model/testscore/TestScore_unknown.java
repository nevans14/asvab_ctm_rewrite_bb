package com.citm.spring.model.testscore;

public class TestScore_unknown {

	//Control Information
//		private	String	aptCd;
//		private	String	statusCd;
		private	String	platform;
		private	String	ansSeqNum;
//		private	String	ssnNum;
		private	String	MepsId;
		private	String	metSiteId;
		private	String	lastName;
//		private	String	service;
//		private	String	testType;
		private	String	gender;
		private	String	educationLevel;
		private	String	certification;
//		private	String	administratorSsnNum;
		private	String	testForm;
		private	String	scoringMethod;
//		private	String	rpGrp;
//		private	String	ethnicGrp;
		private	String	administeredYear;
		private	String	administeredMonth;
		private	String	administeredDay;
//		private	String	scoredYear;
//		private	String	scoredMonth;
//		private	String	socredDay;
//		private	String	scoredHour;
//		private	String	scoredMinute;
//		private	String	scoredSecond;
//		private	String	dob;
		
		//SS ASVAB Combine Control
		private	String	seqNum;
		
		//Scoring Software Controls
//		private	String	transformationTblVersion;
//		private	String	itemParameterTblVersion;
//		private	String	priorDistTblVer;
		
		//AFQT, Raw, SS, Composites
		private	String	testCompletionCd;
//		private	String	rawScore;
		private	String	standardScores;
		private	String	afqt;
		private	String	armyScore;
		private	String	navyScore;
		private	String	airForceScore;
		private	String	marineScore;
		private	String	bookletNum;
		
		//Theta, Item Data, Scoring
//		private	String	GS_CompletionCd;
//		private	String	GS_UnroundedTheta;
//		private	String	GS_Iterations;
//		private	String	GS_ActualItemResponse;
//		private	String	GS_DichotomousItemScoring;
//		private	String	AR_CompletionCd;
//		private	String	AR_UnroundedTheta;
//		private	String	AR_Iterations;
//		private	String	AR_ActualItemResponse;
//		private	String	AR_DichotomousItemScoring;
//		private	String	WK_CompletionCd;
//		private	String	WK_UnroundedTheta;
//		private	String	WK_Iterations;
//		private	String	WK_ActualItemResponse;
//		private	String	WK_DichotomousItemScoring;
//		private	String	PC_CompletionCd;
//		private	String	PC_UnroundedTheta;
//		private	String	PC_Iterations;
//		private	String	PC_ActualItemResponse;
//		private	String	PC_DichotomousItemScoring;
//		private	String	MK_CompletionCd;
//		private	String	MK_UnroundedTheta;
//		private	String	MK_Iterations;
//		private	String	MK_ActualItemResponse;
//		private	String	MK_DichotomousItemScoring;
//		private	String	EI_CompletionCd;
//		private	String	EI_UnroundedTheta;
//		private	String	EI_Iterations;
//		private	String	EI_ActualItemResponse;
//		private	String	EI_DichotomousItemScoring;
//		private	String	AS_CompletionCd;
//		private	String	AS_UnroundedTheta;
//		private	String	AS_Iterations;
//		private	String	AS_ActualItemResponse;
//		private	String	AS_DichotomousItemScoring;
//		private	String	MC_CompletionCd;
//		private	String	MC_UnroundedTheta;
//		private	String	MC_Iterations;
//		private	String	MC_ActualItemResponse;
//		private	String	MC_DichotomousItemScoring;
//		private	String	AO_CompletionCd;
//		private	String	AO_UnroundedTheta;
//		private	String	AO_Iterations;
//		private	String	AO_ActualItemResponse;
//		private	String	AO_DichotomousItemScoring;
//		private	String	VE_CompletionCd;
//		private	String	dataEntryType;
		
		//High School Information
		private	String	schoolCode;
		private	String	schoolSessionNbr;
		private	String	schoolSpecialInstructions;
		private	String	releaseToServiceDate;
		private	String	schoolCouncellorCd;
		private	String	serviceResponsibleForScheduling;
		
		//Test Scoring Indicators And Controls
//		private	String	shotIndicator;
//		private	String	specialStudies;
//		private	String	page1Status;
//		private	String	page2Status;
//		private	String	page3Status;
		
		//Student Information
//		private	String	studentName;
//		private	String	studentAddress;
		private	String	studentCity;
		private	String	studentState;
		private	String	studentZipCode;
//		private	String	studentTelephoneNumber;
		private	String	studentIntentions;
		
		//Student Result SheetScores
		private	String	milCareerScore;
		private	String	milCareerScoreCategory;
		
		//Verbal Ability
		private	String	VA_YP_Score;
		private	String	VA_GS_Percentage;
		private	String	VA_GOS_Percentage;
		private	String	VA_COMP_Percentage;
		private	String	VA_TP_Percentage;
		private	String	VA_SGS;
		private	String	VA_USL;
		private	String	VA_LSL;
		
		//Mathematical Ability
		private	String	MA_YP_Score;
		private	String	MA_GS_Percentage;
		private	String	MA_GOS_Percentage;
		private	String	MA_COMP_Percentage;
		private	String	MA_TP_Percentage;
		private	String	MA_SGS;
		private	String	MA_USL;
		private	String	MA_LSL;
		
		//Science & Technical Ability
		private	String	TEC_YP_Score;
		private	String	TEC_GS_Percentage;
		private	String	TEC_GOS_Percentage;
		private	String	TEC_COMP_Percentage;
		private	String	TEC_TP_Percentage;
		private	String	TEC_SGS;
		private	String	TEC_USL;
		private	String	TEC_LSL;
		
		//General Science
		private	String	GS_YP_Score;
		private	String	GS_GS_Percentage;
		private	String	GS_GOS_Percentage;
		private	String	GS_COMP_Percentage;
		private	String	GS_TP_Percentage;
		private	String	GS_SGS;
		private	String	GS_USL;
		private	String	GS_LSL;
		
		//Arithmetic Reasoning
		private	String	AR_YP_Score;
		private	String	AR_GS_Percentage;
		private	String	AR_GOS_Percentage;
		private	String	AR_COMP_Percentage;
		private	String	AR_TP_Percentage;
		private	String	AR_SGS;
		private	String	AR_USL;
		private	String	AR_LSL;
		
		//Word Knowledge
		private	String	WK_YP_Score;
		private	String	WK_GS_Percentage;
		private	String	WK_GOS_Percentage;
		private	String	WK_COMP_Percentage;
		private	String	WK_TP_Percentage;
		private	String	WK_SGS;
		private	String	WK_USL;
		private	String	WK_LSL;
		
		//Paragraph Comprehension
		private	String	PC_YP_Score;
		private	String	PC_GS_Percentage;
		private	String	PC_GOS_Percentage;
		private	String	PC_COMP_Percentage;
		private	String	PC_TP_Percentage;
		private	String	PC_SGS;
		private	String	PC_USL;
		private	String	PC_LSL;
		
		//Mathematics Knowledge
		private	String	MK_YP_Score;
		private	String	MK_GS_Percentage;
		private	String	MK_GOS_Percentage;
		private	String	MK_COMP_Percentage;
		private	String	MK_TP_Percentage;
		private	String	MK_SGS;
		private	String	MK_USL;
		private	String	MK_LSL;
		
		
		//Electronics Information
		private	String	EI_YP_Score;
		private	String	EI_GS_Percentage;
		private	String	EI_GOS_Percentage;
		private	String	EI_COMP_Percentage;
		private	String	EI_TP_Percentage;
		private	String	EI_SGS;
		private	String	EI_USL;
		private	String	EI_LSL;
		
		//Auto Shop Information
		private	String	AS_YP_Score;
		private	String	AS_GS_Percentage;
		private	String	AS_GOS_Percentage;
		private	String	AS_COMP_Percentage;
		private	String	AS_TP_Percentage;
		private	String	AS_SGS;
		private	String	AS_USL;
		private	String	AS_LSL;
		
		//Mechanical Comprehension
		private	String	MC_YP_Score;
		private	String	MC_GS_Percentage;
		private	String	MC_GOS_Percentage;
		private	String	MC_COMP_Percentage;
		private	String	MC_TP_Percentage;
		private	String	MC_SGS;
		private	String	MC_USL;
		private	String	MC_LSL;
		
		//AssemblingObjects
		private	String	AO_YP_Score;
		private	String	AO_GS_Percentage;
		private	String	AO_GOS_Percentage;
		private	String	AO_COMP_Percentage;
		private	String	AO_TP_Percentage;
		private	String	AO_SGS;
		private	String	AO_USL;
		private	String	AO_LSL;
		
		//Race Fields
		private	String	americanIndian;
		private	String	asian;
		private	String	africanAmerican;
		private	String	nativeHawiian;
		private	String	white;
		private	String	reserved;
		private	String	hispanic;
		private	String	notHispanic;

		/**
		 * @return the platform
		 */
		public String getPlatform() {
			return platform;
		}
		/**
		 * @param platform the platform to set
		 */
		public void setPlatform(String platform) {
			this.platform = platform;
		}
		/**
		 * @return the ansSeqNum
		 */
		public String getAnsSeqNum() {
			return ansSeqNum;
		}
		/**
		 * @param ansSeqNum the ansSeqNum to set
		 */
		public void setAnsSeqNum(String ansSeqNum) {
			this.ansSeqNum = ansSeqNum;
		}
		/**
		 * @return the mepsId
		 */
		public String getMepsId() {
			return MepsId;
		}
		/**
		 * @param mepsId the mepsId to set
		 */
		public void setMepsId(String mepsId) {
			MepsId = mepsId;
		}
		/**
		 * @return the metSiteId
		 */
		public String getMetSiteId() {
			return metSiteId;
		}
		/**
		 * @param metSiteId the metSiteId to set
		 */
		public void setMetSiteId(String metSiteId) {
			this.metSiteId = metSiteId;
		}
		/**
		 * @return the lastName
		 */
		public String getLastName() {
			return lastName;
		}
		/**
		 * @param lastName the lastName to set
		 */
		public void setLastName(String lastName) {
			this.lastName = lastName;
		}
		/**
		 * @return the gender
		 */
		public String getGender() {
			return gender;
		}
		/**
		 * @param gender the gender to set
		 */
		public void setGender(String gender) {
			this.gender = gender;
		}
		/**
		 * @return the educationLevel
		 */
		public String getEducationLevel() {
			return educationLevel;
		}
		/**
		 * @param educationLevel the educationLevel to set
		 */
		public void setEducationLevel(String educationLevel) {
			this.educationLevel = educationLevel;
		}
		/**
		 * @return the certification
		 */
		public String getCertification() {
			return certification;
		}
		/**
		 * @param certification the certification to set
		 */
		public void setCertification(String certification) {
			this.certification = certification;
		}
		/**
		 * @return the testForm
		 */
		public String getTestForm() {
			return testForm;
		}
		/**
		 * @param testForm the testForm to set
		 */
		public void setTestForm(String testForm) {
			this.testForm = testForm;
		}
		/**
		 * @return the scoringMethod
		 */
		public String getScoringMethod() {
			return scoringMethod;
		}
		/**
		 * @param scoringMethod the scoringMethod to set
		 */
		public void setScoringMethod(String scoringMethod) {
			this.scoringMethod = scoringMethod;
		}
		/**
		 * @return the administeredYear
		 */
		public String getAdministeredYear() {
			return administeredYear;
		}
		/**
		 * @param administeredYear the administeredYear to set
		 */
		public void setAdministeredYear(String administeredYear) {
			this.administeredYear = administeredYear;
		}
		/**
		 * @return the administeredMonth
		 */
		public String getAdministeredMonth() {
			return administeredMonth;
		}
		/**
		 * @param administeredMonth the administeredMonth to set
		 */
		public void setAdministeredMonth(String administeredMonth) {
			this.administeredMonth = administeredMonth;
		}
		/**
		 * @return the administeredDay
		 */
		public String getAdministeredDay() {
			return administeredDay;
		}
		/**
		 * @param administeredDay the administeredDay to set
		 */
		public void setAdministeredDay(String administeredDay) {
			this.administeredDay = administeredDay;
		}
		/**
		 * @return the seqNum
		 */
		public String getSeqNum() {
			return seqNum;
		}
		/**
		 * @param seqNum the seqNum to set
		 */
		public void setSeqNum(String seqNum) {
			this.seqNum = seqNum;
		}
		/**
		 * @return the testCompletionCd
		 */
		public String getTestCompletionCd() {
			return testCompletionCd;
		}
		/**
		 * @param testCompletionCd the testCompletionCd to set
		 */
		public void setTestCompletionCd(String testCompletionCd) {
			this.testCompletionCd = testCompletionCd;
		}
		/**
		 * @return the standardScores
		 */
		public String getStandardScores() {
			return standardScores;
		}
		/**
		 * @param standardScores the standardScores to set
		 */
		public void setStandardScores(String standardScores) {
			this.standardScores = standardScores;
		}
		/**
		 * @return the afqt
		 */
		public String getAfqt() {
			return afqt;
		}
		/**
		 * @param afqt the afqt to set
		 */
		public void setAfqt(String afqt) {
			this.afqt = afqt;
		}
		/**
		 * @return the armyScore
		 */
		public String getArmyScore() {
			return armyScore;
		}
		/**
		 * @param armyScore the armyScore to set
		 */
		public void setArmyScore(String armyScore) {
			this.armyScore = armyScore;
		}
		/**
		 * @return the navyScore
		 */
		public String getNavyScore() {
			return navyScore;
		}
		/**
		 * @param navyScore the navyScore to set
		 */
		public void setNavyScore(String navyScore) {
			this.navyScore = navyScore;
		}
		/**
		 * @return the airForceScore
		 */
		public String getAirForceScore() {
			return airForceScore;
		}
		/**
		 * @param airForceScore the airForceScore to set
		 */
		public void setAirForceScore(String airForceScore) {
			this.airForceScore = airForceScore;
		}
		/**
		 * @return the marineScore
		 */
		public String getMarineScore() {
			return marineScore;
		}
		/**
		 * @param marineScore the marineScore to set
		 */
		public void setMarineScore(String marineScore) {
			this.marineScore = marineScore;
		}
		/**
		 * @return the bookletNum
		 */
		public String getBookletNum() {
			return bookletNum;
		}
		/**
		 * @param bookletNum the bookletNum to set
		 */
		public void setBookletNum(String bookletNum) {
			this.bookletNum = bookletNum;
		}
		/**
		 * @return the schoolCode
		 */
		public String getSchoolCode() {
			return schoolCode;
		}
		/**
		 * @param schoolCode the schoolCode to set
		 */
		public void setSchoolCode(String schoolCode) {
			this.schoolCode = schoolCode;
		}
		/**
		 * @return the schoolSessionNbr
		 */
		public String getSchoolSessionNbr() {
			return schoolSessionNbr;
		}
		/**
		 * @param schoolSessionNbr the schoolSessionNbr to set
		 */
		public void setSchoolSessionNbr(String schoolSessionNbr) {
			this.schoolSessionNbr = schoolSessionNbr;
		}
		/**
		 * @return the schoolSpecialInstructions
		 */
		public String getSchoolSpecialInstructions() {
			return schoolSpecialInstructions;
		}
		/**
		 * @param schoolSpecialInstructions the schoolSpecialInstructions to set
		 */
		public void setSchoolSpecialInstructions(String schoolSpecialInstructions) {
			this.schoolSpecialInstructions = schoolSpecialInstructions;
		}
		/**
		 * @return the releaseToServiceDate
		 */
		public String getReleaseToServiceDate() {
			return releaseToServiceDate;
		}
		/**
		 * @param releaseToServiceDate the releaseToServiceDate to set
		 */
		public void setReleaseToServiceDate(String releaseToServiceDate) {
			this.releaseToServiceDate = releaseToServiceDate;
		}
		/**
		 * @return the schoolCouncellorCd
		 */
		public String getSchoolCouncellorCd() {
			return schoolCouncellorCd;
		}
		/**
		 * @param schoolCouncellorCd the schoolCouncellorCd to set
		 */
		public void setSchoolCouncellorCd(String schoolCouncellorCd) {
			this.schoolCouncellorCd = schoolCouncellorCd;
		}
		/**
		 * @return the serviceResponsibleForScheduling
		 */
		public String getServiceResponsibleForScheduling() {
			return serviceResponsibleForScheduling;
		}
		/**
		 * @param serviceResponsibleForScheduling the serviceResponsibleForScheduling to set
		 */
		public void setServiceResponsibleForScheduling(
				String serviceResponsibleForScheduling) {
			this.serviceResponsibleForScheduling = serviceResponsibleForScheduling;
		}
		/**
		 * @return the studentCity
		 */
		public String getStudentCity() {
			return studentCity;
		}
		/**
		 * @param studentCity the studentCity to set
		 */
		public void setStudentCity(String studentCity) {
			this.studentCity = studentCity;
		}
		/**
		 * @return the studentState
		 */
		public String getStudentState() {
			return studentState;
		}
		/**
		 * @param studentState the studentState to set
		 */
		public void setStudentState(String studentState) {
			this.studentState = studentState;
		}
		/**
		 * @return the studentZipCode
		 */
		public String getStudentZipCode() {
			return studentZipCode;
		}
		/**
		 * @param studentZipCode the studentZipCode to set
		 */
		public void setStudentZipCode(String studentZipCode) {
			this.studentZipCode = studentZipCode;
		}
		/**
		 * @return the studentIntentions
		 */
		public String getStudentIntentions() {
			return studentIntentions;
		}
		/**
		 * @param studentIntentions the studentIntentions to set
		 */
		public void setStudentIntentions(String studentIntentions) {
			this.studentIntentions = studentIntentions;
		}
		/**
		 * @return the milCareerScore
		 */
		public String getMilCareerScore() {
			return milCareerScore;
		}
		/**
		 * @param milCareerScore the milCareerScore to set
		 */
		public void setMilCareerScore(String milCareerScore) {
			this.milCareerScore = milCareerScore;
		}
		/**
		 * @return the milCareerScoreCategory
		 */
		public String getMilCareerScoreCategory() {
			return milCareerScoreCategory;
		}
		/**
		 * @param milCareerScoreCategory the milCareerScoreCategory to set
		 */
		public void setMilCareerScoreCategory(String milCareerScoreCategory) {
			this.milCareerScoreCategory = milCareerScoreCategory;
		}
		/**
		 * @return the vA_YP_Score
		 */
		public String getVA_YP_Score() {
			return VA_YP_Score;
		}
		/**
		 * @param vA_YP_Score the vA_YP_Score to set
		 */
		public void setVA_YP_Score(String vA_YP_Score) {
			VA_YP_Score = vA_YP_Score;
		}
		/**
		 * @return the vA_GS_Percentage
		 */
		public String getVA_GS_Percentage() {
			return VA_GS_Percentage;
		}
		/**
		 * @param vA_GS_Percentage the vA_GS_Percentage to set
		 */
		public void setVA_GS_Percentage(String vA_GS_Percentage) {
			VA_GS_Percentage = vA_GS_Percentage;
		}
		/**
		 * @return the vA_GOS_Percentage
		 */
		public String getVA_GOS_Percentage() {
			return VA_GOS_Percentage;
		}
		/**
		 * @param vA_GOS_Percentage the vA_GOS_Percentage to set
		 */
		public void setVA_GOS_Percentage(String vA_GOS_Percentage) {
			VA_GOS_Percentage = vA_GOS_Percentage;
		}
		/**
		 * @return the vA_COMP_Percentage
		 */
		public String getVA_COMP_Percentage() {
			return VA_COMP_Percentage;
		}
		/**
		 * @param vA_COMP_Percentage the vA_COMP_Percentage to set
		 */
		public void setVA_COMP_Percentage(String vA_COMP_Percentage) {
			VA_COMP_Percentage = vA_COMP_Percentage;
		}
		/**
		 * @return the vA_TP_Percentage
		 */
		public String getVA_TP_Percentage() {
			return VA_TP_Percentage;
		}
		/**
		 * @param vA_TP_Percentage the vA_TP_Percentage to set
		 */
		public void setVA_TP_Percentage(String vA_TP_Percentage) {
			VA_TP_Percentage = vA_TP_Percentage;
		}
		/**
		 * @return the vA_SGS
		 */
		public String getVA_SGS() {
			return VA_SGS;
		}
		/**
		 * @param vA_SGS the vA_SGS to set
		 */
		public void setVA_SGS(String vA_SGS) {
			VA_SGS = vA_SGS;
		}
		/**
		 * @return the vA_USL
		 */
		public String getVA_USL() {
			return VA_USL;
		}
		/**
		 * @param vA_USL the vA_USL to set
		 */
		public void setVA_USL(String vA_USL) {
			VA_USL = vA_USL;
		}
		/**
		 * @return the vA_LSL
		 */
		public String getVA_LSL() {
			return VA_LSL;
		}
		/**
		 * @param vA_LSL the vA_LSL to set
		 */
		public void setVA_LSL(String vA_LSL) {
			VA_LSL = vA_LSL;
		}
		/**
		 * @return the mA_YP_Score
		 */
		public String getMA_YP_Score() {
			return MA_YP_Score;
		}
		/**
		 * @param mA_YP_Score the mA_YP_Score to set
		 */
		public void setMA_YP_Score(String mA_YP_Score) {
			MA_YP_Score = mA_YP_Score;
		}
		/**
		 * @return the mA_GS_Percentage
		 */
		public String getMA_GS_Percentage() {
			return MA_GS_Percentage;
		}
		/**
		 * @param mA_GS_Percentage the mA_GS_Percentage to set
		 */
		public void setMA_GS_Percentage(String mA_GS_Percentage) {
			MA_GS_Percentage = mA_GS_Percentage;
		}
		/**
		 * @return the mA_GOS_Percentage
		 */
		public String getMA_GOS_Percentage() {
			return MA_GOS_Percentage;
		}
		/**
		 * @param mA_GOS_Percentage the mA_GOS_Percentage to set
		 */
		public void setMA_GOS_Percentage(String mA_GOS_Percentage) {
			MA_GOS_Percentage = mA_GOS_Percentage;
		}
		/**
		 * @return the mA_COMP_Percentage
		 */
		public String getMA_COMP_Percentage() {
			return MA_COMP_Percentage;
		}
		/**
		 * @param mA_COMP_Percentage the mA_COMP_Percentage to set
		 */
		public void setMA_COMP_Percentage(String mA_COMP_Percentage) {
			MA_COMP_Percentage = mA_COMP_Percentage;
		}
		/**
		 * @return the mA_TP_Percentage
		 */
		public String getMA_TP_Percentage() {
			return MA_TP_Percentage;
		}
		/**
		 * @param mA_TP_Percentage the mA_TP_Percentage to set
		 */
		public void setMA_TP_Percentage(String mA_TP_Percentage) {
			MA_TP_Percentage = mA_TP_Percentage;
		}
		/**
		 * @return the mA_SGS
		 */
		public String getMA_SGS() {
			return MA_SGS;
		}
		/**
		 * @param mA_SGS the mA_SGS to set
		 */
		public void setMA_SGS(String mA_SGS) {
			MA_SGS = mA_SGS;
		}
		/**
		 * @return the mA_USL
		 */
		public String getMA_USL() {
			return MA_USL;
		}
		/**
		 * @param mA_USL the mA_USL to set
		 */
		public void setMA_USL(String mA_USL) {
			MA_USL = mA_USL;
		}
		/**
		 * @return the mA_LSL
		 */
		public String getMA_LSL() {
			return MA_LSL;
		}
		/**
		 * @param mA_LSL the mA_LSL to set
		 */
		public void setMA_LSL(String mA_LSL) {
			MA_LSL = mA_LSL;
		}
		/**
		 * @return the tEC_YP_Score
		 */
		public String getTEC_YP_Score() {
			return TEC_YP_Score;
		}
		/**
		 * @param tEC_YP_Score the tEC_YP_Score to set
		 */
		public void setTEC_YP_Score(String tEC_YP_Score) {
			TEC_YP_Score = tEC_YP_Score;
		}
		/**
		 * @return the tEC_GS_Percentage
		 */
		public String getTEC_GS_Percentage() {
			return TEC_GS_Percentage;
		}
		/**
		 * @param tEC_GS_Percentage the tEC_GS_Percentage to set
		 */
		public void setTEC_GS_Percentage(String tEC_GS_Percentage) {
			TEC_GS_Percentage = tEC_GS_Percentage;
		}
		/**
		 * @return the tEC_GOS_Percentage
		 */
		public String getTEC_GOS_Percentage() {
			return TEC_GOS_Percentage;
		}
		/**
		 * @param tEC_GOS_Percentage the tEC_GOS_Percentage to set
		 */
		public void setTEC_GOS_Percentage(String tEC_GOS_Percentage) {
			TEC_GOS_Percentage = tEC_GOS_Percentage;
		}
		/**
		 * @return the tEC_COMP_Percentage
		 */
		public String getTEC_COMP_Percentage() {
			return TEC_COMP_Percentage;
		}
		/**
		 * @param tEC_COMP_Percentage the tEC_COMP_Percentage to set
		 */
		public void setTEC_COMP_Percentage(String tEC_COMP_Percentage) {
			TEC_COMP_Percentage = tEC_COMP_Percentage;
		}
		/**
		 * @return the tEC_TP_Percentage
		 */
		public String getTEC_TP_Percentage() {
			return TEC_TP_Percentage;
		}
		/**
		 * @param tEC_TP_Percentage the tEC_TP_Percentage to set
		 */
		public void setTEC_TP_Percentage(String tEC_TP_Percentage) {
			TEC_TP_Percentage = tEC_TP_Percentage;
		}
		/**
		 * @return the tEC_SGS
		 */
		public String getTEC_SGS() {
			return TEC_SGS;
		}
		/**
		 * @param tEC_SGS the tEC_SGS to set
		 */
		public void setTEC_SGS(String tEC_SGS) {
			TEC_SGS = tEC_SGS;
		}
		/**
		 * @return the tEC_USL
		 */
		public String getTEC_USL() {
			return TEC_USL;
		}
		/**
		 * @param tEC_USL the tEC_USL to set
		 */
		public void setTEC_USL(String tEC_USL) {
			TEC_USL = tEC_USL;
		}
		/**
		 * @return the tEC_LSL
		 */
		public String getTEC_LSL() {
			return TEC_LSL;
		}
		/**
		 * @param tEC_LSL the tEC_LSL to set
		 */
		public void setTEC_LSL(String tEC_LSL) {
			TEC_LSL = tEC_LSL;
		}
		/**
		 * @return the gS_YP_Score
		 */
		public String getGS_YP_Score() {
			return GS_YP_Score;
		}
		/**
		 * @param gS_YP_Score the gS_YP_Score to set
		 */
		public void setGS_YP_Score(String gS_YP_Score) {
			GS_YP_Score = gS_YP_Score;
		}
		/**
		 * @return the gS_GS_Percentage
		 */
		public String getGS_GS_Percentage() {
			return GS_GS_Percentage;
		}
		/**
		 * @param gS_GS_Percentage the gS_GS_Percentage to set
		 */
		public void setGS_GS_Percentage(String gS_GS_Percentage) {
			GS_GS_Percentage = gS_GS_Percentage;
		}
		/**
		 * @return the gS_GOS_Percentage
		 */
		public String getGS_GOS_Percentage() {
			return GS_GOS_Percentage;
		}
		/**
		 * @param gS_GOS_Percentage the gS_GOS_Percentage to set
		 */
		public void setGS_GOS_Percentage(String gS_GOS_Percentage) {
			GS_GOS_Percentage = gS_GOS_Percentage;
		}
		/**
		 * @return the gS_COMP_Percentage
		 */
		public String getGS_COMP_Percentage() {
			return GS_COMP_Percentage;
		}
		/**
		 * @param gS_COMP_Percentage the gS_COMP_Percentage to set
		 */
		public void setGS_COMP_Percentage(String gS_COMP_Percentage) {
			GS_COMP_Percentage = gS_COMP_Percentage;
		}
		/**
		 * @return the gS_TP_Percentage
		 */
		public String getGS_TP_Percentage() {
			return GS_TP_Percentage;
		}
		/**
		 * @param gS_TP_Percentage the gS_TP_Percentage to set
		 */
		public void setGS_TP_Percentage(String gS_TP_Percentage) {
			GS_TP_Percentage = gS_TP_Percentage;
		}
		/**
		 * @return the gS_SGS
		 */
		public String getGS_SGS() {
			return GS_SGS;
		}
		/**
		 * @param gS_SGS the gS_SGS to set
		 */
		public void setGS_SGS(String gS_SGS) {
			GS_SGS = gS_SGS;
		}
		/**
		 * @return the gS_USL
		 */
		public String getGS_USL() {
			return GS_USL;
		}
		/**
		 * @param gS_USL the gS_USL to set
		 */
		public void setGS_USL(String gS_USL) {
			GS_USL = gS_USL;
		}
		/**
		 * @return the gS_LSL
		 */
		public String getGS_LSL() {
			return GS_LSL;
		}
		/**
		 * @param gS_LSL the gS_LSL to set
		 */
		public void setGS_LSL(String gS_LSL) {
			GS_LSL = gS_LSL;
		}
		/**
		 * @return the aR_YP_Score
		 */
		public String getAR_YP_Score() {
			return AR_YP_Score;
		}
		/**
		 * @param aR_YP_Score the aR_YP_Score to set
		 */
		public void setAR_YP_Score(String aR_YP_Score) {
			AR_YP_Score = aR_YP_Score;
		}
		/**
		 * @return the aR_GS_Percentage
		 */
		public String getAR_GS_Percentage() {
			return AR_GS_Percentage;
		}
		/**
		 * @param aR_GS_Percentage the aR_GS_Percentage to set
		 */
		public void setAR_GS_Percentage(String aR_GS_Percentage) {
			AR_GS_Percentage = aR_GS_Percentage;
		}
		/**
		 * @return the aR_GOS_Percentage
		 */
		public String getAR_GOS_Percentage() {
			return AR_GOS_Percentage;
		}
		/**
		 * @param aR_GOS_Percentage the aR_GOS_Percentage to set
		 */
		public void setAR_GOS_Percentage(String aR_GOS_Percentage) {
			AR_GOS_Percentage = aR_GOS_Percentage;
		}
		/**
		 * @return the aR_COMP_Percentage
		 */
		public String getAR_COMP_Percentage() {
			return AR_COMP_Percentage;
		}
		/**
		 * @param aR_COMP_Percentage the aR_COMP_Percentage to set
		 */
		public void setAR_COMP_Percentage(String aR_COMP_Percentage) {
			AR_COMP_Percentage = aR_COMP_Percentage;
		}
		/**
		 * @return the aR_TP_Percentage
		 */
		public String getAR_TP_Percentage() {
			return AR_TP_Percentage;
		}
		/**
		 * @param aR_TP_Percentage the aR_TP_Percentage to set
		 */
		public void setAR_TP_Percentage(String aR_TP_Percentage) {
			AR_TP_Percentage = aR_TP_Percentage;
		}
		/**
		 * @return the aR_SGS
		 */
		public String getAR_SGS() {
			return AR_SGS;
		}
		/**
		 * @param aR_SGS the aR_SGS to set
		 */
		public void setAR_SGS(String aR_SGS) {
			AR_SGS = aR_SGS;
		}
		/**
		 * @return the aR_USL
		 */
		public String getAR_USL() {
			return AR_USL;
		}
		/**
		 * @param aR_USL the aR_USL to set
		 */
		public void setAR_USL(String aR_USL) {
			AR_USL = aR_USL;
		}
		/**
		 * @return the aR_LSL
		 */
		public String getAR_LSL() {
			return AR_LSL;
		}
		/**
		 * @param aR_LSL the aR_LSL to set
		 */
		public void setAR_LSL(String aR_LSL) {
			AR_LSL = aR_LSL;
		}
		/**
		 * @return the wK_YP_Score
		 */
		public String getWK_YP_Score() {
			return WK_YP_Score;
		}
		/**
		 * @param wK_YP_Score the wK_YP_Score to set
		 */
		public void setWK_YP_Score(String wK_YP_Score) {
			WK_YP_Score = wK_YP_Score;
		}
		/**
		 * @return the wK_GS_Percentage
		 */
		public String getWK_GS_Percentage() {
			return WK_GS_Percentage;
		}
		/**
		 * @param wK_GS_Percentage the wK_GS_Percentage to set
		 */
		public void setWK_GS_Percentage(String wK_GS_Percentage) {
			WK_GS_Percentage = wK_GS_Percentage;
		}
		/**
		 * @return the wK_GOS_Percentage
		 */
		public String getWK_GOS_Percentage() {
			return WK_GOS_Percentage;
		}
		/**
		 * @param wK_GOS_Percentage the wK_GOS_Percentage to set
		 */
		public void setWK_GOS_Percentage(String wK_GOS_Percentage) {
			WK_GOS_Percentage = wK_GOS_Percentage;
		}
		/**
		 * @return the wK_COMP_Percentage
		 */
		public String getWK_COMP_Percentage() {
			return WK_COMP_Percentage;
		}
		/**
		 * @param wK_COMP_Percentage the wK_COMP_Percentage to set
		 */
		public void setWK_COMP_Percentage(String wK_COMP_Percentage) {
			WK_COMP_Percentage = wK_COMP_Percentage;
		}
		/**
		 * @return the wK_TP_Percentage
		 */
		public String getWK_TP_Percentage() {
			return WK_TP_Percentage;
		}
		/**
		 * @param wK_TP_Percentage the wK_TP_Percentage to set
		 */
		public void setWK_TP_Percentage(String wK_TP_Percentage) {
			WK_TP_Percentage = wK_TP_Percentage;
		}
		/**
		 * @return the wK_SGS
		 */
		public String getWK_SGS() {
			return WK_SGS;
		}
		/**
		 * @param wK_SGS the wK_SGS to set
		 */
		public void setWK_SGS(String wK_SGS) {
			WK_SGS = wK_SGS;
		}
		/**
		 * @return the wK_USL
		 */
		public String getWK_USL() {
			return WK_USL;
		}
		/**
		 * @param wK_USL the wK_USL to set
		 */
		public void setWK_USL(String wK_USL) {
			WK_USL = wK_USL;
		}
		/**
		 * @return the wK_LSL
		 */
		public String getWK_LSL() {
			return WK_LSL;
		}
		/**
		 * @param wK_LSL the wK_LSL to set
		 */
		public void setWK_LSL(String wK_LSL) {
			WK_LSL = wK_LSL;
		}
		/**
		 * @return the pC_YP_Score
		 */
		public String getPC_YP_Score() {
			return PC_YP_Score;
		}
		/**
		 * @param pC_YP_Score the pC_YP_Score to set
		 */
		public void setPC_YP_Score(String pC_YP_Score) {
			PC_YP_Score = pC_YP_Score;
		}
		/**
		 * @return the pC_GS_Percentage
		 */
		public String getPC_GS_Percentage() {
			return PC_GS_Percentage;
		}
		/**
		 * @param pC_GS_Percentage the pC_GS_Percentage to set
		 */
		public void setPC_GS_Percentage(String pC_GS_Percentage) {
			PC_GS_Percentage = pC_GS_Percentage;
		}
		/**
		 * @return the pC_GOS_Percentage
		 */
		public String getPC_GOS_Percentage() {
			return PC_GOS_Percentage;
		}
		/**
		 * @param pC_GOS_Percentage the pC_GOS_Percentage to set
		 */
		public void setPC_GOS_Percentage(String pC_GOS_Percentage) {
			PC_GOS_Percentage = pC_GOS_Percentage;
		}
		/**
		 * @return the pC_COMP_Percentage
		 */
		public String getPC_COMP_Percentage() {
			return PC_COMP_Percentage;
		}
		/**
		 * @param pC_COMP_Percentage the pC_COMP_Percentage to set
		 */
		public void setPC_COMP_Percentage(String pC_COMP_Percentage) {
			PC_COMP_Percentage = pC_COMP_Percentage;
		}
		/**
		 * @return the pC_TP_Percentage
		 */
		public String getPC_TP_Percentage() {
			return PC_TP_Percentage;
		}
		/**
		 * @param pC_TP_Percentage the pC_TP_Percentage to set
		 */
		public void setPC_TP_Percentage(String pC_TP_Percentage) {
			PC_TP_Percentage = pC_TP_Percentage;
		}
		/**
		 * @return the pC_SGS
		 */
		public String getPC_SGS() {
			return PC_SGS;
		}
		/**
		 * @param pC_SGS the pC_SGS to set
		 */
		public void setPC_SGS(String pC_SGS) {
			PC_SGS = pC_SGS;
		}
		/**
		 * @return the pC_USL
		 */
		public String getPC_USL() {
			return PC_USL;
		}
		/**
		 * @param pC_USL the pC_USL to set
		 */
		public void setPC_USL(String pC_USL) {
			PC_USL = pC_USL;
		}
		/**
		 * @return the pC_LSL
		 */
		public String getPC_LSL() {
			return PC_LSL;
		}
		/**
		 * @param pC_LSL the pC_LSL to set
		 */
		public void setPC_LSL(String pC_LSL) {
			PC_LSL = pC_LSL;
		}
		/**
		 * @return the mK_YP_Score
		 */
		public String getMK_YP_Score() {
			return MK_YP_Score;
		}
		/**
		 * @param mK_YP_Score the mK_YP_Score to set
		 */
		public void setMK_YP_Score(String mK_YP_Score) {
			MK_YP_Score = mK_YP_Score;
		}
		/**
		 * @return the mK_GS_Percentage
		 */
		public String getMK_GS_Percentage() {
			return MK_GS_Percentage;
		}
		/**
		 * @param mK_GS_Percentage the mK_GS_Percentage to set
		 */
		public void setMK_GS_Percentage(String mK_GS_Percentage) {
			MK_GS_Percentage = mK_GS_Percentage;
		}
		/**
		 * @return the mK_GOS_Percentage
		 */
		public String getMK_GOS_Percentage() {
			return MK_GOS_Percentage;
		}
		/**
		 * @param mK_GOS_Percentage the mK_GOS_Percentage to set
		 */
		public void setMK_GOS_Percentage(String mK_GOS_Percentage) {
			MK_GOS_Percentage = mK_GOS_Percentage;
		}
		/**
		 * @return the mK_COMP_Percentage
		 */
		public String getMK_COMP_Percentage() {
			return MK_COMP_Percentage;
		}
		/**
		 * @param mK_COMP_Percentage the mK_COMP_Percentage to set
		 */
		public void setMK_COMP_Percentage(String mK_COMP_Percentage) {
			MK_COMP_Percentage = mK_COMP_Percentage;
		}
		/**
		 * @return the mK_TP_Percentage
		 */
		public String getMK_TP_Percentage() {
			return MK_TP_Percentage;
		}
		/**
		 * @param mK_TP_Percentage the mK_TP_Percentage to set
		 */
		public void setMK_TP_Percentage(String mK_TP_Percentage) {
			MK_TP_Percentage = mK_TP_Percentage;
		}
		/**
		 * @return the mK_SGS
		 */
		public String getMK_SGS() {
			return MK_SGS;
		}
		/**
		 * @param mK_SGS the mK_SGS to set
		 */
		public void setMK_SGS(String mK_SGS) {
			MK_SGS = mK_SGS;
		}
		/**
		 * @return the mK_USL
		 */
		public String getMK_USL() {
			return MK_USL;
		}
		/**
		 * @param mK_USL the mK_USL to set
		 */
		public void setMK_USL(String mK_USL) {
			MK_USL = mK_USL;
		}
		/**
		 * @return the mK_LSL
		 */
		public String getMK_LSL() {
			return MK_LSL;
		}
		/**
		 * @param mK_LSL the mK_LSL to set
		 */
		public void setMK_LSL(String mK_LSL) {
			MK_LSL = mK_LSL;
		}
		/**
		 * @return the eI_YP_Score
		 */
		public String getEI_YP_Score() {
			return EI_YP_Score;
		}
		/**
		 * @param eI_YP_Score the eI_YP_Score to set
		 */
		public void setEI_YP_Score(String eI_YP_Score) {
			EI_YP_Score = eI_YP_Score;
		}
		/**
		 * @return the eI_GS_Percentage
		 */
		public String getEI_GS_Percentage() {
			return EI_GS_Percentage;
		}
		/**
		 * @param eI_GS_Percentage the eI_GS_Percentage to set
		 */
		public void setEI_GS_Percentage(String eI_GS_Percentage) {
			EI_GS_Percentage = eI_GS_Percentage;
		}
		/**
		 * @return the eI_GOS_Percentage
		 */
		public String getEI_GOS_Percentage() {
			return EI_GOS_Percentage;
		}
		/**
		 * @param eI_GOS_Percentage the eI_GOS_Percentage to set
		 */
		public void setEI_GOS_Percentage(String eI_GOS_Percentage) {
			EI_GOS_Percentage = eI_GOS_Percentage;
		}
		/**
		 * @return the eI_COMP_Percentage
		 */
		public String getEI_COMP_Percentage() {
			return EI_COMP_Percentage;
		}
		/**
		 * @param eI_COMP_Percentage the eI_COMP_Percentage to set
		 */
		public void setEI_COMP_Percentage(String eI_COMP_Percentage) {
			EI_COMP_Percentage = eI_COMP_Percentage;
		}
		/**
		 * @return the eI_TP_Percentage
		 */
		public String getEI_TP_Percentage() {
			return EI_TP_Percentage;
		}
		/**
		 * @param eI_TP_Percentage the eI_TP_Percentage to set
		 */
		public void setEI_TP_Percentage(String eI_TP_Percentage) {
			EI_TP_Percentage = eI_TP_Percentage;
		}
		/**
		 * @return the eI_SGS
		 */
		public String getEI_SGS() {
			return EI_SGS;
		}
		/**
		 * @param eI_SGS the eI_SGS to set
		 */
		public void setEI_SGS(String eI_SGS) {
			EI_SGS = eI_SGS;
		}
		/**
		 * @return the eI_USL
		 */
		public String getEI_USL() {
			return EI_USL;
		}
		/**
		 * @param eI_USL the eI_USL to set
		 */
		public void setEI_USL(String eI_USL) {
			EI_USL = eI_USL;
		}
		/**
		 * @return the eI_LSL
		 */
		public String getEI_LSL() {
			return EI_LSL;
		}
		/**
		 * @param eI_LSL the eI_LSL to set
		 */
		public void setEI_LSL(String eI_LSL) {
			EI_LSL = eI_LSL;
		}
		/**
		 * @return the aS_YP_Score
		 */
		public String getAS_YP_Score() {
			return AS_YP_Score;
		}
		/**
		 * @param aS_YP_Score the aS_YP_Score to set
		 */
		public void setAS_YP_Score(String aS_YP_Score) {
			AS_YP_Score = aS_YP_Score;
		}
		/**
		 * @return the aS_GS_Percentage
		 */
		public String getAS_GS_Percentage() {
			return AS_GS_Percentage;
		}
		/**
		 * @param aS_GS_Percentage the aS_GS_Percentage to set
		 */
		public void setAS_GS_Percentage(String aS_GS_Percentage) {
			AS_GS_Percentage = aS_GS_Percentage;
		}
		/**
		 * @return the aS_GOS_Percentage
		 */
		public String getAS_GOS_Percentage() {
			return AS_GOS_Percentage;
		}
		/**
		 * @param aS_GOS_Percentage the aS_GOS_Percentage to set
		 */
		public void setAS_GOS_Percentage(String aS_GOS_Percentage) {
			AS_GOS_Percentage = aS_GOS_Percentage;
		}
		/**
		 * @return the aS_COMP_Percentage
		 */
		public String getAS_COMP_Percentage() {
			return AS_COMP_Percentage;
		}
		/**
		 * @param aS_COMP_Percentage the aS_COMP_Percentage to set
		 */
		public void setAS_COMP_Percentage(String aS_COMP_Percentage) {
			AS_COMP_Percentage = aS_COMP_Percentage;
		}
		/**
		 * @return the aS_TP_Percentage
		 */
		public String getAS_TP_Percentage() {
			return AS_TP_Percentage;
		}
		/**
		 * @param aS_TP_Percentage the aS_TP_Percentage to set
		 */
		public void setAS_TP_Percentage(String aS_TP_Percentage) {
			AS_TP_Percentage = aS_TP_Percentage;
		}
		/**
		 * @return the aS_SGS
		 */
		public String getAS_SGS() {
			return AS_SGS;
		}
		/**
		 * @param aS_SGS the aS_SGS to set
		 */
		public void setAS_SGS(String aS_SGS) {
			AS_SGS = aS_SGS;
		}
		/**
		 * @return the aS_USL
		 */
		public String getAS_USL() {
			return AS_USL;
		}
		/**
		 * @param aS_USL the aS_USL to set
		 */
		public void setAS_USL(String aS_USL) {
			AS_USL = aS_USL;
		}
		/**
		 * @return the aS_LSL
		 */
		public String getAS_LSL() {
			return AS_LSL;
		}
		/**
		 * @param aS_LSL the aS_LSL to set
		 */
		public void setAS_LSL(String aS_LSL) {
			AS_LSL = aS_LSL;
		}
		/**
		 * @return the mC_YP_Score
		 */
		public String getMC_YP_Score() {
			return MC_YP_Score;
		}
		/**
		 * @param mC_YP_Score the mC_YP_Score to set
		 */
		public void setMC_YP_Score(String mC_YP_Score) {
			MC_YP_Score = mC_YP_Score;
		}
		/**
		 * @return the mC_GS_Percentage
		 */
		public String getMC_GS_Percentage() {
			return MC_GS_Percentage;
		}
		/**
		 * @param mC_GS_Percentage the mC_GS_Percentage to set
		 */
		public void setMC_GS_Percentage(String mC_GS_Percentage) {
			MC_GS_Percentage = mC_GS_Percentage;
		}
		/**
		 * @return the mC_GOS_Percentage
		 */
		public String getMC_GOS_Percentage() {
			return MC_GOS_Percentage;
		}
		/**
		 * @param mC_GOS_Percentage the mC_GOS_Percentage to set
		 */
		public void setMC_GOS_Percentage(String mC_GOS_Percentage) {
			MC_GOS_Percentage = mC_GOS_Percentage;
		}
		/**
		 * @return the mC_COMP_Percentage
		 */
		public String getMC_COMP_Percentage() {
			return MC_COMP_Percentage;
		}
		/**
		 * @param mC_COMP_Percentage the mC_COMP_Percentage to set
		 */
		public void setMC_COMP_Percentage(String mC_COMP_Percentage) {
			MC_COMP_Percentage = mC_COMP_Percentage;
		}
		/**
		 * @return the mC_TP_Percentage
		 */
		public String getMC_TP_Percentage() {
			return MC_TP_Percentage;
		}
		/**
		 * @param mC_TP_Percentage the mC_TP_Percentage to set
		 */
		public void setMC_TP_Percentage(String mC_TP_Percentage) {
			MC_TP_Percentage = mC_TP_Percentage;
		}
		/**
		 * @return the mC_SGS
		 */
		public String getMC_SGS() {
			return MC_SGS;
		}
		/**
		 * @param mC_SGS the mC_SGS to set
		 */
		public void setMC_SGS(String mC_SGS) {
			MC_SGS = mC_SGS;
		}
		/**
		 * @return the mC_USL
		 */
		public String getMC_USL() {
			return MC_USL;
		}
		/**
		 * @param mC_USL the mC_USL to set
		 */
		public void setMC_USL(String mC_USL) {
			MC_USL = mC_USL;
		}
		/**
		 * @return the mC_LSL
		 */
		public String getMC_LSL() {
			return MC_LSL;
		}
		/**
		 * @param mC_LSL the mC_LSL to set
		 */
		public void setMC_LSL(String mC_LSL) {
			MC_LSL = mC_LSL;
		}
		/**
		 * @return the aO_YP_Score
		 */
		public String getAO_YP_Score() {
			return AO_YP_Score;
		}
		/**
		 * @param aO_YP_Score the aO_YP_Score to set
		 */
		public void setAO_YP_Score(String aO_YP_Score) {
			AO_YP_Score = aO_YP_Score;
		}
		/**
		 * @return the aO_GS_Percentage
		 */
		public String getAO_GS_Percentage() {
			return AO_GS_Percentage;
		}
		/**
		 * @param aO_GS_Percentage the aO_GS_Percentage to set
		 */
		public void setAO_GS_Percentage(String aO_GS_Percentage) {
			AO_GS_Percentage = aO_GS_Percentage;
		}
		/**
		 * @return the aO_GOS_Percentage
		 */
		public String getAO_GOS_Percentage() {
			return AO_GOS_Percentage;
		}
		/**
		 * @param aO_GOS_Percentage the aO_GOS_Percentage to set
		 */
		public void setAO_GOS_Percentage(String aO_GOS_Percentage) {
			AO_GOS_Percentage = aO_GOS_Percentage;
		}
		/**
		 * @return the aO_COMP_Percentage
		 */
		public String getAO_COMP_Percentage() {
			return AO_COMP_Percentage;
		}
		/**
		 * @param aO_COMP_Percentage the aO_COMP_Percentage to set
		 */
		public void setAO_COMP_Percentage(String aO_COMP_Percentage) {
			AO_COMP_Percentage = aO_COMP_Percentage;
		}
		/**
		 * @return the aO_TP_Percentage
		 */
		public String getAO_TP_Percentage() {
			return AO_TP_Percentage;
		}
		/**
		 * @param aO_TP_Percentage the aO_TP_Percentage to set
		 */
		public void setAO_TP_Percentage(String aO_TP_Percentage) {
			AO_TP_Percentage = aO_TP_Percentage;
		}
		/**
		 * @return the aO_SGS
		 */
		public String getAO_SGS() {
			return AO_SGS;
		}
		/**
		 * @param aO_SGS the aO_SGS to set
		 */
		public void setAO_SGS(String aO_SGS) {
			AO_SGS = aO_SGS;
		}
		/**
		 * @return the aO_USL
		 */
		public String getAO_USL() {
			return AO_USL;
		}
		/**
		 * @param aO_USL the aO_USL to set
		 */
		public void setAO_USL(String aO_USL) {
			AO_USL = aO_USL;
		}
		/**
		 * @return the aO_LSL
		 */
		public String getAO_LSL() {
			return AO_LSL;
		}
		/**
		 * @param aO_LSL the aO_LSL to set
		 */
		public void setAO_LSL(String aO_LSL) {
			AO_LSL = aO_LSL;
		}
		/**
		 * @return the americanIndian
		 */
		public String getAmericanIndian() {
			return americanIndian;
		}
		/**
		 * @param americanIndian the americanIndian to set
		 */
		public void setAmericanIndian(String americanIndian) {
			this.americanIndian = americanIndian;
		}
		/**
		 * @return the asian
		 */
		public String getAsian() {
			return asian;
		}
		/**
		 * @param asian the asian to set
		 */
		public void setAsian(String asian) {
			this.asian = asian;
		}
		/**
		 * @return the africanAmerican
		 */
		public String getAfricanAmerican() {
			return africanAmerican;
		}
		/**
		 * @param africanAmerican the africanAmerican to set
		 */
		public void setAfricanAmerican(String africanAmerican) {
			this.africanAmerican = africanAmerican;
		}
		/**
		 * @return the nativeHawiian
		 */
		public String getNativeHawiian() {
			return nativeHawiian;
		}
		/**
		 * @param nativeHawiian the nativeHawiian to set
		 */
		public void setNativeHawiian(String nativeHawiian) {
			this.nativeHawiian = nativeHawiian;
		}
		/**
		 * @return the white
		 */
		public String getWhite() {
			return white;
		}
		/**
		 * @param white the white to set
		 */
		public void setWhite(String white) {
			this.white = white;
		}
		/**
		 * @return the reserved
		 */
		public String getReserved() {
			return reserved;
		}
		/**
		 * @param reserved the reserved to set
		 */
		public void setReserved(String reserved) {
			this.reserved = reserved;
		}
		/**
		 * @return the hispanic
		 */
		public String getHispanic() {
			return hispanic;
		}
		/**
		 * @param hispanic the hispanic to set
		 */
		public void setHispanic(String hispanic) {
			this.hispanic = hispanic;
		}
		/**
		 * @return the notHispanic
		 */
		public String getNotHispanic() {
			return notHispanic;
		}
		/**
		 * @param notHispanic the notHispanic to set
		 */
		public void setNotHispanic(String notHispanic) {
			this.notHispanic = notHispanic;
		}
}
