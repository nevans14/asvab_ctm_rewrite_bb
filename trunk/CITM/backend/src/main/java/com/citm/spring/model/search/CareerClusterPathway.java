package com.citm.spring.model.search;

import java.io.Serializable;
import java.util.ArrayList;

public class CareerClusterPathway implements Serializable {

	private static final long serialVersionUID = 9209812941632133732L;
	Integer ccId, pathwayId;
	String pathwayTitle, pathwayDescription, imageId, imageDescription;
	ArrayList<CareerClusterSampleJobs> sampleJobs;

	public String getImageId() {
		return imageId;
	}

	public void setImageId(String imageId) {
		this.imageId = imageId;
	}

	public String getImageDescription() {
		return imageDescription;
	}

	public void setImageDescription(String imageDescription) {
		this.imageDescription = imageDescription;
	}

	public Integer getCcId() {
		return ccId;
	}

	public void setCcId(Integer ccId) {
		this.ccId = ccId;
	}

	public Integer getPathwayId() {
		return pathwayId;
	}

	public void setPathwayId(Integer pathwayId) {
		this.pathwayId = pathwayId;
	}

	public String getPathwayTitle() {
		return pathwayTitle;
	}

	public void setPathwayTitle(String pathwayTitle) {
		this.pathwayTitle = pathwayTitle;
	}

	public String getPathwayDescription() {
		return pathwayDescription;
	}

	public void setPathwayDescription(String pathwayDescription) {
		this.pathwayDescription = pathwayDescription;
	}

	public ArrayList<CareerClusterSampleJobs> getSampleJobs() {
		return sampleJobs;
	}

	public void setSampleJobs(ArrayList<CareerClusterSampleJobs> sampleJobs) {
		this.sampleJobs = sampleJobs;
	}

}
