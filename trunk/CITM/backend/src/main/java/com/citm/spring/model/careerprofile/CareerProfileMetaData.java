package com.citm.spring.model.careerprofile;

public class CareerProfileMetaData {

	private int profileId;
	
	private int metadataId;
	
	private String metadataKey;
	
	private String metadataValue;

	public int getProfileId() {
		return profileId;
	}

	public void setProfileId(int profileId) {
		this.profileId = profileId;
	}

	/**
	 * @return the metadataId
	 */
	public int getMetadataId() {
		return metadataId;
	}

	/**
	 * @param metadataId the metadataId to set
	 */
	public void setMetadataId(int metadataId) {
		this.metadataId = metadataId;
	}

	/**
	 * @return the metadataKey
	 */
	public String getMetadataKey() {
		return metadataKey;
	}

	/**
	 * @param metadataKey the metadataKey to set
	 */
	public void setMetadataKey(String metadataKey) {
		this.metadataKey = metadataKey;
	}

	/**
	 * @return the metadataValue
	 */
	public String getMetadataValue() {
		return metadataValue;
	}

	/**
	 * @param metadataValue the metadataValue to set
	 */
	public void setMetadataValue(String metadataValue) {
		this.metadataValue = metadataValue;
	}
}
