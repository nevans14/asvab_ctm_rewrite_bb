package com.citm.spring.model.detail;

import java.io.Serializable;

public class AirforceOfficerAttributeMap implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5952044204095449416L;
	Integer officerJobAttributeId;
	String officerJobAttributeTitle, officerJobAttributeDefinition;

	public Integer getOfficerJobAttributeId() {
		return officerJobAttributeId;
	}

	public void setOfficerJobAttributeId(Integer officerJobAttributeId) {
		this.officerJobAttributeId = officerJobAttributeId;
	}

	public String getOfficerJobAttributeTitle() {
		return officerJobAttributeTitle;
	}

	public void setOfficerJobAttributeTitle(String officerJobAttributeTitle) {
		this.officerJobAttributeTitle = officerJobAttributeTitle;
	}

	public String getOfficerJobAttributeDefinition() {
		return officerJobAttributeDefinition;
	}

	public void setOfficerJobAttributeDefinition(String officerJobAttributeDefinition) {
		this.officerJobAttributeDefinition = officerJobAttributeDefinition;
	}
}
