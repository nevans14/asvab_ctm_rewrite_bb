package com.citm.spring.model.testscore;

public class ParagraphComprehension {

	private	int	PC_YP_Score;
	private	int	PC_GS_Percentage;
	private	int	PC_GOS_Percentage;
	private	int	PC_COMP_Percentage;
	private	int	PC_TP_Percentage;
	private	int	PC_SGS;
	private	int	PC_USL;
	private	int	PC_LSL;
	/**
	 * @return the pC_YP_Score
	 */
	public int getPC_YP_Score() {
		return PC_YP_Score;
	}
	/**
	 * @param pC_YP_Score the pC_YP_Score to set
	 */
	public void setPC_YP_Score(int pC_YP_Score) {
		PC_YP_Score = pC_YP_Score;
	}
	/**
	 * @return the pC_GS_Percentage
	 */
	public int getPC_GS_Percentage() {
		return PC_GS_Percentage;
	}
	/**
	 * @param pC_GS_Percentage the pC_GS_Percentage to set
	 */
	public void setPC_GS_Percentage(int pC_GS_Percentage) {
		PC_GS_Percentage = pC_GS_Percentage;
	}
	/**
	 * @return the pC_GOS_Percentage
	 */
	public int getPC_GOS_Percentage() {
		return PC_GOS_Percentage;
	}
	/**
	 * @param pC_GOS_Percentage the pC_GOS_Percentage to set
	 */
	public void setPC_GOS_Percentage(int pC_GOS_Percentage) {
		PC_GOS_Percentage = pC_GOS_Percentage;
	}
	/**
	 * @return the pC_COMP_Percentage
	 */
	public int getPC_COMP_Percentage() {
		return PC_COMP_Percentage;
	}
	/**
	 * @param pC_COMP_Percentage the pC_COMP_Percentage to set
	 */
	public void setPC_COMP_Percentage(int pC_COMP_Percentage) {
		PC_COMP_Percentage = pC_COMP_Percentage;
	}
	/**
	 * @return the pC_TP_Percentage
	 */
	public int getPC_TP_Percentage() {
		return PC_TP_Percentage;
	}
	/**
	 * @param pC_TP_Percentage the pC_TP_Percentage to set
	 */
	public void setPC_TP_Percentage(int pC_TP_Percentage) {
		PC_TP_Percentage = pC_TP_Percentage;
	}
	/**
	 * @return the pC_SGS
	 */
	public int getPC_SGS() {
		return PC_SGS;
	}
	/**
	 * @param pC_SGS the pC_SGS to set
	 */
	public void setPC_SGS(int pC_SGS) {
		PC_SGS = pC_SGS;
	}
	/**
	 * @return the pC_USL
	 */
	public int getPC_USL() {
		return PC_USL;
	}
	/**
	 * @param pC_USL the pC_USL to set
	 */
	public void setPC_USL(int pC_USL) {
		PC_USL = pC_USL;
	}
	/**
	 * @return the pC_LSL
	 */
	public int getPC_LSL() {
		return PC_LSL;
	}
	/**
	 * @param pC_LSL the pC_LSL to set
	 */
	public void setPC_LSL(int pC_LSL) {
		PC_LSL = pC_LSL;
	}

	
}
