package com.citm.spring.webservice.rest;

import java.util.HashMap;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
@PropertySource("classpath:application.properties")
public class VersionController {
	
	@Value("${application.version}")
	private String version;	
	@Value("${application.last.updated}")
	private String lastUpdated;
	
	private HashMap<String, String> map;
	
	@RequestMapping(value = "/app-info", method = RequestMethod.GET)
	public @ResponseBody HashMap<String, String> version() {
		map = new HashMap<String, String>() 
		{
			private static final long serialVersionUID = 7536216535524649351L;
			{
				put("version", version);
				put("lastUpdated", lastUpdated);
			}
		};
		return map;
	}
}