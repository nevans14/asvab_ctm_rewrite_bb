package com.citm.spring.model.detail;

import java.io.Serializable;

public class AirforceOfficerContextMap implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2746671540660240694L;
	Integer officerJobContextId;
	String officerJobContext, officerJobContextDefinition;

	public Integer getOfficerJobContextId() {
		return officerJobContextId;
	}

	public void setOfficerJobContextId(Integer officerJobContextId) {
		this.officerJobContextId = officerJobContextId;
	}

	public String getOfficerJobContext() {
		return officerJobContext;
	}

	public void setOfficerJobContext(String officerJobContext) {
		this.officerJobContext = officerJobContext;
	}

	public String getOfficerJobContextDefinition() {
		return officerJobContextDefinition;
	}

	public void setOfficerJobContextDefinition(String officerJobContextDefinition) {
		this.officerJobContextDefinition = officerJobContextDefinition;
	}
}
