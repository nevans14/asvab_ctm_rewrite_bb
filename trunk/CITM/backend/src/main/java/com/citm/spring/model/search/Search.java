package com.citm.spring.model.search;

import java.io.Serializable;
import java.util.ArrayList;

public class Search implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3138888859318849281L;
	private String mcId, title, interestOne, interestTwo, interestThree, isArmy, isNavy, isMarines, isAirForce,
			isCoastGuard, isEnlisted, isOfficer, isWarrantOfficer, isHotJob, isActiveDuty, isReserve, isOpenToWomen,
			isEntryLevel, jobFamilyId, photoCredit;
	private Integer codeOne, codeTwo, codeThree, codeFour, ccId;
	private float verbal, math, science;
	private ArrayList<SearchMOCDetail> mocs;
	private ArrayList<SearchAlsoCalledDetail> alsoCalled;

	// accessors
	public String getPhotoCredit() { return photoCredit; }
	public String getJobFamilyId() { return jobFamilyId; }
	public Integer getCcId() { return ccId;	}
	public Integer getCodeOne() { return codeOne; }
	public Integer getCodeTwo() { return codeTwo; }
	public Integer getCodeThree() { return codeThree; }
	public Integer getCodeFour() { return codeFour; }
	public String getIsActiveDuty() { return isActiveDuty; }
	public String getIsReserve() { return isReserve; }
	public String getIsOpenToWomen() { return isOpenToWomen; }
	public String getIsEntryLevel() { return isEntryLevel; }
	public String getIsHotJob() { return isHotJob; }
	public String getIsEnlisted() { return isEnlisted; }
	public String getIsOfficer() { return isOfficer; }
	public String getIsWarrantOfficer() { return isWarrantOfficer; }
	public String getIsArmy() { return isArmy; }
	public String getIsNavy() { return isNavy; }
	public String getIsMarines() { return isMarines; }
	public String getIsAirForce() { return isAirForce; }
	public String getIsCoastGuard() { return isCoastGuard; }
	public String getMcId() { return mcId; }
	public String getTitle() { return title; }
	public String getInterestOne() { return interestOne; }
	public String getInterestTwo() { return interestTwo; }
	public String getInterestThree() { return interestThree; }
	public float getVerbal() { return verbal; }
	public float getMath() { return math; }
	public float getScience() { return science; }
	public ArrayList<SearchMOCDetail> getMocs() { return mocs; }
	public ArrayList<SearchAlsoCalledDetail> getAlsoCalled() { return alsoCalled; }
	
	// mutators
	public void setPhotoCredit(String photoCredit) { this.photoCredit = photoCredit; }
	public void setJobFamilyId(String jobFamilyId) { this.jobFamilyId = jobFamilyId; }
	public void setCcId(Integer ccId) { this.ccId = ccId; }
	public void setCodeOne(Integer codeOne) { this.codeOne = codeOne; }
	public void setCodeTwo(Integer codeTwo) { this.codeTwo = codeTwo; }
	public void setCodeThree(Integer codeThree) { this.codeThree = codeThree; }
	public void setCodeFour(Integer codeFour) { this.codeFour = codeFour; }
	public void setIsActiveDuty(String isActiveDuty) { this.isActiveDuty = isActiveDuty; }
	public void setIsReserve(String isReserve) { this.isReserve = isReserve; }
	public void setIsOpenToWomen(String isOpenToWomen) { this.isOpenToWomen = isOpenToWomen; }
	public void setIsEntryLevel(String isEntryLevel) { this.isEntryLevel = isEntryLevel; }
	public void setIsHotJob(String isHotJob) { this.isHotJob = isHotJob; }
	public void setIsEnlisted(String isEnlisted) { this.isEnlisted = isEnlisted; }
	public void setIsOfficer(String isOfficer) { this.isOfficer = isOfficer; }
	public void setIsWarrantOfficer(String isWarrantOfficer) { this.isWarrantOfficer = isWarrantOfficer; }
	public void setIsArmy(String isArmy) { this.isArmy = isArmy; }
	public void setIsNavy(String isNavy) { this.isNavy = isNavy; }
	public void setIsMarines(String isMarines) { this.isMarines = isMarines; }
	public void setIsAirForce(String isAirForce) { this.isAirForce = isAirForce; }
	public void setIsCoastGuard(String isCoastGuard) { this.isCoastGuard = isCoastGuard; }
	public void setMcId(String mcId) { this.mcId = mcId; }
	public void setTitle(String title) { this.title = title; }
	public void setInterestOne(String interestOne) { this.interestOne = interestOne; }
	public void setInterestTwo(String interestTwo) { this.interestTwo = interestTwo; }
	public void setInterestThree(String interestThree) { this.interestThree = interestThree; }
	public void setVerbal(float verbal) { this.verbal = verbal; }
	public void setMath(float math) { this.math = math; }
	public void setScience(float science) { this.science = science; }
	public void setMocs(ArrayList<SearchMOCDetail> mocs) { this.mocs = mocs; }
	public void setAlsoCalled(ArrayList<SearchAlsoCalledDetail> alsoCalled) { this.alsoCalled = alsoCalled; }	
}
