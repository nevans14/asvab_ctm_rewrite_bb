package com.citm.spring.model.detail;

import java.io.Serializable;

public class airforceCommunityMap implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3882038400120582548L;
	Integer communityId;
	String coumunityTitle, communityDefinition;

	public String getCommunityDefinition() {
		return communityDefinition;
	}

	public void setCommunityDefinition(String communityDefinition) {
		this.communityDefinition = communityDefinition;
	}

	public Integer getCommunityId() {
		return communityId;
	}

	public void setCommunityId(Integer communityId) {
		this.communityId = communityId;
	}

	public String getCoumunityTitle() {
		return coumunityTitle;
	}

	public void setCoumunityTitle(String coumunityTitle) {
		this.coumunityTitle = coumunityTitle;
	}
}
