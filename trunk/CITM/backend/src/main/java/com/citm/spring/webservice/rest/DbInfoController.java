package com.citm.spring.webservice.rest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.citm.spring.dao.mybatis.DbInfoDAO;
import com.citm.spring.model.DbInfo;

@RestController
@RequestMapping("/db-info")
public class DbInfoController {
	
	private Logger logger = LogManager.getLogger(this.getClass().getName());
	
	@Autowired
	private DbInfoDAO dao;
	
	@RequestMapping(value = "/get-by-name/{name}/", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<DbInfo> getByName(@PathVariable String name) {
		DbInfo results = null;
		try {
			results = dao.getDbInfo(name);
		} catch (Exception ex) {
			logger.error(ex.getMessage());
			return new ResponseEntity<DbInfo>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<DbInfo>(results, HttpStatus.OK);
	}
	
}