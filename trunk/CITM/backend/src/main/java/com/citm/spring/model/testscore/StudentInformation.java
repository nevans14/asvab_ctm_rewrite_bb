package com.citm.spring.model.testscore;

public class StudentInformation {


	private	String	studentName;
	private	String	firstName;
	private	String	lastName;
	private	String	middleInitial;
	private	String	email;
	private	String	studentCity;
	private	String	studentState;
	private	String	studentZipCode;
	private	String	studentIntentions;

	
	public StudentInformation(){
		
	}
	
	/**
	 * @return the studentName
	 */
	public String getStudentName() {
		return studentName;
	}
	/**
	 * @param studentName the studentName to set
	 */
	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}
/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return the middleInitial
	 */
	public String getMiddleInitial() {
		return middleInitial;
	}

	/**
	 * @param middleInitial the middleInitial to set
	 */
	public void setMiddleInitial(String middleInitial) {
		this.middleInitial = middleInitial;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	//	/**
//	 * @return the studentAddress
//	 */
//	public String getStudentAddress() {
//		return studentAddress;
//	}
//	/**
//	 * @param studentAddress the studentAddress to set
//	 */
//	public void setStudentAddress(String studentAddress) {
//		this.studentAddress = studentAddress;
//	}
	/**
	 * @return the studentCity
	 */
	public String getStudentCity() {
		return studentCity;
	}
	/**
	 * @param studentCity the studentCity to set
	 */
	public void setStudentCity(String studentCity) {
		this.studentCity = studentCity;
	}
	/**
	 * @return the studentState
	 */
	public String getStudentState() {
		return studentState;
	}
	/**
	 * @param studentState the studentState to set
	 */
	public void setStudentState(String studentState) {
		this.studentState = studentState;
	}
	/**
	 * @return the studentZipCode
	 */
	public String getStudentZipCode() {
		return studentZipCode;
	}
	/**
	 * @param studentZipCode the studentZipCode to set
	 */
	public void setStudentZipCode(String studentZipCode) {
		this.studentZipCode = studentZipCode;
	}
//	/**
//	 * @return the studentTelephoneNumber
//	 */
//	public String getStudentTelephoneNumber() {
//		return studentTelephoneNumber;
//	}
//	/**
//	 * @param studentTelephoneNumber the studentTelephoneNumber to set
//	 */
//	public void setStudentTelephoneNumber(String studentTelephoneNumber) {
//		this.studentTelephoneNumber = studentTelephoneNumber;
//	}
	/**
	 * @return the studentIntentions
	 */
	public String getStudentIntentions() {
		return studentIntentions;
	}
	/**
	 * @param studentIntentions the studentIntentions to set
	 */
	public void setStudentIntentions(String studentIntentions) {
		this.studentIntentions = studentIntentions;
	}

	
}
