package com.citm.spring.dao.mybatis.impl;

import java.util.ArrayList;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.citm.spring.dao.mybatis.service.NotesService;
import com.citm.spring.model.notes.CareerClusterNotes;
import com.citm.spring.model.notes.Notes;

@Repository
public class NotesDAOImpl {
    private Logger logger = LogManager.getLogger(this.getClass().getName());
	@Autowired
	NotesService service;

	public ArrayList<Notes> getNotes(Integer userId) throws Exception {
		return service.getNotes(userId);
	}

	public int insertNote(Notes noteObject) throws Exception {
		return service.insertNote(noteObject);
	}

	public int updateNote(Notes noteObject) throws Exception {
		return service.updateNote(noteObject);
	}
	
	public int deleteNote(Integer userId, int id) throws Exception {
		return service.deleteNote(userId, id);
	}

	public String getOccupationNotes(Integer userId, String socId) throws Exception {
		return service.getOccupationNotes(userId, socId);
	}

	public ArrayList<CareerClusterNotes> getCareerClusterNotes(Integer userId) throws Exception {
		return service.getCareerClusterNotes(userId);
	}

	public int insertCareerClusterNote(CareerClusterNotes noteObject) throws Exception {
		return service.insertCareerClusterNote(noteObject);
	}

	public int updateCareerClusterNote(CareerClusterNotes noteObject) throws Exception {
		return service.updateCareerClusterNote(noteObject);
	}
	
	public int deleteCareerClusterNote(Integer userId, Integer ccNoteId) {
		return service.deleteCareerClusterNote(userId, ccNoteId);
	}

	public String getCareerClusterNote(Integer userId, Integer ccId) throws Exception {
		return service.getCareerClusterNote(userId, ccId);
	}

}
