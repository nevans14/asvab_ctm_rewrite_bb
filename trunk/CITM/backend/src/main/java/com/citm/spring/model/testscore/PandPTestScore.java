package com.citm.spring.model.testscore;

import java.math.BigDecimal;

public class PandPTestScore {

	private BigDecimal resultId;
		
	private String formId;
	
	private String scoreType;
	
	private String scoreSection;
	
	private BigDecimal score;

	/**
	 * @return the resultId
	 */
	public BigDecimal getResultId() {
		return resultId;
	}

	/**
	 * @param resultId the resultId to set
	 */
	public void setResultId(BigDecimal resultId) {
		this.resultId = resultId;
	}

	/**
	 * @return the formId
	 */
	public String getFormId() {
		return formId;
	}

	/**
	 * @param formId the formId to set
	 */
	public void setFormId(String formId) {
		this.formId = formId;
	}

	/**
	 * @return the scoreType
	 */
	public String getScoreType() {
		return scoreType;
	}

	/**
	 * @param scoreType the scoreType to set
	 */
	public void setScoreType(String scoreType) {
		this.scoreType = scoreType;
	}

	/**
	 * @return the scoreSection
	 */
	public String getScoreSection() {
		return scoreSection;
	}

	/**
	 * @param scoreSection the scoreSection to set
	 */
	public void setScoreSection(String scoreSection) {
		this.scoreSection = scoreSection;
	}

	/**
	 * @return the score
	 */
	public BigDecimal getScore() {
		return score;
	}

	/**
	 * @param score the score to set
	 */
	public void setScore(BigDecimal score) {
		this.score = score;
	}
}
