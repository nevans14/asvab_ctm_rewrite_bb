package com.citm.spring.model.detail;

import java.io.Serializable;

public class CoastGuardDetail implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8199187177380937544L;
	String mcId, socId, svc, mpc, title, motdTitle, mocId, description, duties, training, qualifications, specialNotes,
			active, reserve, youtubeUrl, goCom;
	Integer motdId;

	public String getMcId() {
		return mcId;
	}

	public void setMcId(String mcId) {
		this.mcId = mcId;
	}

	public String getSocId() {
		return socId;
	}

	public void setSocId(String socId) {
		this.socId = socId;
	}

	public String getSvc() {
		return svc;
	}

	public void setSvc(String svc) {
		this.svc = svc;
	}

	public String getMpc() {
		return mpc;
	}

	public void setMpc(String mpc) {
		this.mpc = mpc;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getMotdTitle() {
		return motdTitle;
	}

	public void setMotdTitle(String motdTitle) {
		this.motdTitle = motdTitle;
	}

	public String getMocId() {
		return mocId;
	}

	public void setMocId(String mocId) {
		this.mocId = mocId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDuties() {
		return duties;
	}

	public void setDuties(String duties) {
		this.duties = duties;
	}

	public String getTraining() {
		return training;
	}

	public void setTraining(String training) {
		this.training = training;
	}

	public String getQualifications() {
		return qualifications;
	}

	public void setQualifications(String qualifications) {
		this.qualifications = qualifications;
	}

	public String getSpecialNotes() {
		return specialNotes;
	}

	public void setSpecialNotes(String specialNotes) {
		this.specialNotes = specialNotes;
	}

	public String getActive() {
		return active;
	}

	public void setActive(String active) {
		this.active = active;
	}

	public String getReserve() {
		return reserve;
	}

	public void setReserve(String reserve) {
		this.reserve = reserve;
	}

	public String getYoutubeUrl() {
		return youtubeUrl;
	}

	public void setYoutubeUrl(String youtubeUrl) {
		this.youtubeUrl = youtubeUrl;
	}

	public String getGoCom() {
		return goCom;
	}

	public void setGoCom(String goCom) {
		this.goCom = goCom;
	}

	public Integer getMotdId() {
		return motdId;
	}

	public void setMotdId(Integer motdId) {
		this.motdId = motdId;
	}
}
