package com.citm.spring.dao.mybatis.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.citm.spring.model.testscore.CompositeFavoritesFlags;
import com.citm.spring.model.testscore.ManualTestScore;
import com.citm.spring.model.testscore.StudentTestingProgam;
import com.citm.spring.model.testscore.StudentTestingProgamSelects;


public interface TestScoreMapper {

	int insertTestScore(@Param("testScoreObject") List<StudentTestingProgam> testScoreObject);
	
	String getAccessCode(Integer userId);
	
	StudentTestingProgamSelects getTestScore(@Param("userId") Integer userId);
	
	StudentTestingProgamSelects getTestScoreByAccessCode(@Param("accessCode") String accessCode);
	
	StudentTestingProgamSelects getMockTestScoreWithUserId(@Param("userId") Integer userId);

	StudentTestingProgamSelects getMockTestScoreByAccessCode(@Param("accessCode") String accessCode);
	
	int insertManualTestScore(@Param("manualTestScoreObj") ManualTestScore manualTestScoreObj);
	
	ManualTestScore getManualTestScore(@Param("userId") int userId);
	
	int updateManualTestScore(@Param("manualTestScoreObj") ManualTestScore manualTestScoreObj);

	int deleteManualTestScore(@Param("userId") int userId);
	
	int setCompositeFavoritesFlags(@Param("compositeFavoritesFlagsObj") CompositeFavoritesFlags compositeFavoritesFlagsObj);
	
	CompositeFavoritesFlags getCompositeFavoritesFlags(@Param("userId") int userId);
	
	int updateCompositeFavoritesFlags(@Param("compositeFavoritesFlagsObj") CompositeFavoritesFlags compositeFavoritesFlagsObj);

	int deleteCompositeFavoritesFlags(@Param("userId") int userId);

	boolean compositeFavoritesFlagsExists(@Param("userId") int userId);
	
	boolean emailExists(@Param("userName") String userName);
	
	String latestTestDate(@Param("accessCode") String accessCode);
}
