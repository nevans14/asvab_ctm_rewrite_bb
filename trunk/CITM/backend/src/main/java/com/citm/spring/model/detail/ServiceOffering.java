package com.citm.spring.model.detail;

import java.io.Serializable;

public class ServiceOffering implements Serializable {

	private static final long serialVersionUID = 528373087368394637L;
	String mcId, socId, svc, mpc, mocId, mocTitle, compScoreOne, compScoreTwo, navyCompScoreOne, navyReqScoreOne,
			navyCompScoreTwo, navyReqScoreTwo, isHotJob;
	Integer requireScoreOne, requireScoreTwo;

	public String getIsHotJob() {
		return isHotJob;
	}

	public void setIsHotJob(String isHotJob) {
		this.isHotJob = isHotJob;
	}

	public String getNavyCompScoreOne() {
		return navyCompScoreOne;
	}

	public void setNavyCompScoreOne(String navyCompScoreOne) {
		this.navyCompScoreOne = navyCompScoreOne;
	}

	public String getNavyReqScoreOne() {
		return navyReqScoreOne;
	}

	public void setNavyReqScoreOne(String navyReqScoreOne) {
		this.navyReqScoreOne = navyReqScoreOne;
	}

	public String getNavyCompScoreTwo() {
		return navyCompScoreTwo;
	}

	public void setNavyCompScoreTwo(String navyCompScoreTwo) {
		this.navyCompScoreTwo = navyCompScoreTwo;
	}

	public String getNavyReqScoreTwo() {
		return navyReqScoreTwo;
	}

	public void setNavyReqScoreTwo(String navyReqScoreTwo) {
		this.navyReqScoreTwo = navyReqScoreTwo;
	}

	public Integer getRequireScoreOne() {
		return requireScoreOne;
	}

	public void setRequireScoreOne(Integer requireScoreOne) {
		this.requireScoreOne = requireScoreOne;
	}

	public Integer getRequireScoreTwo() {
		return requireScoreTwo;
	}

	public void setRequireScoreTwo(Integer requireScoreTwo) {
		this.requireScoreTwo = requireScoreTwo;
	}

	public String getCompScoreOne() {
		return compScoreOne;
	}

	public void setCompScoreOne(String compScoreOne) {
		this.compScoreOne = compScoreOne;
	}

	public String getCompScoreTwo() {
		return compScoreTwo;
	}

	public void setCompScoreTwo(String compScoreTwo) {
		this.compScoreTwo = compScoreTwo;
	}

	public String getMcId() {
		return mcId;
	}

	public void setMcId(String mcId) {
		this.mcId = mcId;
	}

	public String getSocId() {
		return socId;
	}

	public void setSocId(String socId) {
		this.socId = socId;
	}

	public String getSvc() {
		return svc;
	}

	public void setSvc(String svc) {
		this.svc = svc;
	}

	public String getMpc() {
		return mpc;
	}

	public void setMpc(String mpc) {
		this.mpc = mpc;
	}

	public String getMocId() {
		return mocId;
	}

	public void setMocId(String mocId) {
		this.mocId = mocId;
	}

	public String getMocTitle() {
		return mocTitle;
	}

	public void setMocTitle(String mocTitle) {
		this.mocTitle = mocTitle.trim();
	}
}
