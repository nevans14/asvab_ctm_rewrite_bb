package com.citm.spring.dao.mybatis.impl;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.citm.spring.dao.mybatis.service.ContactUsService;
import com.citm.spring.model.contact.EmailContactUs;

@Repository
public class ContactUsDAOImpl {

	@Autowired
	ContactUsService service;

	public ArrayList<String> getEmail(int roleId) throws Exception {
		return service.getEmail(roleId);
	}

	public int insertGeneralContactUs(EmailContactUs emailObject) throws Exception {
		return service.insertGeneralContactUs(emailObject);
	}
	
	public int insertServiceContactUs(EmailContactUs emailObject) throws Exception {
		return service.insertServiceContactUs(emailObject);
	}
	
	public int insertContactUsByService(String svc, String dateContacted) throws Exception {
		return service.insertContactUsByService(svc, dateContacted);
	}
}
