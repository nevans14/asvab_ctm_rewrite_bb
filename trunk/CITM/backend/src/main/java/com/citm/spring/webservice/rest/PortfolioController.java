package com.citm.spring.webservice.rest;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;
import java.util.Base64;

import javax.imageio.ImageIO;

import com.citm.spring.utils.UserManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.citm.spring.dao.mybatis.impl.PortfolioDAOImpl;
import com.citm.spring.model.favorites.FavoriteOccupation;
import com.citm.spring.model.portfolio.Achievement;
import com.citm.spring.model.portfolio.ActScore;
import com.citm.spring.model.portfolio.AsvabScore;
import com.citm.spring.model.portfolio.Education;
import com.citm.spring.model.portfolio.FyiScore;
import com.citm.spring.model.portfolio.OtherScore;
import com.citm.spring.model.portfolio.PhysicalFitness;
import com.citm.spring.model.portfolio.RelatedCareer;
import com.citm.spring.model.portfolio.ResourceGeneric;
import com.citm.spring.model.portfolio.ResourceSpecific;
import com.citm.spring.model.portfolio.SatScore;
import com.citm.spring.model.portfolio.UserInfo;
import com.citm.spring.model.portfolio.WorkExperience;
import io.netty.util.internal.StringUtil;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;

@Controller
@RequestMapping("/portfolio")
@PropertySource("classpath:application.properties")
public class PortfolioController {
	static Logger logger = LogManager.getLogger("PortfolioController");
	static AmazonS3 client = null;

    @Value("${aws.accessKeyId}")
    private String awsAccessKeyId;
    
    @Value("${aws.secretKey}")
	private String awsSecretKey;
	
    @Autowired
	private UserManager userManager;
	@Autowired
	PortfolioDAOImpl dao;
	
	/**
	 * Returns system name that user have access to.
	 */
	//TODO: Collaborate with Dave to incorporate this functionality into login process.
	@RequestMapping(value = "/get-user-system", method = RequestMethod.GET)
	public @ResponseBody Map<String, String> getUserSystem() {
		String results = null;

		try {
			results = dao.getUserSystem(userManager.getUserId());
		} catch (Exception e) {
			logger.error("Error", e);
		}
		// if null then cep-citm user.
		return Collections.singletonMap("results", StringUtils.isEmpty(results) ? "CEP-CITM" : results.trim());
	}

	@RequestMapping(value = "/PortfolioStarted", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<Integer> isPortfolioStarted() {
	    logger.info("isPortfolioStarted");
		Integer results = null;
		try {
			results = dao.isPortfolioStarted(userManager.getUserId());
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<Integer>(HttpStatus.BAD_REQUEST);
		}

		return new ResponseEntity<Integer>(results, HttpStatus.OK);
	}

	@RequestMapping(value = "/WorkExperience", method = RequestMethod.GET)
	public @ResponseBody ArrayList<WorkExperience> getWorkExperience() {
	    logger.info("getWorkExperience");
		ArrayList<WorkExperience> results = dao.getWorkExperience(userManager.getUserId());
		return results;
	}

	@RequestMapping(value = "/InsertWorkExperience", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<WorkExperience> insertWorkExperience(
			@RequestBody WorkExperience workExperienceObject) {
	    logger.info("insertWorkExperience");
		int rowsInserted = 0;
		workExperienceObject.setUserId(userManager.getUserId());
		try {
			rowsInserted = dao.insertWorkExperience(workExperienceObject);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<WorkExperience>(HttpStatus.BAD_REQUEST);
		}
		if (rowsInserted != 1) {
		    logger.error("Unexpected Response");
		    return new ResponseEntity<WorkExperience>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<WorkExperience>(workExperienceObject, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/DeleteWorkExperience/{id}", method = RequestMethod.DELETE)
	public @ResponseBody ResponseEntity<Integer> deleteWorkExperience(@PathVariable Integer id) {
	    logger.info("deleteWorkExperience");
		int result = 0;
		try {
			result = dao.deleteWorkExperience(userManager.getUserId(), id);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<Integer>(HttpStatus.BAD_REQUEST);
		}
		if (result != 1) {
		    logger.error("Unexpected Response");
			return new ResponseEntity<Integer>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<Integer>(result, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/UpdateWorkExperience", method = RequestMethod.PUT)
	public @ResponseBody ResponseEntity<WorkExperience> updateWorkExperience(
			@RequestBody WorkExperience workExperienceObject) {
	    logger.info("updateWorkExperience");
		int rowsUpdated = 0;
		try {
			rowsUpdated = dao.updateWorkExperience(workExperienceObject);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<WorkExperience>(HttpStatus.BAD_REQUEST);
		}
		if (rowsUpdated != 1) {
			return new ResponseEntity<WorkExperience>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<WorkExperience>(workExperienceObject, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/Education", method = RequestMethod.GET)
	public @ResponseBody ArrayList<Education> getEducation() {
	    logger.info("getEducation");
		ArrayList<Education> results = dao.getEducation(userManager.getUserId());
		return results;
	}

	@RequestMapping(value = "/InsertEducation", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<Education> insertEducation(@RequestBody Education educationObject) {
	    logger.info("insertEducation");
		int rowsInserted = 0;
		educationObject.setUserId(userManager.getUserId());
		try {
			rowsInserted = dao.insertEducation(educationObject);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<Education>(HttpStatus.BAD_REQUEST);
		}
		if (rowsInserted != 1) {
			return new ResponseEntity<Education>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<Education>(educationObject, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/DeleteEducation/{id}", method = RequestMethod.DELETE)
	public @ResponseBody ResponseEntity<Integer> deleteEducation(@PathVariable Integer id) {
	    logger.info("deleteEducation");
		int result = 0;

		try {
			result = dao.deleteEducation(userManager.getUserId(), id);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<Integer>(HttpStatus.BAD_REQUEST);
		}

		if (result != 1) {
			return new ResponseEntity<Integer>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<Integer>(result, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/UpdateEducation", method = RequestMethod.PUT)
	public @ResponseBody ResponseEntity<Education> updateEducation(@RequestBody Education educationObject) {
	    logger.info("updateEducation");
		int rowsUpdated = 0;
		try {
			rowsUpdated = dao.updateEducation(educationObject);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<Education>(HttpStatus.BAD_REQUEST);
		}
		if (rowsUpdated != 1) {
			return new ResponseEntity<Education>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<Education>(educationObject, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/Achievement", method = RequestMethod.GET)
	public @ResponseBody ArrayList<Achievement> getAchievements() {
	    logger.info("getAchievements");
		ArrayList<Achievement> results = dao.getAchievements(userManager.getUserId());
		return results;
	}

	@RequestMapping(value = "/InsertAchievement", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<Achievement> insertAchievement(@RequestBody Achievement achievementObject) {
	    logger.info("insertAchievement");
		int rowsInserted = 0;
		achievementObject.setUserId(userManager.getUserId());
		try {
			rowsInserted = dao.insertAchievement(achievementObject);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<Achievement>(HttpStatus.BAD_REQUEST);
		}
		if (rowsInserted != 1) {
			return new ResponseEntity<Achievement>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<Achievement>(achievementObject, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/DeleteAchievement/{id}", method = RequestMethod.DELETE)
	public @ResponseBody ResponseEntity<Integer> deleteAchievement(@PathVariable Integer id) {
	    logger.info("deleteAchievement");
		int result = 0;
		try {
			result = dao.deleteAchievement(userManager.getUserId(), id);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<Integer>(HttpStatus.BAD_REQUEST);
		}
		if (result != 1) {
			return new ResponseEntity<Integer>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<Integer>(result, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/UpdateAchievement", method = RequestMethod.PUT)
	public @ResponseBody ResponseEntity<Achievement> updateAchievement(@RequestBody Achievement achievementObject) {
	    logger.info("updateAchievement");
		int rowsUpdated = 0;
		try {
			rowsUpdated = dao.updateAchievement(achievementObject);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<Achievement>(HttpStatus.BAD_REQUEST);
		}
		if (rowsUpdated != 1) {
			return new ResponseEntity<Achievement>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<Achievement>(achievementObject, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/ASVAB", method = RequestMethod.GET)
	public @ResponseBody ArrayList<AsvabScore> getAsvabScore() {
	    logger.info("getAsvabScore");
		ArrayList<AsvabScore> results = dao.getAsvabScore(userManager.getUserId());
		return results;
	}

	@RequestMapping(value = "/InsertASVAB", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<AsvabScore> insertAsvabScore(@RequestBody AsvabScore asvabObject) {
	    logger.info("insertAsvabScore");
		int rowsInserted = 0;
		asvabObject.setUserId(userManager.getUserId());
		try {
			rowsInserted = dao.insertAsvabScore(asvabObject);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<AsvabScore>(HttpStatus.BAD_REQUEST);
		}
		if (rowsInserted != 1) {
			return new ResponseEntity<AsvabScore>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<AsvabScore>(asvabObject, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/DeleteASVAB/{id}", method = RequestMethod.DELETE)
	public @ResponseBody ResponseEntity<Integer> deleteAsvabScore(@PathVariable Integer id) {
	    logger.info("deleteAsvabScore");
		int result = 0;
		try {
			result = dao.deleteAsvabScore(userManager.getUserId(), id);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<Integer>(HttpStatus.BAD_REQUEST);
		}

		if (result != 1) {
			return new ResponseEntity<Integer>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<Integer>(result, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/UpdateASVAB", method = RequestMethod.PUT)
	public @ResponseBody ResponseEntity<AsvabScore> updateAsvabScore(@RequestBody AsvabScore asvabObject) {
	    logger.info("updateAsvabScore");
		int rowsUpdated = 0;
		try {
			rowsUpdated = dao.updateAsvabScore(asvabObject);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<AsvabScore>(HttpStatus.BAD_REQUEST);
		}
		if (rowsUpdated != 1) {
			return new ResponseEntity<AsvabScore>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<AsvabScore>(asvabObject, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/ACT", method = RequestMethod.GET)
	public @ResponseBody ArrayList<ActScore> getActScore() {
	    logger.info("getActScore");
		ArrayList<ActScore> results = dao.getActScore(userManager.getUserId());
		return results;
	}

	@RequestMapping(value = "/InsertACT", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<ActScore> insertActScore(@RequestBody ActScore actObject) {
	    logger.info("insertActScore");
		int rowsInserted = 0;
		actObject.setUserId(userManager.getUserId());
		try {
			rowsInserted = dao.insertActScore(actObject);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<ActScore>(HttpStatus.BAD_REQUEST);
		}
		if (rowsInserted != 1) {
			return new ResponseEntity<ActScore>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<ActScore>(actObject, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/DeleteACT/{id}", method = RequestMethod.DELETE)
	public @ResponseBody ResponseEntity<Integer> deleteActScore(@PathVariable Integer id) {
	    logger.info("deleteActScore");
		int result = 0;
		try {
			result = dao.deleteActScore(userManager.getUserId(), id);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<Integer>(HttpStatus.BAD_REQUEST);
		}

		if (result != 1) {
			return new ResponseEntity<Integer>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<Integer>(result, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/UpdateACT", method = RequestMethod.PUT)
	public @ResponseBody ResponseEntity<ActScore> updateActScore(@RequestBody ActScore actObject) {
	    logger.info("updateActScore");
		int rowsUpdated = 0;
		try {
			rowsUpdated = dao.updateActScore(actObject);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<ActScore>(HttpStatus.BAD_REQUEST);
		}
		if (rowsUpdated != 1) {
			return new ResponseEntity<ActScore>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<ActScore>(actObject, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/SAT", method = RequestMethod.GET)
	public @ResponseBody ArrayList<SatScore> getSatScore() {
	    logger.info("getSatScore");
		ArrayList<SatScore> results = dao.getSatScore(userManager.getUserId());
		return results;
	}

	@RequestMapping(value = "/InsertSAT", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<SatScore> insertSatScore(@RequestBody SatScore satObject) {
	    logger.info("insertSatScore");
		int rowsInserted = 0;
		satObject.setUserId(userManager.getUserId());
		try {
			rowsInserted = dao.insertSatScore(satObject);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<SatScore>(HttpStatus.BAD_REQUEST);
		}
		if (rowsInserted != 1) {
			return new ResponseEntity<SatScore>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<SatScore>(satObject, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/DeleteSAT/{id}", method = RequestMethod.DELETE)
	public @ResponseBody ResponseEntity<Integer> deleteSatScore(@PathVariable Integer id) {
	    logger.info("deleteSatScore");
		int result = 0;
		try {
			result = dao.deleteSatScore(userManager.getUserId(), id);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<Integer>(HttpStatus.BAD_REQUEST);
		}
		if (result != 1) {
			return new ResponseEntity<Integer>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<Integer>(result, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/UpdateSAT", method = RequestMethod.PUT)
	public @ResponseBody ResponseEntity<SatScore> updateSatScore(@RequestBody SatScore satObject) {
	    logger.info("updateSatScore");
		int rowsUpdated = 0;
		try {
			rowsUpdated = dao.updateSatScore(satObject);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<SatScore>(HttpStatus.BAD_REQUEST);
		}
		if (rowsUpdated != 1) {
			return new ResponseEntity<SatScore>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<SatScore>(satObject, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/FYI", method = RequestMethod.GET)
	public @ResponseBody ArrayList<FyiScore> getFyiScore() {
	    logger.info("getFyiScore");
		ArrayList<FyiScore> results = dao.getFyiScore(userManager.getUserId());
		return results;
	}

	@RequestMapping(value = "/InsertFYI", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<FyiScore> insertFyiScore(@RequestBody FyiScore fyiObject) {
	    logger.info("insertFyiScore");
		int rowsInserted = 0;
		fyiObject.setUserId(userManager.getUserId());
		try {
			rowsInserted = dao.insertFyiScore(fyiObject);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<FyiScore>(HttpStatus.BAD_REQUEST);
		}
		if (rowsInserted != 1) {
			return new ResponseEntity<FyiScore>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<FyiScore>(fyiObject, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/DeleteFYI/{id}", method = RequestMethod.DELETE)
	public @ResponseBody ResponseEntity<Integer> deleteFyiScore(@PathVariable Integer id) {
	    logger.info("deleteFyiScore");
		int result = 0;
		try {
			result = dao.deleteFyiScore(userManager.getUserId(), id);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<Integer>(HttpStatus.BAD_REQUEST);
		}
		if (result != 1) {
			return new ResponseEntity<Integer>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<Integer>(result, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/UpdateFYI", method = RequestMethod.PUT)
	public @ResponseBody ResponseEntity<FyiScore> updateFyiScore(@RequestBody FyiScore fyiObject) {
	    logger.info("updateFyiScore");
		int rowsUpdate = 0;
		try {
			rowsUpdate = dao.updateFyiScore(fyiObject);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<FyiScore>(HttpStatus.BAD_REQUEST);
		}
		if (rowsUpdate != 1) {
			return new ResponseEntity<FyiScore>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<FyiScore>(fyiObject, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/Other", method = RequestMethod.GET)
	public @ResponseBody ArrayList<OtherScore> getOtherScore() {
	    logger.info("getOtherScore");
		ArrayList<OtherScore> results = dao.getOtherScore(userManager.getUserId());
		return results;
	}

	@RequestMapping(value = "/InsertOther", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<OtherScore> insertOtherScore(@RequestBody OtherScore otherObject) {
	    logger.info("insertOtherScore");
		int rowsInserted = 0;
		otherObject.setUserId(userManager.getUserId());
		try {
			rowsInserted = dao.insertOtherScore(otherObject);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<OtherScore>(HttpStatus.BAD_REQUEST);
		}
		if (rowsInserted != 1) {
			return new ResponseEntity<OtherScore>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<OtherScore>(otherObject, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/DeleteOther/{id}", method = RequestMethod.DELETE)
	public @ResponseBody ResponseEntity<Integer> deleteOtherScore(@PathVariable Integer id) {
	    logger.info("deleteOtherScore");
		int result = 0;

		try {
			result = dao.deleteOtherScore(userManager.getUserId(), id);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<Integer>(HttpStatus.BAD_REQUEST);
		}
		if (result != 1) {
			return new ResponseEntity<Integer>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<Integer>(result, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/UpdateOther", method = RequestMethod.PUT)
	public @ResponseBody ResponseEntity<OtherScore> updateOtherScore(@RequestBody OtherScore otherObject) {
	    logger.info("updateOtherScore");
		int rowsUpdate = 0;
		try {
			rowsUpdate = dao.updateOtherScore(otherObject);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<OtherScore>(HttpStatus.BAD_REQUEST);
		}
		if (rowsUpdate != 1) {
			return new ResponseEntity<OtherScore>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<OtherScore>(otherObject, HttpStatus.OK);
		}
	}
	
	@RequestMapping(value = "/upload", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<UserInfo> handleFileUpload(
			@RequestPart(value = "file", required = false) MultipartFile file,
			@RequestParam("id") Integer id,
			@RequestParam("name") String name,
			@RequestParam("schoolName") String schoolName,
			@RequestParam("graduationYear") Integer graduationYear) {

		Integer results = null;

		// Set user object.
		UserInfo userInfo = new UserInfo();
		userInfo.setUserId(userManager.getUserId());
		userInfo.setId(id);
		userInfo.setName(name);
		userInfo.setSchoolName(schoolName);
		userInfo.setGraduationYear(graduationYear);

		try {

			// Save user info to database.
			results = dao.updateUserInfo(userInfo);

			// Save image to server.
			if (file != null && !file.isEmpty()) {
				try {

					if(this.getAwsClient()) {

						String bucketName = "dev-media.careersinthemilitary.com/citm-uploads";
						String fileName = Integer.toString(userInfo.getUserId()) + ".png";

						// // Try deleting a file so to replace it if it already exists
						// try {
						// 	if ( client.doesObjectExist(bucketName, fileName)) {
						// 		client.deleteObject("dev-media.careersinthemilitary.com/citm-uploads", fileName);
						// 	}
						// } catch (AmazonServiceException e) {
						// 	logger.error ("Error on delete S3 object, going ahead with put, error is:  " + e.getCause());
						// } catch (Exception ex) {
						// 	logger.error ("Error on delete S3 object, going ahead with put, error is:  " + ex.getMessage());
						// }

						// ObjectMetadata metaData = new ObjectMetadata();
						// byte[] bytes = DigestUtils.md5Digest(file.getBytes());
						// String streamMD5 = new String(Base64.getEncoder().encodeToString(bytes));
						// metaData.setContentMD5(streamMD5);
						// InputStream fileInputStream = new ByteArrayInputStream(file.getBytes());
						client.putObject(new PutObjectRequest(bucketName, fileName, file.getInputStream(), null));
						
					} else {
						logger.error("Amazon client not created.");

						return new ResponseEntity<UserInfo>(HttpStatus.BAD_REQUEST);
					}
				} catch (Exception e) {
					logger.error("Error", e);
					return new ResponseEntity<UserInfo>(HttpStatus.BAD_REQUEST);
				}
			} else {

				// Image upload is optional so return OK.
				return new ResponseEntity<UserInfo>(userInfo, HttpStatus.OK);
			}
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<UserInfo>(HttpStatus.BAD_REQUEST);
		}

		return new ResponseEntity<UserInfo>(userInfo, HttpStatus.OK);

	}

	@RequestMapping(value = "/get-user-info", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<ArrayList<UserInfo>> getUserInfo() {
		ArrayList<UserInfo> results = null;
		try {
			results = dao.getUserInfo(userManager.getUserId());
			
			if(this.getAwsClient()) {

				String bucketName = "dev-media.careersinthemilitary.com/citm-uploads";
				String fileName = Integer.toString(userManager.getUserId()) + ".png";

				if ( client.doesObjectExist(bucketName, fileName)) {
					S3Object object = client.getObject(bucketName, fileName);
					BufferedImage buf = ImageIO.read(object.getObjectContent());
					ByteArrayOutputStream baos = new ByteArrayOutputStream();
					ImageIO.write( buf, "png", baos );
					baos.flush();
					byte[] imageInByte = baos.toByteArray();
					baos.close();
					if (imageInByte != null) {
						String base64String = Base64.getEncoder().encodeToString(imageInByte);
						results.get(0).setPhoto(base64String);
						results.get(0).setfileName(fileName);
					}
				}
			}
			
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<ArrayList<UserInfo>>(HttpStatus.BAD_REQUEST);
		}

		return new ResponseEntity<ArrayList<UserInfo>>(results, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/PhysicalFitness", method = RequestMethod.GET)
	public @ResponseBody ArrayList<PhysicalFitness> getPhysicalFitness() {
	    logger.info("getPhysicalFitness");
		ArrayList<PhysicalFitness> results = dao.getPhysicalFitness(userManager.getUserId());
		return results;
	}

	@RequestMapping(value = "/InsertPhysicalFitness", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<PhysicalFitness> insertPhysicalFitness(
			@RequestBody PhysicalFitness physicalFitnessObject) {
	    logger.info("insertPhysicalFitness");
		int rowsInserted = 0;
		physicalFitnessObject.setUserId(userManager.getUserId());
		try {
			rowsInserted = dao.insertPhysicalFitness(physicalFitnessObject);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<PhysicalFitness>(HttpStatus.BAD_REQUEST);
		}
		if (rowsInserted != 1) {
		    logger.error("Unexpected Response");
		    return new ResponseEntity<PhysicalFitness>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<PhysicalFitness>(physicalFitnessObject, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/DeletePhysicalFitness/{id}", method = RequestMethod.DELETE)
	public @ResponseBody ResponseEntity<Integer> deletePhysicalFitness(@PathVariable Integer id) {
	    logger.info("deletePhysicalFitness");
		int result = 0;
		try {
			result = dao.deletePhysicalFitness(userManager.getUserId(), id);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<Integer>(HttpStatus.BAD_REQUEST);
		}
		if (result != 1) {
		    logger.error("Unexpected Response");
			return new ResponseEntity<Integer>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<Integer>(result, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/UpdatePhysicalFitness", method = RequestMethod.PUT)
	public @ResponseBody ResponseEntity<PhysicalFitness> updatePhysicalFitness(
			@RequestBody PhysicalFitness physicalFitnessObject) {
	    logger.info("updatePhysicalFitness");
		int rowsUpdated = 0;
		try {
			rowsUpdated = dao.updatePhysicalFitness(physicalFitnessObject);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<PhysicalFitness>(HttpStatus.BAD_REQUEST);
		}
		if (rowsUpdated != 1) {
			return new ResponseEntity<PhysicalFitness>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<PhysicalFitness>(physicalFitnessObject, HttpStatus.OK);
		}
	}
	
	@RequestMapping(value = "/getFavoriteOccupation", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<ArrayList<com.citm.spring.model.portfolio.FavoriteOccupation>> getFavoriteOccupation() {
	    logger.info("getFavoriteOccupation");
		ArrayList<com.citm.spring.model.portfolio.FavoriteOccupation> results;
		try {
			results = dao.getFavoriteOccupation(userManager.getUserId());
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<ArrayList<com.citm.spring.model.portfolio.FavoriteOccupation>>(HttpStatus.BAD_REQUEST);
		}

		return new ResponseEntity<ArrayList<com.citm.spring.model.portfolio.FavoriteOccupation>>(results, HttpStatus.OK);
	}

	@RequestMapping(value = "/get-related-career-list", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<ArrayList<RelatedCareer>> getRelatedCareerList() {
		logger.info("getRelatedCareerList");
		ArrayList<RelatedCareer> results;
		try {
			results = dao.getRelatedCareerList(userManager.getUserId());
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<ArrayList<RelatedCareer>>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<ArrayList<RelatedCareer>>(results, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/get-specific-resource", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<ArrayList<ResourceSpecific>> getResourceSpecific() {
		logger.info("getResourceSpecific");
		ArrayList<ResourceSpecific> results;
		try {
			results = dao.getResourceSpecific();
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<ArrayList<ResourceSpecific>>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<ArrayList<ResourceSpecific>>(results, HttpStatus.OK);
	}

	@RequestMapping(value = "/get-generic-resource", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<ArrayList<ResourceGeneric>> getResourceGeneric() {
		logger.info("getGenericSpecific");
		ArrayList<ResourceGeneric> results;
		try {
			results = dao.getResourceGeneric();
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<ArrayList<ResourceGeneric>>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<ArrayList<ResourceGeneric>>(results, HttpStatus.OK);
	}
	
	private  boolean getAwsClient() {
		if (client == null) {
			try {
				// String accessKeyId = "aws.accessKeyId=AKIAXEPTH7VO6WM7AT4H";
				// String secretKey = "aws.secretKey=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";

				Regions region = Regions.US_EAST_1;

				BasicAWSCredentials awsCreds = new BasicAWSCredentials(awsAccessKeyId, awsSecretKey);

				client = AmazonS3ClientBuilder.standard()
				.withRegion(region)
				.withCredentials(new AWSStaticCredentialsProvider(awsCreds))
				.build();
				return true;
			} catch (Exception e) {
				logger.error("Create AWS Client error: " + e);
				return false;
			}
		} else {
			return true;
		}
	}


}
