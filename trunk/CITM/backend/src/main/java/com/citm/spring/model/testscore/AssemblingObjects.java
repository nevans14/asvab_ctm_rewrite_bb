package com.citm.spring.model.testscore;

public class AssemblingObjects {

	private	int	AO_YP_Score;
	private	int	AO_GS_Percentage;
	private	int	AO_GOS_Percentage;
	private	int	AO_COMP_Percentage;
	private	int	AO_TP_Percentage;
	private	int	AO_SGS;
	private	int	AO_USL;
	private	int	AO_LSL;
	/**
	 * @return the aO_YP_Score
	 */
	public int getAO_YP_Score() {
		return AO_YP_Score;
	}
	/**
	 * @param aO_YP_Score the aO_YP_Score to set
	 */
	public void setAO_YP_Score(int aO_YP_Score) {
		AO_YP_Score = aO_YP_Score;
	}
	/**
	 * @return the aO_GS_Percentage
	 */
	public int getAO_GS_Percentage() {
		return AO_GS_Percentage;
	}
	/**
	 * @param aO_GS_Percentage the aO_GS_Percentage to set
	 */
	public void setAO_GS_Percentage(int aO_GS_Percentage) {
		AO_GS_Percentage = aO_GS_Percentage;
	}
	/**
	 * @return the aO_GOS_Percentage
	 */
	public int getAO_GOS_Percentage() {
		return AO_GOS_Percentage;
	}
	/**
	 * @param aO_GOS_Percentage the aO_GOS_Percentage to set
	 */
	public void setAO_GOS_Percentage(int aO_GOS_Percentage) {
		AO_GOS_Percentage = aO_GOS_Percentage;
	}
	/**
	 * @return the aO_COMP_Percentage
	 */
	public int getAO_COMP_Percentage() {
		return AO_COMP_Percentage;
	}
	/**
	 * @param aO_COMP_Percentage the aO_COMP_Percentage to set
	 */
	public void setAO_COMP_Percentage(int aO_COMP_Percentage) {
		AO_COMP_Percentage = aO_COMP_Percentage;
	}
	/**
	 * @return the aO_TP_Percentage
	 */
	public int getAO_TP_Percentage() {
		return AO_TP_Percentage;
	}
	/**
	 * @param aO_TP_Percentage the aO_TP_Percentage to set
	 */
	public void setAO_TP_Percentage(int aO_TP_Percentage) {
		AO_TP_Percentage = aO_TP_Percentage;
	}
	/**
	 * @return the aO_SGS
	 */
	public int getAO_SGS() {
		return AO_SGS;
	}
	/**
	 * @param aO_SGS the aO_SGS to set
	 */
	public void setAO_SGS(int aO_SGS) {
		AO_SGS = aO_SGS;
	}
	/**
	 * @return the aO_USL
	 */
	public int getAO_USL() {
		return AO_USL;
	}
	/**
	 * @param aO_USL the aO_USL to set
	 */
	public void setAO_USL(int aO_USL) {
		AO_USL = aO_USL;
	}
	/**
	 * @return the aO_LSL
	 */
	public int getAO_LSL() {
		return AO_LSL;
	}
	/**
	 * @param aO_LSL the aO_LSL to set
	 */
	public void setAO_LSL(int aO_LSL) {
		AO_LSL = aO_LSL;
	}
	
}
