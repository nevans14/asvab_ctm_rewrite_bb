package com.citm.spring.dao.mybatis;

import com.citm.spring.model.DbInfo;

public interface DbInfoDAO {
	
	public DbInfo getDbInfo(String name);
	
}