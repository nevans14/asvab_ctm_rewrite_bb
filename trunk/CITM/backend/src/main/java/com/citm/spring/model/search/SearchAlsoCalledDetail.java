package com.citm.spring.model.search;

import java.io.Serializable;

public class SearchAlsoCalledDetail implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 408019535412214137L;
	private String mcId, alsoCalled;
	private Integer id;
	
	// accessors
	public String getMcId() {
		return mcId;
	}
	public String getAlsoCalled() {
		return alsoCalled;
	}
	public Integer getId() {
		return id;
	}
	// mutators
	public void setId(Integer id) {
		this.id = id;
	}
	public void setMcId(String mcId) {
		this.mcId = mcId;
	}
	public void setAlsoCalled(String alsoCalled) {
		this.alsoCalled = alsoCalled;
	}
}