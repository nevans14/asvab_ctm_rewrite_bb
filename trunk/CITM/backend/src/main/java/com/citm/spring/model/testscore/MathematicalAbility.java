package com.citm.spring.model.testscore;;

/**
 * @author royan
 *
 */
public class MathematicalAbility {

	private	int	MA_YP_Score;
	private	int	MA_GS_Percentage;
	private	int	MA_GOS_Percentage;
	private	int	MA_COMP_Percentage;
	private	int	MA_TP_Percentage;
	private	int	MA_SGS;
	private	int	MA_USL;
	private	int	MA_LSL;
	/**
	 * @return the mA_YP_Score
	 */
	public int getMA_YP_Score() {
		return MA_YP_Score;
	}
	/**
	 * @param mA_YP_Score the mA_YP_Score to set
	 */
	public void setMA_YP_Score(int mA_YP_Score) {
		MA_YP_Score = mA_YP_Score;
	}
	/**
	 * @return the mA_GS_Percentage
	 */
	public int getMA_GS_Percentage() {
		return MA_GS_Percentage;
	}
	/**
	 * @param mA_GS_Percentage the mA_GS_Percentage to set
	 */
	public void setMA_GS_Percentage(int mA_GS_Percentage) {
		MA_GS_Percentage = mA_GS_Percentage;
	}
	/**
	 * @return the mA_GOS_Percentage
	 */
	public int getMA_GOS_Percentage() {
		return MA_GOS_Percentage;
	}
	/**
	 * @param mA_GOS_Percentage the mA_GOS_Percentage to set
	 */
	public void setMA_GOS_Percentage(int mA_GOS_Percentage) {
		MA_GOS_Percentage = mA_GOS_Percentage;
	}
	/**
	 * @return the mA_COMP_Percentage
	 */
	public int getMA_COMP_Percentage() {
		return MA_COMP_Percentage;
	}
	/**
	 * @param mA_COMP_Percentage the mA_COMP_Percentage to set
	 */
	public void setMA_COMP_Percentage(int mA_COMP_Percentage) {
		MA_COMP_Percentage = mA_COMP_Percentage;
	}
	/**
	 * @return the mA_TP_Percentage
	 */
	public int getMA_TP_Percentage() {
		return MA_TP_Percentage;
	}
	/**
	 * @param mA_TP_Percentage the mA_TP_Percentage to set
	 */
	public void setMA_TP_Percentage(int mA_TP_Percentage) {
		MA_TP_Percentage = mA_TP_Percentage;
	}
	/**
	 * @return the mA_SGS
	 */
	public int getMA_SGS() {
		return MA_SGS;
	}
	/**
	 * @param mA_SGS the mA_SGS to set
	 */
	public void setMA_SGS(int mA_SGS) {
		MA_SGS = mA_SGS;
	}
	/**
	 * @return the mA_USL
	 */
	public int getMA_USL() {
		return MA_USL;
	}
	/**
	 * @param mA_USL the mA_USL to set
	 */
	public void setMA_USL(int mA_USL) {
		MA_USL = mA_USL;
	}
	/**
	 * @return the mA_LSL
	 */
	public int getMA_LSL() {
		return MA_LSL;
	}
	/**
	 * @param mA_LSL the mA_LSL to set
	 */
	public void setMA_LSL(int mA_LSL) {
		MA_LSL = mA_LSL;
	}
	
}
