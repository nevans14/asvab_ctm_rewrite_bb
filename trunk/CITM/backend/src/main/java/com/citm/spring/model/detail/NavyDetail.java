package com.citm.spring.model.detail;

import java.io.Serializable;

public class NavyDetail implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7683611328810440018L;
	String mcId, socId, svc, mpc, title, mocId, description, rating, ratingName, link, physicalRequirement,
			citizenshipRequirement, minEducation, asvabRequirement, compositeScoreOne, requiredScoreOne,
			compositeScoreTwo, requiredScoreTwo, basicTraining, technicalTrainingDuration, techTrainingLocation,
			requiredHighSchoolCourses, desiredHighSchoolCourses, notes, youTubeLink, linkToNavy;	
	public String getRating() {
		return rating;
	}
	public void setRating(String rating) {
		this.rating = rating;
	}
	public String getMcId() {
		return mcId;
	}
	public void setMcId(String mcId) {
		this.mcId = mcId;
	}
	public String getSocId() {
		return socId;
	}
	public void setSocId(String socId) {
		this.socId = socId;
	}
	public String getSvc() {
		return svc;
	}
	public void setSvc(String svc) {
		this.svc = svc;
	}
	public String getMpc() {
		return mpc;
	}
	public void setMpc(String mpc) {
		this.mpc = mpc;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getMocId() {
		return mocId;
	}
	public void setMocId(String mocId) {
		this.mocId = mocId;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getRatingName() {
		return ratingName;
	}
	public void setRatingName(String ratingName) {
		this.ratingName = ratingName;
	}
	public String getLink() {
		return link;
	}
	public void setLink(String link) {
		this.link = link;
	}
	public String getPhysicalRequirement() {
		return physicalRequirement;
	}
	public void setPhysicalRequirement(String physicalRequirement) {
		this.physicalRequirement = physicalRequirement;
	}
	public String getCitizenshipRequirement() {
		return citizenshipRequirement;
	}
	public void setCitizenshipRequirement(String citizenshipRequirement) {
		this.citizenshipRequirement = citizenshipRequirement;
	}
	public String getMinEducation() {
		return minEducation;
	}
	public void setMinEducation(String minEducation) {
		this.minEducation = minEducation;
	}
	public String getAsvabRequirement() {
		return asvabRequirement;
	}
	public void setAsvabRequirement(String asvabRequirement) {
		this.asvabRequirement = asvabRequirement;
	}
	public String getCompositeScoreOne() {
		return compositeScoreOne;
	}
	public void setCompositeScoreOne(String compositeScoreOne) {
		this.compositeScoreOne = compositeScoreOne;
	}
	public String getRequiredScoreOne() {
		return requiredScoreOne;
	}
	public void setRequiredScoreOne(String requiredScoreOne) {
		this.requiredScoreOne = requiredScoreOne;
	}
	public String getCompositeScoreTwo() {
		return compositeScoreTwo;
	}
	public void setCompositeScoreTwo(String compositeScoreTwo) {
		this.compositeScoreTwo = compositeScoreTwo;
	}
	public String getRequiredScoreTwo() {
		return requiredScoreTwo;
	}
	public void setRequiredScoreTwo(String requiredScoreTwo) {
		this.requiredScoreTwo = requiredScoreTwo;
	}
	public String getBasicTraining() {
		return basicTraining;
	}
	public void setBasicTraining(String basicTraining) {
		this.basicTraining = basicTraining;
	}
	public String getTechnicalTrainingDuration() {
		return technicalTrainingDuration;
	}
	public void setTechnicalTrainingDuration(String technicalTrainingDuration) {
		this.technicalTrainingDuration = technicalTrainingDuration;
	}
	public String getTechTrainingLocation() {
		return techTrainingLocation;
	}
	public void setTechTrainingLocation(String techTrainingLocation) {
		this.techTrainingLocation = techTrainingLocation;
	}
	public String getRequiredHighSchoolCourses() {
		return requiredHighSchoolCourses;
	}
	public void setRequiredHighSchoolCourses(String requiredHighSchoolCourses) {
		this.requiredHighSchoolCourses = requiredHighSchoolCourses;
	}
	public String getDesiredHighSchoolCourses() {
		return desiredHighSchoolCourses;
	}
	public void setDesiredHighSchoolCourses(String desiredHighSchoolCourses) {
		this.desiredHighSchoolCourses = desiredHighSchoolCourses;
	}
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	public String getYouTubeLink() { 
		return youTubeLink;
	}
	public void setYouTubeLink(String youTubeLink) {
		this.youTubeLink = youTubeLink;
	}
	public String getLinkToNavy() {
		return linkToNavy;
	}
	public void setLinkToNavy(String linkToNavy) {
		this.linkToNavy = linkToNavy;
	}
}
