package com.citm.spring.model.search;

import java.io.Serializable;

public class SearchMOCDetail implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -6332700195877345391L;
	private String moc, mocTitle, svc, mpc;
	private Integer id;

	// accessors
	public Integer getId() {
		return id;
	}
	public String getMoc() {
		return moc;
	}
	public String getMocTitle() {
		return mocTitle;
	}
	public String getSvc() {
		return svc;
	}
	public String getMpc() {
		return mpc;
	}
	// mutators
	public void setId(Integer id) {
		this.id = id;
	}
	public void setMoc(String moc) {
		this.moc = moc;
	}
	public void setMocTitle(String mocTitle) {
		this.mocTitle = mocTitle;
	}
	public void setSvc(String svc) {
		this.svc = svc;
	}
	public void setMpc(String mpc) {
		this.mpc = mpc;
	}
}