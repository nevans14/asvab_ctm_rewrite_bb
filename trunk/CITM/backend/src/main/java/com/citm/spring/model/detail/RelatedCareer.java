package com.citm.spring.model.detail;

import java.io.Serializable;

public class RelatedCareer implements Serializable {

	private static final long serialVersionUID = -8577024325455265206L;
	String relatedId, type, title, onetTitle;

	public String getOnetTitle() {
		return onetTitle;
	}

	public void setOnetTitle(String onetTitle) {
		this.onetTitle = onetTitle;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getRelatedId() {
		return relatedId;
	}

	public void setRelatedId(String relatedId) {
		this.relatedId = relatedId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
}
