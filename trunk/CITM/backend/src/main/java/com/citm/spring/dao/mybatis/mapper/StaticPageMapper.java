package com.citm.spring.dao.mybatis.mapper;

import java.util.ArrayList;

import org.apache.ibatis.annotations.Param;

import com.citm.spring.model.staticpage.StaticPage;

public interface StaticPageMapper {
	
	StaticPage getPageById(int pageId);
    
    StaticPage getPageByPageNameOld(String pageName);

    StaticPage getPageByPageName(@Param("pageName") String pageName, @Param("website") String website);

    ArrayList<StaticPage> getPageListByName(@Param("pageName") String pageName, @Param("website") String website);

	
}