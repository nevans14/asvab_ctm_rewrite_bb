package com.citm.spring.model.portfolio;

import java.io.Serializable;

public class Achievement implements Serializable {

	private static final long serialVersionUID = -2200320117520565805L;
	private Integer id;
	private Integer userId;
	private String description;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
