package com.citm.spring.dao.mybatis.impl;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.citm.spring.dao.mybatis.TestScoreDao;
import com.citm.spring.dao.mybatis.service.TestScoreService;
import com.citm.spring.model.testscore.CompositeFavoritesFlags;
import com.citm.spring.model.testscore.ManualTestScore;
import com.citm.spring.model.testscore.StudentTestingProgam;
import com.citm.spring.model.testscore.StudentTestingProgamSelects;

@Repository
public class TestScoreDAOImpl implements TestScoreDao{

	@Autowired
	private TestScoreService service;
	
    static Logger logger = LogManager.getLogger("TestScoreDAOImpl");
    
	/**
	 * Insert student's test score record.
	 * 
	 * @param testScoreObject
	 * @return
	 */
	public int insertTestScore(List<StudentTestingProgam> testScoreObject) {
		return service.insertTestScore(testScoreObject);

	}
	
	public String getAccessCode(Integer userId) throws Exception {
		return service.getAccessCode(userId);
	}

	/**
	 * Retrieve student's test score record.
	 * 
	 * @param accessCode
	 * @return
	 * @throws Exception
	 */
	public StudentTestingProgamSelects getTestScore(Integer userId) {
		StudentTestingProgamSelects results = new StudentTestingProgamSelects();

		try {
			results = service.getTestScore(userId);
		} catch (Exception e) {
			logger.error("Mybatis Exception: ", e);
		}
		return results;
	}
	
	/**
	 * Retrieve student's test score record.
	 * 
	 * @param accessCode
	 * @return
	 * @throws Exception
	 */
	public StudentTestingProgamSelects getTestScore(String accessCode) {
		StudentTestingProgamSelects results = new StudentTestingProgamSelects();

		try {
			results = service.getTestScore(accessCode);
		} catch (Exception e) {
			logger.error("Mybatis Exception: ", e);
		}
		return results;
	}
	
	@Override
	public int insertManualTestScore(ManualTestScore manualTestScoreObj) {
		return service.insertManualTestScore(manualTestScoreObj);
	}

	@Override
	public ManualTestScore getManualTestScore(int userId) {
		ManualTestScore results = new ManualTestScore();

		try {
			results = service.getManualTestScore(userId);
		} catch (Exception e) {
			logger.error("Mybatis Exception: ", e);
		}
		return results;
	}

	@Override
	public int updateManualTestScore(ManualTestScore manualTestScoreObj) {
		return  service.updateManualTestScore(manualTestScoreObj);
	}

	@Override
	public int deleteManualTestScore(int userId) {
		return service.deleteManualTestScore(userId);
	}

	@Override
	public int setCompositeFavoritesFlags(CompositeFavoritesFlags compositeFavoritesFlagsObj) {
		return service.setCompositeFavoritesFlags(compositeFavoritesFlagsObj);
	}

	@Override
	public CompositeFavoritesFlags getCompositeFavoritesFlags(int userId) {
		CompositeFavoritesFlags results = new CompositeFavoritesFlags();

		try {
			results = service.getCompositeFavoritesFlags(userId);
		} catch (Exception e) {
			logger.error("Mybatis Exception: ", e);
		}
		return results;
	}

	@Override
	public int updateCompositeFavoritesFlags(CompositeFavoritesFlags compositeFavoritesFlagsObj) {
		return  service.updateCompositeFavoritesFlags(compositeFavoritesFlagsObj);
	}

	@Override
	public int deleteCompositeFavoritesFlags(int userId) {
		return service.deleteCompositeFavoritesFlags(userId);
	}

	@Override
	public boolean compositeFavoritesFlagsExists(int userId) {
		return service.compositeFavoritesFlagsExists(userId);
	}

	@Override
	public boolean emailExists(String userName) {
		return service.emailExistsI(userName);
	}

	@Override
	public String latestTestDate(String accessCode) {
		return service.latestTestDate(accessCode);
	}

}
