package com.citm.spring.dao.mybatis.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.citm.spring.dao.mybatis.mapper.CareerDetailMapper;
import com.citm.spring.model.detail.WhatTheyDo;
import com.citm.spring.model.detail.RelatedCareer;
import com.citm.spring.model.detail.SeeScoreArmy;
import com.citm.spring.model.detail.ServiceOffering;
import com.citm.spring.model.detail.SkillRating;
import com.citm.spring.model.detail.TrainingProvided;
import com.citm.spring.model.detail.AdditionalInfo;
import com.citm.spring.model.detail.AirForceDetail;
import com.citm.spring.model.detail.ArmyDetail;
import com.citm.spring.model.detail.CareerPath;
import com.citm.spring.model.detail.CoastGuardDetail;
import com.citm.spring.model.detail.HelpfulAttribute;
import com.citm.spring.model.detail.HiddenJob;
import com.citm.spring.model.detail.MarineDetail;
import com.citm.spring.model.detail.MilitaryCareer;
import com.citm.spring.model.detail.NavyDetail;
import com.citm.spring.model.detail.Profile;
import com.citm.spring.model.detail.RecentlyViewed;
import com.citm.spring.model.careerprofile.*;

@Service
public class CareerDetailService {

	@Autowired
	CareerDetailMapper mapper;

	public MilitaryCareer getMilitaryCareer(String mcId) {
		return mapper.getMilitaryCareer(mcId);
	}

	public ArrayList<WhatTheyDo> getWhatTheyDo(String mcId) {
		return mapper.getWhatTheyDo(mcId);
	}

	public ArrayList<TrainingProvided> getTrainingProvided(String mcId) {
		return mapper.getTrainingProvided(mcId);
	}

	public ArrayList<RelatedCareer> getRelatedCareers(String mcId) {
		return mapper.getRelatedCareers(mcId);
	}

	public ArrayList<RelatedCareer> getRelatedCivilianCareers(String mcId) {
		return mapper.getRelatedCivilianCareers(mcId);
	}

	public ArrayList<AdditionalInfo> getAdditionalCareerInfo(String mcId) {
		return mapper.getAdditionalCareerInfo(mcId);
	}

	public ArrayList<Profile> getProfiles(String mcId) {
		return mapper.getProfiles(mcId);
	}

	public int getHotJob(String mcId) {
		return mapper.getHotJob(mcId);
	}

	public ArrayList<HelpfulAttribute> getHelpfulAttribute(String mcId) {
		return mapper.getHelpfulAttribute(mcId);
	}

	public ArrayList<ServiceOffering> getServiceOffering(String mcId) {
		return mapper.getServiceOffering(mcId);
	}

	public ArrayList<ServiceOffering> getServiceOfferingCompositeScores() {
		return mapper.getServiceOfferingCompositeScores();
	}

	public SkillRating getSkillRating(String mcId) {
		return mapper.getSkillRating(mcId);
	}

	public String getHideJobList(int userId) {
		return mapper.getHideJobList(userId);
	}

	public int setHideJobList(HiddenJob obj) {
		return mapper.setHideJobList(obj);
	}

	public ArrayList<CareerPath> getCareerPath(String mcId) {
		return mapper.getCareerPath(mcId);
	}

	public String getRecentlyViewed(int userId) {
		return mapper.getRecentlyViewed(userId);
	}

	public int setRecentlyViewed(RecentlyViewed obj) {
		return mapper.setRecentlyViewed(obj);
	}

	public ArrayList<String> getOtherTitles(String mcId) {
		return mapper.getOtherTitles(mcId);
	}

	public AirForceDetail getAirForceDetail(String mcId, String mocId) {
		return mapper.getAirForceDetail(mcId, mocId);
	}

	public ArmyDetail getArmyDetail(String mcId, String mocId) {
		return mapper.getArmyDetail(mcId, mocId);
	}

	public CoastGuardDetail getCoastGuardDetail(String mcId, String mocId) {
		return mapper.getCoastGuardDetail(mcId, mocId);
	}

	public MarineDetail getMarineDetail(String mcId, String mocId) {
		return mapper.getMarineDetail(mcId, mocId);
	}

	public NavyDetail getNavyDetail(String mcId, String mocId) {
		return mapper.getNavyDetail(mcId, mocId);
	}

	public ArrayList<SeeScoreArmy> getSeeScoreArmy(String mcId) {
		return mapper.getSeeScoreArmy(mcId);
	}
	
	public ArrayList<CareerProfile> getFeaturedProfiles(String mcId) {
		return mapper.getFeaturedProfiles(mcId);
	}

}
