package com.citm.spring.model.staticpage;

import java.io.Serializable;

public class StaticPage implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -103088488209086300L;
	private Integer pageId;
	private String pageTitle, pageDescription, pageHtml;
	
	public Integer getPageId() { return this.pageId; }
	public void setPageId (Integer pageId) { this.pageId = pageId; }
	
	public String getPageTitle() { return this.pageTitle; }
	public void setPageTitle(String pageTitle) { this.pageTitle = pageTitle; }
	
	public String getPageDescription() { return this.pageDescription; }
	public void setPageDescription(String pageDescription) { this.pageDescription = pageDescription; }
	
	public String getPageHtml() { return this.pageHtml; }
	public void setPageHtml(String pageHtml) { this.pageHtml = pageHtml; }
	
}