package com.citm.spring.model.portfolio;

import java.io.Serializable;

public class ResourceSpecific implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4629497921476124437L;
	String title, link, ccId, mcId;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public String getCcId() {
		return ccId;
	}

	public void setCcId(String ccId) {
		this.ccId = ccId;
	}

	public String getMcId() {
		return mcId;
	}

	public void setMcId(String mcId) {
		this.mcId = mcId;
	}
}
