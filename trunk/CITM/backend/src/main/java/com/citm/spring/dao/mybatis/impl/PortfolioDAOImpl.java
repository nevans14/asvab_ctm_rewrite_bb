package com.citm.spring.dao.mybatis.impl;

import java.util.ArrayList;

import org.apache.ibatis.annotations.Param;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.citm.spring.model.portfolio.FavoriteOccupation;
import com.citm.spring.dao.mybatis.service.PortfolioService;
import com.citm.spring.model.portfolio.Achievement;
import com.citm.spring.model.portfolio.ActScore;
import com.citm.spring.model.portfolio.AsvabScore;
import com.citm.spring.model.portfolio.Education;
import com.citm.spring.model.portfolio.FyiScore;
import com.citm.spring.model.portfolio.OtherScore;
import com.citm.spring.model.portfolio.PhysicalFitness;
import com.citm.spring.model.portfolio.RelatedCareer;
import com.citm.spring.model.portfolio.ResourceGeneric;
import com.citm.spring.model.portfolio.ResourceSpecific;
import com.citm.spring.model.portfolio.SatScore;
import com.citm.spring.model.portfolio.UserInfo;
import com.citm.spring.model.portfolio.WorkExperience;

@Repository
public class PortfolioDAOImpl {
    static Logger logger = LogManager.getLogger("PortfolioDAOImpl");

	@Autowired
	private PortfolioService service;

	/**
	 * Returns 0 if no records found for user's portfolio.
	 * 
	 * @param userId
	 * @return
	 * @throws Exception
	 */
	public int isPortfolioStarted(Integer userId) throws Exception {
		return service.isPortfolioStarted(userId);
	}

	/**
	 * Returns user's work experience records.
	 * 
	 * @param userId
	 * @return
	 */
	public ArrayList<WorkExperience> getWorkExperience(Integer userId) {
		ArrayList<WorkExperience> results = new ArrayList<WorkExperience>();

		try {
			results = service.getWorkExperience(userId);
		} catch (Exception e) {
			logger.error("Mybatis Exception: ", e);
		}
		return results;
	}

	/**
	 * Insert user's work experience inputs.
	 * 
	 * @param workExperienceObject
	 * @return
	 * @throws Exception
	 */
	public int insertWorkExperience(WorkExperience workExperienceObject) throws Exception {
		int results = 0;

		results = service.insertWorkExperience(workExperienceObject);

		return results;
	}

	/**
	 * Delete user's work experience inputs.
	 * 
	 * @param userId
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public int deleteWorkExperience(Integer userId, Integer id) throws Exception {
		int results = 0;

		results = service.deleteWorkExperience(userId, id);

		return results;
	}

	/**
	 * Update user's work experience.
	 * 
	 * @param workExperienceObject
	 * @return
	 * @throws Exception
	 */
	public int updateWorkExperience(WorkExperience workExperienceObject) throws Exception {
		int results = 0;

		results = service.updateWorkExperience(workExperienceObject);

		return results;
	}

	/**
	 * Returns user's education records.
	 * 
	 * @param userId
	 * @return
	 */
	public ArrayList<Education> getEducation(Integer userId) {
		ArrayList<Education> results = new ArrayList<Education>();

		try {
			results = service.getEducation(userId);
		} catch (Exception e) {
			logger.error("Mybatis Exception: ", e);
		}
		return results;
	}

	/**
	 * Insert user's education inputs.
	 * 
	 * @param educationObject
	 * @return
	 * @throws Exception
	 */
	public int insertEducation(Education educationObject) throws Exception {
		int results = 0;

		results = service.insertEducation(educationObject);

		return results;
	}

	/**
	 * Delete user's education inputs.
	 * 
	 * @param userId
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public int deleteEducation(Integer userId, Integer id) throws Exception {
		int results = 0;

		results = service.deleteEducation(userId, id);

		return results;
	}

	/**
	 * Update user's education.
	 * 
	 * @param educationObject
	 * @return
	 * @throws Exception
	 */
	public int updateEducation(Education educationObject) throws Exception {
		int results = 0;

		results = service.updateEducation(educationObject);

		return results;
	}

	/**
	 * Returns user's achievement records.
	 * 
	 * @param userId
	 * @return
	 */
	public ArrayList<Achievement> getAchievements(Integer userId) {
		ArrayList<Achievement> results = new ArrayList<Achievement>();

		try {
			results = service.getAchievements(userId);
		} catch (Exception e) {
			logger.error("Mybatis Exception: ", e);
		}
		return results;
	}

	/**
	 * Insert user's achievement inputs.
	 * 
	 * @param achievementObject
	 * @return
	 * @throws Exception
	 */
	public int insertAchievement(Achievement achievementObject) throws Exception {
		int results = 0;

		results = service.insertAchievement(achievementObject);

		return results;
	}

	/**
	 * Delete user's achievement inputs.
	 * 
	 * @param userId
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public int deleteAchievement(Integer userId, Integer id) throws Exception {
		int results = 0;

		results = service.deleteAchievement(userId, id);

		return results;
	}

	/**
	 * Update user's achievement inputs.
	 * 
	 * @param achievementObject
	 * @return
	 * @throws Exception
	 */
	public int updateAchievement(Achievement achievementObject) throws Exception {
		int results = 0;

		results = service.updateAchievement(achievementObject);

		return results;
	}

	

	





	/**
	 * Returns user's ASVAB scores.
	 * 
	 * @param userId
	 * @return
	 */
	public ArrayList<AsvabScore> getAsvabScore(Integer userId) {
		ArrayList<AsvabScore> results = new ArrayList<AsvabScore>();

		try {
			results = service.getAsvabScore(userId);
		} catch (Exception e) {
			logger.error("Mybatis Exception: ", e);
		}
		return results;
	}

	/**
	 * Insert user's ASVAB score.
	 * 
	 * @param asvabObject
	 * @return
	 * @throws Exception
	 */
	public int insertAsvabScore(AsvabScore asvabObject) throws Exception {
		int results = 0;

		results = service.insertAsvabScore(asvabObject);

		return results;
	}

	/**
	 * Delete user's ASVAB score.
	 * 
	 * @param userId
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public int deleteAsvabScore(Integer userId, Integer id) throws Exception {
		int results = 0;

		results = service.deleteAsvabScore(userId, id);

		return results;
	}

	/**
	 * Update user's ASVAB score.
	 * 
	 * @param asvabObject
	 * @return
	 * @throws Exception
	 */
	public int updateAsvabScore(AsvabScore asvabObject) throws Exception {
		int results = 0;

		results = service.updateAsvabScore(asvabObject);

		return results;
	}

	/**
	 * Returns user's ACT scores.
	 * 
	 * @param userId
	 * @return
	 */
	public ArrayList<ActScore> getActScore(Integer userId) {
		ArrayList<ActScore> results = new ArrayList<ActScore>();

		try {
			results = service.getActScore(userId);
		} catch (Exception e) {
			logger.error("Mybatis Exception: ", e);
		}
		return results;
	}

	/**
	 * Insert user's ACT score.
	 * 
	 * @param actObject
	 * @return
	 * @throws Exception
	 */
	public int insertActScore(ActScore actObject) throws Exception {
		int results = 0;

		results = service.insertActScore(actObject);

		return results;
	}

	/**
	 * Delete user's ACT score.
	 * 
	 * @param userId
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public int deleteActScore(Integer userId, Integer id) throws Exception {
		int results = 0;

		results = service.deleteActScore(userId, id);

		return results;
	}

	/**
	 * Update user's ACT score.
	 * 
	 * @param actObject
	 * @return
	 * @throws Exception
	 */
	public int updateActScore(ActScore actObject) throws Exception {
		int results = 0;

		results = service.updateActScore(actObject);

		return results;
	}

	/**
	 * Returns user's SAT scores.
	 * 
	 * @param userId
	 * @return
	 */
	public ArrayList<SatScore> getSatScore(Integer userId) {
		ArrayList<SatScore> results = new ArrayList<SatScore>();

		try {
			results = service.getSatScore(userId);
		} catch (Exception e) {
			logger.error("Mybatis Exception: ", e);
		}
		return results;
	}

	/**
	 * Insert user's SAT score.
	 * 
	 * @param satObject
	 * @return
	 * @throws Exception
	 */
	public int insertSatScore(SatScore satObject) throws Exception {
		int results = 0;

		results = service.insertSatScore(satObject);

		return results;
	}

	/**
	 * Delete user's SAT score.
	 * 
	 * @param userId
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public int deleteSatScore(Integer userId, Integer id) throws Exception {
		int results = 0;

		results = service.deleteSatScore(userId, id);

		return results;
	}

	/**
	 * Update user's SAT score.
	 * 
	 * @param satObject
	 * @return
	 * @throws Exception
	 */
	public int updateSatScore(SatScore satObject) throws Exception {
		int results = 0;

		results = service.updateSatScore(satObject);

		return results;
	}

	/**
	 * Returns user's FYI scores.
	 * 
	 * @param userId
	 * @return
	 */
	public ArrayList<FyiScore> getFyiScore(Integer userId) {
		ArrayList<FyiScore> results = new ArrayList<FyiScore>();

		try {
			results = service.getFyiScore(userId);
		} catch (Exception e) {
			logger.error("Mybatis Exception: ", e);
		}
		return results;
	}

	/**
	 * Insert user's FYI score.
	 * 
	 * @param fyiObject
	 * @return
	 * @throws Exception
	 */
	public int insertFyiScore(FyiScore fyiObject) throws Exception {
		int results = 0;

		results = service.insertFyiScore(fyiObject);

		return results;
	}

	/**
	 * Delete user's FYI score.
	 * 
	 * @param userId
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public int deleteFyiScore(Integer userId, Integer id) throws Exception {
		int results = 0;

		results = service.deleteFyiScore(userId, id);

		return results;
	}

	/**
	 * Update user's FYIScore.
	 * 
	 * @param fyiObject
	 * @return
	 * @throws Exception
	 */
	public int updateFyiScore(FyiScore fyiObject) throws Exception {
		int results = 0;

		results = service.updateFyiScore(fyiObject);

		return results;
	}

	/**
	 * Returns user's other scores.
	 * 
	 * @param userId
	 * @return
	 */
	public ArrayList<OtherScore> getOtherScore(Integer userId) {
		ArrayList<OtherScore> results = new ArrayList<OtherScore>();

		try {
			results = service.getOtherScore(userId);
		} catch (Exception e) {
			logger.error("Mybatis Exception: ", e);
		}
		return results;
	}

	/**
	 * Insert user's other score.
	 * 
	 * @param otherObject
	 * @return
	 * @throws Exception
	 */
	public int insertOtherScore(OtherScore otherObject) throws Exception {
		int results = 0;

		results = service.insertOtherScore(otherObject);

		return results;
	}

	/**
	 * Delete user's other score.
	 * 
	 * @param userId
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public int deleteOtherScore(Integer userId, Integer id) throws Exception {
		int results = 0;

		results = service.deleteOtherScore(userId, id);

		return results;
	}

	/**
	 * Update user's other score.
	 * 
	 * @param otherObject
	 * @return
	 * @throws Exception
	 */
	public int updateOtherScore(OtherScore otherObject) throws Exception {
		int results = 0;

		results = service.updateOtherScore(otherObject);

		return results;
	}

	public int updateUserInfo(UserInfo userInfo) throws Exception {
		return service.updateUserInfo(userInfo);
	}

	public ArrayList<UserInfo> getUserInfo(Integer userId) throws Exception {
		return service.getUserInfo(userId);
	}
	
	/**
	 * Returns user's physical fitness records.
	 * 
	 * @param userId
	 * @return
	 */
	public ArrayList<PhysicalFitness> getPhysicalFitness(Integer userId) {
		ArrayList<PhysicalFitness> results = new ArrayList<PhysicalFitness>();

		try {
			results = service.getPhysicalFitness(userId);
		} catch (Exception e) {
			logger.error("Mybatis Exception: ", e);
		}
		return results;
	}

	/**
	 * Insert user's physical fitness inputs.
	 * 
	 * @param physicalFitnessObject
	 * @return
	 * @throws Exception
	 */
	public int insertPhysicalFitness(PhysicalFitness physicalFitnessObject) throws Exception {
		int results = 0;

		results = service.insertPhysicalFitness(physicalFitnessObject);

		return results;
	}

	/**
	 * Delete user's physical fitness inputs.
	 * 
	 * @param userId
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public int deletePhysicalFitness(Integer userId, Integer id) throws Exception {
		int results = 0;

		results = service.deletePhysicalFitness(userId, id);

		return results;
	}

	/**
	 * Update user's physical fitness.
	 * 
	 * @param physicalFitnessObject
	 * @return
	 * @throws Exception
	 */
	public int updatePhysicalFitness(PhysicalFitness physicalFitnessObject) throws Exception {
		int results = 0;

		results = service.updatePhysicalFitness(physicalFitnessObject);

		return results;
	}
	
	public ArrayList<FavoriteOccupation> getFavoriteOccupation(Integer userId) throws Exception {
		ArrayList<FavoriteOccupation> results = new ArrayList<FavoriteOccupation>();

		results = service.getFavoriteOccupation(userId);

		return results;
	}
	
	public ArrayList<RelatedCareer> getRelatedCareerList(Integer userId) throws Exception{
		return service.getRelatedCareerList(userId);
	}
	
	public ArrayList<ResourceSpecific> getResourceSpecific() throws Exception {
		return service.getResourceSpecific();
	}

	public ArrayList<ResourceGeneric> getResourceGeneric() throws Exception {
		return service.getResourceGeneric();
	}
	
	public String getUserSystem(@Param("userId") Integer userId) throws Exception {
		return service.getUserSystem(userId);
	}

	
	/**
	 * Gets user's name and grade.
	 * 
	 * @param userId
	 * @return
	 * @throws Exception
	 */
	/*public ArrayList<UserInfo> selectNameGrade(Integer userId) throws Exception {
		ArrayList<UserInfo> results = new ArrayList<UserInfo>();

		results = service.selectNameGrade(userId);

		return results;
	}*/

	/**
	 * Insert user's name and gpa. If already exists, then updates records.
	 * 
	 * @param userId
	 * @param name
	 * @param gpa
	 * @return
	 * @throws Exception
	 */
	/*public int updateInsertNameGrade(UserInfo userInfo) throws Exception {
		int result = 0;

		result = service.updateInsertNameGrade(userInfo);

		return result;
	}*/
}
