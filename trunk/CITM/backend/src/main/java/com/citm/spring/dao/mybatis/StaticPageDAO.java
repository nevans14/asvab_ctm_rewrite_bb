package com.citm.spring.dao.mybatis;

import java.util.ArrayList;

import com.citm.spring.model.staticpage.StaticPage;

public interface StaticPageDAO {
	StaticPage getPageById(int pageId);

    StaticPage getPageByPageNameOld(String pageName);

    StaticPage getPageByPageName(String pageName, String website);

    ArrayList<StaticPage> getPageListByName(String pageName, String website);
}