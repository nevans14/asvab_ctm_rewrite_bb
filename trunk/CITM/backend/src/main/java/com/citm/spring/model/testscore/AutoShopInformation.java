package com.citm.spring.model.testscore;

public class AutoShopInformation {

	private	int	AS_YP_Score;
	private	int	AS_GS_Percentage;
	private	int	AS_GOS_Percentage;
	private	int	AS_COMP_Percentage;
	private	int	AS_TP_Percentage;
	private	int	AS_SGS;
	private	int	AS_USL;
	private	int	AS_LSL;
	/**
	 * @return the aS_YP_Score
	 */
	public int getAS_YP_Score() {
		return AS_YP_Score;
	}
	/**
	 * @param aS_YP_Score the aS_YP_Score to set
	 */
	public void setAS_YP_Score(int aS_YP_Score) {
		AS_YP_Score = aS_YP_Score;
	}
	/**
	 * @return the aS_GS_Percentage
	 */
	public int getAS_GS_Percentage() {
		return AS_GS_Percentage;
	}
	/**
	 * @param aS_GS_Percentage the aS_GS_Percentage to set
	 */
	public void setAS_GS_Percentage(int aS_GS_Percentage) {
		AS_GS_Percentage = aS_GS_Percentage;
	}
	/**
	 * @return the aS_GOS_Percentage
	 */
	public int getAS_GOS_Percentage() {
		return AS_GOS_Percentage;
	}
	/**
	 * @param aS_GOS_Percentage the aS_GOS_Percentage to set
	 */
	public void setAS_GOS_Percentage(int aS_GOS_Percentage) {
		AS_GOS_Percentage = aS_GOS_Percentage;
	}
	/**
	 * @return the aS_COMP_Percentage
	 */
	public int getAS_COMP_Percentage() {
		return AS_COMP_Percentage;
	}
	/**
	 * @param aS_COMP_Percentage the aS_COMP_Percentage to set
	 */
	public void setAS_COMP_Percentage(int aS_COMP_Percentage) {
		AS_COMP_Percentage = aS_COMP_Percentage;
	}
	/**
	 * @return the aS_TP_Percentage
	 */
	public int getAS_TP_Percentage() {
		return AS_TP_Percentage;
	}
	/**
	 * @param aS_TP_Percentage the aS_TP_Percentage to set
	 */
	public void setAS_TP_Percentage(int aS_TP_Percentage) {
		AS_TP_Percentage = aS_TP_Percentage;
	}
	/**
	 * @return the aS_SGS
	 */
	public int getAS_SGS() {
		return AS_SGS;
	}
	/**
	 * @param aS_SGS the aS_SGS to set
	 */
	public void setAS_SGS(int aS_SGS) {
		AS_SGS = aS_SGS;
	}
	/**
	 * @return the aS_USL
	 */
	public int getAS_USL() {
		return AS_USL;
	}
	/**
	 * @param aS_USL the aS_USL to set
	 */
	public void setAS_USL(int aS_USL) {
		AS_USL = aS_USL;
	}
	/**
	 * @return the aS_LSL
	 */
	public int getAS_LSL() {
		return AS_LSL;
	}
	/**
	 * @param aS_LSL the aS_LSL to set
	 */
	public void setAS_LSL(int aS_LSL) {
		AS_LSL = aS_LSL;
	}
	
}
