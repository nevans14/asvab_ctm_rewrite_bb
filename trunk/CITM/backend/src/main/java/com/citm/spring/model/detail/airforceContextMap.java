package com.citm.spring.model.detail;

import java.io.Serializable;

public class airforceContextMap implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6550612214897115290L;
	Integer jobContextId;
	String jobContext;

	public Integer getJobContextId() {
		return jobContextId;
	}

	public void setJobContextId(Integer jobContextId) {
		this.jobContextId = jobContextId;
	}

	public String getJobContext() {
		return jobContext;
	}

	public void setJobContext(String jobContext) {
		this.jobContext = jobContext;
	}
}
