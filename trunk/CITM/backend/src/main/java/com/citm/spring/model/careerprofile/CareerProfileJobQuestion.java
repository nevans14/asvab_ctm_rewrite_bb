package com.citm.spring.model.careerprofile;

public class CareerProfileJobQuestion {
	
	private int profileId;
	
	private int jobQuestionId;

	private String question;
	
	private String answer;
	
	private String orderNum;

	public int getProfileId() {
		return profileId;
	}

	public void setProfileId(int profileId) {
		this.profileId = profileId;
	}

	/**
	 * @return the jobQuestionId
	 */
	public int getJobQuestionId() {
		return jobQuestionId;
	}

	/**
	 * @param jobQuestionId the jobQuestionId to set
	 */
	public void setJobQuestionId(int jobQuestionId) {
		this.jobQuestionId = jobQuestionId;
	}

	/**
	 * @return the question
	 */
	public String getQuestion() {
		return question;
	}

	/**
	 * @param question the question to set
	 */
	public void setQuestion(String question) {
		this.question = question;
	}

	/**
	 * @return the answer
	 */
	public String getAnswer() {
		return answer;
	}

	/**
	 * @param answer the answer to set
	 */
	public void setAnswer(String answer) {
		this.answer = answer;
	}

	/**
	 * @return the orderNum
	 */
	public String getOrderNum() {
		return orderNum;
	}

	/**
	 * @param orderNum the orderNum to set
	 */
	public void setOrderNum(String orderNum) {
		this.orderNum = orderNum;
	}

}
