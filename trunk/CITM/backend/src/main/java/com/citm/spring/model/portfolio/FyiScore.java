package com.citm.spring.model.portfolio;

import java.io.Serializable;

public class FyiScore implements Serializable {

	private static final long serialVersionUID = 5891491240347231808L;	
	private Integer userId;
	private char interestCodeOne;
	private char interestCodeTwo;
	private char interestCodeThree;
	private char gender;
	private String scoreChoice;

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public char getInterestCodeOne() {
		return interestCodeOne;
	}

	public void setInterestCodeOne(char interestCodeOne) {
		this.interestCodeOne = interestCodeOne;
	}

	public char getInterestCodeTwo() {
		return interestCodeTwo;
	}

	public void setInterestCodeTwo(char interestCodeTwo) {
		this.interestCodeTwo = interestCodeTwo;
	}

	public char getInterestCodeThree() {
		return interestCodeThree;
	}

	public void setInterestCodeThree(char interestCodeThree) {
		this.interestCodeThree = interestCodeThree;
	}
	
	public char getGender() {
		return gender;
	}
	
	public void setGender(char gender) {
		this.gender = gender;
	}
	
	public String getScoreChoice() {
		return scoreChoice;
	}
	
	public void setScoreChoice(String scoreChoice) {
		this.scoreChoice = scoreChoice;
	}
}
