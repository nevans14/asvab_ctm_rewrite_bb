package com.citm.spring.dao.mybatis.service;

import java.util.ArrayList;

import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.citm.spring.dao.mybatis.mapper.ContactUsMapper;
import com.citm.spring.model.contact.EmailContactUs;

@Service
public class ContactUsService {

	@Autowired
	ContactUsMapper mapper;

	public ArrayList<String> getEmail(int roleId) {
		return mapper.getEmail(roleId);
	}

	public int insertGeneralContactUs(EmailContactUs emailObject) {
		return mapper.insertGeneralContactUs(emailObject);
	}
	
	public int insertServiceContactUs(EmailContactUs emailObject) {
		return mapper.insertServiceContactUs(emailObject);
	}
	
	public int insertContactUsByService(String svc, String dateContacted) {
		return mapper.insertContactUsByService(svc, dateContacted);
	}
}
