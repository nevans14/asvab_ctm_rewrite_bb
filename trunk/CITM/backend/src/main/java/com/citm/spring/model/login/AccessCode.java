package com.citm.spring.model.login;

import java.util.Date;

public class AccessCode {
	private Integer userId, unlimitedFyi;
	private String accessCode, role;
	private Date expireDate;
	
	/*Constructors*/
	public AccessCode() {}
	public AccessCode(String accessCode, String role, Date expireDate, Integer unlimitedFyi) {
		this.accessCode = accessCode;
		this.role = role;
		this.expireDate = expireDate;
		this.unlimitedFyi = unlimitedFyi;
	}
	
	public void setUserId(Integer userId) { this.userId = userId; }
	public Integer getUserId() { return userId; }
	
	public void setUnlimitedFyi(Integer unlimitedFyi) { this.unlimitedFyi = unlimitedFyi; }
	public Integer getUnlimitedFyi() { return unlimitedFyi; }
	
	public void setAccessCode(String accessCode) { this.accessCode = accessCode; }
	public String getAccessCode() { return accessCode; }
	
	public void setRole(String role) { this.role = role; }
	public String getRole() { return role; }
	
	public void setExpireDate(Date expireDate) { this.expireDate = expireDate; }
	public Date getExpireDate() { return expireDate; }
}