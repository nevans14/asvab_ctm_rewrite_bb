package com.citm.spring.webservice.rest;

import java.util.ArrayList;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.citm.spring.dao.mybatis.StaticPageDAO;
import com.citm.spring.model.staticpage.StaticPage;

@RestController
@RequestMapping("/pages")
public class StaticPageController {
	
	@Autowired
	StaticPageDAO dao;
	static Logger logger = LogManager.getLogger("StaticPageController");
	
	@RequestMapping(value = "/get-by-page-id/{pageId}/", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<StaticPage> getPageById(@PathVariable int pageId) {
		StaticPage results = null;
		
		try {
			results = dao.getPageById(pageId);
		} catch (Exception e) {
			return new ResponseEntity<StaticPage>(HttpStatus.BAD_REQUEST);
		}
		
		return new ResponseEntity<StaticPage>(results, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/get-by-page-name/{pageName}/", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<StaticPage> getPageByPageName(@PathVariable String pageName) {
		StaticPage results = null;
		
		try {
			results = dao.getPageByPageNameOld(pageName);
		} catch (Exception e) {
			return new ResponseEntity<StaticPage>(HttpStatus.BAD_REQUEST);
		}
		
		return new ResponseEntity<StaticPage>(results, HttpStatus.OK);
	}
	
    @RequestMapping(value = "/get-by-page-name-system/{pagename}/{website}/", method = RequestMethod.GET)
    public @ResponseBody ResponseEntity<StaticPage> getPageByPageName(@PathVariable("pagename") String pageName, @PathVariable("website") String website) {
        StaticPage results = null;
        
        try {
            results = dao.getPageByPageName(pageName, website);
        } catch (Exception e) {
            logger.error("Error getting pageByPageName", e);
            return new ResponseEntity<StaticPage>(HttpStatus.BAD_REQUEST);
        }
        
        return new ResponseEntity<StaticPage>(results, HttpStatus.OK);
    }
	
		
    @RequestMapping(value = "/get-list-by-name-system/{pagename}/{website}/", method = RequestMethod.GET)
    public @ResponseBody ArrayList<StaticPage> getPageListByName(@PathVariable("pagename") String pageName, @PathVariable("website") String website) {
        
		ArrayList<StaticPage> results = new ArrayList<StaticPage>();

		try {
			results = dao.getPageListByName(pageName, website);
		} catch (Exception e) {
			logger.error("Error", e);
		}
		return results;
    }

}