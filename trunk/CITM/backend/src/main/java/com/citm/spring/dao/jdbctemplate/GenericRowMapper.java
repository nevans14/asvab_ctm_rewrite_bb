package com.citm.spring.dao.jdbctemplate; 

    import java.sql.ResultSet; 
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

//import mil.osd.dmdc.psa.aces.processengine.dao.MySubjectsList_mapper.columnList;

import org.springframework.jdbc.core.RowMapper;

    public class GenericRowMapper implements RowMapper<Map<String,String>> { 

    	@Override
			public Map<String,String> mapRow(ResultSet rs, int i) throws SQLException { 
  
				Map<String,String> returnMap = new HashMap<String,String>();
				ResultSetMetaData rsmd = rs.getMetaData();
				Integer count = rsmd.getColumnCount() ;
				for ( int j = 1 ; j <= count ; j ++ ) {
					String columnName = rsmd.getColumnName(j) ;
					String columnType = rsmd.getColumnTypeName(j);
					String columnValue="";
//					System.out.println(columnName);
					try { 				
						if ( columnType.equalsIgnoreCase("nvarchar") || 
								columnType.equalsIgnoreCase("nchar") || 
								columnType.equalsIgnoreCase("longnvarchar") ) {
							columnValue = rs.getNString(rs.findColumn(columnName ));							
						} else {
							columnValue = rs.getString(rs.findColumn(columnName ));							
						}
					} catch ( SQLException se ) {
						System.out.println("*** Warning ***********************************************************");
						System.out.println(columnName.toString() + " Column not found in configured query, this may cause and issue reporting");
					}
					returnMap.put(columnName, columnValue);
				}

		   		return returnMap;
			}
}
