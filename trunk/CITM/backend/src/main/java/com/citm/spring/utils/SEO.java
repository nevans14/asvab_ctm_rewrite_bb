//package com.citm.spring.utils;
//
//import java.io.IOException;
//import java.io.PrintWriter;
//
//import javax.servlet.ServletConfig;
//import javax.servlet.ServletException;
//import javax.servlet.http.HttpServlet;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//
//import org.apache.logging.log4j.LogManager;
//import org.apache.logging.log4j.Logger;
//import org.springframework.util.StringUtils;
//import org.springframework.web.context.support.SpringBeanAutowiringSupport;
//
//public class SEO extends HttpServlet {
//
//	/**
//	 * 
//	 */
//	private static final long serialVersionUID = 1328977028542486L;
//
//	static Logger logger = LogManager.getLogger("SEO");
//
//	public void init(ServletConfig config) throws ServletException {
//		super.init(config);
//		SpringBeanAutowiringSupport.processInjectionBasedOnServletContext(this, config.getServletContext());
//	}
//
//	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//
//		String userAgent = request.getHeader("user-agent");
//
//		// Full URL.
//		String url = request.getRequestURL().toString();
//
//		if (!StringUtils.isEmpty(userAgent)) {
//			
//			// Google adwords.
//			if (userAgent.toLowerCase().contains("adsbot") || userAgent.toLowerCase().contains("mediapartners")) {
//				response.sendError(307);
//			}
//
//			// Checks to see if the user is a crawler.
//			if (userAgent.toLowerCase().contains("googlebot") || userAgent.toLowerCase().contains("bingbot")
//					|| userAgent.toLowerCase().contains("slurp")) {
//
//				logger.debug(userAgent + ": This is a bot");
//
//				// Crawler is trying to access Find Your Interest score page.
//				if (request.getRequestURI().toLowerCase().contains("advanced-search")) {
//
//					String title = "Advanced Search | Careers in the Military";
//					String description = "Search for jobs by branch, enlisted/officer, active/reserve, skill importance and other criteria through Careers in the Military.";
//					makeSEOBotPage(request, response, title, description);
//
//				} else if (request.getRequestURI().toLowerCase().contains("guided-exploration")) {
//
//					String title = "Guided Exploration | Careers in the Military";
//					String description = "Answer questions based on your skills and interests and find military career options that are right for you.";
//					makeSEOBotPage(request, response, title, description);
//
//				} else if (request.getRequestURI().toLowerCase().contains("contact-us")) {
//
//					String title = "Contact Us | Careers in the Military";
//					String description = "Many of the questions you have about a military career have been asked before. Explore the Frequently Asked Questions to find the answers you need.";
//					makeSEOBotPage(request, response, title, description);
//
//				} else if (request.getRequestURI().toLowerCase().contains("parent")) {
//
//					String title = "Parents of Soldiers | Careers in the Military";
//					String description = "Your child is interested pursuing a career in the military. Get details about enlistment and help them gather information about their options.";
//					makeSEOBotPage(request, response, title, description);
//
//				} else if (request.getRequestURI().toLowerCase().contains("options-contact-recruiter")) {
//
//					String title = "Contact a Recruiter | Careers in the Military";
//					String description = "Meeting with a recruiter does not obligate you to serve; it's simply a chance to get information. Contact a recruiter to find careers in the military for you.";
//					makeSEOBotPage(request, response, title, description);
//
//				} else if (request.getRequestURI().toLowerCase().contains("options-reasons-to-consider")) {
//
//					String title = "Reasons to Consider the Military | Careers in the Military";
//					String description = "Find out why others pursued a career in the military and learn about the options you have once you're enlisted.";
//					makeSEOBotPage(request, response, title, description);
//
//				} else if (request.getRequestURI().toLowerCase().contains("options-benefits")) {
//
//					String title = "Benefits of Enlisting | Careers in the Military";
//					String description = "Those with careers in the military receive health care, housing and food allowances, educational opportunities and special home loans, among other benefits.";
//					makeSEOBotPage(request, response, title, description);
//
//				} else if (request.getRequestURI().toLowerCase().contains("options-pay")) {
//
//					String title = "Types of Pay | Careers in the Military";
//					String description = "Military salaries come from base pay and special pay. However, those with careers in the military are often eligible for additional compensation and bonuses.";
//					makeSEOBotPage(request, response, title, description);
//
//				} else if (request.getRequestURI().toLowerCase().contains("options-becoming-an-officer")) {
//
//					String title = "Become a Military Officer | Careers in the Military";
//					String description = "Searching careers in the military? Officers can enter after they've completed a four-year college degree or can receive training through enlisted service.";
//					makeSEOBotPage(request, response, title, description);
//
//				} else if (request.getRequestURI().toLowerCase().contains("options-enlistment-process")) {
//
//					String title = "Enlistment Process | Careers in the Military";
//					String description = "To enlist, visit a Military Entrance Processing Station. Here, your physical qualifications, skills, and moral standards are assessed by each branch of service.";
//					makeSEOBotPage(request, response, title, description);
//
//				} else if (request.getRequestURI().toLowerCase().contains("options-enlistment-requirements")) {
//
//					String title = "Enlistment Requirements | Careers in the Military";
//					String description = "Want a career in the military? There are a few requirements. Generally, you must be 18 (17 with parental consent), be a U.S. citizen and have a high school diploma.";
//					makeSEOBotPage(request, response, title, description);
//
//				} else if (request.getRequestURI().toLowerCase().contains("options-enlisted-vs-officer")) {
//
//					String title = "Enlisted vs. Officer | Careers in the Military";
//					String description = "Officers manage enlisted personnel; they plan missions, provide orders, & assign tasks. Both careers provide competitive educational, training, & compensation.";
//					makeSEOBotPage(request, response, title, description);
//
//				} else if (request.getRequestURI().toLowerCase().contains("options-boot-camp-services")) {
//
//					String title = "Different Types of Boot Camp | Careers in the Military";
//					String description = "To serve in the military you must complete boot camp. Explore the physical fitness requirements, boot camp duration and locations to prepare you for each branch.";
//					makeSEOBotPage(request, response, title, description);
//
//				} else if (request.getRequestURI().toLowerCase().contains("options-types-of-service")) {
//
//					String title = "Different Types of Service | Careers in the Military";
//					String description = "The military is comprised of 12 branches: five Active Duty and seven part-time duty. Discover which service and career in the military is best for you.";
//					makeSEOBotPage(request, response, title, description);
//
//				} else if (request.getRequestURI().toLowerCase().contains("options-military")) {
//
//					String title = "Different Branches of Military | Careers in the Military";
//					String description = "Each branch of the military has a unique mission. Explore careers in the military, their benefits, and general time-commitment that give you the life you want.";
//					makeSEOBotPage(request, response, title, description);
//
//				} else if (request.getRequestURI().toLowerCase().contains("options")) {
//
//					String title = "Learn About Your Options | Careers in the Military";
//					String description = "There are many options when choosing careers in the military.  Compare the types of service, enlistment requirements and processes, pay, benefits and more.";
//					makeSEOBotPage(request, response, title, description);
//
//				} else {
//					response.sendError(308);
//					//response.sendError(HttpServletResponse.SC_TEMPORARY_REDIRECT);
//				}
//
//			} else {
//				response.sendError(308);
//				//response.sendError(HttpServletResponse.SC_TEMPORARY_REDIRECT);
//				// throw new ServletException("Not Crawler.");
//			}
//
//		} else {
//			response.sendError(308);
//			//response.sendError(HttpServletResponse.SC_TEMPORARY_REDIRECT);
//			// throw new ServletException("Not Crawler.");
//		}
//
//	}
//
//	/**
//	 * Creates and returns crawler friendly HTML page with title and descript
//	 * set for SEO purpose.
//	 * 
//	 * @param request
//	 * @param response
//	 * @param occupation
//	 * @param url
//	 * @param socId
//	 * @throws IOException
//	 */
//	private void makeSEOBotPage(HttpServletRequest request, HttpServletResponse response, String title,
//			String description) throws IOException {
//
//		PrintWriter out = response.getWriter();
//		out.println("<!DOCTYPE html>");
//		out.println("<html>");
//		out.println("<head>");
//		out.println("<meta charset=\"UTF-8\">");
//		out.println("<title>" + title + "</title>");
//		out.println("<meta name=\"description\" content=\"" + description + "\" />");
//		out.println("</head>");
//		out.println("<body>");
//		out.println("<h1>SEO Crawler Page</h1>");
//		out.println("</body>");
//		out.println("</html>");
//	}
//
//}
