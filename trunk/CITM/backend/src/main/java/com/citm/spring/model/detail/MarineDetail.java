package com.citm.spring.model.detail;

import java.io.Serializable;

public class MarineDetail implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -777353691659388893L;
	String mcId, socId, svc, mpc, title, motdTitle, mocId, description;
	Integer motdId;

	public String getMcId() {
		return mcId;
	}

	public void setMcId(String mcId) {
		this.mcId = mcId;
	}

	public String getSocId() {
		return socId;
	}

	public void setSocId(String socId) {
		this.socId = socId;
	}

	public String getSvc() {
		return svc;
	}

	public void setSvc(String svc) {
		this.svc = svc;
	}

	public String getMpc() {
		return mpc;
	}

	public void setMpc(String mpc) {
		this.mpc = mpc;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getMotdTitle() {
		return motdTitle;
	}

	public void setMotdTitle(String motdTitle) {
		this.motdTitle = motdTitle;
	}

	public String getMocId() {
		return mocId;
	}

	public void setMocId(String mocId) {
		this.mocId = mocId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getMotdId() {
		return motdId;
	}

	public void setMotdId(Integer motdId) {
		this.motdId = motdId;
	}
}
