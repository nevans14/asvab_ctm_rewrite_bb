package com.citm.spring.webservice.rest;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.citm.spring.CEPMailer;
import com.citm.spring.dao.mybatis.impl.ContactUsDAOImpl;
import com.citm.spring.model.contact.EmailContactUs;
import com.citm.spring.utils.VerifyRecaptcha;

@Controller
@RequestMapping("/contactUs")
public class ContactUs {

	static Logger logger = LogManager.getLogger("ContactUs");

	@Autowired
	CEPMailer cepMailer;

	@Autowired
	ContactUsDAOImpl contactUsDao;

	/**
	 * This service is used for general contact us to email administrators.
	 * 
	 * @param emailObject
	 * @return
	 */
	@RequestMapping(value = "/email", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<Map<String, String>> email(@RequestBody EmailContactUs emailObject) {

		try {

			// Verify recaptcha with Google.
			VerifyRecaptcha verifyRecaptcha = new VerifyRecaptcha();
			boolean isVerified = verifyRecaptcha.verify(emailObject.getRecaptchaResponse());

			// If verify then proceed with sending inquire email.
			if (isVerified) {

				// Message body.
				String message = "User Type: " + emailObject.getUserType() + "\n" + "First Name: "
						+ emailObject.getFirstName() + "\n" + "Last Name: " + emailObject.getLastName() + "\n"
						+ "User Email: " + emailObject.getEmail() + "\n" + "Topic: " + emailObject.getTopics() + "\n"
						+ "Message: " + emailObject.getMessage() + "\n" + "City: " + emailObject.getCity() + "\n"
						+ "State: " + emailObject.getState() + "\n" + "Zip: " + emailObject.getZip() + "\n"
						+ "Question Type: " + emailObject.getQuestionType() + "\n";

				String subject = "Someone is trying to contact us";

				// Get administrator emails.
				try {
					ArrayList<String> emails = contactUsDao.getEmail(6);

					// Send inquire to administrators.
					for (int i = 0; i < emails.size(); i++) {
						cepMailer.sendEmail(emails.get(i), emailObject.getEmail(), message, subject, null);
					}

				} catch (Exception e) {
					logger.error("Error getting administrator emails." + e);
				}

				// Store inquiry to database.
				try {
					int i = contactUsDao.insertGeneralContactUs(emailObject);

					// If zero records were inserted then throw exception.
					if (i == 0) {
						throw new RuntimeException("Error: Inserted zero records.");
					}
				} catch (Exception e) {
					logger.error("Error storing inquire to database.", e);
				}

			} else {

				// If Google recaptcha is not valid, then return error message.
				return new ResponseEntity<Map<String, String>>(
						Collections.singletonMap("errorMessage", "Robots Not Allowed!"), HttpStatus.BAD_REQUEST);

			}
		} catch (Exception e) {
			logger.error("Error sending email:" + e);
			return new ResponseEntity<Map<String, String>>(HttpStatus.BAD_REQUEST);
		}

		// Send confirmation receipt to user.
		String userEmail = emailObject.getEmail();
		String message = "Hello,\n\nThank you for your inquiry. We have received your message and we will get back to you ASAP.\n\nThank you,\nThe CITM Team";
		String subject = "CITM Received Your Inquiry";
		try {
			cepMailer.sendEmail(userEmail, message, subject, null);
		} catch (Exception e) {
			logger.error("Error sending email:" + e);
		}

		return new ResponseEntity<Map<String, String>>(HttpStatus.OK);
	}

	/**
	 * This rest service is to send contact us emails to Army and Navy only.
	 * 
	 * @param emailObject
	 * @return
	 */
	@RequestMapping(value = "/service-contact-us", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<Map<String, String>> serviceContactUs(@RequestBody EmailContactUs emailObject) {
		try {
			// Verify recaptcha with Google.
			VerifyRecaptcha verifyRecaptcha = new VerifyRecaptcha();
			boolean isVerified = verifyRecaptcha.verify(emailObject.getRecaptchaResponse());
			// If verify then proceed with sending inquire email.
			if (isVerified) {
				String subject = "ASVAB CEP";
				// Get administrator emails.
				try {
					ArrayList<String> emails = new ArrayList<String>();
					// services to contact
					ArrayList<String> svcList = emailObject.getSvcList();
					for (int a = 0; a < svcList.size(); a++) {
						String svcNm = null;
						switch (svcList.get(a)) {
							case "A": // Army
								svcNm = "Army";
								emails = contactUsDao.getEmail(4);
								storeContactService("ARMY");
								break;
							case "C": // Coast Guard
								svcNm = "Coast Guard";
								emails = contactUsDao.getEmail(7);
								storeContactService("COAST GUARD");
								break;
							case "F": // Air Force
								svcNm = "Air Force";
								if(emailObject.getContactType().equalsIgnoreCase("2")) {
									emails = contactUsDao.getEmail(10);
								} else {
									emails = contactUsDao.getEmail(8);	
								}
								storeContactService("AIR FORCE");
								break;
							case "G": // Nation Guard(Army/Air)
							case "FG":
								if (svcList.get(a).equals("G")) {
									svcNm = "Army National Guard";
									emails = contactUsDao.getEmail(9);
								} else {
									svcNm = "Air National Guard";
									emails = new ArrayList<String>(); // no email for air national guard, email sent to Air Force
								}
								storeContactService("NATIONAL GUARD");
								break;	
							case "M": // Marine Corps
								svcNm = "Marine Corps";
								emails = contactUsDao.getEmail(11);
								storeContactService("MARINE CORPS");
								break;
							case "N": // Navy
								svcNm = "Navy";
								emails = contactUsDao.getEmail(5);
								storeContactService("NAVY");
								break;
						}
						// Message body.
						String message = 
								String.format("Contact For %s\n", svcNm) 
								+ String.format("User Type: %s\n", emailObject.getUserType())
								+ String.format("First Name: %s\n", emailObject.getFirstName())
								+ String.format("Last Name: %s\n", emailObject.getLastName())
								+ String.format("User Email: %s\n", emailObject.getEmail())
								+ String.format("City: %s\n", emailObject.getCity())
								+ String.format("State: %s\n", emailObject.getState())
								+ String.format("Zip: %s\n", emailObject.getZip())
								+ String.format("Interest Type: %s\n", emailObject.getContactType())
								+ String.format("ASVAB Participant: %s\n", emailObject.getAsvabParticipant())
								+ String.format("Message: %s\n", emailObject.getMessage());
						// Send inquire to administrators.
						for (int i = 0; i < emails.size(); i++) {
							cepMailer.sendEmail(emails.get(i), emailObject.getEmail(), message, subject, null);
						}
					}
				} catch (Exception e) {
					logger.error("Error getting administrator emails." + e);
					return new ResponseEntity<Map<String, String>>(HttpStatus.BAD_REQUEST);
				}
				// Store inquiry in database.
				try {
					int i = contactUsDao.insertServiceContactUs(emailObject);					
					// If zero records were inserted then throw exception.
					if (i == 0) {
						throw new RuntimeException("Error: Inserted zero records.");
					}
				} catch (Exception e) {
					logger.error("Error storing inquire to database.", e);
					return new ResponseEntity<Map<String, String>>(HttpStatus.BAD_REQUEST);
				}
			} else {
				// If Google recaptcha is not valid, then return error message.
				return new ResponseEntity<Map<String, String>>(
						Collections.singletonMap("errorMessage", "Robots Not Allowed!"), HttpStatus.BAD_REQUEST);
			}
		} catch (Exception e) {
			logger.error("Error sending email:" + e);
			return new ResponseEntity<Map<String, String>>(HttpStatus.BAD_REQUEST);
		}
		// Send confirmation receipt to user.
		String userEmail = emailObject.getEmail();
		String message = "Hello,\n\nThank you for your inquiry. We have received your message and we will get back to you ASAP.\n\nThank you,\nThe CITM Team";
		String subject = "CITM Received Your Inquiry";
		try {
			cepMailer.sendEmail(userEmail, message, subject, null);
		} catch (Exception e) {
			logger.error("Error sending email:" + e);
		}
		return new ResponseEntity<Map<String, String>>(HttpStatus.OK);
	}
	
	// helpers
	private int storeContactService(String svc) {
		DateFormat df = new SimpleDateFormat("yyyy/MM/dd");
		Date date = new Date();
		try {
			return contactUsDao.insertContactUsByService(svc, df.format(date));
		} catch (Exception e) {
			return 0;
		}
	}
}
