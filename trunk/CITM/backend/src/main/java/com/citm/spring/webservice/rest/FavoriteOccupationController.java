package com.citm.spring.webservice.rest;

import java.util.ArrayList;

import com.citm.spring.utils.UserManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.citm.spring.dao.mybatis.impl.FavoriteOccupationDAOImpl;
import com.citm.spring.model.favorites.FavoriteCareerCluster;
import com.citm.spring.model.favorites.FavoriteOccupation;
import com.citm.spring.model.favorites.FavoriteService;

@Controller
@RequestMapping("/favorite")
public class FavoriteOccupationController {
    private Logger logger = LogManager.getLogger(this.getClass().getName());

	@Autowired
	UserManager userManager;
	@Autowired
	FavoriteOccupationDAOImpl dao;

	@RequestMapping(value = "/getFavoriteOccupation", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<ArrayList<FavoriteOccupation>> getFavoriteOccupation() {
	    logger.info("getFavoriteOccupation");
	    if (!userManager.isAuthenticated()) {
	    	return new ResponseEntity<>(HttpStatus.FORBIDDEN);
		}
		ArrayList<FavoriteOccupation> results = null;
		try {
			results = dao.getFavoriteOccupation(userManager.getUserId());
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<ArrayList<FavoriteOccupation>>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<ArrayList<FavoriteOccupation>>(results, HttpStatus.OK);
	}

	@RequestMapping(value = "/insertFavoriteOccupation", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<FavoriteOccupation> insertFavoriteOccupation(
			@RequestBody FavoriteOccupation favoriteObject) {
	    logger.info("insertFavoriteOccupation");
		if (!userManager.isAuthenticated()) {
			return new ResponseEntity<>(HttpStatus.FORBIDDEN);
		}
		int rowsInserted = 0;
		favoriteObject.setUserId(userManager.getUserId());
		try {
			rowsInserted = dao.insertFavoriteOccupation(favoriteObject);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<FavoriteOccupation>(HttpStatus.BAD_REQUEST);
		}
		if (rowsInserted != 1) {
		    logger.error("Unexpected result ..");
			return new ResponseEntity<FavoriteOccupation>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<FavoriteOccupation>(favoriteObject, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/deleteFavoriteOccupation/{id}", method = RequestMethod.DELETE)
	public @ResponseBody ResponseEntity<Integer> deleteFavoriteOccupation(@PathVariable Integer id) {
		logger.info("deleteFavoriteOccupation");
		if (!userManager.isAuthenticated()) {
			return new ResponseEntity<>(HttpStatus.FORBIDDEN);
		}
		int result = 0;
		try {
			result = dao.deleteFavoriteOccupation(id);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<Integer>(HttpStatus.BAD_REQUEST);
		}
		if (result != 1) {
			return new ResponseEntity<Integer>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<Integer>(result, HttpStatus.OK);
		}
	}
	
	@RequestMapping(value = "/get-favorite-service", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<ArrayList<FavoriteService>> getFavoriteService() {
	    logger.info("getFavoriteService");
	    if (!userManager.isAuthenticated()) {
	    	return new ResponseEntity<>(HttpStatus.FORBIDDEN);
		}
		ArrayList<FavoriteService> results;
		try {
			results = dao.getFavoriteService(userManager.getUserId());
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<ArrayList<FavoriteService>>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<ArrayList<FavoriteService>>(results, HttpStatus.OK);
	}

	@RequestMapping(value = "/insert-favorite-service", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<FavoriteService> insertFavoriteService(@RequestBody FavoriteService serviceObject) {
	    logger.info("insertFavoriteService");
		if (!userManager.isAuthenticated()) {
			return new ResponseEntity<>(HttpStatus.FORBIDDEN);
		}
		int rowsInserted = 0;
		serviceObject.setUserId(userManager.getUserId());
		try {
			rowsInserted = dao.insertFavoriteService(serviceObject);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<FavoriteService>(HttpStatus.BAD_REQUEST);
		}
		if (rowsInserted != 1) {
		    logger.error("Unexpected result ..");
			return new ResponseEntity<FavoriteService>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<FavoriteService>(serviceObject, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/delete-favorite-service/{id}", method = RequestMethod.DELETE)
	public @ResponseBody ResponseEntity<Integer> deleteFavoriteService(@PathVariable Integer id) {
		logger.info("deleteFavoriteService");
		if (!userManager.isAuthenticated()) {
			return new ResponseEntity<>(HttpStatus.FORBIDDEN);
		}
		int result = 0;
		try {
			result = dao.deleteFavoriteService(id);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<Integer>(HttpStatus.BAD_REQUEST);
		}

		if (result != 1) {
			return new ResponseEntity<Integer>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<Integer>(result, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/getFavoriteCareerCluster", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<ArrayList<FavoriteCareerCluster>> getFavoriteCareerCluster() {
		if (!userManager.isAuthenticated()) {
			return new ResponseEntity<>(HttpStatus.FORBIDDEN);
		}
		ArrayList<FavoriteCareerCluster> results;
		try {
			results = dao.getFavoriteCareerCluster(userManager.getUserId());
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<ArrayList<FavoriteCareerCluster>>(HttpStatus.BAD_REQUEST);
		}

		return new ResponseEntity<ArrayList<FavoriteCareerCluster>>(results, HttpStatus.OK);
	}

	@RequestMapping(value = "/insertFavoriteCareerCluster", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<FavoriteCareerCluster> insertFavoriteCareerCluster(@RequestBody FavoriteCareerCluster favoriteObject) {
		if (!userManager.isAuthenticated()) {
			return new ResponseEntity<>(HttpStatus.FORBIDDEN);
		}
		int rowsInserted = 0;
		favoriteObject.setUserId(userManager.getUserId());
		try {
			rowsInserted = dao.insertFavoriteCareerCluster(favoriteObject);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<FavoriteCareerCluster>(HttpStatus.BAD_REQUEST);
		}
		if (rowsInserted != 1) {
			return new ResponseEntity<FavoriteCareerCluster>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<FavoriteCareerCluster>(favoriteObject, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/deleteFavoriteCareerCluster/{id}", method = RequestMethod.DELETE)
	public @ResponseBody ResponseEntity<Integer> deleteFavoriteCareerCluster(@PathVariable Integer id) {
		if (!userManager.isAuthenticated()) {
			return new ResponseEntity<>(HttpStatus.FORBIDDEN);
		}
		int result = 0;
		try {
			result = dao.deleteFavoriteCareerCluster(id);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<Integer>(HttpStatus.BAD_REQUEST);
		}
		if (result != 1) {
			return new ResponseEntity<Integer>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<Integer>(result, HttpStatus.OK);
		}
	}
}
