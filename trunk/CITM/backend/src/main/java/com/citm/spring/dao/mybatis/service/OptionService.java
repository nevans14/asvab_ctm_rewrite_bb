package com.citm.spring.dao.mybatis.service;

import java.util.ArrayList;

import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.citm.spring.dao.mybatis.mapper.CareerDetailMapper;
import com.citm.spring.dao.mybatis.mapper.OptionMapper;
import com.citm.spring.model.detail.WhatTheyDo;
import com.citm.spring.model.option.Pay;
import com.citm.spring.model.option.PayChart;
import com.citm.spring.model.detail.RelatedCareer;
import com.citm.spring.model.detail.ServiceOffering;
import com.citm.spring.model.detail.SkillRating;
import com.citm.spring.model.detail.AdditionalInfo;
import com.citm.spring.model.detail.AirForceDetail;
import com.citm.spring.model.detail.ArmyDetail;
import com.citm.spring.model.detail.CareerPath;
import com.citm.spring.model.detail.CoastGuardDetail;
import com.citm.spring.model.detail.HelpfulAttribute;
import com.citm.spring.model.detail.HiddenJob;
import com.citm.spring.model.detail.MarineDetail;
import com.citm.spring.model.detail.MilitaryCareer;
import com.citm.spring.model.detail.Profile;
import com.citm.spring.model.detail.RecentlyViewed;

@Service
public class OptionService {

	@Autowired
	OptionMapper mapper;

	public ArrayList<Pay> getRank() {
		return mapper.getRank();
	}

	public ArrayList<Pay> getPay() {
		return mapper.getPay();
	}

	public ArrayList<PayChart> getPayChart() {
		return mapper.getPayChart();
	}

}
