package com.citm.spring.model.contact;

import java.io.Serializable;
import java.util.ArrayList;

public class EmailContactUs implements Serializable {

	private static final long serialVersionUID = 874683555665428550L;
	String userType, questionType, firstName, lastName, email, topics, message, city, state, zip, recaptchaResponse, svcCd, contactType, asvabParticipant;
	ArrayList<String> svcList;
	
	//********************  ACCESSORS   **********************//
	// User Info
	public String getFirstName() { return firstName; }
	public String getLastName() { return lastName; }
	public String getEmail() { return email; }
	public String getUserType() { return userType; }	
	public String getAsvabParticipant() { return asvabParticipant; }
	// Location
	public String getCity() { return city; }
	public String getState() { return state; }
	public String getZip() { return zip; }
	// Contact
	public String getTopics() {	return topics; }
	public String getContactType() { return contactType; }	
	public String getQuestionType() { return questionType; }
	public String getMessage() { return message; }
	public String getRecaptchaResponse() { return recaptchaResponse; }
	// Service
	public String getSvcCd() { return svcCd; }
	public ArrayList<String> getSvcList() {	return svcList;	}
	//******************** END ACCESSORS *********************//
	//********************   MUTATORS    *********************//
	// User Info
	public void setFirstName(String firstName) { this.firstName = firstName; }
	public void setLastName(String lastName) { this.lastName = lastName; }
	public void setEmail(String email) { this.email = email; }
	public void setUserType(String userType) { this.userType = userType; }
	public void setAsvabParticipant(String asvabParticipant) { this.asvabParticipant = asvabParticipant; }
	// Location
	public void setCity(String city) { this.city = city; }
	public void setState(String state) { this.state = state; }
	public void setZip(String zip) { this.zip = zip; }
	// Contact
	public void setTopics(String topics) { this.topics = topics; }
	public void setContactType(String contactType) { this.contactType = contactType; }
	public void setQuestionType(String questionType) { this.questionType = questionType; }
	public void setMessage(String message) { this.message = message; }
	public void setRecaptchaResponse(String recaptchaResponse) { this.recaptchaResponse = recaptchaResponse; }
	// Service
	public void setSvcCd(String svcCd) { this.svcCd = svcCd; }
	public void setSvcList(ArrayList<String> svcList) { this.svcList = svcList; }
	//********************** END MUTATORS *************************//
}
