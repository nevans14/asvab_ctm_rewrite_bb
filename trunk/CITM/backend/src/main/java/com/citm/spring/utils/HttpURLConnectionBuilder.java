package com.citm.spring.utils;

import org.json.JSONObject;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import java.io.DataOutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.Map;

public class HttpURLConnectionBuilder {

    public enum HttpRequestMethod {
        GET("GET"),
        POST("POST"),
        PUT("PUT"),
        HEAD("HEAD"),
        DELETE("DELETE");

        private final String requestMethod;

        HttpRequestMethod(String requestMethod) {
            this.requestMethod = requestMethod;
        }

        public String getRequestMethod() {
            return requestMethod;
        }
    }

    private URLConnection urlConnection;
    private HashMap<String, String> properties, json;
    private boolean isBypassingCertificate;
    private String url;
    private HttpRequestMethod requestType;

    public HttpURLConnectionBuilder() {
        this.properties = new HashMap<>();
        this.requestType = HttpRequestMethod.GET;
    }

    /**
     * Sets the request url. Required.
     * @param url
     * @return
     */
    public HttpURLConnectionBuilder withUrl(String url) {
        this.url = url;
        return this;
    }

    /**
     * The request method type of the request. Default is <code>HttpRequestMethod.GET</code>.
     * @param requestType
     * @return
     */
    public HttpURLConnectionBuilder withRequestType(HttpRequestMethod requestType) {
        this.requestType = requestType;
        return this;
    }

    /**
     * Bypasses the SSL Certificate of the target request. Default is false.
     * @param isBypassingCertificate
     * @return
     */
    public HttpURLConnectionBuilder withIsBypassingCertificate(boolean isBypassingCertificate) {
        this.isBypassingCertificate = isBypassingCertificate;
        return this;
    }

    /**
     * Adds a new request property for the request.
     * @param propName
     * @param propValue
     * @return
     */
    public HttpURLConnectionBuilder withProperty(String propName, String propValue) {
        this.properties.put(propName, propValue);
        return this;
    }

    /**
     * Adds a new JSON object to write to request. Requires the Request Method to be <code>HttpRequestMethod.POST</code>
     * @param json
     * @return
     */
    public HttpURLConnectionBuilder withJson(HashMap<String, String> json) {
        if (this.json == null) {
            this.json = new HashMap<>();
        }
        this.json = json;
        return this;
    }

    /**
     * Builds a new HttpURLConnection object
     * @return
     * @throws Exception
     */
    public URLConnection build() throws Exception {
        // validation checking
        if (this.url.isEmpty()) {
            throw new Exception("URL cannot be empty.");
        }
        if (this.json != null && this.requestType != HttpRequestMethod.POST) {
            throw new Exception("Request Method must be POST if request has a JSON object.");
        }
        if (this.json == null && this.requestType == HttpRequestMethod.POST) {
            throw new Exception("JSON must be set if the request method is POST.");
        }
        // passes validation
        // create the URL object
        URL requestUrl = new URL(this.url);
        // open the connection
        this.urlConnection = requestUrl.openConnection();
        // set request properties (if any)
        if (this.properties.size() > 0) {
            for (Map.Entry<String, String> prop : this.properties.entrySet()) {
                this.urlConnection.setRequestProperty(prop.getKey(), prop.getValue());
            }
        }
        // set request method
        ((HttpURLConnection) this.urlConnection).setRequestMethod(this.requestType.getRequestMethod());
        // check if request method is POST, set DoOutput
        if (this.requestType == HttpRequestMethod.POST) {
            this.urlConnection.setDoOutput(true);
            String requestParameter = JsonApiHelper.addParameters(false, "", this.json);
            // write params before call
            DataOutputStream dos = new DataOutputStream(this.urlConnection.getOutputStream());
            dos.writeBytes(requestParameter);
            dos.flush();
            dos.close();
        }
        // check if bypassing certificate, trust all
        if (this.isBypassingCertificate) {
            SSLContext sslCtx = SSLContext.getInstance("TLSv1.2");
            sslCtx.init(null, null, null);
            ((HttpsURLConnection)this.urlConnection).setSSLSocketFactory(sslCtx.getSocketFactory());
        }
        // return HTTP connection
        return this.urlConnection;
    }
}
