package com.citm.spring.model.search;

import java.io.Serializable;

public class JobFamily implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 503027810596497856L;
	Integer id, photoId;
	String title, description;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getPhotoId() {
		return photoId;
	}

	public void setPhotoId(Integer photoId) {
		this.photoId = photoId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
