package com.citm.spring.model.testscore;

import com.citm.spring.model.testscore.AFQTRawSSComposites;
import com.citm.spring.model.testscore.ArithmeticReasoning;
import com.citm.spring.model.testscore.AssemblingObjects;
import com.citm.spring.model.testscore.AutoShopInformation;
import com.citm.spring.model.testscore.ControlInformation;
import com.citm.spring.model.testscore.ElectronicsInformation;
import com.citm.spring.model.testscore.GeneralScience;
import com.citm.spring.model.testscore.HighSchoolInformation;
import com.citm.spring.model.testscore.MathematicalAbility;
import com.citm.spring.model.testscore.MathematicsKnowledge;
import com.citm.spring.model.testscore.MechanicalComprehension;
import com.citm.spring.model.testscore.ParagraphComprehension;
import com.citm.spring.model.testscore.RaceFields;
import com.citm.spring.model.testscore.SSASVABCombineControl;
import com.citm.spring.model.testscore.ScienceTechnicalAbility;
import com.citm.spring.model.testscore.StudentInformation;
import com.citm.spring.model.testscore.StudentResultSheetScores;
import com.citm.spring.model.testscore.VerbalAbility;
import com.citm.spring.model.testscore.WordKnowledge;

public class TestScore {
	
	private ControlInformation controlInformation;
	
	private SSASVABCombineControl sSASVABCombineControl;
	
//	private ScoringSoftwareControls scoringSoftwareControls;
	
	private AFQTRawSSComposites aFQTRawSSComposites;
	
//	private ThetaItemDataScoring thetaItemDataScoring;
	
	private HighSchoolInformation highSchoolInformation;
	
//	private TestScoringIndicatorsAndControls testScoringIndicatorsAndControls;
	
	private StudentInformation studentInformation;

	private StudentResultSheetScores studentResultSheetScores;
	
	private VerbalAbility verbalAbility;
	
	private MathematicalAbility mathematicalAbility;
	
	private ScienceTechnicalAbility scienceTechnicalAbility;
	
	private GeneralScience generalScience;
	
	private ArithmeticReasoning arithmeticReasoning;
	
	private WordKnowledge wordKnowledge;
	
	private ParagraphComprehension paragraphComprehension;
	
	private MathematicsKnowledge mathematicsKnowledge;
	
	private ElectronicsInformation electronicsInformation;
	
	private AutoShopInformation autoShopInformation;
	
	private MechanicalComprehension mechanicalComprehension;
	
	private AssemblingObjects assemblingObjects;
	
	private RaceFields raceFields;

    private String resource;
    
	/**
	 * @return the controlInformation
	 */
	public ControlInformation getControlInformation() {
		return controlInformation;
	}

	/**
	 * @param controlInformation the controlInformation to set
	 */
	public void setControlInformation(ControlInformation controlInformation) {
		this.controlInformation = controlInformation;
	}

	/**
	 * @return the sSASVABCombineControl
	 */
	public SSASVABCombineControl getsSASVABCombineControl() {
		return sSASVABCombineControl;
	}

	/**
	 * @param sSASVABCombineControl the sSASVABCombineControl to set
	 */
	public void setsSASVABCombineControl(SSASVABCombineControl sSASVABCombineControl) {
		this.sSASVABCombineControl = sSASVABCombineControl;
	}

	/**
	 * @return the aFQTRawSSComposites
	 */
	public AFQTRawSSComposites getaFQTRawSSComposites() {
		return aFQTRawSSComposites;
	}

	/**
	 * @param aFQTRawSSComposites the aFQTRawSSComposites to set
	 */
	public void setaFQTRawSSComposites(AFQTRawSSComposites aFQTRawSSComposites) {
		this.aFQTRawSSComposites = aFQTRawSSComposites;
	}

	/**
	 * @return the highSchoolInformation
	 */
	public HighSchoolInformation getHighSchoolInformation() {
		return highSchoolInformation;
	}

	/**
	 * @param highSchoolInformation the highSchoolInformation to set
	 */
	public void setHighSchoolInformation(HighSchoolInformation highSchoolInformation) {
		this.highSchoolInformation = highSchoolInformation;
	}

	/**
	 * @return the studentInformation
	 */
	public StudentInformation getStudentInformation() {
		return studentInformation;
	}

	/**
	 * @param studentInformation the studentInformation to set
	 */
	public void setStudentInformation(StudentInformation studentInformation) {
		this.studentInformation = studentInformation;
	}

	/**
	 * @return the studentResultSheetScores
	 */
	public StudentResultSheetScores getStudentResultSheetScores() {
		return studentResultSheetScores;
	}

	/**
	 * @param studentResultSheetScores the studentResultSheetScores to set
	 */
	public void setStudentResultSheetScores(StudentResultSheetScores studentResultSheetScores) {
		this.studentResultSheetScores = studentResultSheetScores;
	}

	/**
	 * @return the verbalAbility
	 */
	public VerbalAbility getVerbalAbility() {
		return verbalAbility;
	}

	/**
	 * @param verbalAbility the verbalAbility to set
	 */
	public void setVerbalAbility(VerbalAbility verbalAbility) {
		this.verbalAbility = verbalAbility;
	}

	/**
	 * @return the mathematicalAbility
	 */
	public MathematicalAbility getMathematicalAbility() {
		return mathematicalAbility;
	}

	/**
	 * @param mathematicalAbility the mathematicalAbility to set
	 */
	public void setMathematicalAbility(MathematicalAbility mathematicalAbility) {
		this.mathematicalAbility = mathematicalAbility;
	}

	/**
	 * @return the scienceTechnicalAbility
	 */
	public ScienceTechnicalAbility getScienceTechnicalAbility() {
		return scienceTechnicalAbility;
	}

	/**
	 * @param scienceTechnicalAbility the scienceTechnicalAbility to set
	 */
	public void setScienceTechnicalAbility(ScienceTechnicalAbility scienceTechnicalAbility) {
		this.scienceTechnicalAbility = scienceTechnicalAbility;
	}

	/**
	 * @return the generalScience
	 */
	public GeneralScience getGeneralScience() {
		return generalScience;
	}

	/**
	 * @param generalScience the generalScience to set
	 */
	public void setGeneralScience(GeneralScience generalScience) {
		this.generalScience = generalScience;
	}

	/**
	 * @return the arithmeticReasoning
	 */
	public ArithmeticReasoning getArithmeticReasoning() {
		return arithmeticReasoning;
	}

	/**
	 * @param arithmeticReasoning the arithmeticReasoning to set
	 */
	public void setArithmeticReasoning(ArithmeticReasoning arithmeticReasoning) {
		this.arithmeticReasoning = arithmeticReasoning;
	}

	/**
	 * @return the wordKnowledge
	 */
	public WordKnowledge getWordKnowledge() {
		return wordKnowledge;
	}

	/**
	 * @param wordKnowledge the wordKnowledge to set
	 */
	public void setWordKnowledge(WordKnowledge wordKnowledge) {
		this.wordKnowledge = wordKnowledge;
	}

	/**
	 * @return the paragraphComprehension
	 */
	public ParagraphComprehension getParagraphComprehension() {
		return paragraphComprehension;
	}

	/**
	 * @param paragraphComprehension the paragraphComprehension to set
	 */
	public void setParagraphComprehension(ParagraphComprehension paragraphComprehension) {
		this.paragraphComprehension = paragraphComprehension;
	}

	/**
	 * @return the mathematicsKnowledge
	 */
	public MathematicsKnowledge getMathematicsKnowledge() {
		return mathematicsKnowledge;
	}

	/**
	 * @param mathematicsKnowledge the mathematicsKnowledge to set
	 */
	public void setMathematicsKnowledge(MathematicsKnowledge mathematicsKnowledge) {
		this.mathematicsKnowledge = mathematicsKnowledge;
	}

	/**
	 * @return the electronicsInformation
	 */
	public ElectronicsInformation getElectronicsInformation() {
		return electronicsInformation;
	}

	/**
	 * @param electronicsInformation the electronicsInformation to set
	 */
	public void setElectronicsInformation(
			ElectronicsInformation electronicsInformation) {
		this.electronicsInformation = electronicsInformation;
	}

	/**
	 * @return the autoShopInformation
	 */
	public AutoShopInformation getAutoShopInformation() {
		return autoShopInformation;
	}

	/**
	 * @param autoShopInformation the autoShopInformation to set
	 */
	public void setAutoShopInformation(AutoShopInformation autoShopInformation) {
		this.autoShopInformation = autoShopInformation;
	}

	/**
	 * @return the mechanicalComprehension
	 */
	public MechanicalComprehension getMechanicalComprehension() {
		return mechanicalComprehension;
	}

	/**
	 * @param mechanicalComprehension the mechanicalComprehension to set
	 */
	public void setMechanicalComprehension(MechanicalComprehension mechanicalComprehension) {
		this.mechanicalComprehension = mechanicalComprehension;
	}

	/**
	 * @return the assemblingObjects
	 */
	public AssemblingObjects getAssemblingObjects() {
		return assemblingObjects;
	}

	/**
	 * @param assemblingObjects the assemblingObjects to set
	 */
	public void setAssemblingObjects(AssemblingObjects assemblingObjects) {
		this.assemblingObjects = assemblingObjects;
	}

	/**
	 * @return the raceFields
	 */
	public RaceFields getRaceFields() {
		return raceFields;
	}

	/**
	 * @param raceFields the raceFields to set
	 */
	public void setRaceFields(RaceFields raceFields) {
		this.raceFields = raceFields;
	}
	/**
	 * @return the resource
	 */
	public String getResource() {
		return resource;
	}
	/**
	 * @param resource the resource to set
	 */
	public void setResource(String resource) {
		this.resource = resource;
	}
}
