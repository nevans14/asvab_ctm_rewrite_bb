package com.citm.spring.utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

@Component
public class UserManager {

    private Logger logger = LogManager.getLogger(this.getClass().getName());

    @Autowired
    private HttpServletRequest request;

    public boolean isAuthenticated() {
        boolean isAuthenticated = false;
        HttpSession session;
        SecurityContext securityContext = null;
        if (request != null) {
            session = request.getSession();
            if (session != null) {
                securityContext = (SecurityContext) session.getAttribute("SPRING_SECURITY_CONTEXT_KEY");
            }
            if (securityContext != null) {
                isAuthenticated = securityContext.getAuthentication().isAuthenticated();
            }
        }
        return isAuthenticated;
    }

    /**
     * Returns the Logged-in user's User ID
     * @return Integer
     */
    public Integer getUserId() {
        HttpSession session;
        Object attribute = null;
        if (request != null) {
            session = request.getSession();
            if (session != null) {
                attribute = session.getAttribute("ASVAB_USER_ID");
            }
            if (attribute != null) {
                try {
                    return Integer.parseInt(attribute.toString());
                } catch (Exception ex) {
                    logger.error(ex.getMessage());
                }
            }
        }
        return null;
    }

    /***
     * Returns the Logged-in user's role
     * @return String
     */
    public String getRole() {
        HttpSession session;
        Object attribute = null;
        if (request != null) {
            session = request.getSession();
            if (session != null) {
                attribute = session.getAttribute("USER_ROLE");
            }
            if (attribute != null) {
                return attribute.toString();
            }
        }
        return null;
    }

    /**
     * Returns the Logged-in user's access code
     * @return String
     */
    public String getAccessCode() {
        HttpSession session;
        Object attribute = null;
        if (request != null) {
            session = request.getSession();
            if (session != null) {
                attribute = session.getAttribute("USER_ACCESS_CODE");
            }
            if (attribute != null) {
                return attribute.toString();
            }
        }
        return null;
    }

    public void createMock() {
        // create authorities
        List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>() {{
            add(new SimpleGrantedAuthority("ROLE_USER"));
        }};
        // create dummy authentication
        UsernamePasswordAuthenticationToken authenticationToken =
                new UsernamePasswordAuthenticationToken("MyUserName", "MyPassword", authorities);
        // set authentication
        SecurityContextHolder.getContext().setAuthentication(authenticationToken);
        // create session
        HttpSession session = request.getSession(true);
        // set session information
        session.setAttribute("SPRING_SECURITY_CONTEXT_KEY", SecurityContextHolder.getContext());
        session.setAttribute("ASVAB_USER_ID", 2208286);
        session.setAttribute("ASVAB_USER_ROLE", "USER_ROLE");
        session.setAttribute("USER_ROLE", "P");
        session.setAttribute("USER_ACCESS_CODE", "205602147L");
    }
}
