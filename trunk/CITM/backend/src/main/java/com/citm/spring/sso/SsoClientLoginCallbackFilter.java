package com.citm.spring.sso;

import java.io.IOException;
import java.net.URLEncoder;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.savedrequest.RequestCache;
import org.springframework.security.web.savedrequest.SavedRequest;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.web.util.matcher.RequestMatcher;
import org.springframework.session.ExpiringSession;
import org.springframework.session.SessionRepository;
import org.springframework.web.filter.OncePerRequestFilter;

/**
 * This class utilizes Redis and Spring Session to share httpsession between
 * CEP and CITM. When user login CEP, they are redirected here to set the
 * session and create a cookie for Angular. This addresses cross domain
 * SSO.
 *
 */
@PropertySource("classpath:SsoProperties.properties")
public class SsoClientLoginCallbackFilter extends OncePerRequestFilter {

	public final String TOKEN_PARAM = "token";
	private final int COOKIE_DEFAULT_AGE = -1;
	private final String COOKIE_DEFAULT_PATH = "/";
	
	@Value("${citm.frontendurl}")
	private String citmFrontend;

	@Value("${cep.frontendurl")
	private String cepFrontend;

	@Value("${citm.domain}")
	private String domain;
	private String CEP_DASHBOARD_URL = "dashboard";

	private SessionRepository<ExpiringSession> sessionRepository;
	private RequestCache requestCache;

	static Logger logger = LogManager.getLogger("SsoClientLoginCallbackFilter");
	private RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();
	private RequestMatcher requestMatcher = new AntPathRequestMatcher("/login_callback/test");

	@SuppressWarnings("unchecked")
	public <S extends ExpiringSession> SsoClientLoginCallbackFilter(SessionRepository<S> sessionRepository,
			RequestCache requestCache) {
		this.sessionRepository = (SessionRepository<ExpiringSession>) sessionRepository;
		this.requestCache = requestCache;
	}

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {
		try {
			if (requestMatcher.matches(request)) {
				String token = request.getParameter(TOKEN_PARAM);
				
				if (token != null) {	
					String sessionId = decodeAndDecrypt(token);
					ExpiringSession session = sessionRepository.getSession(sessionId);
					SecurityContext securityContext = session.getAttribute("SPRING_SECURITY_CONTEXT_KEY");
					String userRole = session.getAttribute("USER_ROLE");
					String userId = session.getAttribute("ASVAB_USER_ID");

					if (session != null && securityContext.getAuthentication().isAuthenticated()) {
						logger.debug("found valid session {}", session);
						// Cookie cookie = new Cookie("SESSION", sessionId);
						// cookie.setPath(COOKIE_DEFAULT_PATH);
						// cookie.setMaxAge(COOKIE_DEFAULT_AGE);
						// response.addCookie(cookie);

						// Create cookie used by Angular.
						String jsonString = String.format("{\"currentUser\":{\"username\":\"%s\",\"role\":\"%s\",\"authdata\":\"null==\"}}", userId, userRole);
						String data = URLEncoder.encode(jsonString, "UTF-8");
						// cookie = new Cookie("globals", URLEncoder.encode(jsonString, "UTF-8"));
						//cookie.setDomain(domain);
						// cookie.setPath(COOKIE_DEFAULT_PATH);
						// cookie.setMaxAge(COOKIE_DEFAULT_AGE);
						// response.addCookie(cookie);

						// String targetUrl = defaultTargetUrl;
						SavedRequest savedRequest = requestCache.getRequest(request, response);

						if (savedRequest != null) {
							logger.debug("re-enforcing cached request at {} {}", savedRequest.getRedirectUrl(),
									savedRequest.getMethod());
							session.setAttribute("SPRING_SECURITY_SAVED_REQUEST", savedRequest);
							sessionRepository.save(session);
						} else {
							sessionRepository.save(session);
						}						
						redirectStrategy.sendRedirect(request, response, citmFrontend + "sso-login/" + CEP_DASHBOARD_URL + '/' + data);
						return;
					}
				}
			}
		} catch (Exception e) {
			logger.error("CEP SSO Failed", e);
			redirectStrategy.sendRedirect(request, response, cepFrontend + CEP_DASHBOARD_URL);
			return;
		}
		filterChain.doFilter(request, response);
	}

	private String decodeAndDecrypt(String token) {
		try {
			// decode
			byte[] decoded = Base64.getUrlDecoder().decode(token);

			String key = "G~Y@86-FtH&gq'_e"; // 128 bit key, better be handled
												// in external properties
			// Create key and cipher
			Key aesKey = new SecretKeySpec(key.getBytes(), "AES");
			Cipher cipher = Cipher.getInstance("AES");
			// Decrypt
			cipher.init(Cipher.DECRYPT_MODE, aesKey);
			String decrypted = new String(cipher.doFinal(decoded));
			return decrypted;

		} catch (IllegalBlockSizeException | BadPaddingException | NoSuchAlgorithmException | NoSuchPaddingException
				| InvalidKeyException e) {

		}
		return null;
	}

}
