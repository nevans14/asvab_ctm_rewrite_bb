package com.citm.spring.dao.mybatis.impl;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.citm.spring.dao.mybatis.StaticPageDAO;
import com.citm.spring.dao.mybatis.service.StaticPageService;
import com.citm.spring.model.staticpage.StaticPage;

@Repository
public class StaticPageDAOImpl implements StaticPageDAO {
	@Autowired
	StaticPageService service;
	
	public StaticPage getPageById(int pageId) {
		return service.getPageById(pageId);
	}
	
	public StaticPage getPageByPageNameOld(String pageName) {
		return service.getPageByPageNameOld(pageName);
	}
	
    public StaticPage getPageByPageName(String pageName, String website) {
        return service.getPageByPageName(pageName, website);
    }
		
    public ArrayList<StaticPage> getPageListByName(String pageName, String website) {
        return service.getPageListByName(pageName, website);
    }
}