package com.citm.spring.webservice.rest;

import java.util.Date;
import java.util.ArrayList;

import com.citm.spring.SpringRedisConfig;
import com.citm.spring.utils.SimpleUtils;
import com.citm.spring.utils.UserManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.citm.spring.dao.mybatis.impl.CareerSearchDAOImpl;
import com.citm.spring.model.search.CareerCluster;
import com.citm.spring.model.search.CareerClusterPathway;
import com.citm.spring.model.search.JobFamily;
import com.citm.spring.model.search.SaveSearch;
import com.citm.spring.model.search.Search;
import com.citm.spring.model.search.ServiceContacts;

@Controller
@RequestMapping("/search")
public class CareerSearchController {

	private Logger logger = LogManager.getLogger(this.getClass().getName());
	
	@Autowired
	CareerSearchDAOImpl dao;
	@Autowired
	UserManager userManager;

	private ConfigurableApplicationContext ctx = new AnnotationConfigApplicationContext(SpringRedisConfig.class);
	@SuppressWarnings("unchecked")
	private RedisTemplate<String, Object> redisTemplate = (RedisTemplate<String, Object>)ctx.getBean("redisTemplate");
	private ValueOperations<String, Object> values = redisTemplate.opsForValue();

	@RequestMapping(value = "/get-search-list", method = RequestMethod.GET)
	public @ResponseBody ArrayList<Search> getSearchList() {
		ArrayList<Search> results = new ArrayList<Search>();
		try {
			if (values.get("searchList") != null) {
				results = (ArrayList<Search>)values.get("searchList");
			} else {
				results = dao.getSearchList();
				values.set("searchList", results);
				// keep the cache for a day
				redisTemplate.expireAt("searchList", SimpleUtils.datePlusDays(new Date(), 1));
			}
		} catch (Exception e) {
			logger.error("Error", e);
		}
		return results;
	}

	@RequestMapping(value = "/insert-save-search", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<SaveSearch> insertSaveSearch(@RequestBody SaveSearch saveSearchObject) {
		int rowsInserted = 0;
		saveSearchObject.setUserId(userManager.getUserId());
		try {
			rowsInserted = dao.insertSaveSearch(saveSearchObject);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<SaveSearch>(HttpStatus.BAD_REQUEST);
		}
		if (rowsInserted != 1) {
			return new ResponseEntity<SaveSearch>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<SaveSearch>(saveSearchObject, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/get-save-search-list", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<ArrayList<SaveSearch>> getSaveSearch() {
		ArrayList<SaveSearch> results;
		try {
			results = dao.getSaveSearch(userManager.getUserId());
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<ArrayList<SaveSearch>>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<ArrayList<SaveSearch>>(results, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/delete-save-search/{id}", method = RequestMethod.DELETE)
	public @ResponseBody ResponseEntity<Integer> deleteSaveSearch(@PathVariable Integer id) {
		int result = 0;
		try {
			result = dao.deleteSaveSearch(userManager.getUserId(), id);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<Integer>(HttpStatus.BAD_REQUEST);
		}

		if (result != 1) {
			return new ResponseEntity<Integer>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<Integer>(result, HttpStatus.OK);
		}
	}
	
	@RequestMapping(value = "/get-career-cluster", method = RequestMethod.GET)
	public @ResponseBody ArrayList<CareerCluster> getCareerCluster() {
		ArrayList<CareerCluster> results = new ArrayList<CareerCluster>();

		try {
			results = dao.getCareerCluster();
		} catch (Exception e) {
			logger.error("Error", e);
		}
		return results;
	}
	
	@RequestMapping(value = "/get-service-contacts", method = RequestMethod.GET)
	public @ResponseBody ArrayList<ServiceContacts> getServiceContacts() {
		ArrayList<ServiceContacts> results = new ArrayList<ServiceContacts>();

		try {
			results = dao.getServiceContacts();
		} catch (Exception e) {
			logger.error("Error", e);
		}
		return results;
	}
	
	@RequestMapping(value = "/get-career-cluster-pathway/{ccId}", method = RequestMethod.GET)
	public @ResponseBody ArrayList<CareerClusterPathway> getCareerClusterPathway(@PathVariable int ccId) {
		ArrayList<CareerClusterPathway> results = new ArrayList<CareerClusterPathway>();

		try {
			results = dao.getCareerClusterPathway(ccId);
		} catch (Exception e) {
			logger.error("Error", e);
		}
		return results;
	}
	
	@RequestMapping(value = "/get-job-family", method = RequestMethod.GET)
	public @ResponseBody ArrayList<JobFamily> getJobFamily() {
		ArrayList<JobFamily> results = new ArrayList<JobFamily>();

		try {
			results = dao.getJobFamily();
		} catch (Exception e) {
			logger.error("Error", e);
		}
		return results;
	}

}
