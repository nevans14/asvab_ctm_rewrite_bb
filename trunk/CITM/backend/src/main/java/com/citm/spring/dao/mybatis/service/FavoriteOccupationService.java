package com.citm.spring.dao.mybatis.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.citm.spring.dao.mybatis.mapper.FavoriteOccupationMapper;
import com.citm.spring.model.favorites.FavoriteCareerCluster;
import com.citm.spring.model.favorites.FavoriteOccupation;
import com.citm.spring.model.favorites.FavoriteService;

@Service
public class FavoriteOccupationService {

	@Autowired
	FavoriteOccupationMapper mapper;

	public ArrayList<FavoriteOccupation> getFavoriteOccupation(Integer userId) {
		return mapper.getFavoriteOccupation(userId);
	}

	public int insertFavoriteOccupation(FavoriteOccupation favoriteObject) {
		return mapper.insertFavoriteOccupation(favoriteObject);
	}

	public int deleteFavoriteOccupation(Integer id) {
		return mapper.deleteFavoriteOccupation(id);
	}
	
	public ArrayList<FavoriteService> getFavoriteService(Integer userId) {
		return mapper.getFavoriteService(userId);
	}

	public int insertFavoriteService(FavoriteService serviceObject) {
		return mapper.insertFavoriteService(serviceObject);
	}

	public int deleteFavoriteService(Integer id) {
		return mapper.deleteFavoriteService(id);
	}

	public ArrayList<FavoriteCareerCluster> getFavoriteCareerCluster(Integer userId) {
		return mapper.getFavoriteCareerCluster(userId);
	}

	public int insertFavoriteCareerCluster(FavoriteCareerCluster favoriteObject) {
		return mapper.insertFavoriteCareerCluster(favoriteObject);
	}

	public int deleteFavoriteCareerCluster(Integer id) {
		return mapper.deleteFavoriteCareerCluster(id);
	}

}
