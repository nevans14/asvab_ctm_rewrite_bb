package com.citm.spring.dao.mybatis.impl;

import java.util.ArrayList;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.citm.spring.dao.mybatis.service.OptionService;
import com.citm.spring.model.option.Pay;
import com.citm.spring.model.option.PayChart;

@Repository
public class OptionDAOImpl {
	static Logger logger = LogManager.getLogger("OptionDAOImpl");
	@Autowired
	OptionService service;

	public ArrayList<Pay> getRank() throws Exception {
		return service.getRank();
	}

	public ArrayList<Pay> getPay() throws Exception {
		return service.getPay();
	}

	public ArrayList<PayChart> getPayChart() throws Exception {
		return service.getPayChart();
	}

}
