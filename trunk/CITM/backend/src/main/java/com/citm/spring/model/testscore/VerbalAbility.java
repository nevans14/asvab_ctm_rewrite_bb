package com.citm.spring.model.testscore;

public class VerbalAbility {

	private	int	VA_YP_Score;
	private	int	VA_GS_Percentage;
	private	int	VA_GOS_Percentage;
	private	int	VA_COMP_Percentage;
	private	int	VA_TP_Percentage;
	private	int	VA_SGS;
	private	int	VA_USL;
	private	int	VA_LSL;
	/**
	 * @return the vA_YP_Score
	 */
	public int getVA_YP_Score() {
		return VA_YP_Score;
	}
	/**
	 * @param vA_YP_Score the vA_YP_Score to set
	 */
	public void setVA_YP_Score(int vA_YP_Score) {
		VA_YP_Score = vA_YP_Score;
	}
	/**
	 * @return the vA_GS_Percentage
	 */
	public int getVA_GS_Percentage() {
		return VA_GS_Percentage;
	}
	/**
	 * @param vA_GS_Percentage the vA_GS_Percentage to set
	 */
	public void setVA_GS_Percentage(int vA_GS_Percentage) {
		VA_GS_Percentage = vA_GS_Percentage;
	}
	/**
	 * @return the vA_GOS_Percentage
	 */
	public int getVA_GOS_Percentage() {
		return VA_GOS_Percentage;
	}
	/**
	 * @param vA_GOS_Percentage the vA_GOS_Percentage to set
	 */
	public void setVA_GOS_Percentage(int vA_GOS_Percentage) {
		VA_GOS_Percentage = vA_GOS_Percentage;
	}
	/**
	 * @return the vA_COMP_Percentage
	 */
	public int getVA_COMP_Percentage() {
		return VA_COMP_Percentage;
	}
	/**
	 * @param vA_COMP_Percentage the vA_COMP_Percentage to set
	 */
	public void setVA_COMP_Percentage(int vA_COMP_Percentage) {
		VA_COMP_Percentage = vA_COMP_Percentage;
	}
	/**
	 * @return the vA_TP_Percentage
	 */
	public int getVA_TP_Percentage() {
		return VA_TP_Percentage;
	}
	/**
	 * @param vA_TP_Percentage the vA_TP_Percentage to set
	 */
	public void setVA_TP_Percentage(int vA_TP_Percentage) {
		VA_TP_Percentage = vA_TP_Percentage;
	}
	/**
	 * @return the vA_SGS
	 */
	public int getVA_SGS() {
		return VA_SGS;
	}
	/**
	 * @param vA_SGS the vA_SGS to set
	 */
	public void setVA_SGS(int vA_SGS) {
		VA_SGS = vA_SGS;
	}
	/**
	 * @return the vA_USL
	 */
	public int getVA_USL() {
		return VA_USL;
	}
	/**
	 * @param vA_USL the vA_USL to set
	 */
	public void setVA_USL(int vA_USL) {
		VA_USL = vA_USL;
	}
	/**
	 * @return the vA_LSL
	 */
	public int getVA_LSL() {
		return VA_LSL;
	}
	/**
	 * @param vA_LSL the vA_LSL to set
	 */
	public void setVA_LSL(int vA_LSL) {
		VA_LSL = vA_LSL;
	}

	
}
