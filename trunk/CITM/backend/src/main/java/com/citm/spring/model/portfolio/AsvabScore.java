package com.citm.spring.model.portfolio;

import java.io.Serializable;

public class AsvabScore implements Serializable {

	private static final long serialVersionUID = 231185583971219586L;
	private Integer id;
	private Integer userId;
	private double verbalScore;
	private double mathScore;
	private double scienceScore;
	private double afqtScore;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public double getVerbalScore() {
		return verbalScore;
	}

	public void setVerbalScore(double verbalScore) {
		this.verbalScore = verbalScore;
	}

	public double getMathScore() {
		return mathScore;
	}

	public void setMathScore(double mathScore) {
		this.mathScore = mathScore;
	}

	public double getScienceScore() {
		return scienceScore;
	}

	public void setScienceScore(double scienceScore) {
		this.scienceScore = scienceScore;
	}

	public double getAfqtScore() {
		return afqtScore;
	}

	public void setAfqtScore(double afqtScore) {
		this.afqtScore = afqtScore;
	}
}
