package com.citm.spring.model.detail;

import java.io.Serializable;
import java.util.ArrayList;

public class AirForceDetail implements Serializable {

	private static final long serialVersionUID = -3368368013653962910L;
	private String mocId, careerField, link, specialty, task, minEducation, asvabRequirement, basicTraining,
			officerTrainingSchool, physicalDemand, airmensWeek, technicalTraining, techSchoolLocation, requiredHSCourse,
			desiredHSCourse, videos, mcId, socId, svc, mpc, title, motdTitle, compScoreOne, compScoreTwo;
	private Integer motdId;
	private ArrayList<airforceCommunityMap> airforceCommunityMap;
	private ArrayList<airforceContextMap> airforceContextMap;
	private ArrayList<airforceActivityMap> airforceActivityMap;
	private ArrayList<AirforceOfficerCommunityMap> airforceOfficerCommunityMap;
	private ArrayList<AirforceOfficerContextMap> airforceOfficerContextMap;
	private ArrayList<AirforceOfficerActivityMap> airforceOfficerActivityMap;
	private ArrayList<AirforceOfficerAttributeMap> airforceOfficerAttributeMap;

	public ArrayList<AirforceOfficerCommunityMap> getAirforceOfficerCommunityMap() {
		return airforceOfficerCommunityMap;
	}

	public void setAirforceOfficerCommunityMap(ArrayList<AirforceOfficerCommunityMap> airforceOfficerCommunityMap) {
		this.airforceOfficerCommunityMap = airforceOfficerCommunityMap;
	}

	public ArrayList<AirforceOfficerContextMap> getAirforceOfficerContextMap() {
		return airforceOfficerContextMap;
	}

	public void setAirforceOfficerContextMap(ArrayList<AirforceOfficerContextMap> airforceOfficerContextMap) {
		this.airforceOfficerContextMap = airforceOfficerContextMap;
	}

	public ArrayList<AirforceOfficerActivityMap> getAirforceOfficerActivityMap() {
		return airforceOfficerActivityMap;
	}

	public void setAirforceOfficerActivityMap(ArrayList<AirforceOfficerActivityMap> airforceOfficerActivityMap) {
		this.airforceOfficerActivityMap = airforceOfficerActivityMap;
	}

	public ArrayList<AirforceOfficerAttributeMap> getAirforceOfficerAttributeMap() {
		return airforceOfficerAttributeMap;
	}

	public void setAirforceOfficerAttributeMap(ArrayList<AirforceOfficerAttributeMap> airforceOfficerAttributeMap) {
		this.airforceOfficerAttributeMap = airforceOfficerAttributeMap;
	}

	public String getCompScoreOne() {
		return compScoreOne;
	}

	public void setCompScoreOne(String compScoreOne) {
		this.compScoreOne = compScoreOne;
	}

	public String getCompScoreTwo() {
		return compScoreTwo;
	}

	public void setCompScoreTwo(String compScoreTwo) {
		this.compScoreTwo = compScoreTwo;
	}

	public ArrayList<airforceCommunityMap> getAirforceCommunityMap() {
		return airforceCommunityMap;
	}

	public void setAirforceCommunityMap(ArrayList<airforceCommunityMap> airforceCommunityMap) {
		this.airforceCommunityMap = airforceCommunityMap;
	}

	public ArrayList<airforceContextMap> getAirforceContextMap() {
		return airforceContextMap;
	}

	public void setAirforceContextMap(ArrayList<airforceContextMap> airforceContextMap) {
		this.airforceContextMap = airforceContextMap;
	}

	public ArrayList<airforceActivityMap> getAirforceActivityMap() {
		return airforceActivityMap;
	}

	public void setAirforceActivityMap(ArrayList<airforceActivityMap> airforceActivityMap) {
		this.airforceActivityMap = airforceActivityMap;
	}

	public String getMocId() {
		return mocId;
	}

	public void setMocId(String mocId) {
		this.mocId = mocId;
	}

	public String getCareerField() {
		return careerField;
	}

	public void setCareerField(String careerField) {
		this.careerField = careerField;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public String getSpecialty() {
		return specialty;
	}

	public void setSpecialty(String specialty) {
		this.specialty = specialty;
	}

	public String getTask() {
		return task;
	}

	public void setTask(String task) {
		this.task = task;
	}

	public String getMinEducation() {
		return minEducation;
	}

	public void setMinEducation(String minEducation) {
		this.minEducation = minEducation;
	}

	public String getAsvabRequirement() {
		return asvabRequirement;
	}

	public void setAsvabRequirement(String asvabRequirement) {
		this.asvabRequirement = asvabRequirement;
	}

	public String getBasicTraining() {
		return basicTraining;
	}

	public void setBasicTraining(String basicTraining) {
		this.basicTraining = basicTraining;
	}

	public String getOfficerTrainingSchool() {
		return officerTrainingSchool;
	}

	public void setOfficerTrainingSchool(String officerTrainingSchool) {
		this.officerTrainingSchool = officerTrainingSchool;
	}

	public String getPhysicalDemand() {
		return physicalDemand;
	}

	public void setPhysicalDemand(String physicalDemand) {
		this.physicalDemand = physicalDemand;
	}

	public String getAirmensWeek() {
		return airmensWeek;
	}

	public void setAirmensWeek(String airmensWeek) {
		this.airmensWeek = airmensWeek;
	}

	public String getTechnicalTraining() {
		return technicalTraining;
	}

	public void setTechnicalTraining(String technicalTraining) {
		this.technicalTraining = technicalTraining;
	}

	public String getTechSchoolLocation() {
		return techSchoolLocation;
	}

	public void setTechSchoolLocation(String techSchoolLocation) {
		this.techSchoolLocation = techSchoolLocation;
	}

	public String getRequiredHSCourse() {
		return requiredHSCourse;
	}

	public void setRequiredHSCourse(String requiredHSCourse) {
		this.requiredHSCourse = requiredHSCourse;
	}

	public String getDesiredHSCourse() {
		return desiredHSCourse;
	}

	public void setDesiredHSCourse(String desiredHSCourse) {
		this.desiredHSCourse = desiredHSCourse;
	}

	public String getVideos() {
		return videos;
	}

	public void setVideos(String videos) {
		this.videos = videos;
	}

	public String getMcId() {
		return mcId;
	}

	public void setMcId(String mcId) {
		this.mcId = mcId;
	}

	public String getSocId() {
		return socId;
	}

	public void setSocId(String socId) {
		this.socId = socId;
	}

	public String getSvc() {
		return svc;
	}

	public void setSvc(String svc) {
		this.svc = svc;
	}

	public String getMpc() {
		return mpc;
	}

	public void setMpc(String mpc) {
		this.mpc = mpc;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getMotdTitle() {
		return motdTitle;
	}

	public void setMotdTitle(String motdTitle) {
		this.motdTitle = motdTitle;
	}

	public Integer getMotdId() {
		return motdId;
	}

	public void setMotdId(Integer motdId) {
		this.motdId = motdId;
	}
}
