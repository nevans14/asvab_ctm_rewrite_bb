package com.citm.spring.webservice.rest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.citm.spring.model.JavascriptError;

@Controller
@RequestMapping("/javascript-error-logging")
public class JavascriptErrorLogging {

	static Logger logger = LogManager.getLogger("JavascriptErrorLogging");

	@RequestMapping(value = "/log-error", method = RequestMethod.POST)
	public @ResponseBody void logJavascriptError(@RequestBody JavascriptError error) {
		logger.error("Angular Error: Location: " + error.getErrorUrl() + "\nError Message: " + error.getErrorMessage() + "\n"
				+ error.getErrorStackTrace());
	}
}
