package com.citm.spring.model.detail;

import java.io.Serializable;

public class SeeScoreArmy implements Serializable {

	private static final long serialVersionUID = -1108044921063413692L;
	String compositeCd, mcId, socId, svc, mpc, mocId, mocTitle;
	Integer score, groupId, opId;

	public String getCompositeCd() {
		return compositeCd;
	}

	public void setCompositeCd(String compositeCd) {
		this.compositeCd = compositeCd;
	}

	public String getMcId() {
		return mcId;
	}

	public void setMcId(String mcId) {
		this.mcId = mcId;
	}

	public String getSocId() {
		return socId;
	}

	public void setSocId(String socId) {
		this.socId = socId;
	}

	public String getSvc() {
		return svc;
	}

	public void setSvc(String svc) {
		this.svc = svc;
	}

	public String getMpc() {
		return mpc;
	}

	public void setMpc(String mpc) {
		this.mpc = mpc;
	}

	public String getMocId() {
		return mocId;
	}

	public void setMocId(String mocId) {
		this.mocId = mocId;
	}

	public String getMocTitle() {
		return mocTitle;
	}

	public void setMocTitle(String mocTitle) {
		this.mocTitle = mocTitle;
	}

	public Integer getScore() {
		return score;
	}

	public void setScore(Integer score) {
		this.score = score;
	}

	public Integer getGroupId() {
		return groupId;
	}

	public void setGroupId(Integer groupId) {
		this.groupId = groupId;
	}

	public Integer getOpId() {
		return opId;
	}

	public void setOpId(Integer opId) {
		this.opId = opId;
	}

}
