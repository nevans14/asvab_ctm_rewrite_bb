package com.citm.spring.model.testscore;

public class ControlInformation {

//	private	int	aptCd;
//	private	int	statusCd;
	private String  accessCode;
	private	String	platform;
	private	int	ansSeqNum;
//	private	int	ssnNum;
	private	int	mepsId;
	private	int	metSiteId;
//	private	int	service;
//	private	int	testType;
	private	String	gender;
	private	int	educationLevel;
	private	int	certification;
//	private	int	administratorSsnNum;
	private	String	testForm;
	private	String	scoringMethod;
//	private	int	rpGrp;
//	private	int	ethnicGrp;
	private	int	administeredYear;
	private	int	administeredMonth;
	private	int	administeredDay;
//	private	int	scoredYear;
//	private	int	scoredMonth;
//	private	int	socredDay;
//	private	int	scoredHour;
//	private	int	scoredMinute;
//	private	int	scoredSecond;
//	private	int	dob;
//	/**
//	 * @return the aptCd
//	 */
//	public int getAptCd() {
//		return aptCd;
//	}
//	/**
//	 * @param aptCd the aptCd to set
//	 */
//	public void setAptCd(int aptCd) {
//		this.aptCd = aptCd;
//	}
//	/**
//	 * @return the statusCd
//	 */
//	public int getStatusCd() {
//		return statusCd;
//	}
//	/**
//	 * @param statusCd the statusCd to set
//	 */
//	public void setStatusCd(int statusCd) {
//		this.statusCd = statusCd;
//	}
	/**
	 * @return the accessCode
	 */
	public String getAccessCode() {
		return accessCode;
	}
	/**
	 * @param accessCode the accessCode to set
	 */
	public void setAccessCode(String accessCode) {
		this.accessCode = accessCode;
	}
	/**
	 * @return the platform
	 */
	public String getPlatform() {
		return platform;
	}
	/**
	 * @param platform the platform to set
	 */
	public void setPlatform(String platform) {
		this.platform = platform;
	}
	/**
	 * @return the ansSeqNum
	 */
	public int getAnsSeqNum() {
		return ansSeqNum;
	}
	/**
	 * @param ansSeqNum the ansSeqNum to set
	 */
	public void setAnsSeqNum(int ansSeqNum) {
		this.ansSeqNum = ansSeqNum;
	}
//	/**
//	 * @return the ssnNum
//	 */
//	public int getSsnNum() {
//		return ssnNum;
//	}
//	/**
//	 * @param ssnNum the ssnNum to set
//	 */
//	public void setSsnNum(int ssnNum) {
//		this.ssnNum = ssnNum;
//	}

	/**
	 * @return the mepsId
	 */
	public int getMepsId() {
		return mepsId;
	}
	/**
	 * @param mepsId the mepsId to set
	 */
	public void setMepsId(int mepsId) {
		this.mepsId = mepsId;
	}
	/**
	 * @return the metSiteId
	 */
	public int getMetSiteId() {
		return metSiteId;
	}
	/**
	 * @param metSiteId the metSiteId to set
	 */
	public void setMetSiteId(int metSiteId) {
		this.metSiteId = metSiteId;
	}
//	/**
//	 * @return the service
//	 */
//	public int getService() {
//		return service;
//	}
//	/**
//	 * @param service the service to set
//	 */
//	public void setService(int service) {
//		this.service = service;
//	}
//	/**
//	 * @return the testType
//	 */
//	public int getTestType() {
//		return testType;
//	}
//	/**
//	 * @param testType the testType to set
//	 */
//	public void setTestType(int testType) {
//		this.testType = testType;
//	}
	/**
	 * @return the gender
	 */
	public String getGender() {
		return gender;
	}
	/**
	 * @param gender the gender to set
	 */
	public void setGender(String gender) {
		this.gender = gender;
	}
	/**
	 * @return the educationLevel
	 */
	public int getEducationLevel() {
		return educationLevel;
	}
	/**
	 * @param educationLevel the educationLevel to set
	 */
	public void setEducationLevel(int educationLevel) {
		this.educationLevel = educationLevel;
	}
	/**
	 * @return the certification
	 */
	public int getCertification() {
		return certification;
	}
	/**
	 * @param certification the certification to set
	 */
	public void setCertification(int certification) {
		this.certification = certification;
	}
//	/**
//	 * @return the administratorSsnNum
//	 */
//	public int getAdministratorSsnNum() {
//		return administratorSsnNum;
//	}
//	/**
//	 * @param administratorSsnNum the administratorSsnNum to set
//	 */
//	public void setAdministratorSsnNum(int administratorSsnNum) {
//		this.administratorSsnNum = administratorSsnNum;
//	}
	/**
	 * @return the testForm
	 */
	public String getTestForm() {
		return testForm;
	}
	/**
	 * @param testForm the testForm to set
	 */
	public void setTestForm(String testForm) {
		this.testForm = testForm;
	}
	/**
	 * @return the scoringMethod
	 */
	public String getScoringMethod() {
		return scoringMethod;
	}
	/**
	 * @param scoringMethod the scoringMethod to set
	 */
	public void setScoringMethod(String scoringMethod) {
		this.scoringMethod = scoringMethod;
	}
//	/**
//	 * @return the rpGrp
//	 */
//	public int getRpGrp() {
//		return rpGrp;
//	}
//	/**
//	 * @param rpGrp the rpGrp to set
//	 */
//	public void setRpGrp(int rpGrp) {
//		this.rpGrp = rpGrp;
//	}
//	/**
//	 * @return the ethnicGrp
//	 */
//	public int getEthnicGrp() {
//		return ethnicGrp;
//	}
//	/**
//	 * @param ethnicGrp the ethnicGrp to set
//	 */
//	public void setEthnicGrp(int ethnicGrp) {
//		this.ethnicGrp = ethnicGrp;
//	}
	/**
	 * @return the administeredYear
	 */
	public int getAdministeredYear() {
		return administeredYear;
	}
	/**
	 * @param administeredYear the administeredYear to set
	 */
	public void setAdministeredYear(int administeredYear) {
		this.administeredYear = administeredYear;
	}
	/**
	 * @return the administeredMonth
	 */
	public int getAdministeredMonth() {
		return administeredMonth;
	}
	/**
	 * @param administeredMonth the administeredMonth to set
	 */
	public void setAdministeredMonth(int administeredMonth) {
		this.administeredMonth = administeredMonth;
	}
	/**
	 * @return the administeredDay
	 */
	public int getAdministeredDay() {
		return administeredDay;
	}
	/**
	 * @param administeredDay the administeredDay to set
	 */
	public void setAdministeredDay(int administeredDay) {
		this.administeredDay = administeredDay;
	}
//	/**
//	 * @return the scoredYear
//	 */
//	public int getScoredYear() {
//		return scoredYear;
//	}
//	/**
//	 * @param scoredYear the scoredYear to set
//	 */
//	public void setScoredYear(int scoredYear) {
//		this.scoredYear = scoredYear;
//	}
//	/**
//	 * @return the scoredMonth
//	 */
//	public int getScoredMonth() {
//		return scoredMonth;
//	}
//	/**
//	 * @param scoredMonth the scoredMonth to set
//	 */
//	public void setScoredMonth(int scoredMonth) {
//		this.scoredMonth = scoredMonth;
//	}
//	/**
//	 * @return the socredDay
//	 */
//	public int getSocredDay() {
//		return socredDay;
//	}
//	/**
//	 * @param socredDay the socredDay to set
//	 */
//	public void setSocredDay(int socredDay) {
//		this.socredDay = socredDay;
//	}
//	/**
//	 * @return the scoredHour
//	 */
//	public int getScoredHour() {
//		return scoredHour;
//	}
//	/**
//	 * @param scoredHour the scoredHour to set
//	 */
//	public void setScoredHour(int scoredHour) {
//		this.scoredHour = scoredHour;
//	}
//	/**
//	 * @return the scoredMinute
//	 */
//	public int getScoredMinute() {
//		return scoredMinute;
//	}
//	/**
//	 * @param scoredMinute the scoredMinute to set
//	 */
//	public void setScoredMinute(int scoredMinute) {
//		this.scoredMinute = scoredMinute;
//	}
//	/**
//	 * @return the scoredSecond
//	 */
//	public int getScoredSecond() {
//		return scoredSecond;
//	}
//	/**
//	 * @param scoredSecond the scoredSecond to set
//	 */
//	public void setScoredSecond(int scoredSecond) {
//		this.scoredSecond = scoredSecond;
//	}
//	/**
//	 * @return the dob
//	 */
//	public int getDob() {
//		return dob;
//	}
//	/**
//	 * @param dob the dob to set
//	 */
//	public void setDob(int dob) {
//		this.dob = dob;
//	}	
}
