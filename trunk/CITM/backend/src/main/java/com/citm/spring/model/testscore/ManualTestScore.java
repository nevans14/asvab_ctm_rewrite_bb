package com.citm.spring.model.testscore;

public class ManualTestScore {
	
	private int userId;
	
	private String verbal;
	
	private String math;
	
	private String scienceTechnology;
	
	private String afqt;

	/**
	 * @return the userId
	 */
	public int getUserId() {
		return userId;
	}

	/**
	 * @param userId the userId to set
	 */
	public void setUserId(int userId) {
		this.userId = userId;
	}

	/**
	 * @return the verbal
	 */
	public String getVerbal() {
		return verbal;
	}

	/**
	 * @param verbal the verbal to set
	 */
	public void setVerbal(String verbal) {
		this.verbal = verbal;
	}

	/**
	 * @return the math
	 */
	public String getMath() {
		return math;
	}

	/**
	 * @param math the math to set
	 */
	public void setMath(String math) {
		this.math = math;
	}

	/**
	 * @return the scienceTechnology
	 */
	public String getScienceTechnology() {
		return scienceTechnology;
	}

	/**
	 * @param scienceTechnology the scienceTechnology to set
	 */
	public void setScienceTechnology(String scienceTechnology) {
		this.scienceTechnology = scienceTechnology;
	}

	/**
	 * @return the afqt
	 */
	public String getAfqt() {
		return afqt;
	}

	/**
	 * @param afqt the afqt to set
	 */
	public void setAfqt(String afqt) {
		this.afqt = afqt;
	}

}
