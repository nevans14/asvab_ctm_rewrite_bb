package com.citm.spring.dao.mybatis;

import java.util.List;

import com.citm.spring.model.testscore.CompositeFavoritesFlags;
import com.citm.spring.model.testscore.ManualTestScore;
import com.citm.spring.model.testscore.StudentTestingProgam;
import com.citm.spring.model.testscore.StudentTestingProgamSelects;

public interface TestScoreDao {

	public int insertTestScore(List<StudentTestingProgam> testScore);
	
	public StudentTestingProgamSelects getTestScore(Integer userId);
	
	public int insertManualTestScore(ManualTestScore manualTestScoreObj);
	
	public ManualTestScore getManualTestScore(int userId);
	
	public int updateManualTestScore(ManualTestScore manualTestScoreObj);
	
	public int deleteManualTestScore(int userId);
	
	public int setCompositeFavoritesFlags(CompositeFavoritesFlags compositeFavoritesFlagsObj);
	
	public CompositeFavoritesFlags getCompositeFavoritesFlags(int userId);
	
	public int updateCompositeFavoritesFlags(CompositeFavoritesFlags compositeFavoritesFlagsObj);
	
	public int deleteCompositeFavoritesFlags(int userId);
	
	public boolean compositeFavoritesFlagsExists(int userId);
	
	public boolean emailExists(String userName);
	
	public String latestTestDate(String accessCode);
}
