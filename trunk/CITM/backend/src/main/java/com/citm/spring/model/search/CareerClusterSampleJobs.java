package com.citm.spring.model.search;

import java.io.Serializable;

public class CareerClusterSampleJobs implements Serializable {

	private static final long serialVersionUID = 3677578672731255552L;
	String mcId, title;

	public String getMcId() {
		return mcId;
	}

	public void setMcId(String mcId) {
		this.mcId = mcId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
}
