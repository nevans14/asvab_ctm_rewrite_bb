package com.citm.spring.model.detail;

import java.io.Serializable;

public class TrainingList implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2010507574782216913L;
	String trainingProvided;

	public String getTrainingProvided() {
		return trainingProvided;
	}

	public void setTrainingProvided(String trainingProvided) {
		this.trainingProvided = trainingProvided;
	}
}
