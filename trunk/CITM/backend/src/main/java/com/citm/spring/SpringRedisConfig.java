package com.citm.spring;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.StringRedisSerializer;

@Configuration
public class SpringRedisConfig {

    private final String HOST_NAME = "localhost";
    private final int PORT_NUMBER = 6379;

    private JedisConnectionFactory connectionFactory;
    private RedisTemplate<String, Object> redisTemplate;

    @Bean
    public JedisConnectionFactory connectionFactory() {
        if (this.connectionFactory == null) {
            this.connectionFactory = new JedisConnectionFactory();
            connectionFactory.setHostName(HOST_NAME);
            connectionFactory.setPort(PORT_NUMBER);
        }
        return connectionFactory;
    }

    @Bean
    public RedisTemplate<String, Object> redisTemplate() {
        if (this.redisTemplate == null) {
            this.redisTemplate = new RedisTemplate<>();
            this.redisTemplate.setConnectionFactory(connectionFactory());
            this.redisTemplate.setKeySerializer(new StringRedisSerializer());
        }
        return redisTemplate;
    }
}
