package com.citm.spring.model;

public class DbInfo {
	private String name, value, value2;
	private Integer id;
	
	public void setName(String name) { this.name = name; }
	public String getName() { return name; }
	
	public void setValue(String value) { this.value = value; }
	public String getValue() { return value; }
	
	public void setValue2(String value2) { this.value2 = value2; }
	public String getValue2() { return value2; }
	
	public void setId(Integer id) { this.id = id; }
	public Integer getId() { return id; }
}