package com.citm.spring.dao.mybatis.mapper;

import java.util.ArrayList;

import org.apache.ibatis.annotations.Param;

import com.citm.spring.model.portfolio.FavoriteOccupation;
import com.citm.spring.model.portfolio.Achievement;
import com.citm.spring.model.portfolio.ActScore;
import com.citm.spring.model.portfolio.AsvabScore;
import com.citm.spring.model.portfolio.Education;
import com.citm.spring.model.portfolio.FyiScore;
import com.citm.spring.model.portfolio.OtherScore;
import com.citm.spring.model.portfolio.PhysicalFitness;
import com.citm.spring.model.portfolio.RelatedCareer;
import com.citm.spring.model.portfolio.ResourceGeneric;
import com.citm.spring.model.portfolio.ResourceSpecific;
import com.citm.spring.model.portfolio.SatScore;
import com.citm.spring.model.portfolio.UserInfo;
import com.citm.spring.model.portfolio.WorkExperience;

public interface PortfolioMapper {

	int isPortfolioStarted(@Param("userId") Integer userId);

	ArrayList<WorkExperience> getWorkExperience(@Param("userId") Integer userId);

	int insertWorkExperience(@Param("workExperienceObject") WorkExperience workExperienceObject);

	int deleteWorkExperience(@Param("userId") Integer userId, @Param("id") Integer id);

	int updateWorkExperience(@Param("workExperienceObject") WorkExperience workExperienceObject);

	ArrayList<Education> getEducation(@Param("userId") Integer userId);

	int insertEducation(@Param("educationObject") Education educationObject);

	int deleteEducation(@Param("userId") Integer userId, @Param("id") Integer id);

	int updateEducation(@Param("educationObject") Education educationObject);

	ArrayList<Achievement> getAchievements(@Param("userId") Integer userId);

	int insertAchievement(@Param("achievementObject") Achievement achievementObject);

	int deleteAchievement(@Param("userId") Integer userId, @Param("id") Integer id);

	int updateAchievement(@Param("achievementObject") Achievement achievementObject);

	ArrayList<AsvabScore> getAsvabScore(@Param("userId") Integer userId);

	int insertAsvabScore(@Param("asvabObject") AsvabScore asvabObject);

	int deleteAsvabScore(@Param("userId") Integer userId, @Param("id") Integer id);

	int updateAsvabScore(@Param("asvabObject") AsvabScore asvabObject);

	ArrayList<ActScore> getActScore(@Param("userId") Integer userId);

	int insertActScore(@Param("actObject") ActScore actObject);

	int deleteActScore(@Param("userId") Integer userId, @Param("id") Integer id);

	int updateActScore(@Param("actObject") ActScore actObject);

	ArrayList<SatScore> getSatScore(@Param("userId") Integer userId);

	int insertSatScore(@Param("satObject") SatScore satObject);

	int deleteSatScore(@Param("userId") Integer userId, @Param("id") Integer id);

	int updateSatScore(@Param("satObject") SatScore satObject);

	ArrayList<FyiScore> getFyiScore(@Param("userId") Integer userId);

	int insertFyiScore(@Param("fyiObject") FyiScore fyiObject);

	int deleteFyiScore(@Param("userId") Integer userId, @Param("id") Integer id);

	int updateFyiScore(@Param("fyiObject") FyiScore fyiObject);

	ArrayList<OtherScore> getOtherScore(@Param("userId") Integer userId);

	int insertOtherScore(@Param("otherObject") OtherScore otherObject);

	int deleteOtherScore(@Param("userId") Integer userId, @Param("id") Integer id);

	int updateOtherScore(@Param("otherObject") OtherScore otherObject);
	
	int updateUserInfo(@Param("userInfo") UserInfo userInfo);
	
	ArrayList<UserInfo>getUserInfo(@Param("userId") Integer userId);
	
	ArrayList<PhysicalFitness> getPhysicalFitness(@Param("userId") Integer userId);

	int insertPhysicalFitness(@Param("physicalFitnessObject") PhysicalFitness physicalFitnessObject);

	int deletePhysicalFitness(@Param("userId") Integer userId, @Param("id") Integer id);

	int updatePhysicalFitness(@Param("physicalFitnessObject") PhysicalFitness physicalFitnessObject);
	
	ArrayList<FavoriteOccupation> getFavoriteOccupation(@Param("userId") Integer userId);
	
	ArrayList<RelatedCareer> getRelatedCareerList(@Param("userId") Integer userId);
	
	ArrayList<ResourceSpecific> getResourceSpecific();
	
	ArrayList<ResourceGeneric> getResourceGeneric();
	
	String getUserSystem(@Param("userId") Integer userId);


	/*
	 * ArrayList<UserInfo> selectNameGrade(@Param("userId") Integer userId);
	 * 
	 * int updateInsertNameGrade(@Param("userInfo") UserInfo userInfo);
	 */
}
