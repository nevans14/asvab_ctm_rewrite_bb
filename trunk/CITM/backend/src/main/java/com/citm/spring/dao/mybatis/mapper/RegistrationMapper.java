package com.citm.spring.dao.mybatis.mapper;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.citm.spring.model.login.AccessCode;
import com.citm.spring.model.login.MEPS;
import com.citm.spring.model.login.UserRole;
import com.citm.spring.model.login.Users;

public interface RegistrationMapper {
	
	public Users getUser(@Param("email") String email);
	public Users getUserByResetKey(@Param("resetKey") String resetKey);
	public Integer insertUser(@Param("user") Users user);
	public Integer updateUser(@Param("user") Users user);
	
	public AccessCode getLastUsedAccessCode();
	public AccessCode getAccessCode(@Param("accessCode") String accessCode);
	public Integer insertAccessCode(@Param("accessCode") AccessCode accessCode);
	
	public MEPS getByAccessCode(@Param("accessCode") String accessCode);
	
	public int insertAsvabLog(@Param("userId") int userId, @Param("loginDate") Date loginDate);
	
	public List<UserRole> getRoles(@Param("email") String email);
	public int insertRole(@Param("role") UserRole role);
}