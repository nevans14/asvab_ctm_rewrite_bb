package com.citm.spring.utils;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.util.StringUtils;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

public class ReDirectInvaidUrls extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7352113567949965265L;
	
	static Logger logger = LogManager.getLogger("ReDirectInvaidUrls");

	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		SpringBeanAutowiringSupport.processInjectionBasedOnServletContext(this, config.getServletContext());
	}

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		 	String context = request.getContextPath();
		 	StringBuffer requestURL = request.getRequestURL();
		    String queryString = request.getQueryString();

		    if (queryString == null) {
		       requestURL.toString();
		    } else {
		       requestURL.append('?').append(queryString).toString();
		    }
		
		    logger.info("context: "+context);
		    logger.info("requestURL: "+requestURL.toString());
		    logger.info("queryString: "+queryString);
		    if(requestURL.toString().contains("main.advsearch")){
				response.sendRedirect(response.encodeRedirectURL("advanced-search"));
		    }else if (requestURL.toString().contains("main.browsealpha")){
				response.sendRedirect(response.encodeRedirectURL("advanced-search"));
		    } else if (requestURL.toString().contains("main.careerdetail&mc_id=100")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0108.00"));
			} else if (requestURL.toString().contains("main.careerdetail&mc_id=101")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0115.00"));
			} else if (requestURL.toString().contains("main.careerdetail&mc_id=102")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0142.00"));
			} else if (requestURL.toString().contains("main.careerdetail&mc_id=103")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0105.00"));
			} else if (requestURL.toString().contains("main.careerdetail&mc_id=104")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0055.00"));
			} else if (requestURL.toString().contains("main.careerdetail&mc_id=105")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0066.00"));
			} else if (requestURL.toString().contains("main.careerdetail&mc_id=106")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0078.00"));
			} else if (requestURL.toString().contains("main.careerdetail&mc_id=108")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0025.00"));
			} else if (requestURL.toString().contains("main.careerdetail&mc_id=109")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0002.00"));
			} else if (requestURL.toString().contains("main.careerdetail&mc_id=10")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0144.00"));
			} else if (requestURL.toString().contains("main.careerdetail&mc_id=111")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0147.00"));
			} else if (requestURL.toString().contains("main.careerdetail&mc_id=112")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0044.00"));
			} else if (requestURL.toString().contains("main.careerdetail&mc_id=113")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0045.00"));
			} else if (requestURL.toString().contains("main.careerdetail&mc_id=114")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0107.00"));
			} else if (requestURL.toString().contains("main.careerdetail&mc_id=115")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0111.00"));
			} else if (requestURL.toString().contains("main.careerdetail&mc_id=116")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0116.00"));
			} else if (requestURL.toString().contains("main.careerdetail&mc_id=117")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0117.00"));
			} else if (requestURL.toString().contains("main.careerdetail&mc_id=118")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0118.00"));
			} else if (requestURL.toString().contains("main.careerdetail&mc_id=119")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0127.00"));
			} else if (requestURL.toString().contains("main.careerdetail&mc_id=11")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0024.00"));
			} else if (requestURL.toString().contains("main.careerdetail&mc_id=120")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0134.00"));
			} else if (requestURL.toString().contains("main.careerdetail&mc_id=121")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0019.00"));
			} else if (requestURL.toString().contains("main.careerdetail&mc_id=122")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0070.00"));
			} else if (requestURL.toString().contains("main.careerdetail&mc_id=123")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0133.00"));
			} else if (requestURL.toString().contains("main.careerdetail&mc_id=124")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0125.00"));
			} else if (requestURL.toString().contains("main.careerdetail&mc_id=125")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0047.00"));
			} else if (requestURL.toString().contains("main.careerdetail&mc_id=126")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0018.00"));
			} else if (requestURL.toString().contains("main.careerdetail&mc_id=127")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0099.00"));
			} else if (requestURL.toString().contains("main.careerdetail&mc_id=128")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0128.00"));
			} else if (requestURL.toString().contains("main.careerdetail&mc_id=129")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0050.00"));
			} else if (requestURL.toString().contains("main.careerdetail&mc_id=12")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0036.00"));
			} else if (requestURL.toString().contains("main.careerdetail&mc_id=130")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0084.00"));
			} else if (requestURL.toString().contains("main.careerdetail&mc_id=130")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0084.00"));
			} else if (requestURL.toString().contains("main.careerdetail&mc_id=131")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0029.00"));
			} else if (requestURL.toString().contains("main.careerdetail&mc_id=132")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0060.00"));
			} else if (requestURL.toString().contains("main.careerdetail&mc_id=133")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0141.00"));
			} else if (requestURL.toString().contains("main.careerdetail&mc_id=134")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0009.00"));
			} else if (requestURL.toString().contains("main.careerdetail&mc_id=135")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0013.00"));
			} else if (requestURL.toString().contains("main.careerdetail&mc_id=136")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0068.00"));
			} else if (requestURL.toString().contains("main.careerdetail&mc_id=137")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0138.00"));
			} else if (requestURL.toString().contains("main.careerdetail&mc_id=138")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0140.00"));
			} else if (requestURL.toString().contains("main.careerdetail&mc_id=139")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0057.00"));
			} else if (requestURL.toString().contains("main.careerdetail&mc_id=13")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0038.00"));
			} else if (requestURL.toString().contains("main.careerdetail&mc_id=140")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0151.00"));
			} else if (requestURL.toString().contains("main.careerdetail&mc_id=141")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0034.00"));
			} else if (requestURL.toString().contains("main.careerdetail&mc_id=143")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0080.00"));
			} else if (requestURL.toString().contains("main.careerdetail&mc_id=146")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0079.00"));
			} else if (requestURL.toString().contains("main.careerdetail&mc_id=147")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0063.00"));
			} else if (requestURL.toString().contains("main.careerdetail&mc_id=148")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0063.00"));
			} else if (requestURL.toString().contains("main.careerdetail&mc_id=14")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0119.00"));
			} else if (requestURL.toString().contains("main.careerdetail&mc_id=15")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0022.00"));
			} else if (requestURL.toString().contains("main.careerdetail&mc_id=16")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0049.00"));
			} else if (requestURL.toString().contains("main.careerdetail&mc_id=17")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0049.00"));
			} else if (requestURL.toString().contains("main.careerdetail&mc_id=18")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0120.00"));
			} else if (requestURL.toString().contains("main.careerdetail&mc_id=19")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0123.00"));
			} else if (requestURL.toString().contains("main.careerdetail&mc_id=1")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0003.00"));
			} else if (requestURL.toString().contains("main.careerdetail&mc_id=20")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0139.00"));
			} else if (requestURL.toString().contains("main.careerdetail&mc_id=21")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0158.00"));
			} else if (requestURL.toString().contains("main.careerdetail&mc_id=22")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0032.00"));
			} else if (requestURL.toString().contains("main.careerdetail&mc_id=23")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0041.00"));
			} else if (requestURL.toString().contains("main.careerdetail&mc_id=24")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0046.00"));
			} else if (requestURL.toString().contains("main.careerdetail&mc_id=25")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0054.00"));
			} else if (requestURL.toString().contains("main.careerdetail&mc_id=26")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0077.00"));
			} else if (requestURL.toString().contains("main.careerdetail&mc_id=27")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0077.00"));
			} else if (requestURL.toString().contains("main.careerdetail&mc_id=28")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0102.00"));
			} else if (requestURL.toString().contains("main.careerdetail&mc_id=29")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0109.00"));
			} else if (requestURL.toString().contains("main.careerdetail&mc_id=2")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0023.00"));
			} else if (requestURL.toString().contains("main.careerdetail&mc_id=30")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0131.00"));
			} else if (requestURL.toString().contains("main.careerdetail&mc_id=31")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0143.00"));
			} else if (requestURL.toString().contains("main.careerdetail&mc_id=32")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0148.00"));
			} else if (requestURL.toString().contains("main.careerdetail&mc_id=33")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0153.00"));				
			} else if (requestURL.toString().contains("main.careerdetail&mc_id=34")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0026.00"));
			} else if (requestURL.toString().contains("main.careerdetail&mc_id=35")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0043.00"));
			} else if (requestURL.toString().contains("main.careerdetail&mc_id=36")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0042.00"));
			} else if (requestURL.toString().contains("main.careerdetail&mc_id=37")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0093.00"));
			} else if (requestURL.toString().contains("main.careerdetail&mc_id=38")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0096.00"));
			} else if (requestURL.toString().contains("main.careerdetail&mc_id=39")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0097.00"));
			} else if (requestURL.toString().contains("main.careerdetail&mc_id=3")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0059.00"));
			} else if (requestURL.toString().contains("main.careerdetail&mc_id=40")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0052.00"));
			} else if (requestURL.toString().contains("main.careerdetail&mc_id=41")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0106.00"));
			} else if (requestURL.toString().contains("main.careerdetail&mc_id=42")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0112.00"));
			} else if (requestURL.toString().contains("main.careerdetail&mc_id=43")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0114.00"));
			} else if (requestURL.toString().contains("main.careerdetail&mc_id=44")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0095.00"));
			} else if (requestURL.toString().contains("main.careerdetail&mc_id=45")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0071.00"));
			} else if (requestURL.toString().contains("main.careerdetail&mc_id=46")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0132.00"));
			} else if (requestURL.toString().contains("main.careerdetail&mc_id=47")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0150.00"));
			} else if (requestURL.toString().contains("main.careerdetail&mc_id=48")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0090.00"));
			} else if (requestURL.toString().contains("main.careerdetail&mc_id=49")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0121.00"));
			} else if (requestURL.toString().contains("main.careerdetail&mc_id=4")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0110.00"));
			} else if (requestURL.toString().contains("main.careerdetail&mc_id=50")) {
				response.sendRedirect(response.encodeRedirectURL(request.getContextPath() + "/"));
			} else if (requestURL.toString().contains("main.careerdetail&mc_id=51")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0149.00"));
			} else if (requestURL.toString().contains("main.careerdetail&mc_id=52")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0157.00"));
			} else if (requestURL.toString().contains("main.careerdetail&mc_id=53")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0159.00"));
			} else if (requestURL.toString().contains("main.careerdetail&mc_id=54")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0020.00"));
			} else if (requestURL.toString().contains("main.careerdetail&mc_id=55")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0129.00"));
			} else if (requestURL.toString().contains("main.careerdetail&mc_id=56")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0065.00"));
			} else if (requestURL.toString().contains("main.careerdetail&mc_id=57")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0080.00"));
			} else if (requestURL.toString().contains("main.careerdetail&mc_id=58")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0100.00"));
			} else if (requestURL.toString().contains("main.careerdetail&mc_id=59")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0113.00"));
			} else if (requestURL.toString().contains("main.careerdetail&mc_id=5")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0124.00"));
			} else if (requestURL.toString().contains("main.careerdetail&mc_id=6")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0136.00"));
			} else if (requestURL.toString().contains("main.careerdetail&mc_id=61")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0056.00"));
			} else if (requestURL.toString().contains("main.careerdetail&mc_id=62")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0085.00"));
			} else if (requestURL.toString().contains("main.careerdetail&mc_id=63")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0028.00"));
			} else if (requestURL.toString().contains("main.careerdetail&mc_id=64")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0061.00"));
			} else if (requestURL.toString().contains("main.careerdetail&mc_id=65")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0135.00"));
			} else if (requestURL.toString().contains("main.careerdetail&mc_id=66")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0006.00"));
			} else if (requestURL.toString().contains("main.careerdetail&mc_id=67")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0008.00"));
			} else if (requestURL.toString().contains("main.careerdetail&mc_id=68")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0011.00"));
			} else if (requestURL.toString().contains("main.careerdetail&mc_id=69")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0027.00"));
			} else if (requestURL.toString().contains("main.careerdetail&mc_id=60")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0051.00"));
			} else if (requestURL.toString().contains("main.careerdetail&mc_id=70")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0058.00"));
			} else if (requestURL.toString().contains("main.careerdetail&mc_id=71")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0062.00"));
			} else if (requestURL.toString().contains("main.careerdetail&mc_id=72")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0130.00"));
			} else if (requestURL.toString().contains("main.careerdetail&mc_id=73")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0137.00"));
			} else if (requestURL.toString().contains("main.careerdetail&mc_id=74")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0152.00"));
			} else if (requestURL.toString().contains("main.careerdetail&mc_id=75&referer=www.clickfind.com.au")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0154.00"));
			} else if (requestURL.toString().contains("main.careerdetail&mc_id=75")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0154.00"));
			} else if (requestURL.toString().contains("main.careerdetail&mc_id=76")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0156.00"));
			} else if (requestURL.toString().contains("main.careerdetail&mc_id=77")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0012.00"));
			} else if (requestURL.toString().contains("main.careerdetail&mc_id=78")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0021.00"));
			} else if (requestURL.toString().contains("main.careerdetail&mc_id=79")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0067.00"));
			} else if (requestURL.toString().contains("main.careerdetail&mc_id=7")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0014.00"));
			} else if (requestURL.toString().contains("main.careerdetail&mc_id=80")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0091.00"));
			} else if (requestURL.toString().contains("main.careerdetail&mc_id=81")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0122.00"));
			} else if (requestURL.toString().contains("main.careerdetail&mc_id=82")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0015.00"));
			} else if (requestURL.toString().contains("main.careerdetail&mc_id=83")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0017.00"));
			} else if (requestURL.toString().contains("main.careerdetail&mc_id=84")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0031.00"));
			} else if (requestURL.toString().contains("main.careerdetail&mc_id=85")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0074.00"));
			} else if (requestURL.toString().contains("main.careerdetail&mc_id=86")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0145.00"));
			} else if (requestURL.toString().contains("main.careerdetail&mc_id=87")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0005.00"));
			} else if (requestURL.toString().contains("main.careerdetail&mc_id=88")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0007.00"));
			} else if (requestURL.toString().contains("main.careerdetail&mc_id=89")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0030.00"));
			} else if (requestURL.toString().contains("main.careerdetail&mc_id=8")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0016.00"));
			} else if (requestURL.toString().contains("main.careerdetail&mc_id=90")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0033.00"));
			} else if (requestURL.toString().contains("main.careerdetail&mc_id=91")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0081.00"));
			} else if (requestURL.toString().contains("main.careerdetail&mc_id=92")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0048.00"));
			} else if (requestURL.toString().contains("main.careerdetail&mc_id=93")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0053.00"));
			} else if (requestURL.toString().contains("main.careerdetail&mc_id=94")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0072.00"));
			} else if (requestURL.toString().contains("main.careerdetail&mc_id=95")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0076.00"));
			} else if (requestURL.toString().contains("main.careerdetail&mc_id=96")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0086.00"));
			} else if (requestURL.toString().contains("main.careerdetail&mc_id=97")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0087.00"));
			} else if (requestURL.toString().contains("main.careerdetail&mc_id=98")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0092.00"));
			} else if (requestURL.toString().contains("main.careerdetail&mc_id=99")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0103.00"));
			} else if (requestURL.toString().contains("main.careerdetail&mc_id=9")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0073.00"));
			} else if (requestURL.toString().contains("main.careerdetail%26mc_id=38")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0096.00"));
			} else if (requestURL.toString().contains("main.careerpath&mc_id=104")) {
				response.sendRedirect(response.encodeRedirectURL("career-pathway/0055.00"));
			} else if (requestURL.toString().contains("main.careerpath&mc_id=106")) {
				response.sendRedirect(response.encodeRedirectURL("career-pathway/0078.00"));
			} else if (requestURL.toString().contains("main.careerpath&mc_id=10")) {
				response.sendRedirect(response.encodeRedirectURL("career-pathway/0144.00"));
			} else if (requestURL.toString().contains("main.careerpath&mc_id=118")) {
				response.sendRedirect(response.encodeRedirectURL("career-pathway/0118.00"));
			} else if (requestURL.toString().contains("main.careerpath&mc_id=120")) {
				response.sendRedirect(response.encodeRedirectURL("career-pathway/0134.00"));
			} else if (requestURL.toString().contains("main.careerpath&mc_id=125")) {
				response.sendRedirect(response.encodeRedirectURL(request.getContextPath() + "/"));
			} else if (requestURL.toString().contains("main.careerpath&mc_id=135")) {
				response.sendRedirect(response.encodeRedirectURL("career-pathway/0013.00"));
			} else if (requestURL.toString().contains("main.careerpath&mc_id=137")) {
				response.sendRedirect(response.encodeRedirectURL("career-pathway/0138.00"));
			} else if (requestURL.toString().contains("main.careerpath&mc_id=13")) {
				response.sendRedirect(response.encodeRedirectURL("career-pathway/0038.00"));
			} else if (requestURL.toString().contains("main.careerpath&mc_id=140")) {
				response.sendRedirect(response.encodeRedirectURL(request.getContextPath() + "/"));
			} else if (requestURL.toString().contains("main.careerpath&mc_id=17")) {
				response.sendRedirect(response.encodeRedirectURL("career-pathway/0049.00"));
			} else if (requestURL.toString().contains("main.careerpath&mc_id=23")) {
				response.sendRedirect(response.encodeRedirectURL("career-pathway/0101.00"));
			} else if (requestURL.toString().contains("main.careerpath&mc_id=30")) {
				response.sendRedirect(response.encodeRedirectURL("career-pathway/0131.00"));
			} else if (requestURL.toString().contains("main.careerpath&mc_id=40")) {
				response.sendRedirect(response.encodeRedirectURL("career-pathway/0052.00"));
			} else if (requestURL.toString().contains("main.careerpath&mc_id=45")) {
				response.sendRedirect(response.encodeRedirectURL("career-pathway/0071.00"));
			} else if (requestURL.toString().contains("main.careerpath&mc_id=48")) {
				response.sendRedirect(response.encodeRedirectURL("career-pathway/0090.00"));
			} else if (requestURL.toString().contains("main.careerpath&mc_id=55")) {
				response.sendRedirect(response.encodeRedirectURL("career-pathway/0129.00"));
			} else if (requestURL.toString().contains("main.careerpath&mc_id=62")) {
				response.sendRedirect(response.encodeRedirectURL("career-pathway/0085.00"));
			} else if (requestURL.toString().contains("main.careerpath&mc_id=64")) {
				response.sendRedirect(response.encodeRedirectURL("career-pathway/0061.00"));
			} else if (requestURL.toString().contains("main.careerpath&mc_id=65")) {
				response.sendRedirect(response.encodeRedirectURL("career-pathway/0135.00"));
			} else if (requestURL.toString().contains("main.careerpath&mc_id=66")) {
				response.sendRedirect(response.encodeRedirectURL("career-pathway/0006.00"));
			} else if (requestURL.toString().contains("main.careerpath&mc_id=67")) {
				response.sendRedirect(response.encodeRedirectURL("career-pathway/0008.00"));
			} else if (requestURL.toString().contains("main.careerpath&mc_id=68")) {
				response.sendRedirect(response.encodeRedirectURL("career-pathway/0011.00"));
			} else if (requestURL.toString().contains("main.careerpath&mc_id=77")) {
				response.sendRedirect(response.encodeRedirectURL("career-pathway/0012.00"));
			} else if (requestURL.toString().contains("main.careerpath&mc_id=78")) {
				response.sendRedirect(response.encodeRedirectURL(request.getContextPath() + "/"));
			} else if (requestURL.toString().contains("main.careerpath&mc_id=80")) {
				response.sendRedirect(response.encodeRedirectURL("career-pathway/0091.00"));
			} else if (requestURL.toString().contains("main.careerpath&mc_id=82")) {
				response.sendRedirect(response.encodeRedirectURL("career-pathway/0015.00"));
			} else if (requestURL.toString().contains("main.careerpath&mc_id=85")) {
				response.sendRedirect(response.encodeRedirectURL("career-pathway/0074.00"));
			} else if (requestURL.toString().contains("main.careerpath&mc_id=89")) {
				response.sendRedirect(response.encodeRedirectURL("career-pathway/0030.00"));
			} else if (requestURL.toString().contains("main.careerpath&mc_id=90")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0033.00"));
			} else if (requestURL.toString().contains("main.careerpath&mc_id=95")) {
				response.sendRedirect(response.encodeRedirectURL("career-pathway/0076.00"));
			} else if (requestURL.toString().contains("main.careerpath&mc_id=96")) {
				response.sendRedirect(response.encodeRedirectURL("career-pathway/0086.00"));
			} else if (requestURL.toString().contains("main.careerpath&mc_id=9")) {
				response.sendRedirect(response.encodeRedirectURL("career-pathway/0073.00"));
			} else if (requestURL.toString().contains("main.home")) {
				response.sendRedirect(response.encodeRedirectURL(request.getContextPath() + "/"));
			} else if (requestURL.toString().contains("main.jobfamilies")) {
				response.sendRedirect(response.encodeRedirectURL("job-family"));
			} else if (requestURL.toString().contains("main.jobfamily")) {
				response.sendRedirect(response.encodeRedirectURL("job-family"));
			} else if (requestURL.toString().contains("main.jobfamily&job_family_id=11")) {
				response.sendRedirect(response.encodeRedirectURL("job-family"));
			} else if (requestURL.toString().contains("main.jobfamily&job_family_id=12")) {
				response.sendRedirect(response.encodeRedirectURL("job-family"));
			} else if (requestURL.toString().contains("main.jobfamily&job_family_id=13")) {
				response.sendRedirect(response.encodeRedirectURL("job-family"));
			} else if (requestURL.toString().contains("main.jobfamily&job_family_id=15")) {
				response.sendRedirect(response.encodeRedirectURL("job-family"));
			} else if (requestURL.toString().contains("main.jobfamily&job_family_id=17")) {
				response.sendRedirect(response.encodeRedirectURL("job-family"));
			} else if (requestURL.toString().contains("main.jobfamily&job_family_id=18")) {
				response.sendRedirect(response.encodeRedirectURL("job-family"));
			} else if (requestURL.toString().contains("main.jobfamily&job_family_id=10")) {
				response.sendRedirect(response.encodeRedirectURL("job-family"));
			} else if (requestURL.toString().contains("main.jobfamily&job_family_id=1")) {
				response.sendRedirect(response.encodeRedirectURL("job-family"));
			} else if (requestURL.toString().contains("main.jobfamily&job_family_id=20")) {
				response.sendRedirect(response.encodeRedirectURL("job-family"));
			} else if (requestURL.toString().contains("main.jobfamily&job_family_id=22")) {
				response.sendRedirect(response.encodeRedirectURL("job-family"));
			} else if (requestURL.toString().contains("main.jobfamily&job_family_id=2")) {
				response.sendRedirect(response.encodeRedirectURL("job-family"));
			} else if (requestURL.toString().contains("main.jobfamily&job_family_id=5")) {
				response.sendRedirect(response.encodeRedirectURL("job-family"));
			} else if (requestURL.toString().contains("main.jobfamily&job_family_id=6")) {
				response.sendRedirect(response.encodeRedirectURL("job-family"));
			} else if (requestURL.toString().contains("main.jobfamily&job_family_id=7")) {
				response.sendRedirect(response.encodeRedirectURL("job-family"));
			} else if (requestURL.toString().contains("main.jobfamily&job_family_id=9")) {
				response.sendRedirect(response.encodeRedirectURL("job-family"));
			} else if (requestURL.toString().contains("main.outsidenorm")) {
				response.sendRedirect(response.encodeRedirectURL(request.getContextPath() + "/"));
			} else if (requestURL.toString().contains("main.pathlist")) {
				response.sendRedirect(response.encodeRedirectURL(request.getContextPath() + "/"));
			} else if (requestURL.toString().contains("main.pathlist&startrow=1")) {
				response.sendRedirect(response.encodeRedirectURL(request.getContextPath() + "/"));
			} else if (requestURL.toString().contains("main.privacy")) {
				response.sendRedirect(response.encodeRedirectURL(request.getContextPath() + "/"));
			} else if (requestURL.toString().contains("main.profiledetail&profile_id=102")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0115.03"));
			} else if (requestURL.toString().contains("main.profiledetail&profile_id=104")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0080.00"));
			} else if (requestURL.toString().contains("main.profiledetail&profile_id=105")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0100.00"));
			} else if (requestURL.toString().contains("main.profiledetail&profile_id=10")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0118.00"));
			} else if (requestURL.toString().contains("main.profiledetail&profile_id=112")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0062.00"));
			} else if (requestURL.toString().contains("main.profiledetail&profile_id=113")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0130.00"));
			} else if (requestURL.toString().contains("main.profiledetail&profile_id=115")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0156.00"));
			} else if (requestURL.toString().contains("main.profiledetail&profile_id=119")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0031.00"));
			} else if (requestURL.toString().contains("main.profiledetail&profile_id=11")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0134.00"));
			} else if (requestURL.toString().contains("main.profiledetail&profile_id=123")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0145.00"));
			} else if (requestURL.toString().contains("main.profiledetail&profile_id=127")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0048.00"));
			} else if (requestURL.toString().contains("main.profiledetail&profile_id=128")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0050.00"));
			} else if (requestURL.toString().contains("main.profiledetail&profile_id=12")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0138.00"));
			} else if (requestURL.toString().contains("main.profiledetail&profile_id=130")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0096.00"));
			} else if (requestURL.toString().contains("main.profiledetail&profile_id=13")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0151.00"));
			} else if (requestURL.toString().contains("main.profiledetail&profile_id=142")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0070.00"));
			} else if (requestURL.toString().contains("main.profiledetail&profile_id=148")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0073.00"));
			} else if (requestURL.toString().contains("main.profiledetail&profile_id=14")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0076.00"));
			} else if (requestURL.toString().contains("main.profiledetail&profile_id=151")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0016.00"));
			} else if (requestURL.toString().contains("main.profiledetail&profile_id=152")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0138.00"));
			} else if (requestURL.toString().contains("main.profiledetail&profile_id=153")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0109.00"));
			} else if (requestURL.toString().contains("main.profiledetail&profile_id=156")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0051.00"));
			} else if (requestURL.toString().contains("main.profiledetail&profile_id=158")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0134.00"));
			} else if (requestURL.toString().contains("main.profiledetail&profile_id=159")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0118.00"));
			} else if (requestURL.toString().contains("main.profiledetail&profile_id=15")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0030.00"));
			} else if (requestURL.toString().contains("main.profiledetail&profile_id=164")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0033.00"));
			} else if (requestURL.toString().contains("main.profiledetail&profile_id=166")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0076.00"));
			} else if (requestURL.toString().contains("main.profiledetail&profile_id=168")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0076.00"));
			} else if (requestURL.toString().contains("main.profiledetail&profile_id=16")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0017.00"));
			} else if (requestURL.toString().contains("main.profiledetail&profile_id=172")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0029.00"));
			} else if (requestURL.toString().contains("main.profiledetail&profile_id=174")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0128.00"));
			} else if (requestURL.toString().contains("main.profiledetail&profile_id=175")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0085.01"));
			} else if (requestURL.toString().contains("main.profiledetail&profile_id=176")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0030.00"));
			} else if (requestURL.toString().contains("main.profiledetail&profile_id=177")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0012.01"));
			} else if (requestURL.toString().contains("main.profiledetail&profile_id=178")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0100.00"));
			} else if (requestURL.toString().contains("main.profiledetail&profile_id=179")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0068.00"));
			} else if (requestURL.toString().contains("main.profiledetail&profile_id=17")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0052.00"));
			} else if (requestURL.toString().contains("main.profiledetail&profile_id=180")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0138.00"));
			} else if (requestURL.toString().contains("main.profiledetail&profile_id=181")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0037.00"));
			} else if (requestURL.toString().contains("main.profiledetail&profile_id=183")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0147.00"));
			} else if (requestURL.toString().contains("main.profiledetail&profile_id=184")) {
				response.sendRedirect(response.encodeRedirectURL(request.getContextPath() + "/"));
			} else if (requestURL.toString().contains("main.profiledetail&profile_id=185")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0131.00"));
			} else if (requestURL.toString().contains("main.profiledetail&profile_id=18")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0011.00"));
			} else if (requestURL.toString().contains("main.profiledetail&profile_id=1")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0135.00"));
			} else if (requestURL.toString().contains("main.profiledetail&profile_id=22")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0128.00"));
			} else if (requestURL.toString().contains("main.profiledetail&profile_id=23")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0068.00"));
			} else if (requestURL.toString().contains("main.profiledetail&profile_id=24")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0071.00"));
			} else if (requestURL.toString().contains("main.profiledetail&profile_id=25")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0073.00"));
			} else if (requestURL.toString().contains("main.profiledetail&profile_id=26")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0049.00"));
			} else if (requestURL.toString().contains("main.profiledetail&profile_id=28")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0033.00"));
			} else if (requestURL.toString().contains("main.profiledetail&profile_id=29")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0038.00"));
			} else if (requestURL.toString().contains("main.profiledetail&profile_id=2")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0012.00"));
			} else if (requestURL.toString().contains("main.profiledetail&profile_id=30")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0061.00"));
			} else if (requestURL.toString().contains("main.profiledetail&profile_id=31")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0138.00"));
			} else if (requestURL.toString().contains("main.profiledetail&profile_id=32")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0147.00"));
			} else if (requestURL.toString().contains("main.profiledetail&profile_id=33")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0158.00"));
			} else if (requestURL.toString().contains("main.profiledetail&profile_id=34")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0131.00"));
			} else if (requestURL.toString().contains("main.profiledetail&profile_id=35")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0096.00"));
			} else if (requestURL.toString().contains("main.profiledetail&profile_id=36")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0042.00"));
			} else if (requestURL.toString().contains("main.profiledetail&profile_id=37")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0086.00"));
			} else if (requestURL.toString().contains("main.profiledetail&profile_id=38")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0142.00"));
			} else if (requestURL.toString().contains("main.profiledetail&profile_id=39")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0013.00"));
			} else if (requestURL.toString().contains("main.profiledetail&profile_id=3")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0121.00"));
			} else if (requestURL.toString().contains("main.profiledetail&profile_id=40")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0015.00"));
			} else if (requestURL.toString().contains("main.profiledetail&profile_id=41")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0074.00"));
			} else if (requestURL.toString().contains("main.profiledetail&profile_id=42")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0144.00"));
			} else if (requestURL.toString().contains("main.profiledetail&profile_id=43")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0090.00"));
			} else if (requestURL.toString().contains("main.profiledetail&profile_id=48")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0110.00"));
			} else if (requestURL.toString().contains("main.profiledetail&profile_id=4")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0085.00"));
			} else if (requestURL.toString().contains("main.profiledetail&profile_id=51")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0014.00"));
			} else if (requestURL.toString().contains("main.profiledetail&profile_id=5")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0003.00"));
			} else if (requestURL.toString().contains("main.profiledetail&profile_id=63")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0032.00"));
			} else if (requestURL.toString().contains("main.profiledetail&profile_id=66")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0054.00"));
			} else if (requestURL.toString().contains("main.profiledetail&profile_id=67")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0077.00"));
			} else if (requestURL.toString().contains("main.profiledetail&profile_id=71")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0149.00"));
			} else if (requestURL.toString().contains("main.profiledetail&profile_id=73")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0056.00"));
			} else if (requestURL.toString().contains("main.profiledetail&profile_id=78")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0117.00"));
			} else if (requestURL.toString().contains("main.profiledetail&profile_id=7")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0006.00"));
			} else if (requestURL.toString().contains("main.profiledetail&profile_id=8")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0008.00"));
			} else if (requestURL.toString().contains("main.profiledetail&profile_id=92")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0044.00"));
			} else if (requestURL.toString().contains("main.profiledetail&profile_id=99")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0150.00"));
			} else if (requestURL.toString().contains("main.profiledetail&profile_id=9")) {
				response.sendRedirect(response.encodeRedirectURL("career-detail/0129.00"));
			} else if (requestURL.toString().contains("main.profiles")) {
				response.sendRedirect(response.encodeRedirectURL(request.getContextPath() + "/"));
			} else if (requestURL.toString().contains("main.profiles&startrow=1")) {
				response.sendRedirect(response.encodeRedirectURL(request.getContextPath() + "/"));
			} else if (requestURL.toString().contains("main.profiles&startrow=21")) {
				response.sendRedirect(response.encodeRedirectURL(request.getContextPath() + "/"));
			} else if (requestURL.toString().contains("main.profiles&startrow=61")) {
				response.sendRedirect(response.encodeRedirectURL(request.getContextPath() + "/"));
			} else if (requestURL.toString().contains("search.detail&mc_id=114")) {
				response.sendRedirect(response.encodeRedirectURL(request.getContextPath() + "/"));
			} else if (requestURL.toString().contains("services.airforce_enlisted")) {
				response.sendRedirect(response.encodeRedirectURL("options-types-of-service"));
			} else if (requestURL.toString().contains("services.airforce_officer")) {
				response.sendRedirect(response.encodeRedirectURL("options-types-of-service"));
			} else if (requestURL.toString().contains("services.airforce")) {
				response.sendRedirect(response.encodeRedirectURL("options-military"));
			} else if (requestURL.toString().contains("services.army_enlisted")) {
				response.sendRedirect(response.encodeRedirectURL("options-types-of-service"));
			} else if (requestURL.toString().contains("services.army_officer")) {
				response.sendRedirect(response.encodeRedirectURL("options-types-of-service"));
			} else if (requestURL.toString().contains("services.army...")) {
				response.sendRedirect(response.encodeRedirectURL("options-military"));
			} else if (requestURL.toString().contains("services.army")) {
				response.sendRedirect(response.encodeRedirectURL("options-military"));
			} else if (requestURL.toString().contains("services.coastguard_enlisted")) {
				response.sendRedirect(response.encodeRedirectURL("options-types-of-service"));
			} else if (requestURL.toString().contains("services.coastguard_officer")) {
				response.sendRedirect(response.encodeRedirectURL("options-types-of-service"));
			} else if (requestURL.toString().contains("services.coastguard")) {
				response.sendRedirect(response.encodeRedirectURL("options-military"));
			} else if (requestURL.toString().contains("services.marines_enlisted")) {
				response.sendRedirect(response.encodeRedirectURL("options-types-of-service"));
			} else if (requestURL.toString().contains("services.marines_officer")) {
				response.sendRedirect(response.encodeRedirectURL("options-types-of-service"));
			} else if (requestURL.toString().contains("services.marines")) {
				response.sendRedirect(response.encodeRedirectURL("options-military"));
			} else if (requestURL.toString().contains("services.navy_enlisted")) {
				response.sendRedirect(response.encodeRedirectURL("options-types-of-service"));
			} else if (requestURL.toString().contains("services.navy_officer")) {
				response.sendRedirect(response.encodeRedirectURL("options-types-of-service"));
			} else if (requestURL.toString().contains("services.navy")) {
				response.sendRedirect(response.encodeRedirectURL("options-military"));
			} else if (requestURL.toString().contains("services.nguard_enlisted")) {
				response.sendRedirect(response.encodeRedirectURL("options-types-of-service"));
			} else if (requestURL.toString().contains("services.nguard_officer")) {
				response.sendRedirect(response.encodeRedirectURL("options-types-of-service"));
			} else if (requestURL.toString().contains("services.nguard")) {
				response.sendRedirect(response.encodeRedirectURL("options-military"));
			} else if (requestURL.toString().contains("fuseactionnavy_officer")) {
				response.sendRedirect(response.encodeRedirectURL("options-military"));
			}else {
				response.sendRedirect(response.encodeRedirectURL(request.getContextPath() + "/"));
			}
	}

}
