package com.citm.spring.model.favorites;

import java.io.Serializable;

public class FavoriteService implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4707388393644724637L;
	Integer userId, id;
	String svcId;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getSvcId() {
		return svcId;
	}

	public void setSvcId(String svcId) {
		this.svcId = svcId;
	}
}
