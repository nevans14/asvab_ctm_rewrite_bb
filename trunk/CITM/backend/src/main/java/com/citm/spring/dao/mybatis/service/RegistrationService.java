package com.citm.spring.dao.mybatis.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.citm.spring.dao.mybatis.mapper.RegistrationMapper;
import com.citm.spring.model.login.AccessCode;
import com.citm.spring.model.login.MEPS;
import com.citm.spring.model.login.UserRole;
import com.citm.spring.model.login.Users;

@Service
public class RegistrationService {
	@Autowired
	private RegistrationMapper mapper;
	
	public Users getUser(String email) {
		return mapper.getUser(email);
	}
	
	public Users getUserByResetKey(String resetKey) {
		return mapper.getUserByResetKey(resetKey);
	}
	
	public Integer insertUser(Users user) {
		return mapper.insertUser(user);
	}
	
	public Integer updateUser(Users user) {
		return mapper.updateUser(user);
	}
	
	public AccessCode getLastUsedAccessCode() {
		return mapper.getLastUsedAccessCode();
	}
	
	public AccessCode getAccessCode(String accessCode) {
		return mapper.getAccessCode(accessCode);
	}
	
	public Integer insertAccessCode(AccessCode accessCode) {
		return mapper.insertAccessCode(accessCode);
	}
	
	public MEPS getByAccessCode(String accessCode) {
		return mapper.getByAccessCode(accessCode);
	}
	
	public int insertAsvabLog(int userId, Date loginDate) {
		return mapper.insertAsvabLog(userId, loginDate);
	}
	
	public List<UserRole> getRoles(String email) {
		return mapper.getRoles(email);
	}
	
	public int insertRole(UserRole role) {
		return mapper.insertRole(role);
	}
}