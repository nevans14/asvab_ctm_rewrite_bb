package com.citm.spring.model.detail;

import java.io.Serializable;

public class Profile implements Serializable {

	private static final long serialVersionUID = 78849616132546260L;
	int profileId;
	String title, name, description;

	public int getProfileId() {
		return profileId;
	}

	public void setProfileId(int profileId) {
		this.profileId = profileId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
