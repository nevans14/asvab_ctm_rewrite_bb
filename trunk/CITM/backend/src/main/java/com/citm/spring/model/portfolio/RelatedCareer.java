package com.citm.spring.model.portfolio;

import java.io.Serializable;

public class RelatedCareer implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6359706429647042291L;
	String mcId, relatedId;

	public String getMcId() {
		return mcId;
	}

	public void setMcId(String mcId) {
		this.mcId = mcId;
	}

	public String getRelatedId() {
		return relatedId;
	}

	public void setRelatedId(String relatedId) {
		this.relatedId = relatedId;
	}
}
