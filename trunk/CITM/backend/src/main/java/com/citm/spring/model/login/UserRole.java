package com.citm.spring.model.login;

public class UserRole {
	private String username, role;
	
	public UserRole() {}
	public UserRole(String username, String role) {
		this.username = username;
		this.role = role;
	}
	
	public void setUsername(String username) { this.username = username; }
	public String getUsername() { return username; }
	
	public void setRole(String role) { this.role = role; }
	public String getRole() { return role; }
}