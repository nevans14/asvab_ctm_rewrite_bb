package com.citm.spring.model.detail;

import java.io.Serializable;
import java.util.ArrayList;

public class TrainingProvided implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7763267309227425588L;
	String introStatement;
	ArrayList<TrainingList> list;

	public String getIntroStatement() {
		return introStatement;
	}

	public void setIntroStatement(String introStatement) {
		this.introStatement = introStatement;
	}

	public ArrayList<TrainingList> getList() {
		return list;
	}

	public void setList(ArrayList<TrainingList> list) {
		this.list = list;
	}
}
