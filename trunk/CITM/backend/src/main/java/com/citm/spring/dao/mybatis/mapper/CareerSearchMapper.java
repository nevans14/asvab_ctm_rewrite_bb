package com.citm.spring.dao.mybatis.mapper;

import java.util.ArrayList;

import org.apache.ibatis.annotations.Param;

import com.citm.spring.model.search.CareerCluster;
import com.citm.spring.model.search.CareerClusterPathway;
import com.citm.spring.model.search.JobFamily;
import com.citm.spring.model.search.SaveSearch;
import com.citm.spring.model.search.Search;
import com.citm.spring.model.search.ServiceContacts;

public interface CareerSearchMapper {

	ArrayList<Search> getSearchList();

	int insertSaveSearch(@Param("saveSearchObject") SaveSearch saveSearchObject);

	ArrayList<SaveSearch> getSaveSearch(@Param("userId") int userId);

	int deleteSaveSearch(@Param("userId") Integer userId, @Param("id") int id);
	
	ArrayList<CareerCluster> getCareerCluster();
	
	ArrayList<ServiceContacts> getServiceContacts();
	
	ArrayList<CareerClusterPathway> getCareerClusterPathway(@Param("ccId") int ccId);
	
	ArrayList<JobFamily> getJobFamily();
}
