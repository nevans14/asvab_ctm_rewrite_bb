package com.citm.spring.sso;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import org.springframework.security.web.authentication.logout.SimpleUrlLogoutSuccessHandler;

/**
 * This class deletes cookie used by Angular.
 *
 */
@PropertySource("classpath:SsoProperties.properties")
public class SsoLogout extends SimpleUrlLogoutSuccessHandler implements LogoutSuccessHandler {

	@Value("${cep.url}")
	private String ssoUrl;
	@Value("${citm.domain}")
	private String domain;
	
	@Override
	public void onLogoutSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication)
			throws IOException, ServletException {
		
		try {
			Cookie[] cookies = request.getCookies();
			for (int i = 0; i < cookies.length; i++) {
				Cookie cookie = new Cookie(cookies[i].getName(), null);
				cookie.setMaxAge(0);
				cookie.setPath("/");
				//cookie.setDomain(domain);
				response.addCookie(cookie);
			}			
		} catch (Exception e) {
			response.sendRedirect(ssoUrl);
		}

		response.sendRedirect(ssoUrl);
	}
}
