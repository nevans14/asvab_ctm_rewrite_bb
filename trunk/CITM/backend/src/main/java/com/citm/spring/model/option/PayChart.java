package com.citm.spring.model.option;

public class PayChart {

	String payGrade;
	float partial, withoutDependent, withDependent, differential;

	public String getPayGrade() {
		return payGrade;
	}

	public void setPayGrade(String payGrade) {
		this.payGrade = payGrade;
	}

	public float getPartial() {
		return partial;
	}

	public void setPartial(float partial) {
		this.partial = partial;
	}

	public float getWithoutDependent() {
		return withoutDependent;
	}

	public void setWithoutDependent(float withoutDependent) {
		this.withoutDependent = withoutDependent;
	}

	public float getWithDependent() {
		return withDependent;
	}

	public void setWithDependent(float withDependent) {
		this.withDependent = withDependent;
	}

	public float getDifferential() {
		return differential;
	}

	public void setDifferential(float differential) {
		this.differential = differential;
	}
}
