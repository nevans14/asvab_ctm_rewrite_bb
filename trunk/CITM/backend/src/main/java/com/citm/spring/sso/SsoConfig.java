package com.citm.spring.sso;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.security.web.savedrequest.HttpSessionRequestCache;
import org.springframework.security.web.savedrequest.RequestCache;
import org.springframework.security.web.util.matcher.OrRequestMatcher;
import org.springframework.security.web.util.matcher.RequestMatcher;
import org.springframework.session.data.redis.RedisOperationsSessionRepository;
import com.citm.spring.sso.SsoClientLoginCallbackFilter;

@Configuration
public class SsoConfig {

	@Autowired
	private RedisOperationsSessionRepository sessionRepository;

	@Bean
	public SsoClientLoginCallbackFilter ssoClientLoginCallbackFilter() {
		return new SsoClientLoginCallbackFilter(sessionRepository, requestCache());
	}

	@Bean
	public RequestCache requestCache() {
		return new CustomRequestCache();
	}
	
	//To resolve ${} in @Value
	@Bean
	public static PropertySourcesPlaceholderConfigurer propertyConfigInDev() {
		return new PropertySourcesPlaceholderConfigurer();
	}

	public static class CustomRequestCache extends HttpSessionRequestCache {

		@Override
		public void setRequestMatcher(RequestMatcher requestMatcher) {
			List<RequestMatcher> matchers = new ArrayList<>();
			super.setRequestMatcher(new OrRequestMatcher(matchers));
		}
	}

}
