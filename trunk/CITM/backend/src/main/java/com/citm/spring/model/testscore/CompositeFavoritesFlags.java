package com.citm.spring.model.testscore;

public class CompositeFavoritesFlags {
	
	private Integer userId;
	private Integer gs = 0;
	private Integer ar = 0;
	private Integer wk = 0;
	private Integer pc = 0;
	private Integer no = 0;
	private Integer cs = 0;
	private Integer as = 0;
	private Integer mk = 0;
	private Integer mc = 0;
	private Integer el = 0;
	private Integer ve = 0;
	private Integer ao = 0;
	private Integer afqt = 0;
	private Integer gt_army = 0;
	private Integer cl_army = 0;
	private Integer co_army = 0;
	private Integer el_army = 0;
	private Integer fa_army = 0;
	private Integer gm_army = 0;
	private Integer mm_army = 0;
	private Integer of_army = 0;
	private Integer sc_army = 0;
	private Integer st_army = 0;
	private Integer gt_navy = 0;
	private Integer el_navy = 0;
	private Integer bee_navy = 0;
	private Integer eng_navy = 0;
	private Integer mec_navy = 0;
	private Integer mec2_navy = 0;
	private Integer nuc_navy = 0;
	private Integer ops_navy = 0;
	private Integer hm_navy = 0;
	private Integer adm_navy = 0;
	private Integer m_af = 0;
	private Integer a_af = 0;
	private Integer g_af = 0;
	private Integer e_af = 0;
	private Integer mm_mc = 0;
	private Integer gt_mc = 0; 
	private Integer el_mc = 0;
	private Integer cl_mc = 0;
	/**
	 * @return the user_id
	 */
	public Integer getUserId() {
		return userId;
	}
	/**
	 * @param user_id the user_id to set
	 */
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	/**
	 * @return the gs
	 */
	public Integer getGs() {
		return gs;
	}
	/**
	 * @param gs the gs to set
	 */
	public void setGs(Integer gs) {
		this.gs = gs;
	}
	/**
	 * @return the ar
	 */
	public Integer getAr() {
		return ar;
	}
	/**
	 * @param ar the ar to set
	 */
	public void setAr(Integer ar) {
		this.ar = ar;
	}
	/**
	 * @return the wk
	 */
	public Integer getWk() {
		return wk;
	}
	/**
	 * @param wk the wk to set
	 */
	public void setWk(Integer wk) {
		this.wk = wk;
	}
	/**
	 * @return the pc
	 */
	public Integer getPc() {
		return pc;
	}
	/**
	 * @param pc the pc to set
	 */
	public void setPc(Integer pc) {
		this.pc = pc;
	}
	/**
	 * @return the no
	 */
	public Integer getNo() {
		return no;
	}
	/**
	 * @param no the no to set
	 */
	public void setNo(Integer no) {
		this.no = no;
	}
	/**
	 * @return the cs
	 */
	public Integer getCs() {
		return cs;
	}
	/**
	 * @param cs the cs to set
	 */
	public void setCs(Integer cs) {
		this.cs = cs;
	}
	/**
	 * @return the as
	 */
	public Integer getAs() {
		return as;
	}
	/**
	 * @param as the as to set
	 */
	public void setAs(Integer as) {
		this.as = as;
	}
	/**
	 * @return the mk
	 */
	public Integer getMk() {
		return mk;
	}
	/**
	 * @param mk the mk to set
	 */
	public void setMk(Integer mk) {
		this.mk = mk;
	}
	/**
	 * @return the mc
	 */
	public Integer getMc() {
		return mc;
	}
	/**
	 * @param mc the mc to set
	 */
	public void setMc(Integer mc) {
		this.mc = mc;
	}
	/**
	 * @return the el
	 */
	public Integer getEl() {
		return el;
	}
	/**
	 * @param el the el to set
	 */
	public void setEl(Integer el) {
		this.el = el;
	}
	/**
	 * @return the ve
	 */
	public Integer getVe() {
		return ve;
	}
	/**
	 * @param ve the ve to set
	 */
	public void setVe(Integer ve) {
		this.ve = ve;
	}
	/**
	 * @return the ao
	 */
	public Integer getAo() {
		return ao;
	}
	/**
	 * @param ao the ao to set
	 */
	public void setAo(Integer ao) {
		this.ao = ao;
	}
	/**
	 * @return the afqt
	 */
	public Integer getAfqt() {
		return afqt;
	}
	/**
	 * @param afqt the afqt to set
	 */
	public void setAfqt(Integer afqt) {
		this.afqt = afqt;
	}

	public Integer getgt_army() {
		return gt_army;
	}
	public void setgt_army(Integer gt_army) {
		this.gt_army = gt_army;
	}
	/**
	 * @return the cl_army
	 */
	public Integer getCl_army() {
		return cl_army;
	}
	/**
	 * @param cl_army the cl_army to set
	 */
	public void setCl_army(Integer cl_army) {
		this.cl_army = cl_army;
	}
	/**
	 * @return the co_army
	 */
	public Integer getCo_army() {
		return co_army;
	}
	/**
	 * @param co_army the co_army to set
	 */
	public void setCo_army(Integer co_army) {
		this.co_army = co_army;
	}
	/**
	 * @return the el_army
	 */
	public Integer getEl_army() {
		return el_army;
	}
	/**
	 * @param el_army the el_army to set
	 */
	public void setEl_army(Integer el_army) {
		this.el_army = el_army;
	}
	/**
	 * @return the fa_army
	 */
	public Integer getFa_army() {
		return fa_army;
	}
	/**
	 * @param fa_army the fa_army to set
	 */
	public void setFa_army(Integer fa_army) {
		this.fa_army = fa_army;
	}
	/**
	 * @return the gm_army
	 */
	public Integer getGm_army() {
		return gm_army;
	}
	/**
	 * @param gm_army the gm_army to set
	 */
	public void setGm_army(Integer gm_army) {
		this.gm_army = gm_army;
	}
	/**
	 * @return the mm_army
	 */
	public Integer getMm_army() {
		return mm_army;
	}
	/**
	 * @param mm_army the mm_army to set
	 */
	public void setMm_army(Integer mm_army) {
		this.mm_army = mm_army;
	}
	/**
	 * @return the of_army
	 */
	public Integer getOf_army() {
		return of_army;
	}
	/**
	 * @param of_army the of_army to set
	 */
	public void setOf_army(Integer of_army) {
		this.of_army = of_army;
	}
	/**
	 * @return the sc_army
	 */
	public Integer getSc_army() {
		return sc_army;
	}
	/**
	 * @param sc_army the sc_army to set
	 */
	public void setSc_army(Integer sc_army) {
		this.sc_army = sc_army;
	}
	/**
	 * @return the st_army
	 */
	public Integer getSt_army() {
		return st_army;
	}
	/**
	 * @param st_army the st_army to set
	 */
	public void setSt_army(Integer st_army) {
		this.st_army = st_army;
	}
	/**
	 * @return the gt_navy
	 */
	public Integer getGt_navy() {
		return gt_navy;
	}
	/**
	 * @param gt_navy the gt_navy to set
	 */
	public void setGt_navy(Integer gt_navy) {
		this.gt_navy = gt_navy;
	}
	/**
	 * @return the el_navy
	 */
	public Integer getEl_navy() {
		return el_navy;
	}
	/**
	 * @param el_navy the el_navy to set
	 */
	public void setEl_navy(Integer el_navy) {
		this.el_navy = el_navy;
	}
	/**
	 * @return the bee_navy
	 */
	public Integer getBee_navy() {
		return bee_navy;
	}
	/**
	 * @param bee_navy the bee_navy to set
	 */
	public void setBee_navy(Integer bee_navy) {
		this.bee_navy = bee_navy;
	}
	/**
	 * @return the eng_navy
	 */
	public Integer getEng_navy() {
		return eng_navy;
	}
	/**
	 * @param eng_navy the eng_navy to set
	 */
	public void setEng_navy(Integer eng_navy) {
		this.eng_navy = eng_navy;
	}
	/**
	 * @return the mec_navy
	 */
	public Integer getMec_navy() {
		return mec_navy;
	}
	/**
	 * @param mec_navy the mec_navy to set
	 */
	public void setMec_navy(Integer mec_navy) {
		this.mec_navy = mec_navy;
	}
	/**
	 * @return the mec2_navy
	 */
	public Integer getMec2_navy() {
		return mec2_navy;
	}
	/**
	 * @param mec2_navy the mec2_navy to set
	 */
	public void setMec2_navy(Integer mec2_navy) {
		this.mec2_navy = mec2_navy;
	}
	/**
	 * @return the nuc_navy
	 */
	public Integer getNuc_navy() {
		return nuc_navy;
	}
	/**
	 * @param nuc_navy the nuc_navy to set
	 */
	public void setNuc_navy(Integer nuc_navy) {
		this.nuc_navy = nuc_navy;
	}
	/**
	 * @return the ops_navy
	 */
	public Integer getOps_navy() {
		return ops_navy;
	}
	/**
	 * @param ops_navy the ops_navy to set
	 */
	public void setOps_navy(Integer ops_navy) {
		this.ops_navy = ops_navy;
	}
	/**
	 * @return the hm_navy
	 */
	public Integer getHm_navy() {
		return hm_navy;
	}
	/**
	 * @param hm_navy the hm_navy to set
	 */
	public void setHm_navy(Integer hm_navy) {
		this.hm_navy = hm_navy;
	}
	/**
	 * @return the adm_navy
	 */
	public Integer getAdm_navy() {
		return adm_navy;
	}
	/**
	 * @param adm_navy the adm_navy to set
	 */
	public void setAdm_navy(Integer adm_navy) {
		this.adm_navy = adm_navy;
	}
	/**
	 * @return the m_af
	 */
	public Integer getM_af() {
		return m_af;
	}
	/**
	 * @param m_af the m_af to set
	 */
	public void setM_af(Integer m_af) {
		this.m_af = m_af;
	}
	/**
	 * @return the a_af
	 */
	public Integer getA_af() {
		return a_af;
	}
	/**
	 * @param a_af the a_af to set
	 */
	public void setA_af(Integer a_af) {
		this.a_af = a_af;
	}
	/**
	 * @return the g_af
	 */
	public Integer getG_af() {
		return g_af;
	}
	/**
	 * @param g_af the g_af to set
	 */
	public void setG_af(Integer g_af) {
		this.g_af = g_af;
	}
	/**
	 * @return the e_af
	 */
	public Integer getE_af() {
		return e_af;
	}
	/**
	 * @param e_af the e_af to set
	 */
	public void setE_af(Integer e_af) {
		this.e_af = e_af;
	}
	/**
	 * @return the mm_mc
	 */
	public Integer getMm_mc() {
		return mm_mc;
	}
	/**
	 * @param mm_mc the mm_mc to set
	 */
	public void setMm_mc(Integer mm_mc) {
		this.mm_mc = mm_mc;
	}
	/**
	 * @return the gt_mc
	 */
	public Integer getGt_mc() {
		return gt_mc;
	}
	/**
	 * @param gt_mc the gt_mc to set
	 */
	public void setGt_mc(Integer gt_mc) {
		this.gt_mc = gt_mc;
	}
	/**
	 * @return the el_mc
	 */
	public Integer getEl_mc() {
		return el_mc;
	}
	/**
	 * @param el_mc the el_mc to set
	 */
	public void setEl_mc(Integer el_mc) {
		this.el_mc = el_mc;
	}
	/**
	 * @return the cl_mc
	 */
	public Integer getCl_mc() {
		return cl_mc;
	}
	/**
	 * @param cl_mc the cl_mc to set
	 */
	public void setCl_mc(Integer cl_mc) {
		this.cl_mc = cl_mc;
	}

}
