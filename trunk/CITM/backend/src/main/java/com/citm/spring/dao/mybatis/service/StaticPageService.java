package com.citm.spring.dao.mybatis.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.citm.spring.dao.mybatis.mapper.StaticPageMapper;
import com.citm.spring.model.staticpage.StaticPage;

@Service
public class StaticPageService {
	
	@Autowired
	StaticPageMapper mapper;
	
	public StaticPage getPageById(int pageId) {
		return mapper.getPageById(pageId);
	}
	
	public StaticPage getPageByPageNameOld(String pageName) {
		return mapper.getPageByPageNameOld(pageName);
	}
	
    public StaticPage getPageByPageName(String pageName, String website) {
        return mapper.getPageByPageName(pageName, website);
	}
	
    public ArrayList<StaticPage> getPageListByName(String pageName, String website) {
        return mapper.getPageListByName(pageName, website);
	}
}