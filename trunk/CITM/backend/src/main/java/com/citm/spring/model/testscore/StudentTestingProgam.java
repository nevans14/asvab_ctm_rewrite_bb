package com.citm.spring.model.testscore;

public class StudentTestingProgam {
	
    private TestScore studentTestProgram;
	/**
	 * @return the studentTestProgram
	 */
	public TestScore getStudentTestProgram() {
		return studentTestProgram;
	}
	/**
	 * @param studentTestProgram the studentTestProgram to set
	 */
	public void setStudentTestProgram(TestScore studentTestProgram) {
		this.studentTestProgram = studentTestProgram;
	}
}
