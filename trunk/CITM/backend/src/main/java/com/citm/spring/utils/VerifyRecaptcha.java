package com.citm.spring.utils;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.StringReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;

public class VerifyRecaptcha {

	public static final String url = "https://www.google.com/recaptcha/api/siteverify";
	public static final String secret = "6LcGnigTAAAAANT5_pMq0jejG5dl8rCIb86yBMN-";
	private final static String USER_AGENT = "";

	public static boolean verify(String gRecaptchaResponse) throws IOException {
		if (gRecaptchaResponse == null || "".equals(gRecaptchaResponse)) {
			return false;
		}
		try {
			URL requestUrl = new URL(url);
			HttpsURLConnection connection = (HttpsURLConnection)requestUrl.openConnection();
			connection.setRequestProperty("User-Agent", USER_AGENT);
			connection.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
			SSLContext sslContext = SSLContext.getInstance("TLSv1.2");
			sslContext.init(null,null,null);
			// bypass cert
			connection.setSSLSocketFactory(sslContext.getSocketFactory());
			String postParams = "secret=" + secret + "&response=" + gRecaptchaResponse;
			// Send post request
			connection.setDoOutput(true);
			DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
			wr.writeBytes(postParams);
			wr.flush();
			wr.close();

			String response = JsonApiHelper.sendRequest(connection);
			// parse JSON response and return 'success' value
			JsonReader jsonReader = Json.createReader(new StringReader(response));
			JsonObject jsonObject = jsonReader.readObject();
			jsonReader.close();
			return jsonObject.getBoolean("success");
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
}