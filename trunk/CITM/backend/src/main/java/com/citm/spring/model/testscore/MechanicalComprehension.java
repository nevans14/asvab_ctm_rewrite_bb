package com.citm.spring.model.testscore;

public class MechanicalComprehension {

	private	int	MC_YP_Score;
	private	int	MC_GS_Percentage;
	private	int	MC_GOS_Percentage;
	private	int	MC_COMP_Percentage;
	private	int	MC_TP_Percentage;
	private	int	MC_SGS;
	private	int	MC_USL;
	private	int	MC_LSL;
	/**
	 * @return the mC_YP_Score
	 */
	public int getMC_YP_Score() {
		return MC_YP_Score;
	}
	/**
	 * @param mC_YP_Score the mC_YP_Score to set
	 */
	public void setMC_YP_Score(int mC_YP_Score) {
		MC_YP_Score = mC_YP_Score;
	}
	/**
	 * @return the mC_GS_Percentage
	 */
	public int getMC_GS_Percentage() {
		return MC_GS_Percentage;
	}
	/**
	 * @param mC_GS_Percentage the mC_GS_Percentage to set
	 */
	public void setMC_GS_Percentage(int mC_GS_Percentage) {
		MC_GS_Percentage = mC_GS_Percentage;
	}
	/**
	 * @return the mC_GOS_Percentage
	 */
	public int getMC_GOS_Percentage() {
		return MC_GOS_Percentage;
	}
	/**
	 * @param mC_GOS_Percentage the mC_GOS_Percentage to set
	 */
	public void setMC_GOS_Percentage(int mC_GOS_Percentage) {
		MC_GOS_Percentage = mC_GOS_Percentage;
	}
	/**
	 * @return the mC_COMP_Percentage
	 */
	public int getMC_COMP_Percentage() {
		return MC_COMP_Percentage;
	}
	/**
	 * @param mC_COMP_Percentage the mC_COMP_Percentage to set
	 */
	public void setMC_COMP_Percentage(int mC_COMP_Percentage) {
		MC_COMP_Percentage = mC_COMP_Percentage;
	}
	/**
	 * @return the mC_TP_Percentage
	 */
	public int getMC_TP_Percentage() {
		return MC_TP_Percentage;
	}
	/**
	 * @param mC_TP_Percentage the mC_TP_Percentage to set
	 */
	public void setMC_TP_Percentage(int mC_TP_Percentage) {
		MC_TP_Percentage = mC_TP_Percentage;
	}
	/**
	 * @return the mC_SGS
	 */
	public int getMC_SGS() {
		return MC_SGS;
	}
	/**
	 * @param mC_SGS the mC_SGS to set
	 */
	public void setMC_SGS(int mC_SGS) {
		MC_SGS = mC_SGS;
	}
	/**
	 * @return the mC_USL
	 */
	public int getMC_USL() {
		return MC_USL;
	}
	/**
	 * @param mC_USL the mC_USL to set
	 */
	public void setMC_USL(int mC_USL) {
		MC_USL = mC_USL;
	}
	/**
	 * @return the mC_LSL
	 */
	public int getMC_LSL() {
		return MC_LSL;
	}
	/**
	 * @param mC_LSL the mC_LSL to set
	 */
	public void setMC_LSL(int mC_LSL) {
		MC_LSL = mC_LSL;
	}

	
}
