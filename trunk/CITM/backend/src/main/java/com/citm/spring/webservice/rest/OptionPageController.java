package com.citm.spring.webservice.rest;

import java.util.ArrayList;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.citm.spring.dao.mybatis.impl.OptionDAOImpl;
import com.citm.spring.model.option.Pay;
import com.citm.spring.model.option.PayChart;

@Controller
@RequestMapping("/option")
public class OptionPageController {

	static Logger logger = LogManager.getLogger("OptionPageController");

	@Autowired
	OptionDAOImpl dao;

	@RequestMapping(value = "/get-rank", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<ArrayList<Pay>> getRank() {
		logger.info("getRank");
		ArrayList<Pay> results;
		try {
			results = dao.getRank();
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<ArrayList<Pay>>(HttpStatus.BAD_REQUEST);
		}

		return new ResponseEntity<ArrayList<Pay>>(results, HttpStatus.OK);
	}

	@RequestMapping(value = "/get-Pay", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<ArrayList<Pay>> getPay() {
		logger.info("getPay");
		ArrayList<Pay> results;
		try {
			results = dao.getPay();
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<ArrayList<Pay>>(HttpStatus.BAD_REQUEST);
		}

		return new ResponseEntity<ArrayList<Pay>>(results, HttpStatus.OK);
	}

	@RequestMapping(value = "/get-pay-chart", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<ArrayList<PayChart>> getPayChart() {
		logger.info("getPayChart");
		ArrayList<PayChart> results;
		try {
			results = dao.getPayChart();
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<ArrayList<PayChart>>(HttpStatus.BAD_REQUEST);
		}

		return new ResponseEntity<ArrayList<PayChart>>(results, HttpStatus.OK);
	}

}
