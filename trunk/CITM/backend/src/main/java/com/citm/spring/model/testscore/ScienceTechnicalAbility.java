package com.citm.spring.model.testscore;

public class ScienceTechnicalAbility {

	private	int	TEC_YP_Score;
	private	int	TEC_GS_Percentage;
	private	int	TEC_GOS_Percentage;
	private	int	TEC_COMP_Percentage;
	private	int	TEC_TP_Percentage;
	private	int	TEC_SGS;
	private	int	TEC_USL;
	private	int	TEC_LSL;
	/**
	 * @return the tEC_YP_Score
	 */
	public int getTEC_YP_Score() {
		return TEC_YP_Score;
	}
	/**
	 * @param tEC_YP_Score the tEC_YP_Score to set
	 */
	public void setTEC_YP_Score(int tEC_YP_Score) {
		TEC_YP_Score = tEC_YP_Score;
	}
	/**
	 * @return the tEC_GS_Percentage
	 */
	public int getTEC_GS_Percentage() {
		return TEC_GS_Percentage;
	}
	/**
	 * @param tEC_GS_Percentage the tEC_GS_Percentage to set
	 */
	public void setTEC_GS_Percentage(int tEC_GS_Percentage) {
		TEC_GS_Percentage = tEC_GS_Percentage;
	}
	/**
	 * @return the tEC_GOS_Percentage
	 */
	public int getTEC_GOS_Percentage() {
		return TEC_GOS_Percentage;
	}
	/**
	 * @param tEC_GOS_Percentage the tEC_GOS_Percentage to set
	 */
	public void setTEC_GOS_Percentage(int tEC_GOS_Percentage) {
		TEC_GOS_Percentage = tEC_GOS_Percentage;
	}
	/**
	 * @return the tEC_COMP_Percentage
	 */
	public int getTEC_COMP_Percentage() {
		return TEC_COMP_Percentage;
	}
	/**
	 * @param tEC_COMP_Percentage the tEC_COMP_Percentage to set
	 */
	public void setTEC_COMP_Percentage(int tEC_COMP_Percentage) {
		TEC_COMP_Percentage = tEC_COMP_Percentage;
	}
	/**
	 * @return the tEC_TP_Percentage
	 */
	public int getTEC_TP_Percentage() {
		return TEC_TP_Percentage;
	}
	/**
	 * @param tEC_TP_Percentage the tEC_TP_Percentage to set
	 */
	public void setTEC_TP_Percentage(int tEC_TP_Percentage) {
		TEC_TP_Percentage = tEC_TP_Percentage;
	}
	/**
	 * @return the tEC_SGS
	 */
	public int getTEC_SGS() {
		return TEC_SGS;
	}
	/**
	 * @param tEC_SGS the tEC_SGS to set
	 */
	public void setTEC_SGS(int tEC_SGS) {
		TEC_SGS = tEC_SGS;
	}
	/**
	 * @return the tEC_USL
	 */
	public int getTEC_USL() {
		return TEC_USL;
	}
	/**
	 * @param tEC_USL the tEC_USL to set
	 */
	public void setTEC_USL(int tEC_USL) {
		TEC_USL = tEC_USL;
	}
	/**
	 * @return the tEC_LSL
	 */
	public int getTEC_LSL() {
		return TEC_LSL;
	}
	/**
	 * @param tEC_LSL the tEC_LSL to set
	 */
	public void setTEC_LSL(int tEC_LSL) {
		TEC_LSL = tEC_LSL;
	}

	
}
