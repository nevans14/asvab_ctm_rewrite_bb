package com.citm.spring.model.testscore;

public class RaceFields {

	private	int	americanIndian;
	private	int	asian;
	private	int	africanAmerican;
	private	int	nativeHawiian;
	private	int	white;
	private	int	reserved;
	private	int	hispanic;
	private	int	notHispanic;
	/**
	 * @return the americanIndian
	 */
	public int getAmericanIndian() {
		return americanIndian;
	}
	/**
	 * @param americanIndian the americanIndian to set
	 */
	public void setAmericanIndian(int americanIndian) {
		this.americanIndian = americanIndian;
	}
	/**
	 * @return the asian
	 */
	public int getAsian() {
		return asian;
	}
	/**
	 * @param asian the asian to set
	 */
	public void setAsian(int asian) {
		this.asian = asian;
	}
	/**
	 * @return the africanAmerican
	 */
	public int getAfricanAmerican() {
		return africanAmerican;
	}
	/**
	 * @param africanAmerican the africanAmerican to set
	 */
	public void setAfricanAmerican(int africanAmerican) {
		this.africanAmerican = africanAmerican;
	}
	/**
	 * @return the nativeHawiian
	 */
	public int getNativeHawiian() {
		return nativeHawiian;
	}
	/**
	 * @param nativeHawiian the nativeHawiian to set
	 */
	public void setNativeHawiian(int nativeHawiian) {
		this.nativeHawiian = nativeHawiian;
	}
	/**
	 * @return the white
	 */
	public int getWhite() {
		return white;
	}
	/**
	 * @param white the white to set
	 */
	public void setWhite(int white) {
		this.white = white;
	}
	/**
	 * @return the reserved
	 */
	public int getReserved() {
		return reserved;
	}
	/**
	 * @param reserved the reserved to set
	 */
	public void setReserved(int reserved) {
		this.reserved = reserved;
	}
	/**
	 * @return the hispanic
	 */
	public int getHispanic() {
		return hispanic;
	}
	/**
	 * @param hispanic the hispanic to set
	 */
	public void setHispanic(int hispanic) {
		this.hispanic = hispanic;
	}
	/**
	 * @return the notHispanic
	 */
	public int getNotHispanic() {
		return notHispanic;
	}
	/**
	 * @param notHispanic the notHispanic to set
	 */
	public void setNotHispanic(int notHispanic) {
		this.notHispanic = notHispanic;
	}


}
