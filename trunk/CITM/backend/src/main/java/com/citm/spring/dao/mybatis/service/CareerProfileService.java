package com.citm.spring.dao.mybatis.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.citm.spring.dao.mybatis.mapper.CareerProfileMapper;
import com.citm.spring.model.careerprofile.CareerProfile;
import com.citm.spring.model.careerprofile.HotJobs;

@Service
public class CareerProfileService {

	@Autowired
	private CareerProfileMapper careerProfileMapper;

	public List<CareerProfile> selectCareerProfiles() {
		return careerProfileMapper.selectCareerProfiles();
	}

	public List<HotJobs> getHotJobTitle(String mcId) {
		return careerProfileMapper.getHotJobTitle(mcId);
	}
}

