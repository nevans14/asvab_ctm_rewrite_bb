package com.citm.spring;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class HttpsRedirectFilter implements Filter {
	private final String PROD_DOMAIN = "careersinthemilitary.com";
	
	private boolean isUsingHttps = false;
	private int port;	
	private String uri, protocol, domain;
	
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		HttpServletRequest req = (HttpServletRequest)request;
		HttpServletResponse res = (HttpServletResponse)response;	
		
		this.uri = req.getRequestURI();
		this.protocol = req.getScheme();
		this.domain = req.getServerName();
		
		if (this.domain.toLowerCase().contains(PROD_DOMAIN)) {
			this.port = 443;
		} else {
			this.port = 8443;
		}
		
		if (this.protocol.toLowerCase().equals("http") && this.isUsingHttps) {
			response.setContentType("text/html");
			String httpsPath = String.format("https://%s:%d%s", domain, port, uri);
			res.setStatus(HttpServletResponse.SC_MOVED_TEMPORARILY);
			res.setHeader("Location", httpsPath);			
		}
		chain.doFilter(req, res);
	}
	
	@Override
	public void init(FilterConfig arg0) throws ServletException {
		
	}
	
	@Override
	public void destroy() {
		
	}	
}