package com.citm.spring.dao.mybatis;

import java.util.Date;
import java.util.List;

import com.citm.spring.model.login.AccessCode;
import com.citm.spring.model.login.MEPS;
import com.citm.spring.model.login.UserRole;
import com.citm.spring.model.login.Users;

public interface RegistrationDAO {
	
	public Users getUser(String email);
	
	public Users getUserByResetKey(String resetKey);
	
	public Integer insertUser(Users user);
	
	public Integer updateUser(Users user);
	
	public AccessCode getLastUsedAccessCode();
	
	public AccessCode getAccessCode(String accessCode);
	
	public Integer insertAccessCode(AccessCode accessCode);
	
	public MEPS getByAccessCode(String accessCode);
	
	public int insertAsvabLog(int userId, Date loginDate);
	
	public List<UserRole> getRoles(String email);
	
	public int insertRole(UserRole role);
}