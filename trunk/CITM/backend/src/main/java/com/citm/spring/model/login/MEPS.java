package com.citm.spring.model.login;

public class MEPS {
	private String mepsId, name, schoolCode, schoolName, street, city, state, zip;
	
	public void setMepsId(String mepsId) { this.mepsId = mepsId; }
	public String getMepsId() { return mepsId; }
	
	public void setName(String name) { this.name = name; }
	public String getName() { return name; }
	
	public void setSchoolCode(String schoolCode) { this.schoolCode = schoolCode; }
	public String getSchoolCode() { return schoolCode; }
	
	public void setSchoolName(String schoolName) { this.schoolName = schoolName; }
	public String getSchoolName() { return schoolName; }
	
	public void setStreet(String street) { this.street = street; }
	public String getStree() { return street; }
	
	public void setCity(String city) { this.city = city; }
	public String getCity() { return city; }
	
	public void setState(String state) { this.state = state; }
	public String getState() { return state; }
	
	public void setZip(String zip) { this.zip = zip; }
	public String getZip() { return zip; }
}