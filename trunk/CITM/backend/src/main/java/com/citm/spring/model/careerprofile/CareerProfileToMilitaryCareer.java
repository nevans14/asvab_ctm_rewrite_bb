package com.citm.spring.model.careerprofile;

import java.util.List;

import com.citm.spring.model.detail.CareerPath;

public class CareerProfileToMilitaryCareer {
	
	private int profileId;
	
	private int milId;
	
	private String mcId;
	
	private String title;
	
	private String customTitle;
	
	private List<CareerPath> careerPath;

	public int getProfileId() {
		return profileId;
	}

	public void setProfileId(int profileId) {
		this.profileId = profileId;
	}

	/**
	 * @return the mcId
	 */
	public String getMcId() {
		return mcId;
	}

	/**
	 * @param mcId the mcId to set
	 */
	public void setMcId(String mcId) {
		this.mcId = mcId;
	}

	public int getMilId() {
		return milId;
	}

	public void setMilId(int milId) {
		this.milId = milId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public List<CareerPath> getCareerPath() {
		return careerPath;
	}

	public String getCustomTitle() {
		return customTitle;
	}

	public void setCustomTitle(String customTitle) {
		this.customTitle = customTitle;
	}

	public void setCareerPath(List<CareerPath> careerPath) {
		this.careerPath = careerPath;
	}

}
