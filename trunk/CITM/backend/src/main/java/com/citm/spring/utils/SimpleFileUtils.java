package com.citm.spring.utils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;



public class SimpleFileUtils {
	
	/**
	 *  Read a file as a string  ... Look into use buffered reader 
	 * 
	 * @param path
	 * @param encoding
	 * @return
	 * @throws IOException
	 */
	public synchronized static String readFile(String path, Charset encoding) 
			  throws IOException 
			{
			  byte[] encoded = Files.readAllBytes(Paths.get(path));
			  if ( null == encoding ) {
				  return new String(encoded);				  
			  } else {
				  return new String(encoded, encoding);				  
			  }
			}


	public synchronized static String readFileFromClasspath(String path) 
			  throws IOException 	{			  
				
				String lineSeparator = System.getProperty("line.separator");
		        InputStream stream = SimpleFileUtils.class.getResourceAsStream(path);
		        		        
		        BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
		        StringBuilder out = new StringBuilder();
		        String line;
		        while ((line = reader.readLine()) != null) {
		            out.append(  line   + lineSeparator);
		        }
		        reader.close();
		        return out.toString();			  
			  
			}
    
	        public synchronized static InputStream getInputStreamFromFileOnClasspath(String path) 
	                throws IOException    {             

	            InputStream stream = SimpleFileUtils.class.getResourceAsStream(path);

	            return stream;
	        }


	/**
	 * 
	 * 
	 * 
	 * @param is
	 * @return
	 */
	public static synchronized  String getStringFromInputStream(InputStream is) {
 
		BufferedReader br = null;
		StringBuilder sb = new StringBuilder();
 
		String line;
		try {
 
			br = new BufferedReader(new InputStreamReader(is));
			while ((line = br.readLine()) != null) {
				sb.append(line);
			}
 
		} catch (IOException e) {
			e.printStackTrace();
			RuntimeException re = new RuntimeException("getStringFromInputStream:IOException ....!");
			re.setStackTrace(e.getStackTrace());
			throw(re);

			
			
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					RuntimeException re = new RuntimeException("getStringFromInputStream:IOException on close file left open.!");
					re.setStackTrace(e.getStackTrace());
					throw(re);
				}
			}
		}
		return sb.toString();
	}

	
	public static synchronized void writeStringToFile(String fileName, String textToWrite) {
		BufferedWriter writer = null;
		File file = null;
		try
		{
			file =new File(fileName);
    		
    		//if file doesnt exists, then create it
    		if(!file.exists()){
    			file.createNewFile();
    		}		
			
		    writer = new BufferedWriter( new FileWriter( fileName));
		    writer.write( textToWrite);
		}
		catch ( IOException e)
		{
			RuntimeException re = new RuntimeException("writeStringToFile:IOException...");
			re.setStackTrace(e.getStackTrace());
			throw(re);
		}
		finally
		{
		    try
		    {
		        if ( writer != null)
		        writer.close( );
		        if ( file != null )
		        	file = null;
		        fileName = null;
		        textToWrite = null;
		    }
		    catch ( IOException e)
		    {
				RuntimeException re = new RuntimeException("writeStringToFile:IOException on close file left open.!");
				re.setStackTrace(e.getStackTrace());
				throw(re);
		    }
		}
	
	}

	public static synchronized void appendStringToFile(String fileName, String textToWrite) {
		BufferedWriter writer = null;
		File file = null;
		try
		{
			file =new File(fileName);
    		
    		//if file doesnt exists, then create it
    		if(!file.exists()){
    			file.createNewFile();
    		}		
			
		    writer = new BufferedWriter( new FileWriter( fileName,true));
		    writer.write( textToWrite);
		}
		catch ( IOException e)
		{
			RuntimeException re = new RuntimeException("writeStringToFile:IOException...");
			re.setStackTrace(e.getStackTrace());
			throw(re);
		}
		finally
		{
		    try
		    {
		        if ( writer != null)
		        writer.close( );
		        if ( file != null )
		        	file = null;
		        fileName = null;
		        textToWrite = null;
		    }
		    catch ( IOException e)
		    {
				RuntimeException re = new RuntimeException("writeStringToFile:IOException on close file left open.!");
				re.setStackTrace(e.getStackTrace());
				throw(re);
		    }
		}
	
	}

	
	/* *******************************
	try{
		String data = " This content will append to the end of the file";
		
		File file =new File("javaio-appendfile.txt");
		
		//if file doesnt exists, then create it
		if(!file.exists()){
			file.createNewFile();
		}
		
		//true = append file
		FileWriter fileWritter = new FileWriter(file.getName(),true);
	        BufferedWriter bufferWritter = new BufferedWriter(fileWritter);
	        bufferWritter.write(data);
	        bufferWritter.close();
	    
        System.out.println("Done");
        
	}catch(IOException e){
		e.printStackTrace();
	}
}	
	*******************************/
}
