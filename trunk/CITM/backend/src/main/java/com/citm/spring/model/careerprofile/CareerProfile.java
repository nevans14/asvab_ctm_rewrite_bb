package com.citm.spring.model.careerprofile;

import java.util.List;

import com.citm.spring.model.detail.CareerPath;

public class CareerProfile {

	private int profileId;
	
	private String mcId;
	
	private String prefix;
	
	private String firstName;
	
	private String lastName;
	
	private String moc; 
	
	private String teaserText;
	
	private String careerCluster;
	
	private List<CareerProfileImage> imageList;
	
	private List<CareerProfileJobQuestion> jobQuestionList;
	
	private List<CareerProfileMetaData> metaDataList;
	
	private List<CareerProfileSnapshotQuestion> snapshotQuestionList;
	
	private List<CareerProfileToMilitaryCareer> toMilitaryCareerList;
	
	private List<CareerPath> careerPath;

	/**
	 * @return the profileId
	 */
	public int getProfileId() {
		return profileId;
	}

	/**
	 * @param profileId the profileId to set
	 */
	public void setProfileId(int profileId) {
		this.profileId = profileId;
	}

	/**
	 * @return the mcId
	 */
	public String getMcId() {
		return mcId;
	}

	/**
	 * @param mcId the mcId to set
	 */
	public void setMcId(String mcId) {
		this.mcId = mcId;
	}

	/**
	 * @return the prefix
	 */
	public String getPrefix() {
		return prefix;
	}

	/**
	 * @param prefix the prefix to set
	 */
	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return the moc
	 */
	public String getMoc() {
		return moc;
	}

	/**
	 * @param moc the moc to set
	 */
	public void setMoc(String moc) {
		this.moc = moc;
	}

	/**
	 * @return the teaserText
	 */
	public String getTeaserText() {
		return teaserText;
	}

	/**
	 * @param teaserText the teaserText to set
	 */
	public void setTeaserText(String teaserText) {
		this.teaserText = teaserText;
	}

	public String getCareerCluster() {
		return careerCluster;
	}

	public void setCareerCluster(String careerCluster) {
		this.careerCluster = careerCluster;
	}

	/**
	 * @return the imageList
	 */
	public List<CareerProfileImage> getImageList() {
		return imageList;
	}

	/**
	 * @param imageList the imageList to set
	 */
	public void setImageList(List<CareerProfileImage> imageList) {
		this.imageList = imageList;
	}

	/**
	 * @return the jobQuestionList
	 */
	public List<CareerProfileJobQuestion> getJobQuestionList() {
		return jobQuestionList;
	}

	/**
	 * @param jobQuestionList the jobQuestionList to set
	 */
	public void setJobQuestionList(List<CareerProfileJobQuestion> jobQuestionList) {
		this.jobQuestionList = jobQuestionList;
	}

	/**
	 * @return the metaDataList
	 */
	public List<CareerProfileMetaData> getMetaDataList() {
		return metaDataList;
	}

	/**
	 * @param metaDataList the metaDataList to set
	 */
	public void setMetaDataList(List<CareerProfileMetaData> metaDataList) {
		this.metaDataList = metaDataList;
	}

	/**
	 * @return the snapShotQuestionList
	 */
	public List<CareerProfileSnapshotQuestion> getSnapshotQuestionList() {
		return snapshotQuestionList;
	}

	/**
	 * @param snapShotQuestionList the snapShotQuestionList to set
	 */
	public void setSnapshotQuestionList(List<CareerProfileSnapshotQuestion> snapshotQuestionList) {
		this.snapshotQuestionList = snapshotQuestionList;
	}

	/**
	 * @return the toMilitaryCareerList
	 */
	public List<CareerProfileToMilitaryCareer> getToMilitaryCareerList() {
		return toMilitaryCareerList;
	}

	/**
	 * @param toMilitaryCareerList the toMilitaryCareerList to set
	 */
	public void setToMilitaryCareerList(List<CareerProfileToMilitaryCareer> toMilitaryCareerList) {
		this.toMilitaryCareerList = toMilitaryCareerList;
	}

	public List<CareerPath> getCareerPath() {
		return careerPath;
	}

	public void setCareerPath(List<CareerPath> careerPath) {
		this.careerPath = careerPath;
	}

}
