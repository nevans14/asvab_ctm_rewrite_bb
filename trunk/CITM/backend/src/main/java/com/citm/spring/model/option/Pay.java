package com.citm.spring.model.option;

import java.io.Serializable;

public class Pay implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1413015131112853011L;
	String paygrade, title, service, rate, rank, abbreviation;
	float value;

	public float getValue() {
		return value;
	}

	public void setValue(float value) {
		this.value = value;
	}

	public String getPaygrade() {
		return paygrade;
	}

	public void setPaygrade(String paygrade) {
		this.paygrade = paygrade;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getService() {
		return service;
	}

	public void setService(String service) {
		this.service = service;
	}

	public String getRate() {
		return rate;
	}

	public void setRate(String rate) {
		this.rate = rate;
	}

	public String getRank() {
		return rank;
	}

	public void setRank(String rank) {
		this.rank = rank;
	}

	public String getAbbreviation() {
		return abbreviation;
	}

	public void setAbbreviation(String abbreviation) {
		this.abbreviation = abbreviation;
	}
}
