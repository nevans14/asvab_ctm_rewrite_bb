package com.citm.spring.webservice.rest;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.citm.spring.dao.mybatis.impl.CareerProfileDAOImpl;
import com.citm.spring.model.careerprofile.CareerProfile;
import com.citm.spring.model.careerprofile.HotJobs;

@Controller
@RequestMapping("/CareerProfile")
public class CareerProfileController {
	static Logger logger = LogManager.getLogger("CareerProfileController");

	@Autowired
	CareerProfileDAOImpl careerProfileDao;
	
	@RequestMapping(value = "/getCareerProfileList", method = RequestMethod.GET)
	public @ResponseBody List<CareerProfile> selectCareerProfiles() {
		try {
			return careerProfileDao.selectCareerProfiles();	
		} catch (Exception ex) {
			logger.error(ex.getMessage());
			throw(ex);
		}
	}
		
	@RequestMapping(value = "/getHotJobTitle/{mcId}/", method = RequestMethod.GET)
	public @ResponseBody List<HotJobs> getHotJobList(@PathVariable String mcId) {
		try {
			return careerProfileDao.getHotJobTitle(mcId);	
		} catch (Exception ex) {
			logger.error(ex.getMessage());
			throw(ex);
		}
	}
}
