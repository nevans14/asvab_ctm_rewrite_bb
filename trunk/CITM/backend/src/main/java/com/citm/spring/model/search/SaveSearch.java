package com.citm.spring.model.search;

import java.io.Serializable;

public class SaveSearch implements Serializable {

	private static final long serialVersionUID = 5351803400464286949L;
	String keyword, saveTitle;
	Boolean army, marines, airForce, navy, coastGuard, enlisted, officer, warrantOfficer, realistic, investigative,
			artistic, social, enterprising, conventional, hotJob, ccOne, ccTwo, ccThree, ccFour, ccFive, ccSix, ccSeven,
			ccEight, ccNine, ccTen, ccEleven, ccTwelve, ccThirteen, ccFourteen, ccFifteen, ccSixteen;
	Integer userId;

	public Boolean getCcOne() {
		return ccOne;
	}

	public void setCcOne(Boolean ccOne) {
		this.ccOne = ccOne;
	}

	public Boolean getCcTwo() {
		return ccTwo;
	}

	public void setCcTwo(Boolean ccTwo) {
		this.ccTwo = ccTwo;
	}

	public Boolean getCcThree() {
		return ccThree;
	}

	public void setCcThree(Boolean ccThree) {
		this.ccThree = ccThree;
	}

	public Boolean getCcFour() {
		return ccFour;
	}

	public void setCcFour(Boolean ccFour) {
		this.ccFour = ccFour;
	}

	public Boolean getCcFive() {
		return ccFive;
	}

	public void setCcFive(Boolean ccFive) {
		this.ccFive = ccFive;
	}

	public Boolean getCcSix() {
		return ccSix;
	}

	public void setCcSix(Boolean ccSix) {
		this.ccSix = ccSix;
	}

	public Boolean getCcSeven() {
		return ccSeven;
	}

	public void setCcSeven(Boolean ccSeven) {
		this.ccSeven = ccSeven;
	}

	public Boolean getCcEight() {
		return ccEight;
	}

	public void setCcEight(Boolean ccEight) {
		this.ccEight = ccEight;
	}

	public Boolean getCcNine() {
		return ccNine;
	}

	public void setCcNine(Boolean ccNine) {
		this.ccNine = ccNine;
	}

	public Boolean getCcTen() {
		return ccTen;
	}

	public void setCcTen(Boolean ccTen) {
		this.ccTen = ccTen;
	}

	public Boolean getCcEleven() {
		return ccEleven;
	}

	public void setCcEleven(Boolean ccEleven) {
		this.ccEleven = ccEleven;
	}

	public Boolean getCcTwelve() {
		return ccTwelve;
	}

	public void setCcTwelve(Boolean ccTwelve) {
		this.ccTwelve = ccTwelve;
	}

	public Boolean getCcThirteen() {
		return ccThirteen;
	}

	public void setCcThirteen(Boolean ccThirteen) {
		this.ccThirteen = ccThirteen;
	}

	public Boolean getCcFourteen() {
		return ccFourteen;
	}

	public void setCcFourteen(Boolean ccFourteen) {
		this.ccFourteen = ccFourteen;
	}

	public Boolean getCcFifteen() {
		return ccFifteen;
	}

	public void setCcFifteen(Boolean ccFifteen) {
		this.ccFifteen = ccFifteen;
	}

	public Boolean getCcSixteen() {
		return ccSixteen;
	}

	public void setCcSixteen(Boolean ccSixteen) {
		this.ccSixteen = ccSixteen;
	}

	int id;

	public String getSaveTitle() {
		return saveTitle;
	}

	public void setSaveTitle(String saveTitle) {
		this.saveTitle = saveTitle;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public Boolean getArmy() {
		return army;
	}

	public void setArmy(Boolean army) {
		this.army = army;
	}

	public Boolean getMarines() {
		return marines;
	}

	public void setMarines(Boolean marines) {
		this.marines = marines;
	}

	public Boolean getAirForce() {
		return airForce;
	}

	public void setAirForce(Boolean airForce) {
		this.airForce = airForce;
	}

	public Boolean getNavy() {
		return navy;
	}

	public void setNavy(Boolean navy) {
		this.navy = navy;
	}

	public Boolean getCoastGuard() {
		return coastGuard;
	}

	public void setCoastGuard(Boolean coastGuard) {
		this.coastGuard = coastGuard;
	}

	public Boolean getEnlisted() {
		return enlisted;
	}

	public void setEnlisted(Boolean enlisted) {
		this.enlisted = enlisted;
	}

	public Boolean getOfficer() {
		return officer;
	}

	public void setOfficer(Boolean officer) {
		this.officer = officer;
	}

	public Boolean getWarrantOfficer() {
		return warrantOfficer;
	}

	public void setWarrantOfficer(Boolean warrantOfficer) {
		this.warrantOfficer = warrantOfficer;
	}

	public Boolean getRealistic() {
		return realistic;
	}

	public void setRealistic(Boolean realistic) {
		this.realistic = realistic;
	}

	public Boolean getInvestigative() {
		return investigative;
	}

	public void setInvestigative(Boolean investigative) {
		this.investigative = investigative;
	}

	public Boolean getArtistic() {
		return artistic;
	}

	public void setArtistic(Boolean artistic) {
		this.artistic = artistic;
	}

	public Boolean getSocial() {
		return social;
	}

	public void setSocial(Boolean social) {
		this.social = social;
	}

	public Boolean getEnterprising() {
		return enterprising;
	}

	public void setEnterprising(Boolean enterprising) {
		this.enterprising = enterprising;
	}

	public Boolean getConventional() {
		return conventional;
	}

	public void setConventional(Boolean conventional) {
		this.conventional = conventional;
	}

	public Boolean getHotJob() {
		return hotJob;
	}

	public void setHotJob(Boolean hotJob) {
		this.hotJob = hotJob;
	}
}
