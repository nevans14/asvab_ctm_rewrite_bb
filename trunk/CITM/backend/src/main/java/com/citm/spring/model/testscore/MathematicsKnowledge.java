package com.citm.spring.model.testscore;

public class MathematicsKnowledge {

	private	int	MK_YP_Score;
	private	int	MK_GS_Percentage;
	private	int	MK_GOS_Percentage;
	private	int	MK_COMP_Percentage;
	private	int	MK_TP_Percentage;
	private	int	MK_SGS;
	private	int	MK_USL;
	private	int	MK_LSL;
	/**
	 * @return the mK_YP_Score
	 */
	public int getMK_YP_Score() {
		return MK_YP_Score;
	}
	/**
	 * @param mK_YP_Score the mK_YP_Score to set
	 */
	public void setMK_YP_Score(int mK_YP_Score) {
		MK_YP_Score = mK_YP_Score;
	}
	/**
	 * @return the mK_GS_Percentage
	 */
	public int getMK_GS_Percentage() {
		return MK_GS_Percentage;
	}
	/**
	 * @param mK_GS_Percentage the mK_GS_Percentage to set
	 */
	public void setMK_GS_Percentage(int mK_GS_Percentage) {
		MK_GS_Percentage = mK_GS_Percentage;
	}
	/**
	 * @return the mK_GOS_Percentage
	 */
	public int getMK_GOS_Percentage() {
		return MK_GOS_Percentage;
	}
	/**
	 * @param mK_GOS_Percentage the mK_GOS_Percentage to set
	 */
	public void setMK_GOS_Percentage(int mK_GOS_Percentage) {
		MK_GOS_Percentage = mK_GOS_Percentage;
	}
	/**
	 * @return the mK_COMP_Percentage
	 */
	public int getMK_COMP_Percentage() {
		return MK_COMP_Percentage;
	}
	/**
	 * @param mK_COMP_Percentage the mK_COMP_Percentage to set
	 */
	public void setMK_COMP_Percentage(int mK_COMP_Percentage) {
		MK_COMP_Percentage = mK_COMP_Percentage;
	}
	/**
	 * @return the mK_TP_Percentage
	 */
	public int getMK_TP_Percentage() {
		return MK_TP_Percentage;
	}
	/**
	 * @param mK_TP_Percentage the mK_TP_Percentage to set
	 */
	public void setMK_TP_Percentage(int mK_TP_Percentage) {
		MK_TP_Percentage = mK_TP_Percentage;
	}
	/**
	 * @return the mK_SGS
	 */
	public int getMK_SGS() {
		return MK_SGS;
	}
	/**
	 * @param mK_SGS the mK_SGS to set
	 */
	public void setMK_SGS(int mK_SGS) {
		MK_SGS = mK_SGS;
	}
	/**
	 * @return the mK_USL
	 */
	public int getMK_USL() {
		return MK_USL;
	}
	/**
	 * @param mK_USL the mK_USL to set
	 */
	public void setMK_USL(int mK_USL) {
		MK_USL = mK_USL;
	}
	/**
	 * @return the mK_LSL
	 */
	public int getMK_LSL() {
		return MK_LSL;
	}
	/**
	 * @param mK_LSL the mK_LSL to set
	 */
	public void setMK_LSL(int mK_LSL) {
		MK_LSL = mK_LSL;
	}

	
}
