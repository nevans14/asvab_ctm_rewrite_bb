package com.citm.spring.model.detail;

import java.io.Serializable;

public class SkillRating implements Serializable{

	private static final long serialVersionUID = 844973920153884712L;
	float verbal, math, science;

	public float getVerbal() {
		return verbal;
	}

	public void setVerbal(float verbal) {
		this.verbal = verbal;
	}

	public float getMath() {
		return math;
	}

	public void setMath(float math) {
		this.math = math;
	}

	public float getScience() {
		return science;
	}

	public void setScience(float science) {
		this.science = science;
	}
}
