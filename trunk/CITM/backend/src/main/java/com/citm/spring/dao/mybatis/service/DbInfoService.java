package com.citm.spring.dao.mybatis.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.citm.spring.dao.mybatis.mapper.DbInfoMapper;
import com.citm.spring.model.DbInfo;

@Service
public class DbInfoService {
	
	@Autowired
	private DbInfoMapper mapper;
	
	public DbInfo getDbInfo(String name) {
		return mapper.getDbInfo(name);
	}
	
}