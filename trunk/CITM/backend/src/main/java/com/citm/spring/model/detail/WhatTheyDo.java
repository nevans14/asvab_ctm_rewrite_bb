package com.citm.spring.model.detail;

import java.io.Serializable;

public class WhatTheyDo implements Serializable {

	private static final long serialVersionUID = 6506761830129579465L;
	String whatTheyDo;

	public String getWhatTheyDo() {
		return whatTheyDo;
	}

	public void setWhatTheyDo(String whatTheyDo) {
		this.whatTheyDo = whatTheyDo;
	}
}
