package com.citm.spring.model.careerprofile;

public class HotJobs {

	
	private String mcId; 
	private String socId; 
	private String svc; 
	private String mpc; 
	private String moc; 
	private String mocTitle; 
	private String overview;
	/**
	 * @return the mcId
	 */
	public String getMcId() {
		return mcId;
	}
	/**
	 * @param mcId the mcId to set
	 */
	public void setMcId(String mcId) {
		this.mcId = mcId;
	}
	/**
	 * @return the socId
	 */
	public String getSocId() {
		return socId;
	}
	/**
	 * @param socId the socId to set
	 */
	public void setSocId(String socId) {
		this.socId = socId;
	}
	/**
	 * @return the svc
	 */
	public String getSvc() {
		return svc;
	}
	/**
	 * @param svc the svc to set
	 */
	public void setSvc(String svc) {
		this.svc = svc;
	}
	/**
	 * @return the mpc
	 */
	public String getMpc() {
		return mpc;
	}
	/**
	 * @param mpc the mpc to set
	 */
	public void setMpc(String mpc) {
		this.mpc = mpc;
	}
	/**
	 * @return the moc
	 */
	public String getMoc() {
		return moc;
	}
	/**
	 * @param moc the moc to set
	 */
	public void setMoc(String moc) {
		this.moc = moc;
	}
	/**
	 * @return the mocTitle
	 */
	public String getMocTitle() {
		return mocTitle;
	}
	/**
	 * @param mocTitle the mocTitle to set
	 */
	public void setMocTitle(String mocTitle) {
		this.mocTitle = mocTitle;
	}
	/**
	 * @return the overview
	 */
	public String getOverview() {
		return overview;
	}
	/**
	 * @param overview the overview to set
	 */
	public void setOverview(String overview) {
		this.overview = overview;
	}

}
