package com.citm.spring.dao.mybatis.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.citm.spring.dao.mybatis.DbInfoDAO;
import com.citm.spring.dao.mybatis.service.DbInfoService;
import com.citm.spring.model.DbInfo;

@Repository
public class DbInfoDAOImpl implements DbInfoDAO {
	@Autowired
	private DbInfoService service;
	
	public DbInfo getDbInfo(String name) {
		return service.getDbInfo(name);
	}
}