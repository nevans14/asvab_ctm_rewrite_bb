package com.citm.spring.model.detail;

import java.io.Serializable;

public class AdditionalInfo implements Serializable {

	private static final long serialVersionUID = -5518454440382481002L;
	String title, url;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

}
