package com.citm.spring.webservice.rest;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;

import com.citm.spring.utils.UserManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.citm.spring.dao.mybatis.impl.NotesDAOImpl;
import com.citm.spring.model.notes.CareerClusterNotes;
import com.citm.spring.model.notes.Notes;

@Controller
@RequestMapping("/notes")
public class NotesController {

	private Logger logger = LogManager.getLogger(this.getClass().getName());

	@Autowired
	UserManager userManager;
	@Autowired
	NotesDAOImpl dao;

	@RequestMapping(value = "/getNotes", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<ArrayList<Notes>> getNotes() {
	    logger.info("getNotes");
	    if (!userManager.isAuthenticated()) {
	    	return new ResponseEntity<>(HttpStatus.FORBIDDEN);
		}
		ArrayList<Notes> results;
		try {
			results = dao.getNotes(userManager.getUserId());
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<ArrayList<Notes>>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<ArrayList<Notes>>(results, HttpStatus.OK);
	}

	@RequestMapping(value = "/getOccupationNotes/{socId}", method = RequestMethod.GET)
	public @ResponseBody Map<String, String> getOccupationNotes(@PathVariable String socId) {
		logger.info("getOccupationNotes");
		if (!userManager.isAuthenticated()) {
			return Collections.singletonMap("note", null);
		}
		String note = null;
		try {
			note = dao.getOccupationNotes(userManager.getUserId(), socId);
		} catch (Exception e) {
			logger.error("Error", e);
		}
		return Collections.singletonMap("note", note);
	}

	@RequestMapping(value = "/insertNote", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<Notes> insertNote(@RequestBody Notes noteObject) {
	    logger.info("insertNote");
		if (!userManager.isAuthenticated()) {
			return new ResponseEntity<>(HttpStatus.FORBIDDEN);
		}
		int rowsInserted = 0;
		noteObject.setUserId(userManager.getUserId());
		try {
			rowsInserted = dao.insertNote(noteObject);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<Notes>(HttpStatus.BAD_REQUEST);
		}
		if (rowsInserted != 1) {
			return new ResponseEntity<Notes>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<Notes>(noteObject, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/updateNote", method = RequestMethod.PUT)
	public @ResponseBody ResponseEntity<Notes> updateNote(@RequestBody Notes noteObject) {
	    logger.info("updateNote");
		if (!userManager.isAuthenticated()) {
			return new ResponseEntity<>(HttpStatus.FORBIDDEN);
		}
		int rowsUpdate = 0;
		try {
			rowsUpdate = dao.updateNote(noteObject);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<Notes>(HttpStatus.BAD_REQUEST);
		}
		if (rowsUpdate != 1) {
			return new ResponseEntity<Notes>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<Notes>(noteObject, HttpStatus.OK);
		}
	}
	
	@RequestMapping(value = "/delete-note/{id}", method = RequestMethod.DELETE)
	public @ResponseBody ResponseEntity<Integer> deleteNote(@PathVariable Integer id) {
		int result = 0;
		if (!userManager.isAuthenticated()) {
			return new ResponseEntity<>(HttpStatus.FORBIDDEN);
		}
		try {
			result = dao.deleteNote(userManager.getUserId(), id);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<Integer>(HttpStatus.BAD_REQUEST);
		}
		if (result != 1) {
			return new ResponseEntity<Integer>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<Integer>(result, HttpStatus.OK);
		}
	}
	
	@RequestMapping(value = "/getCareerClusterNotes", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<ArrayList<CareerClusterNotes>> getCareerClusterNotes() {
		if (!userManager.isAuthenticated()) {
			return new ResponseEntity<>(HttpStatus.FORBIDDEN);
		}
		ArrayList<CareerClusterNotes> results;
		try {
			results = dao.getCareerClusterNotes(userManager.getUserId());
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<ArrayList<CareerClusterNotes>>(HttpStatus.BAD_REQUEST);
		}

		return new ResponseEntity<ArrayList<CareerClusterNotes>>(results, HttpStatus.OK);
	}

	@RequestMapping(value = "/getCareerClusterNote/{ccId}", method = RequestMethod.GET)
	public @ResponseBody Map<String, String> getCareerClusterNote(@PathVariable Integer ccId) {
		if (!userManager.isAuthenticated()) {
			return Collections.singletonMap("note", null);
		}
		String note = null;
		try {
			note = dao.getCareerClusterNote(userManager.getUserId(), ccId);
		} catch (Exception e) {
			logger.error("Error", e);
		}
		return Collections.singletonMap("note", note);
	}

	@RequestMapping(value = "/insertCareerClusterNote", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<CareerClusterNotes> insertCareerClusterNote(@RequestBody CareerClusterNotes noteObject) {
		if (!userManager.isAuthenticated()) {
			return new ResponseEntity<>(HttpStatus.FORBIDDEN);
		}
		int rowsInserted = 0;
		noteObject.setUserId(userManager.getUserId());
		try {
			rowsInserted = dao.insertCareerClusterNote(noteObject);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<CareerClusterNotes>(HttpStatus.BAD_REQUEST);
		}
		if (rowsInserted != 1) {
			return new ResponseEntity<CareerClusterNotes>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<CareerClusterNotes>(noteObject, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/updateCareerClusterNote", method = RequestMethod.PUT)
	public @ResponseBody ResponseEntity<CareerClusterNotes> updateCareerClusterNote(@RequestBody CareerClusterNotes noteObject) {
		if (!userManager.isAuthenticated()) {
			return new ResponseEntity<>(HttpStatus.FORBIDDEN);
		}
		int rowsUpdate = 0;
		try {
			rowsUpdate = dao.updateCareerClusterNote(noteObject);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<CareerClusterNotes>(HttpStatus.BAD_REQUEST);
		}
		if (rowsUpdate != 1) {
			return new ResponseEntity<CareerClusterNotes>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<CareerClusterNotes>(noteObject, HttpStatus.OK);
		}
	}
	
	@RequestMapping(value = "/deleteCareerClusterNote/{ccNoteId}", method = RequestMethod.DELETE)
	public @ResponseBody ResponseEntity<Integer> deleteCareerClusterNote(@PathVariable Integer ccNoteId) {
		if (!userManager.isAuthenticated()) {
			return new ResponseEntity<>(HttpStatus.FORBIDDEN);
		}
		int rowsAffected = 0;
		try {
			rowsAffected = dao.deleteCareerClusterNote(userManager.getUserId(), ccNoteId);
		} catch (Exception ex) {
			logger.error("Error", ex);
		}
		if (rowsAffected > 0) {
			return new ResponseEntity<Integer>(rowsAffected, HttpStatus.OK);
		}
		return new ResponseEntity<Integer>(HttpStatus.BAD_REQUEST);
	}
}
