package com.citm.spring.dao.mybatis.mapper;

import org.apache.ibatis.annotations.Param;

import com.citm.spring.model.DbInfo;

public interface DbInfoMapper {
	public DbInfo getDbInfo(@Param("name") String name);
}