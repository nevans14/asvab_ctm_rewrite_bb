package com.citm.spring.dao.mybatis.mapper;

import java.util.ArrayList;

import org.apache.ibatis.annotations.Param;

import com.citm.spring.model.favorites.FavoriteCareerCluster;
import com.citm.spring.model.favorites.FavoriteOccupation;
import com.citm.spring.model.favorites.FavoriteService;

public interface FavoriteOccupationMapper {

	ArrayList<FavoriteOccupation> getFavoriteOccupation(@Param("userId") Integer userId);

	int insertFavoriteOccupation(@Param("favoriteObject") FavoriteOccupation favoriteObject);

	int deleteFavoriteOccupation(@Param("id") Integer id);
	
	ArrayList<FavoriteService> getFavoriteService(@Param("userId") Integer userId);

	int insertFavoriteService(@Param("serviceObject") FavoriteService serviceObject);

	int deleteFavoriteService(@Param("id") Integer id);
	
	ArrayList<FavoriteCareerCluster> getFavoriteCareerCluster(@Param("userId") Integer userId);

	int insertFavoriteCareerCluster(@Param("favoriteObject") FavoriteCareerCluster favoriteObject);

	int deleteFavoriteCareerCluster(@Param("id") Integer id);

}
