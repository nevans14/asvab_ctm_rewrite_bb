package com.citm.spring.model.testscore;

public class PropertyAttributes {

	private String startingPosition;

	private String fieldLength;
	
	private String fieldName;

	/**
	 * @return the startingPosition
	 */
	public String getStartingPosition() {
		return startingPosition;
	}

	/**
	 * @param startingPosition the startingPosition to set
	 */
	public void setStartingPosition(String startingPosition) {
		this.startingPosition = startingPosition;
	}

	/**
	 * @return the fieldLength
	 */
	public String getFieldLength() {
		return fieldLength;
	}

	/**
	 * @param fieldLength the fieldLength to set
	 */
	public void setFieldLength(String fieldLength) {
		this.fieldLength = fieldLength;
	}

	/**
	 * @return the fieldName
	 */
	public String getFieldName() {
		return fieldName;
	}

	/**
	 * @param fieldName the fieldName to set
	 */
	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}
	
}
