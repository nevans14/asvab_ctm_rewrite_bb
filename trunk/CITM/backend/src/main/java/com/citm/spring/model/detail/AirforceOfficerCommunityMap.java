package com.citm.spring.model.detail;

import java.io.Serializable;

public class AirforceOfficerCommunityMap implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5150905071137401703L;
	Integer officerCommunityId;
	String officerCommunityTitle, officerfCommunityDefinition;

	public Integer getOfficerCommunityId() {
		return officerCommunityId;
	}

	public void setOfficerCommunityId(Integer officerCommunityId) {
		this.officerCommunityId = officerCommunityId;
	}

	public String getOfficerCommunityTitle() {
		return officerCommunityTitle;
	}

	public void setOfficerCommunityTitle(String officerCommunityTitle) {
		this.officerCommunityTitle = officerCommunityTitle;
	}

	public String getOfficerfCommunityDefinition() {
		return officerfCommunityDefinition;
	}

	public void setOfficerfCommunityDefinition(String officerfCommunityDefinition) {
		this.officerfCommunityDefinition = officerfCommunityDefinition;
	}
}
