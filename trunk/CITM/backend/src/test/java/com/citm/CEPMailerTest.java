package com.citm;

import javax.sql.DataSource;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.citm.spring.CEPMailer;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:controllerContext.xml")
public class CEPMailerTest {
    
    @Autowired
    CEPMailer cepMailer;
    @Autowired
    DataSource dataSource;
    
    
    
    @Test
    public void testRegistrationEmail() {
        
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

        // get website url 
        String websiteUrl = jdbcTemplate.queryForObject("SELECT value FROM  dbInfo WHERE name='citm_website_url'", String.class);

        // Compose Registration email
          String htmlEmailMessage = jdbcTemplate.queryForObject("SELECT value2 FROM  dbInfo WHERE name='citm_registration_email'", String.class);

          htmlEmailMessage = htmlEmailMessage.replaceAll("#WEBSITE_URL#", websiteUrl);   
          htmlEmailMessage = htmlEmailMessage.replaceAll("#NEW_LINE#", "\n");
        
        
        
        try {
            cepMailer.sendResetRegisterHtmlEmail("daffy@duck.com", "wiley@acme.com", htmlEmailMessage, "What is up duck.");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
