package com.citm;

import java.util.Date;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.citm.spring.model.login.Registration;
import com.citm.spring.utils.SimpleUtils;
import com.citm.spring.webservice.rest.LoginRegistrationController;
import com.citm.spring.webservice.rest.LoginRegistrationController.RegistrationLoginStatus;



@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:controllerContext.xml")
@Transactional
public class LoginRegistrationControllerTest implements ApplicationContextAware {

    @Autowired
    LoginRegistrationController lrc;

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    BCryptPasswordEncoder bCryptPasswordEncoder;

    
    @Test
    public void testCITM_CITM_LoginAccessCode_Found_expire_date_null() {

        jdbcTemplate.update("INSERT INTO [dbo].[access_codes] ([access_code] ,[expire_date] ,[role]) VALUES "
                + " ('BOBBY-ORE', NULL ,'Z')");

        Registration registration = new Registration();
        registration.setAccessCode("BOBBY-ORE");
        MockHttpServletRequest mockRequest = new MockHttpServletRequest();
        RegistrationLoginStatus rls = lrc.loginAccessCode(registration, mockRequest);

        // Registration Successful
        Assert.assertEquals("message", new Integer(0), rls.getStatusNumber());

        String dateString = jdbcTemplate.queryForObject(
                "SELECT expire_date FROM [dbo].[access_codes] WHERE [access_code] = 'BOBBY-ORE'", String.class);
        dateString = dateString.substring(0, dateString.indexOf(' '));

        String expectedExpireDate = SimpleUtils
                .getStringInStandardFormatFromDate(SimpleUtils.datePlusDays(new Date(), 21));
        expectedExpireDate = expectedExpireDate.substring(0, expectedExpireDate.indexOf(' '));

        Assert.assertEquals("message", 0, expectedExpireDate.compareTo(dateString));

    }

    @Test
    public void testCITM_LoginAccessCode_Found_expire_date_before_today() {
        String expireDate = SimpleUtils.getStringInStandardFormatFromDate(SimpleUtils.datePlusDays(new Date(), -1));
        jdbcTemplate.update("INSERT INTO [dbo].[access_codes] ([access_code] ,[expire_date] ,[role]) VALUES "
                + " ('BOBBY-ORE', ? ,'Z')", expireDate);

        Registration registration = new Registration();
        registration.setAccessCode("BOBBY-ORE");
        RegistrationLoginStatus rls = lrc.loginAccessCode(registration, null);

        // Registration Not Successful
        Assert.assertEquals("message", new Integer(1021), rls.getStatusNumber());
    }

    @Test
    public void testCITM_LoginAccessCode_Found_expire_date_after_today() {
        String expireDate = SimpleUtils.getStringInStandardFormatFromDate(SimpleUtils.datePlusDays(new Date(), 1));
        jdbcTemplate.update("INSERT INTO [dbo].[access_codes] ([access_code] ,[expire_date] ,[role]) VALUES "
                + " ('BOBBY-ORE', ? ,'Z')", expireDate);

        Registration registration = new Registration();
        registration.setAccessCode("BOBBY-ORE");
        MockHttpServletRequest mockRequest = new MockHttpServletRequest();
        RegistrationLoginStatus rls = lrc.loginAccessCode(registration, mockRequest);

        // Registration Successful
        Assert.assertEquals("message", new Integer(0), rls.getStatusNumber());
    }

    @Test
    public void testCITM_LoginAccessCode_Not_Found_less_then_10_characters() {

        Registration registration = new Registration();
        registration.setAccessCode("98765432F");
        MockHttpServletRequest mockRequest = new MockHttpServletRequest();
        RegistrationLoginStatus rls = lrc.loginAccessCode(registration, mockRequest);

        // Registration Unsuccessful
        Assert.assertEquals("message", new Integer(1001), rls.getStatusNumber());

    }

    @Test
    public void testCITM_LoginAccessCode_Not_Found_more_then_10_characters() {

        Registration registration = new Registration();
        registration.setAccessCode("9876543210F");
        RegistrationLoginStatus rls = lrc.loginAccessCode(registration, null);

        // Registration Unsuccessful
        Assert.assertEquals("message", new Integer(1001), rls.getStatusNumber());
    }

    @Test
    public void testCITM_LoginAccessCode_Not_Found_suffix_not_valid_letter() {
        Registration registration = new Registration();
        registration.setAccessCode("987654321A");
        RegistrationLoginStatus rls = lrc.loginAccessCode(registration, null);

        // Registration Unsuccessful
        Assert.assertEquals("message", new Integer(1001), rls.getStatusNumber());
    }

    @Test
    public void testCITM_LoginAccessCode_Not_Found_suffix_in_from_mod_0() {
        Registration registration = new Registration();
        registration.setAccessCode("987654321F");
        MockHttpServletRequest mockRequest = new MockHttpServletRequest();
        RegistrationLoginStatus rls = lrc.loginAccessCode(registration, mockRequest);

        // Registration Successful
        Assert.assertEquals("message", new Integer(0), rls.getStatusNumber());
    }

    @Test
    public void testCITM_LoginAccessCode_Not_Found_suffix_in_from_mod_1() {
        Registration registration = new Registration();
        registration.setAccessCode("987654321M");
        MockHttpServletRequest mockRequest = new MockHttpServletRequest();
        RegistrationLoginStatus rls = lrc.loginAccessCode(registration, mockRequest);

        // Registration Successful
        Assert.assertEquals("message", new Integer(0), rls.getStatusNumber());
    }

    @Test
    public void testCITM_LoginAccessCode_Not_Found_suffix_in_from_mod_2() {
        Registration registration = new Registration();
        registration.setAccessCode("987654321T");
        MockHttpServletRequest mockRequest = new MockHttpServletRequest();
        RegistrationLoginStatus rls = lrc.loginAccessCode(registration, mockRequest);

        // Registration Successful
        Assert.assertEquals("message", new Integer(0), rls.getStatusNumber());
    }

    @Test
    public void testCITM_LoginEmail_Email_NotFound() {

        Registration registration = new Registration();
        registration.setEmail("joe.blow@idaho.gov");
        registration.setPassword("joe.blow.from.idaho");
        MockHttpServletRequest mockRequest = new MockHttpServletRequest();
        RegistrationLoginStatus rls = lrc.loginEmail(registration, mockRequest);

        // Registration Unsuccessful
        Assert.assertEquals("message", new Integer(1002), rls.getStatusNumber());

    }

    @Test
    public void testCITM_LoginEmail_Email_RegistrationFailed() {

        jdbcTemplate.update("INSERT INTO [dbo].[users]  ([user_id]  ,[username] ,[password] ,[enabled] ,[reset_key],[reset_expire_date]) " +
                " VALUES (0 ,'joe.blow@idaho.gov' ,'mypassword'  ,1,NULL ,NULL)");


        Registration reg = new Registration();
        reg.setEmail("joe.blow@idaho.gov");
                     
        reg.setPassword("joe.blow.from.idaho");
        MockHttpServletRequest mockRequest = new MockHttpServletRequest();
        RegistrationLoginStatus rlsLogin = lrc.loginEmail(reg,mockRequest);
  
        // Registration Unsuccessful
        Assert.assertEquals("message", new Integer(1023), rlsLogin.getStatusNumber());
        

        
    }
    @Test
    public void testCITM_LoginEmail_Email_Locked() {
        String expireDate = SimpleUtils.getStringInStandardFormatFromDate(SimpleUtils.datePlusDays(new Date(), 1));
        jdbcTemplate.update("INSERT INTO [dbo].[access_codes] ([access_code] ,[expire_date] ,[role]) VALUES "
                + " ('BOBBY-ORE', ? ,'Z')", expireDate);

        // Note client side does this with javascript version of the encoder.
        String encodedPassWord = bCryptPasswordEncoder.encode("joe.blow.from.idaho");

        Registration registration = new Registration();
        registration.setEmail("joe.blow@idaho.gov");
        registration.setPassword(encodedPassWord);
        registration.setAccessCode("BOBBY-ORE");
        MockHttpServletRequest mockRequest = new MockHttpServletRequest();
        RegistrationLoginStatus rls = lrc.register(registration,mockRequest);

        // Registration successful
        Assert.assertEquals("message", new Integer(0), rls.getStatusNumber());

        jdbcTemplate.update("UPDATE users SET enabled=0 where username='joe.blow@idaho.gov'");

        Registration reg = new Registration();
        reg.setEmail("joe.blow@idaho.gov");
        reg.setPassword("joe.blow.from.idaho");
        
        mockRequest = new MockHttpServletRequest();
        RegistrationLoginStatus rlsLogin = lrc.loginEmail(reg,mockRequest);

        // Registration Unsuccessful
        Assert.assertEquals("message", new Integer(1010), rlsLogin.getStatusNumber());

    }

    @Test
    public void testCITM_LoginEmail_Email_Password_No_Match() {
        String expireDate = SimpleUtils.getStringInStandardFormatFromDate(SimpleUtils.datePlusDays(new Date(), 1));
        jdbcTemplate.update("INSERT INTO [dbo].[access_codes] ([access_code] ,[expire_date] ,[role]) VALUES "
                + " ('BOBBY-ORE', ? ,'Z')", expireDate);

        // Note client side does this with javascript version of the encoder.
        String encodedPassWord = bCryptPasswordEncoder.encode("joe.blow-from.idaho");

        Registration registration = new Registration();
        registration.setEmail("joe.blow@idaho.gov");
        registration.setPassword(encodedPassWord);
        registration.setAccessCode("BOBBY-ORE");
        MockHttpServletRequest mockRequest = new MockHttpServletRequest();
        RegistrationLoginStatus rls = lrc.register(registration, mockRequest);

        // Registration successful
        Assert.assertEquals("message", new Integer(0), rls.getStatusNumber());

        Registration reg = new Registration();
        reg.setEmail("joe.blow@idaho.gov");
        reg.setPassword("joe.blow.from.idaho");
        mockRequest = new MockHttpServletRequest();
        RegistrationLoginStatus rlsLogin = lrc.loginEmail(reg,mockRequest);

        // Registration Unsuccessful
        Assert.assertEquals("message", new Integer(1008), rlsLogin.getStatusNumber());

    }

    @Test
    public void testCITM_LoginEmail_Email_Password_Match() {
        String expireDate = SimpleUtils.getStringInStandardFormatFromDate(SimpleUtils.datePlusDays(new Date(), 1));
        jdbcTemplate.update("INSERT INTO [dbo].[access_codes] ([access_code] ,[expire_date] ,[role]) VALUES "
                + " ('BOBBY-ORE', ? ,'Z')", expireDate);

        // Note client side does this with javascript version of the encoder.
        String encodedPassWord = bCryptPasswordEncoder.encode("joe.blow.from.idaho");

        Registration registration = new Registration();
        registration.setEmail("joe.blow@idaho.gov");
        registration.setPassword(encodedPassWord);
        registration.setAccessCode("BOBBY-ORE");
        MockHttpServletRequest mockRequest = new MockHttpServletRequest();
        RegistrationLoginStatus rls = lrc.register(registration, mockRequest);

        // Registration successful
        Assert.assertEquals("message", new Integer(0), rls.getStatusNumber());

        Registration reg = new Registration();
        reg.setEmail("joe.blow@idaho.gov");
        reg.setPassword("joe.blow.from.idaho");
        mockRequest = new MockHttpServletRequest();
        RegistrationLoginStatus rlsLogin = lrc.loginEmail(reg, mockRequest);

        // Registration Unsuccessful
        Assert.assertEquals("message", new Integer(1), rlsLogin.getStatusNumber());

    }

    @Test
    public void testCITM_Registration_No_AccessCode() {
 //       String expireDate = SimpleUtils.getStringInStandardFormatFromDate(SimpleUtils.datePlusDays(new Date(), 1));
//        jdbcTemplate.update("INSERT INTO [dbo].[access_codes] ([access_code] ,[expire_date] ,[role]) VALUES "
//                + " ('BOBBY-ORE', ? ,'Z')", expireDate);

        // Note client side does this with javascript version of the encoder.
        String encodedPassWord = bCryptPasswordEncoder.encode("joe.blow.from.idaho");

        Registration registration = new Registration();
        registration.setEmail("joe.blow@idaho.gov");
        registration.setPassword(encodedPassWord);
//        registration.setAccessCode("BOBBY-ORE");
        MockHttpServletRequest mockRequest = new MockHttpServletRequest();
        RegistrationLoginStatus rls = lrc.register(registration, mockRequest);

        // Registration successful
        Assert.assertEquals("message", new Integer(1), rls.getStatusNumber());

        Long userId = Long.parseLong(rls.getUser_id() );
        
        // Query Access Code 
        String accessCode = jdbcTemplate.queryForObject("select access_code from [dbo].[access_codes] where user_id = ?", String.class, userId);
        String accessToTest = accessCode.trim().substring(6, 10);
        Assert.assertEquals("access code has citm in it ", true , accessToTest.contentEquals("CITM"));

        // Query Users 
        String system = jdbcTemplate.queryForObject("select system from [dbo].[users] where user_id = ?", String.class, userId);
        
        
        // Registration Successful
        Assert.assertEquals("system set to CITM", true , system.trim().contentEquals("CITM"));

        
        Registration login = new Registration();
        login.setEmail("joe.blow@idaho.gov");
        login.setPassword("joe.blow.from.idaho");
        mockRequest = new MockHttpServletRequest();
        rls = lrc.loginEmail(login, mockRequest);
        
        // Registration Successful
        Assert.assertEquals("Log into CITM only", Integer.valueOf(1) , rls.getStatusNumber() );
    }

    @Test
    public void testCITM_Registration_AccessCode_Found_expire_date_after_today() {
        String expireDate = SimpleUtils.getStringInStandardFormatFromDate(SimpleUtils.datePlusDays(new Date(), 1));
        jdbcTemplate.update("INSERT INTO [dbo].[access_codes] ([access_code] ,[expire_date] ,[role]) VALUES "
                + " ('BOBBY-ORE', ? ,'Z')", expireDate);


        // Note client side does this with javascript version of the encoder.
        String encodedPassWord = bCryptPasswordEncoder.encode("joe.blow.from.idaho");
        
        Registration registration = new Registration();
        registration.setEmail("joe.blow@idaho.gov");
        registration.setPassword(encodedPassWord);
        registration.setAccessCode("BOBBY-ORE");
        MockHttpServletRequest mockRequest = new MockHttpServletRequest();
        RegistrationLoginStatus rls = lrc.register(registration, mockRequest);

        // Registration Not Successful
        Assert.assertEquals("message", new Integer(0), rls.getStatusNumber());
    }

    @Test
    public void testCITM_Registration_AccessCode_Found_expire_date_before_today() {
        String expireDate = SimpleUtils.getStringInStandardFormatFromDate(SimpleUtils.datePlusDays(new Date(), -1));
        jdbcTemplate.update("INSERT INTO [dbo].[access_codes] ([access_code] ,[expire_date] ,[role]) VALUES "
                + " ('BOBBY-ORE', ? ,'Z')", expireDate);

        // Note client side does this with javascript version of the encoder.
        String encodedPassWord = bCryptPasswordEncoder.encode("joe.blow.from.idaho");

        Registration registration = new Registration();
        registration.setEmail("joe.blow@idaho.gov");
        registration.setPassword(encodedPassWord);
        registration.setAccessCode("BOBBY-ORE");
        MockHttpServletRequest mockRequest = new MockHttpServletRequest();
        RegistrationLoginStatus rls = lrc.register(registration, mockRequest);

        // Registration Not Successful
        Assert.assertEquals("message", new Integer(1021), rls.getStatusNumber());
    }

    @Test
    public void testCITM_Registration_AccessCode_Found_expire_date_null() {
        jdbcTemplate.update("INSERT INTO [dbo].[access_codes] ([access_code] ,[expire_date] ,[role]) VALUES "
                + " ('BOBBY-ORE', NULL ,'Z')");

        // Note client side does this with javascript version of the encoder.
        String encodedPassWord = bCryptPasswordEncoder.encode("joe.blow.from.idaho");

        Registration registration = new Registration();
        registration.setEmail("joe.blow@idaho.gov");
        registration.setPassword(encodedPassWord);
        registration.setAccessCode("BOBBY-ORE");
        MockHttpServletRequest mockRequest = new MockHttpServletRequest();
        RegistrationLoginStatus rls = lrc.register(registration, mockRequest);

        // Registration Successful
        Assert.assertEquals("message", new Integer(0), rls.getStatusNumber());

        String dateString = jdbcTemplate.queryForObject(
                "SELECT expire_date FROM [dbo].[access_codes] WHERE [access_code] = 'BOBBY-ORE'", String.class);
        dateString = dateString.substring(0, dateString.indexOf(' '));

        String expectedExpireDate = SimpleUtils
                .getStringInStandardFormatFromDate(SimpleUtils.datePlusDays(new Date(), 21));
        expectedExpireDate = expectedExpireDate.substring(0, expectedExpireDate.indexOf(' '));

        Assert.assertEquals("message", 0, expectedExpireDate.compareTo(dateString));

    }

    @Test
    public void testCITM_Registration_AccessCode_Not_Found_less_then_10_characters() {

        // Note client side does this with javascript version of the encoder.
        String encodedPassWord = bCryptPasswordEncoder.encode("joe.blow.from.idaho");

        Registration registration = new Registration();
        registration.setEmail("joe.blow@idaho.gov");
        registration.setPassword(encodedPassWord);
        registration.setAccessCode("98765432F");
        MockHttpServletRequest mockRequest = new MockHttpServletRequest();
        RegistrationLoginStatus rls = lrc.register(registration, mockRequest);

        // Registration Unsuccessful
        Assert.assertEquals("message", new Integer(1001), rls.getStatusNumber());

    }

    @Test
    public void testCITM_Registration_AccessCode_Not_Found_more_then_10_characters() {

        // Note client side does this with javascript version of the encoder.
        String encodedPassWord = bCryptPasswordEncoder.encode("joe.blow.from.idaho");

        Registration registration = new Registration();
        registration.setEmail("joe.blow@idaho.gov");
        registration.setPassword(encodedPassWord);
        registration.setAccessCode("9876543210F");
        MockHttpServletRequest mockRequest = new MockHttpServletRequest();
        RegistrationLoginStatus rls = lrc.register(registration, mockRequest);

        // Registration Unsuccessful
        Assert.assertEquals("message", new Integer(1001), rls.getStatusNumber());

    }

    @Test
    public void testCITM_Registration_AccessCode_Not_Found_suffix_in_from_mod_0() {

        // Note client side does this with javascript version of the encoder.
        String encodedPassWord = bCryptPasswordEncoder.encode("joe.blow.from.idaho");

        Registration registration = new Registration();
        registration.setEmail("joe.blow@idaho.gov");
        registration.setPassword(encodedPassWord);
        registration.setAccessCode("987654321F");
        MockHttpServletRequest mockRequest = new MockHttpServletRequest();
        RegistrationLoginStatus rls = lrc.register(registration, mockRequest);

        // Registration Successful
        Assert.assertEquals("message", new Integer(0), rls.getStatusNumber());

    }

    @Test
    public void testCITM_Registration_AccessCode_Not_Found_suffix_in_from_mod_1() {
        


        // Note client side does this with javascript version of the encoder.
        String encodedPassWord = bCryptPasswordEncoder.encode("joe.blow.from.idaho");

        Registration registration = new Registration();
        registration.setEmail("joe.blow@idaho.gov");
        registration.setPassword(encodedPassWord);
        registration.setAccessCode("987654321M");
        MockHttpServletRequest mockRequest = new MockHttpServletRequest();
        RegistrationLoginStatus rls = lrc.register(registration, mockRequest);

        // Registration Successful
        Assert.assertEquals("message", new Integer(0), rls.getStatusNumber());

    }

    @Test
    public void testCITM_Registration_AccessCode_Not_Found_suffix_in_from_mod_2() {



        // Note client side does this with javascript version of the encoder.
        String encodedPassWord = bCryptPasswordEncoder.encode("joe.blow.from.idaho");

        Registration registration = new Registration();
        registration.setEmail("joe.blow@idaho.gov");
        registration.setPassword(encodedPassWord);
        registration.setAccessCode("987654321T");
        MockHttpServletRequest mockRequest = new MockHttpServletRequest();        
        RegistrationLoginStatus rls = lrc.register(registration, mockRequest);

        // Registration Successful
        Assert.assertEquals("message", new Integer(0), rls.getStatusNumber());

    }

    @Test
    public void testCITM_Registration_AccessCode_Not_Found_suffix_not_valid_letter() {

 

        // Note client side does this with javascript version of the encoder.
        String encodedPassWord = bCryptPasswordEncoder.encode("joe.blow.from.idaho");

        Registration registration = new Registration();
        registration.setEmail("joe.blow@idaho.gov");
        registration.setPassword(encodedPassWord);
        registration.setAccessCode("987654321A");
        MockHttpServletRequest mockRequest = new MockHttpServletRequest();
        RegistrationLoginStatus rls = lrc.register(registration, mockRequest);

        // Registration unsuccessful
        Assert.assertEquals("message", new Integer(1001), rls.getStatusNumber());
    }

    @Test
    public void testCITM_Registration_PasswordBlank() {


        // Note client side does this with javascript version of the encoder.
        String encodedPassWord = bCryptPasswordEncoder.encode("joe.blow.from.idaho");

        Registration registration = new Registration();
        registration.setEmail("joe.blow@idaho.gov");
        registration.setPassword(encodedPassWord);
        registration.setAccessCode("987654321F");
        MockHttpServletRequest mockRequest = new MockHttpServletRequest();
        RegistrationLoginStatus rls = lrc.register(registration, mockRequest);

        // Registration unsuccessful
        Assert.assertEquals("message", new Integer(0), rls.getStatusNumber());
    }

    
    @Test
    public void testCITM_Registration_Email_RegistrationFailed_Update() {
        
        String expireDate = SimpleUtils.getStringInStandardFormatFromDate(SimpleUtils.datePlusDays(new Date(), 1));
        jdbcTemplate.update("INSERT INTO [dbo].[access_codes] ([access_code] ,[expire_date] ,[role]) VALUES "
                + " ('BOBBY-ORE', ? ,'Z')", expireDate);

        // Note client side does this with javascript version of the encoder.
        String encodedPassWord = bCryptPasswordEncoder.encode("joe.blow.from.idaho");
        
        jdbcTemplate.update("INSERT INTO [dbo].[users]  ([user_id]  ,[username] ,[password] ,[enabled] ,[reset_key],[reset_expire_date]) " +
                " VALUES (0 ,'joe.blow@idaho.gov' , ?  , 1, NULL ,NULL)", encodedPassWord);

        Registration reg = new Registration();
        reg.setEmail("joe.blow@idaho.gov");
        reg.setAccessCode("BOBBY-ORE");
        reg.setPassword(bCryptPasswordEncoder.encode("duke"));
        MockHttpServletRequest mockRequest = new MockHttpServletRequest();
        RegistrationLoginStatus rlsLogin = lrc.register(reg, mockRequest);
  
        // Registration Successful
        Assert.assertEquals("message", new Integer(0), rlsLogin.getStatusNumber());
        
        Long userId = jdbcTemplate.queryForObject("select user_id from users where username = 'joe.blow@idaho.gov'", Long.class);

        Assert.assertNotEquals( new Long( 0 ) , userId );
        
        String encodedPasswordFromDb = jdbcTemplate.queryForObject("SELECT password FROM users WHERE username = ?", String.class, reg.getEmail());
        
        Assert.assertEquals("message", true , bCryptPasswordEncoder.matches("duke", encodedPasswordFromDb) );
        
        
        
    }
    
    @Test
    public void testCITM_Registration_EmailBlank() {

        // Note client side does this with javascript version of the encoder.
        String encodedPassWord = bCryptPasswordEncoder.encode("");

        Registration registration = new Registration();
        registration.setEmail("");
        registration.setPassword(encodedPassWord);
        registration.setAccessCode("98765432F");
        MockHttpServletRequest mockRequest = new MockHttpServletRequest();
        RegistrationLoginStatus rls = lrc.register(registration, mockRequest);

        // Registration unsuccessful
        Assert.assertEquals("message", new Integer(1001), rls.getStatusNumber());
    }

    @Test
    public void testCITM_Registration_EmailNo_at_Symbol() {
        // Note client side does this with javascript version of the encoder.
        String encodedPassWord = bCryptPasswordEncoder.encode("");

        Registration registration = new Registration();
        registration.setEmail("joe.blow.idaho.gov");
        registration.setPassword(encodedPassWord);
        registration.setAccessCode("98765432F");
        MockHttpServletRequest mockRequest = new MockHttpServletRequest();
        RegistrationLoginStatus rls = lrc.register(registration, mockRequest);

        // Registration unsuccessful
        Assert.assertEquals("message", new Integer(1001), rls.getStatusNumber());

    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {

        String[] beans = applicationContext.getBeanDefinitionNames();
        System.out.println("There are " + beans.length + " in the app context!");
        // for ( int i = 0 ; i < beans.length ; i++){
        // System.out.println(beans[i]);
        // }
    }

}
