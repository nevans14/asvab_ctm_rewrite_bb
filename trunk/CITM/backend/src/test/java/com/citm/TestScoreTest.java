package com.citm;

import com.citm.spring.model.testscore.StudentTestingProgam;
import com.citm.spring.utils.UserManager;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.citm.spring.dao.mybatis.impl.TestScoreDAOImpl;
import com.citm.spring.model.testscore.CompositeFavoritesFlags;
import com.citm.spring.model.testscore.StudentTestingProgamSelects;
import com.citm.spring.webservice.rest.TestScoreController;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:controllerContext.xml")
public class TestScoreTest {
	
	@Autowired
	TestScoreController controller;
	
	@Autowired
	TestScoreDAOImpl dao;

	@Autowired
	UserManager userManager;

	@Test
	public void test() {
		userManager.createMock();

		CompositeFavoritesFlags compositeFavoritesFlagsObj = new CompositeFavoritesFlags();
		
		compositeFavoritesFlagsObj.setUserId(2208286);
		compositeFavoritesFlagsObj.setA_af(1);
		
		controller.setCompositeFavoritesFlags(compositeFavoritesFlagsObj);
		
		compositeFavoritesFlagsObj.setAdm_navy(1);
		
		controller.setCompositeFavoritesFlags(compositeFavoritesFlagsObj);
		
		
		ResponseEntity<StudentTestingProgamSelects> testScore = controller.getTestScore();
		System.out.println(testScore.getBody().getResource());
	
		controller.deleteCompositeFavoritesFlags();
		
		System.out.println("USER EXISTS: "+controller.emailExists("ronny.v.tran.ctr@mail.mil"));
		
		System.out.println("USER EXISTS: "+controller.latestTestDate());
	}

}
