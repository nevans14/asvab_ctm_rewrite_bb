package com.citm;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.citm.spring.model.careerprofile.CareerProfile;
import com.citm.spring.webservice.rest.CareerProfileController;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:controllerContext.xml")
public class CareerProfileControllerTest {
	
	@Autowired
	CareerProfileController careerProfileContorller;
	
	@Test
	public void getProfile() {
		
		List<CareerProfile> profile = careerProfileContorller.selectCareerProfiles();
		System.out.println("FirstName: "+profile.get(0).getFirstName());
	}

}
